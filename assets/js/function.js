function big_notification($message,$type){
    var icon = { error : 'fa-minus-circle', warning : 'fa-exclamation-triangle', success : 'fa-check-circle-o', info: 'fa-info-circle'};
    var xicon = icon[$type];
    var a = humane.create({
        baseCls: "humane-bigbox",
        addnCls: "humane-bigbox-" + $type
    });
    
    a.log("<span class='fa " + xicon + "'></span> " + $message)
}