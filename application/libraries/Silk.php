<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require 'nusoap/nusoap.php';

class Silk{
    private $_CI;
    private $nusoap_client;
    private $config;
    private $xmlDocument;
    public $error_message;
    public $status_vlegal;
    
    public function __construct()
    {
        $this->_CI =& get_instance();
        $this->config = $this->_CI->config;
        $this->nusoap_client = new nusoap_client($this->config->item('URL_LIU'),true);
        $error = $this->nusoap_client->getError();
        
        if ($error) {
            $this->error_message = "Error saat akan terhubung ke server LIU : {$error}\n";
            return false;
        }
    }
    
    public function submit($idekspor,$pejabat='',$no_urut='',$alasan='')
    {
		$header = $this->_CI->ekspor_model->review($idekspor);
        
        if($header['sertifikat'] == 0){
            $this->nodocument = date('y') .'.'. str_pad($no_urut, 5, "0", STR_PAD_LEFT) .'-00000.' . $this->config->item('lvlk') . '-ID-' . $header['iso'];
        }else{
            $this->nodocument = date('y') .'.'. str_pad($no_urut, 5, "0", STR_PAD_LEFT) .'-'. str_pad(substr(trim($header['sertifikat']),-5), 5, "0", STR_PAD_LEFT) .'.' . $this->config->item('lvlk') . '-ID-' . $header['iso'];
        }
		
        if($header['status_liu'] == "Draft"){
			$this->sendDocument($idekspor,$pejabat,$no_urut,$header);
            return true;
        }elseif($header['status_liu']=="replace"){
			$this->cancelDocument($idekspor,'Revisi Dokumen');
            $no_urut = $this->_CI->ekspor_model->getNoUrut($header['client_id']);
			$this->sendDocument($idekspor,$pejabat,$no_urut,$header);
			return true;
		}elseif($header['status_liu']=="extend"){
			$this->extendDocument($idekspor,$header,$pejabat);
			return true;
		}elseif($header['status_liu']=="cancel"){
			$this->cancelDocument($idekspor,$alasan);
			return true;
		}
            
    }
    
   
    
    
    function cancelDocument($idekspor,$alasan)
    {
		$data = $this->_CI->ekspor_model->getSendVlegalByIdEkspor($idekspor);
        if(count($data) > 0){
            $this->createXmlCancelDocument($alasan,$data['no_dokumen']);
            
            $arrayParameter = array($this->config->item('USER_LIU'),$this->config->item('PASS_LIU'),$this->xmlDocument->outputMemory(true));
            $response = $this->nusoap_client->call('cancelDocument',$arrayParameter);
            
            $error = $this->nusoap_client->getError();
            if ($error) {
                $err_msg = "Error saat proses pengiriman data ke LIU.\n";
                $err_msg .= $this->nusoap_client->response."\n";
                $err_msg .= $this->nusoap_client->getDebug()."\n";
                $this->error_message = $err_msg;
                return false;
            }
            
            unset($this->xmlDocument);
            
            $xmlReturn = simplexml_load_string($response);
            if($xmlReturn->return_send=="R00"){
                $this->_CI->ekspor_model->updateStatusVLegal($data['vid'],"cancel");
            }
            
            return true;
        }
	}
    
    
    function sendEmail($insert_id,$filename = ''){
		$data        = $this->_CI->ekspor_model->getSendVlegalById($insert_id);
        $client      = $this->_CI->client_model->detail($data['clientid']);        
        $email_list  = $client->emaildv;
        $email_list  = explode(',',$email_list);
        //$email_list  = array('arefun.safety@gmail.com','koersina18@gmail.com','putraarryan1@gmail.com','agnestrl2012@gmail.com','widyantini.wulandari@tuv.com','ibrohim.prayetno@tuv.com','dian.soeminta@tuv.com');
        $email_list  = array('arefun.safety@gmail.com','koersina18@gmail.com','putraarryan1@gmail.com','agnestrl2012@gmail.com');
        
        if($filename == ''){
            $filename = $this->config->item('vlegal_dir') . $data['clientid'].'/'.$data['no_dokumen'] .'.pdf';
        }
        
		$this->_CI->email->set_newline("\r\n");
		$this->_CI->email->clear(TRUE);
		$this->_CI->email->attach($filename);
		foreach($email_list as $rec){
			$this->_CI->email->clear();
			$this->_CI->email->from($this->config->item('email_vlegal'),$this->config->item('email_vlegal_name'));
			$this->_CI->email->to($rec);			
						
			$message = $this->_CI->master_model->get_email_content('vlegalsend');
			$data_email = array ('invoice'      => $data['invoice'],	 
                                 'no_dokumen'   => $data['no_dokumen'],
								);
			$this->_CI->email->subject("Test Document V-Legal - ". $data['no_dokumen'] . " - No. Invoice: " . $data['invoice']);
			$message = $this->_CI->parser->parse_string($message,$data_email,true);
			$this->_CI->email->message($message);
		
			if ($this->_CI->email->send())
			{
				$status = true;
			}else{
				$status = false;
			}
		}
        return $status;
	}
    
    function sendEmailReject($insert_id){
		$data        = $this->_CI->ekspor_model->getVlegalById($insert_id);
        
        $client      = $this->_CI->client_model->detail($data['clientid']);        
        $email_list  = $client->emaildv;
        $email_list  = explode(',',$email_list);
        //$email_list  = array('arefun.safety@gmail.com','koersina18@gmail.com','putraarryan1@gmail.com','agnestrl2012@gmail.com','widyantini.wulandari@tuv.com','ibrohim.prayetno@tuv.com','dian.soeminta@tuv.com');
        $email_list  = array('arefun.safety@gmail.com','koersina18@gmail.com','putraarryan1@gmail.com','agnestrl2012@gmail.com');
		$this->_CI->email->set_newline("\r\n");
		$this->_CI->email->clear(TRUE);
		foreach($email_list as $rec){
			$this->_CI->email->clear();
			$this->_CI->email->from($this->config->item('email_vlegal'),$this->config->item('email_vlegal_name'));
			$this->_CI->email->to($rec);			
						
			$message = $this->_CI->master_model->get_email_content('vlegalreject');
			$data_email = array ('invoice'      => $data['invoice'],	 
                                 'keterangan'   => $data['keterangan'],
								);
			$this->_CI->email->subject("Test Dokumen V-legal Ditolak");
			$message = $this->_CI->parser->parse_string($message,$data_email,true);
			$this->_CI->email->message($message);
		
			if ($this->_CI->email->send())
			{
				$status = true;
			}else{
				$status = false;
			}
		}
        return $status;
	}
    
    
    function rejectDocument($idekspor,$keterangan)
    {
        $header = $this->_CI->ekspor_model->review($idekspor);
        
        if($header['sertifikat'] == 0){
            $this->nodocument = date('y') .'.'. str_pad($header['no_urut'], 5, "0", STR_PAD_LEFT) .'-00000.' . $this->config->item('lvlk') . '-ID-' . $header['iso'];
        }else{
            $this->nodocument = date('y') .'.'. str_pad($header['no_urut'], 5, "0", STR_PAD_LEFT) .'-'. str_pad(substr(trim($header['sertifikat']),-5), 5, "0", STR_PAD_LEFT) .'.' . $this->config->item('lvlk') . '-ID-' . $header['iso'];
        }
        
        $data['no_dokumen']	= $this->nodocument;
        $data['status']		= 'rejected';
        $data['clientid']	= $header['client_id'];
        $data['reviewerid']	= $this->_CI->session->userdata('userid');
        $data['postdate']	= date('Y-m-d');
        $data['invoice']	= $header['invoice'];
        $data['bulan']		= $header['bulan'];
        $data['tahun']		= $header['tahun'];
        $data['keterangan']	= $keterangan;
        $data['barcode']	= '';
        $data['pejabat_ttd']= '';
        $data['idekspor']	= $idekspor;
        
        $insert_id = $this->_CI->ekspor_model->add_vlegal($data);
        $this->_CI->ekspor_model->updateStatusLIU($idekspor,"reject");
        $this->status_vlegal = false;
        $this->sendEmailReject($insert_id);
        
    
    }
    
    /* Pribadi dan Rahasia */
    
     private function sendDocument($idekspor,$pejabat,$no_urut,$header)
    {
        
        // if($this->_CI->duplicate_check($idekspor,$this->config->item('lvlk'))){
            // $no_urut = $this->_CI->ekspor_model->getNoUrut($header['client_id']);
        // }
        
        $this->createXmlSendDocument($idekspor,$header,$pejabat,$no_urut);
        
        $arrayParameter = array($this->config->item('USER_LIU'),$this->config->item('PASS_LIU'),$this->xmlDocument->outputMemory(true));
        $response = $this->nusoap_client->call('sendDocument',$arrayParameter);
        
        $error = $this->nusoap_client->getError();
        if ($error) {
            $err_msg = "Error saat proses pengiriman data ke LIU.\n";
            $err_msg .= $this->nusoap_client->response."\n";
            $err_msg .= $this->nusoap_client->getDebug()."\n";
            $this->error_message = $err_msg;
            return false;
        }
        unset($this->xmlDocument);
        
        $xmlReturn = simplexml_load_string($response);
        
        if($xmlReturn->return_send=="R00"){
            $data['no_dokumen']	= trim($this->nodocument);
            $data['status']		= 'send';
            $data['clientid']	= $header['client_id'];
            $data['reviewerid']	= $this->_CI->session->userdata('userid');
            $data['postdate']	= date('Y-m-d');
            $data['invoice']	= $header['invoice'];
            $data['bulan']		= $header['bulan'];
            $data['tahun']		= $header['tahun'];
            $data['keterangan']	= $xmlReturn->link_cetak;
            $data['barcode']	= $xmlReturn->barcode;
            $data['pejabat_ttd']= $pejabat;
            $data['idekspor']	= $idekspor;
            
            $insert_id = $this->_CI->ekspor_model->add_vlegal($data);
            $this->_CI->ekspor_model->updateStatusLIU($idekspor,"send");
            $filename = $this->downloadFile($header['client_id'],$this->nodocument,$xmlReturn->link_cetak);
            $this->status_vlegal = true;
            $this->sendEmail($insert_id,$filename);
            return true;
        }else{
            $data['no_dokumen']	= trim($this->nodocument);
            $data['status']		= 'rejected';
            $data['clientid']	= $header['client_id'];
            $data['reviewerid']	= $this->_CI->session->userdata('userid');
            $data['postdate']	= date('Y-m-d');
            $data['invoice']	= $header['invoice'];
            $data['bulan']		= $header['bulan'];
            $data['tahun']		= $header['tahun'];
            $data['keterangan']	= $xmlReturn->link_cetak;
            $data['barcode']	= $xmlReturn->barcode;
            $data['pejabat_ttd']= $pejabat;
            $data['idekspor']	= $idekspor;
            
            $insert_id = $this->_CI->ekspor_model->add_vlegal($data);
            $this->_CI->ekspor_model->updateStatusLIU($idekspor,"reject");
            $this->status_vlegal = false;
            
            $this->sendEmailReject($insert_id);
            return true;
        }
    
    }
    
    private function start_document()
    {
        $this->xmlDocument = new XMLWriter();
        $this->xmlDocument->openMemory();
        //$this->xmlDocument->openURI("test.xml");
        $this->xmlDocument->setIndent(true);
        $this->xmlDocument->startDocument('1.0','UTF-8');
    }
    
    private function createXmlSendDocument($idekspor,$header,$pejabat,$no_urut)
    {
        $detail_list    = $this->_CI->ekspor_model->detail_ekspor($idekspor);
        
        $dataPejabat = $this->_CI->master_model->get_pejabat_ttd($pejabat);
        
        if(!empty($dataPejabat->lokasi)){
			$lokasi = $dataPejabat->lokasi;
		}else{
			$lokasi = "Jakarta";
		}
        	
		$nourut = $this->_CI->ekspor_model->updateNoUrut($idekspor,$no_urut);
		
		$date = (!empty($header['editdate']))? $header['editdate']:$header['submitdate'];
		$tgl = explode(" ",$date);
        
        $this->start_document();
        $this->xmlDocument->startElement('vlegal_license');
        $this->xmlDocument->startElement('header');
            $this->xmlDocument->writeElement('negara_tujuan', $header["iso"]);
            $this->xmlDocument->writeElement('skema_kerjasama', '1');
            $this->xmlDocument->writeElement('nama_importir', $header["namabuyer"]);
            $this->xmlDocument->writeElement('alamat_importir', $header["alamat_buyer"]);
            $this->xmlDocument->writeElement('negara_importir', $header["iso"]);
            $this->xmlDocument->writeElement('loading_port', $header["loading_code"]);
            $this->xmlDocument->writeElement('discharge_port', $header["discharge_code"]);
            $this->xmlDocument->writeElement('no_slk', substr($header["sertifikat"],-5));
            $this->xmlDocument->writeElement('no_dokumen', $no_urut);
            $this->xmlDocument->writeElement('tgl_dokumen', date('Ymd'));
            $this->xmlDocument->writeElement('no_invoice', $header["invoice"]);
            $this->xmlDocument->writeElement('tgl_invoice', str_replace("-","",$header["tglinvoice"]));
            $this->xmlDocument->writeElement('transportasi', $header["vessel"]);
            $this->xmlDocument->writeElement('npwp_eksportir', slug($header["npwp"],''));
            $this->xmlDocument->writeElement('nama_eksportir', $header["nama_client"]);
            $this->xmlDocument->writeElement('alamat_eksportir', $header["alamat_client"]);
            $this->xmlDocument->writeElement('kode_propinsi', $header["kode_provinsi"]);
            $this->xmlDocument->writeElement('kode_kabupaten', $header["kode_kota"]);
            $this->xmlDocument->writeElement('no_etpik', $header["etpik"]);
            $this->xmlDocument->writeElement('keterangan', $header["keterangan"]);
            $this->xmlDocument->writeElement('nama_ttd',$pejabat);
            $this->xmlDocument->writeElement('tempat_ttd', $lokasi);
            $this->xmlDocument->writeElement('digital_sign', 1); 
            $this->xmlDocument->writeElement('location_stuffing', $header["stuffing"]);
        $this->xmlDocument->endElement();
        
        $this->xmlDocument->startElement('detil');
        foreach($detail_list as $row){
            $net = (empty($row->berat))? 0 : $row->berat;
            $vol = (empty($row->volume))? 0 : $row->volume;
            $jml = (empty($row->jumlah))? 0 : $row->jumlah;
            
            $nama_jenis_kayu    = implode(array_column(json_decode($row->jenis_kayu,true),'name'),";");
            $nama_asal_negara   = implode(array_column(json_decode($row->negara_asal,true),'id'),";");
            
            $this->xmlDocument->startElement('data_barang');
                $this->xmlDocument->writeElement('kode_hs', slug($row->hscode,''));
                $this->xmlDocument->writeElement('hs_printed', slug($row->hscode,''));
                $this->xmlDocument->writeElement('deskripsi_barang', $row->namaproduk);
                $this->xmlDocument->writeElement('volume', $vol);
                $this->xmlDocument->writeElement('netto', $net);
                $this->xmlDocument->writeElement('jumlah_unit', $jml);
                $this->xmlDocument->writeElement('value', number_format($row->nilai, 2, '.', ''));
                $this->xmlDocument->writeElement('valuta', $header['valuta']);
            $this->xmlDocument->writeElement('nama_ilmiah', $nama_jenis_kayu );
            $this->xmlDocument->writeElement('negara_panen', $nama_asal_negara);
            $this->xmlDocument->endElement();
        }
        $this->xmlDocument->endElement();
        $this->xmlDocument->endElement();
        $this->xmlDocument->endDocument();
    }
    
    private function createXmlCancelDocument($alasan,$no_dokumen)
    {
		$this->start_document();
        $this->xmlDocument->startElement('vlegal_license');
		$this->xmlDocument->writeElement('no_dokumen', $no_dokumen);
		$this->xmlDocument->writeElement('keterangan', $alasan);
		$this->xmlDocument->endElement();
		$this->xmlDocument->endDocument();
    }
    
     private function createXmlExtendDocument($no_dokumen)
    {
		$this->start_document();
        $xmlW->startElement('vlegal_license');
			$xmlW->writeElement('no_dokumen', $no_dokumen);
			$xmlW->writeElement('keterangan', $no_dokumen);
			$xmlW->writeElement('tgl_akhir', $no_dokumen);
		$xmlW->endElement();
		$xmlW->endDocument();
    }
    
    private function extendDocument($idekspor,$header,$pejabat){
        
		$header = $this->_CI->ekspor_model->getSendVlegalByIdEkspor($idekspor);
		$this->createXmlExtendDocument($header['no_dokumen']);
		
        $arrayParameter = array($this->config->item('USER_LIU'),$this->config->item('PASS_LIU'),$this->xmlDocument->outputMemory(true));
        $response = $this->nusoap_client->call('extendDocument',$arrayParameter);

		$xmlReturn = simplexml_load_string($response);
        		
        if($xmlReturn->return_send=="R00"){
            $data['no_dokumen']	= trim($header['no_dokumen']);
            $data['status']		= 'send';
            $data['clientid']	= $header['client_id'];
            $data['reviewerid']	= $this->_CI->session->userdata('userid');
            $data['postdate']	= date('Y-m-d');
            $data['invoice']	= $header['invoice'];
            $data['bulan']		= $header['bulan'];
            $data['tahun']		= $header['tahun'];
            $data['keterangan']	= $xmlReturn->link_cetak;
            $data['barcode']	= $xmlReturn->barcode;
            $data['pejabat_ttd']= $pejabat;
            $data['idekspor']	= $idekspor;
            
            $insert_id = $this->_CI->ekspor_model->add_vlegal($data);
            $this->_CI->ekspor_model->updateStatusLIU($idekspor,"send");
            $filename = $this->downloadFile($header['client_id'],$this->nodocument,$xmlReturn->link_cetak,true);
            $this->status_vlegal = true;
            //$this->sendEmail($insert_id,$filename);
            return true;
        }else{
            $data['no_dokumen']	= trim($header['no_dokumen']);
            $data['status']		= 'rejected';
            $data['clientid']	= $header['client_id'];
            $data['reviewerid']	= $this->_CI->session->userdata('userid');
            $data['postdate']	= date('Y-m-d');
            $data['invoice']	= $header['invoice'];
            $data['bulan']		= $header['bulan'];
            $data['tahun']		= $header['tahun'];
            $data['keterangan']	= $xmlReturn->link_cetak;
            $data['barcode']	= $xmlReturn->barcode;
            $data['pejabat_ttd']= $pejabat;
            $data['idekspor']	= $idekspor;
            
            $insert_id = $this->_CI->ekspor_model->add_vlegal($data);
            $this->_CI->ekspor_model->updateStatusLIU($idekspor,"rejected");
            $this->status_vlegal = false;
            $this->sendEmailReject($insert_id);
            return true;
        }
        unset($this->xmlDocument);
		return true;
	}
    
    
    private function downloadFile($client_id,$nodocument,$url,$extend=false){
		$dir = $this->config->item('vlegal_dir');
        if(!file_exists($dir.$client_id)) mkdir($dir.$client_id,0777);
        $extend_name = ($extend)? " Extended" : "";
        $filename   = "$dir$client_id/$nodocument$extend_name.pdf";
		$file       = file_get_contents($url);
		file_put_contents($filename,$file);
        return $filename;
	}
    
    
    
}

?>