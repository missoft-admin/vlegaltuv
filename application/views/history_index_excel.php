<table cellpadding="0" cellspacing="0" width="100%" class="table" {bordernya}>
	<thead>
		<tr>                                    
			<th {bordernya} colspan="25"><h1> SPBU AUDIT REPORT PER {tanggalnya}</h1></th>                                                 
		</tr>
		<tr>                                    
			<th {headernya} width="5%">No.</th>
			<th {headernya} width="10%">Sent Date</th>
			<th {headernya} width="5%">Audit Date</th>
			<th {headernya} width="5%">Submit Date</th>
			<th {headernya} width="10%">No. SPBU</th>
			<th {headernya} width="5%">Region</th>
			<th {headernya} width="5%">Year</th>
			<th {headernya} width="10%">Alamat</th>
			<th {headernya} width="5%">Kota</th>
			<th {headernya} width="5%">Rayon</th>
			<th {headernya} width="5%">Audit</th>
			<th {headernya} width="10%">Next Audit</th>
			<th {headernya} style="text-align:center" width="10%">Silver</th>                                                 
            <th {headernya} style="text-align:center" width="10%">Gold</th>                                                 
            <th {headernya} style="text-align:center" width="10%">Diamond</th> 
			<th {headernya} width="10%">Total Score</th>                                                 
			<th {headernya} width="5%">WTMS</th>                                                 
			<th {headernya} width="5%">Q & Q</th>                                                 
			<th {headernya} width="5%">WMEF</th>                                                 
			<th {headernya} width="5%">CPF</th>                                                 
			<th {headernya} width="5%">CPO</th>                                                 
			<th {headernya} width="10%">Kelas SPBU</th>                                                 
			<th {headernya} width="10%">Gagal Element</th>                                                 
			<th {headernya} width="10%">Auditor</th>                                                 
		</tr>
	</thead>
	<tbody>
		<?php $no = 0 ?>
		<?php foreach($query as $row){ ?>
		<?php $no = $no + 1 ?>
		<tr>                                    
			<td><?= $no?></td>
			<td><?php echo human_date_short($row->send_date) ?></td>						
			<td><?php echo human_date_short($row->audit_date) ?></td>						
			<td><?php echo (is_null($row->submit_date))? "N/A" : human_date_short($row->submit_date) ?></td>						
			<td><?php echo $row->kode_spbu ?></td>						
			<td><?php echo $row->namaupms ?></td>						
			<td><?php echo $row->tahun ?></td>						
			<td><?php echo $row->alamat ?></td>						
			<td><?php echo $row->nama_kota ?></td>						
			<td><?php echo $row->nama_sales_area ?></td>						
			<td>
                <?php echo $row->initial_audit ?> 
                <?php echo ($row->audit_id == 1 && $row->re_ca == 1)? '<span style="color:red">*</span>' : ""?>
            </td>						
			<td><?php echo $row->next_audit ?></td>						
			
			<td style="<?= ($row->hasil_audit_silver == 1)? "color:green;" : "color:red;" ?>font-weight:bold"><?= ($row->hasil_audit_silver == 1)? "CERTIFIED" : "NOT CERTIFIED" ?></td>
            <td style="<?= ($row->hasil_audit_gold == 1)? "color:green;" : "color:red;" ?>font-weight:bold"><?= ($row->hasil_audit_gold == 1)? "CERTIFIED" : "NOT CERTIFIED" ?></td>
            <td style="<?= ($row->hasil_audit_diamond == 1)? "color:green;" : "color:red;" ?>font-weight:bold"><?= ($row->hasil_audit_diamond == 1)? "CERTIFIED" : "NOT CERTIFIED" ?></td>
            <td><?php echo $row->total_score ?></td>						                                  
			<td><?php echo $row->wtms ?></td>						                                  
			<td><?php echo $row->q_and_q ?></td>						                                  
			<td><?php echo $row->wmef ?></td>						                                  
			<td><?php echo $row->cpf ?></td>						                                  
			<td><?php echo $row->cpo ?></td>						                                  						                                  
			<td>
                <?php 
                    if($row->next_class != ''){ 
                        echo ($row->next_audit_id == 1 && $row->Total == $row->Total_CA)? "De-Certified" : $row->next_class;
                        
                    }else{
                        echo ($row->Total == $row->Total_CA)? "De-Certified": "Cabut Pasti Pas";
                    }
                ?>
            </td>						                                  
			<td>
				<?php 
					if($row->hasil_audit_silver == 0 && strtoupper($row->status_spbu) != "CERTIFIED"){
						echo $row->silver_message;
					}elseif($row->hasil_audit_gold == 0 && strtoupper($row->status_spbu) != "CERTIFIED"){
						echo $row->gold_message;
					}elseif($row->hasil_audit_diamond == 0 && strtoupper($row->status_spbu) != "CERTIFIED"){
						echo $row->diamond_message;
					}
				?>
			</td>
			<td><?php echo ($row->auditor2_nama != "")? $row->auditor1_nama." - ".$row->auditor2_nama : $row->auditor1_nama ?></td>
		</tr>                                
		<?php } ?>
	</tbody>
</table>