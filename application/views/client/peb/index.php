<div class="page-header">
	<h1>{first_title} <small>{second_title}</small></h1>
</div>  
<?php echo error_success($this->session)?>
<div class="row-fluid">
	<div class="span12">                    
		<div class="head">
			<div class="isw-grid"></div><h1> {table_title}</h1>
			<ul class="buttons">
				<li>
					<a href="#" class="isw-settings"></a>
					<ul class="dd-list">
						<li><a href="{client_url}peb/add"><span class="isw-plus"></span> Add PEB</a></li>
						<li><a href="{client_url}peb/export"><span class="isw-plus"></span> Export to Excel</a></li>
					</ul>
				</li>
			</ul>                        
			<div class="clear"></div>
		</div>
		<div class="block-fluid table-sorting">
			<div class="dataTables_wrapper">
            <div class="dataTables_filter" id="tSortable_filter" style="width:auto;">
				<form action="{client_url}peb/search" method="post" style="margin-bottom:0;">
				<label>Search: <input type="text" style="width:300px;" value="{searchText}" name="searchText" placeholder="Search By No. V-Legal"></label>
				</form>
			</div>
			<table cellpadding="0" cellspacing="0" width="100%" class="table">
				<thead>
					<tr>                                    
						<th style="text-align:center" rowspan="3" width="5%">No.</th>
                        <th style="text-align:center" colspan="5">Dokumen Pelengkap Ekspor</th>
						<th style="text-align:center" colspan="5">Data Barang Ekspor Sesuai PEB</th>
                        <th style="text-align:center" rowspan="3" width="10%">Option</th>                                                 
					</tr>
                    <tr>      
                        <th style="text-align:center" colspan="2">Dokumen Pelengkap Ekspor</th>
						<th style="text-align:center" colspan="2">Data Barang Ekspor Sesuai PEB</th>
						<th style="text-align:center" rowspan="2" width="10%">Lokasi Stuffing</th>                                                 
                        <th style="text-align:center" rowspan="2" width="10%">Volume <br/> (m3)</th>                                                 
                        <th style="text-align:center" rowspan="2" width="10%">Netto <br/> (Kg)</th>                                                 
                        <th style="text-align:center" rowspan="2" width="10%">Jumlah <br/> (Unit)</th>                                                 
                        <th style="text-align:center" rowspan="2" width="10%">Nilai</th>                                                 
                        <th style="text-align:center" rowspan="2" width="10%">Nilai <br/> (USD)</th>                                                   
					</tr>
                    <tr>                                    
						<th style="text-align:center" width="15%">Nomor</th>                                                 
						<th style="text-align:center" width="15%">Tanggal</th>
                        <th style="text-align:center" width="15%">Nomor</th>                                                 
						<th style="text-align:center" width="15%">Tanggal</th>                                                 
					</tr>
				</thead>
				<tbody>
					<?php foreach($query as $row){ ?>
					<?php $no = $no + 1 ?>
					<tr>                                    
						<td><?= $no?></td>
						<td><?php echo $row->no_vlegal ?></td>
						<td><?php echo human_date_short($row->tgl_vlegal) ?></td>							
						<td><?php echo $row->nomor_peb ?></td>
						<td><?php echo human_date_short($row->tgl_peb) ?></td>
						<td><?php echo $row->stuffing ?></td>
						<td><?php echo format_angka_id($row->volume,3) ?></td>
						<td><?php echo format_angka_id($row->netto,3) ?></td>
						<td><?php echo format_angka_id($row->jumlah,3) ?></td>
						<td><?php echo format_angka_id($row->nilai,3) ?></td>
						<td><?php echo format_angka_id($row->nilai_usd,3) ?></td>
						
						<td>
							<?php 
                                if($row->status == 0){
                                    echo tool_edit('client/peb/update/'.$row->id);
                                    echo tool_remove('client/peb/delete/'.$row->id);								
                                }
							?>
						</td>                                    
					</tr>                                
					<?php } ?>
				</tbody>
			</table>
			<?php echo $pagination ?>
			
		</div>
		<div class="clear"></div>
	</div>                                
</div>