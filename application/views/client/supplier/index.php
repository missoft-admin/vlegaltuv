<div class="page-header">
	<h1>{first_title} <small>{second_title}</small></h1>
</div>  
<?php echo error_success($this->session)?>
<div class="row-fluid">
	<div class="span12">                    
		<div class="head">
			<div class="isw-grid"></div><h1> {table_title}</h1>
			<ul class="buttons">
				<li>
					<a href="#" class="isw-settings"></a>
					<ul class="dd-list">
						<li><a href="{client_url}supplier/add"><span class="isw-plus"></span> Add Supplier</a></li>
					</ul>
				</li>
			</ul>                        
			<div class="clear"></div>
		</div>
		<div class="block-fluid table-sorting">
			<div class="dataTables_wrapper">
            <div class="dataTables_filter" id="tSortable_filter" style="width:auto;">
				<form action="{client_url}supplier/search" method="post" style="margin-bottom:0;">
				<label>Search: <input type="text" style="width:300px;" value="{searchText}" name="searchText" placeholder="Search Supplier By Nama Supplier / Alamat"></label>
				</form>
			</div>
			<table cellpadding="0" cellspacing="0" width="100%" class="table">
				<thead>
					<tr>                                    
						<th width="5%">No.</th>
						<th width="15%">Clients</th>
						<th width="10%">Negara</th>
						<th width="20%">Nama Supplier</th>
						<th width="20%">Alamat</th>
						<th width="7%">Status</th>
                        <th width="10%">Option</th>                                                 
					</tr>
				</thead>
				<tbody>
					<?php foreach($query as $row){ ?>
					<?php $no = $no + 1 ?>
					<tr>                                    
						<td><?= $no?></td>
						<td><?php echo $row->nama ?></td>
						<td><?php echo $row->negara ?></td>
						<td><?php echo anchor('client/supplier/update/'.$row->idsupplier,$row->supplier) ?></td>							
						<td><?php echo $row->alamat ?></td>
						<td align="center"><?=($row->status ? 'Active':'Not Active'); ?></td>
						<td>
							<?php 
								echo tool_edit('client/supplier/update/'.$row->idsupplier);
								echo tool_remove('client/supplier/delete/'.$row->idsupplier);								
							?>
						</td>                                    
					</tr>                                
					<?php } ?>
				</tbody>
			</table>
			<?php echo $pagination ?>
			
		</div>
		<div class="clear"></div>
	</div>                                
</div>