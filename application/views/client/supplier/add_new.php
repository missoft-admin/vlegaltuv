<div class="page-header">
	<h1>{first_title} <small>{second_title}</small></h1>
</div>  
<?php echo error_success($this->session)?>
<?php if($error != '') echo error_message($error)?>
<div class="row-fluid">
	<div class="span12">
		<div class="head">
			<div class="isw-documents"></div>
			<h1>SUPPLIER FORM</h1>
			<ul class="buttons">
				<li><a href="{client_url}supplier" class="isw-list" title="Supplier List"></a></li>                                                        
			</ul>
			<div class="clear"></div>
		</div>
		<div class="block-fluid">                        
			<?php echo form_open('client/supplier/save','id = "adminform"') ?>
			<div class="row-form">
				<div class="span2">Negara:</div>
				<div class="span10">
					<select name="idnegara">
						<option value="0">Pilih Negara</option>
						<?php foreach ($negara_list as $row){ ?>
							<option <?=($row->idnegara==$idnegara ? 'selected':'') ?> value=<?=$row->idnegara ?>><?=$row->negara ?></option>
						<?php } ?>
						
					</select>
				</div>				
				<div class="clear"></div>
			</div> 
			<div class="row-form">
				<div class="span2">Nama Supplier:</div>
				<div class="span10"><input type="text" name="supplier" placeholder="Nama Supplier" value="{supplier}"/></div>
				<div class="clear"></div>
			</div> 
			<div class="row-form">
				<div class="span2">Alamat:</div>
				<div class="span10"><input type="text" name="alamat" placeholder="Alamat Supplier" value="{alamat}"/></div>
				<div class="clear"></div>
			</div>
			<div class="row-form">
				<div class="span2">Status:</div>
				<div class="span10"><input type="checkbox" name="status" value="{status}"  checked>Active</div>
				<div class="clear"></div>
			</div>			
			<div class="row-form">
				<div class="span12"><button class="btn" type="submit" id="btnsimpan">Simpan</button></div>
				<div class="clear"></div>
			</div> 
			
			<input type="hidden" name="negara" id="negara" value="{negara}"/>
			<?php echo form_hidden('idsupplier', $idsupplier); ?>
			<?php echo form_hidden('client_id', $client_id); ?>
			<?php echo form_close() ?>
		</div>
	</div>
	
</div>
