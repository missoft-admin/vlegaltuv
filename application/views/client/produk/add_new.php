<div class="page-header">
	<h1>{first_title} <small>{second_title}</small></h1>
</div>  
<?php echo error_success($this->session)?>
<?php if($error != '') echo error_message($error)?>
<div class="row-fluid">
	<div class="span12">
		<div class="head">
			<div class="isw-documents"></div>
			<h1>PRODUK FORM</h1>
			<ul class="buttons">
				<li><a href="{client_url}produk" class="isw-list" title="Produk List"></a></li>                                                        
			</ul>
			<div class="clear"></div>
		</div>
		<div class="block-fluid">                        
			<?php echo form_open('client/produk/save','id = "adminform"') ?>
			
			<div class="row-form">
				<div class="span2">Kode HS:</div>
				<div class="span2"><input type="text" maxlength="13" name="kodehs" id="kodehs" placeholder="Kode HS" value="{kodehs}"/></div>
				<div class="span3" id="msgbox_client"></div>
				<div class="clear"></div>
			</div>
			<div class="row-form">
				<div class="span2">Nama Produk:</div>
				<div class="span10"><input type="text" name="produk" placeholder="Nama Produk" value="{produk}"/></div>
				<div class="clear"></div>
			</div> 
			
			<div class="row-form">
				<div class="span2">Status:</div>
				<div class="span10"><input type="checkbox" name="status" value="{status}"  checked>Active</div>
				<div class="clear"></div>
			</div>			
			<div class="row-form">
				<div class="span12"><button class="btn" type="submit" id="btnsimpan">Simpan</button></div>
				<div class="clear"></div>
			</div> 
			
			<input type="hidden" name="negara" id="negara" value="{negara}"/>
			<?php echo form_hidden('idproduk', $idproduk); ?>
			<?php echo form_close() ?>
		</div>
	</div>
	
</div>

<script type="text/javascript">
	$(document).ready(function(){
        
        $("#kodehs").blur(function(){	
            var kodehs=$(this).val();
            if(kodehs != ""){
                $("#msgbox_client").html('<div class="alert alert-info alert-small alert-bold"><i class="icon-refresh icon-white"></i> Checking HS Code...</div>').fadeIn("slow");
                $.post("http://"+window.location.hostname+"/vlegal/client/produk/check_hscode",{ kodehs:$(this).val()} ,function(data){
                    if(data=='no')
                    {
                        $("#msgbox_client").fadeTo(200,0.1,function(){ 
                            $(this).html('<div class="alert alert-error alert-small alert-bold"><i class="icon-exclamation-sign icon-white"></i> HS Code tidak terdaftar!</div>').fadeTo(900,1);
                            $("#kodehs").val("");
                            $("#kodehs").focus();
                        });		
                    }else{	
                        $("#msgbox_client").fadeTo(200,0.1,function(){ 
                            $(this).html('<div class="alert alert-success alert-small alert-bold"><i class="icon-ok icon-white"></i> HS Code Terdaftar</div>').fadeTo(900,1);
                        });	
                        return true;
                    }
                });
            }else {
                $("#msgbox_client").hide("fast");
            }		
        });	
	});
	
</script>