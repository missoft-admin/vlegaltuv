<div class="page-header">
	<h1>{first_title} <small>{second_title}</small></h1>
</div>  
<?php echo error_success($this->session)?>
<div class="row-fluid">
	<div class="span12">                    
		<div class="head">
			<div class="isw-grid"></div><h1> {table_title}</h1>
			<ul class="buttons">
				<li>
					<a href="#" class="isw-settings"></a>
					<ul class="dd-list">
						<li><a href="{client_url}produk/add"><span class="isw-plus"></span> Add Produk</a></li>
					</ul>
				</li>
			</ul>                        
			<div class="clear"></div>
		</div>
		<div class="block-fluid table-sorting">
			<div class="dataTables_wrapper">
            <div class="dataTables_filter" id="tSortable_filter" style="width:auto;">
				<form action="{client_url}produk/search" method="post" style="margin-bottom:0;">
				<label>Search: <input type="text" style="width:300px;" value="{searchText}" name="searchText" placeholder="Search Produk By Nama Produk / Kode HS"></label>
				</form>
			</div>
			<table cellpadding="0" cellspacing="0" width="100%" class="table">
				<thead>
					<tr>                                    
						<th width="5%">No.</th>
						<th width="15%">Kode HS</th>
						<th width="50%">Nama Produk</th>
                        <th width="10%">Stok</th>                                                 
                        <th width="10%">Status</th>                                                 
                        <th width="10%">Option</th>                                                 
					</tr>
				</thead>
				<tbody>
					<?php foreach($query as $row){ ?>
					<?php $no = $no + 1 ?>
					<tr>                                    
						<td><?= $no?></td>
						<td><?php echo $row->kodehs ?></td>
						<td><?php echo anchor('client/produk/update/'.$row->idproduk,$row->produk) ?></td>							
                        <td align="center"><?=$row->stok ?></td>
                        <td align="center"><?=($row->status ? 'Active':'Not Active'); ?></td>
						<td>
							<?php 
								echo tool_edit('client/produk/update/'.$row->idproduk);
								echo tool_remove('client/produk/delete/'.$row->idproduk);								
							?>
						</td>                                    
					</tr>                                
					<?php } ?>
				</tbody>
			</table>
			<?php echo $pagination ?>
			
		</div>
		<div class="clear"></div>
	</div>                                
</div>