	

	<ul class="navigation">            
		<li class="<?= (in_array($this->uri->segment(1),array('home')))? "active":""?>">
			<a href="{client_url}">
				<span class="isw-grid"></span><span class="text">DASHBOARD</span>
			</a>
		</li>
        <li class="openable <?= (in_array($this->uri->segment(2),array('negara','buyer','supplier','kayu','produk','pelabuhan','sortimen','hutan')))? "active":""?>">
		<a href="#"><span class="isw-bookmark"></span><span class="text">MASTER</span></a>
            <ul>
                <li><a href="{client_url}buyer"><span class="icon-flag"></span><span class="text">Buyer</span></a></li>
                <li><a href="{client_url}supplier"><span class="icon-qrcode"></span><span class="text">Supplier</span></a></li>
                <li><a href="{client_url}produk"><span class="icon-tag"></span><span class="text">Data Produk </span></a></li>
                <li><a href="{client_url}sortimen"><span class="icon-eye-close"></span><span class="text">Sortimen Bahan Baku</span></a></li>
                
            </ul>
        </li>
        <li class="openable <?= (in_array($this->uri->segment(2),array('penerimaan','pengeluaran','bahan_baku')))? "active":""?>">
            <a href="#"><span class="isw-text_document"></span><span class="text">BAHAN BAKU</span></a>
            <ul>
                <li><a href="{client_url}penerimaan/"><span class="icon-leaf"></span><span class="text">Penerimaan</span></a></li>
                <li><a href="{client_url}pengeluaran/"><span class="icon-leaf"></span><span class="text">Pengeluaran</span></a></li>
                <li><a href="{client_url}bahan_baku"><span class="icon-eye-close"></span><span class="text">Stok Bahan Baku</span></a></li>
            </ul>
        </li>
        <li class="openable <?= (in_array($this->uri->segment(2),array('produksi')))? "active":""?>">
            <a href="#"><span class="isw-text_document"></span><span class="text">PRODUKSI</span></a>
            <ul>
                <li><a href="{client_url}produksi/"><span class="icon-leaf"></span><span class="text">Realisasi Produksi</span></a></li>
            </ul>
        </li>
        <li class="openable <?= (in_array($this->uri->segment(2),array('penjualan')) || (in_array($this->uri->segment(2),array('ekspor')) && in_array($this->uri->segment(3),array('','index','add','edit','detail','search'))))? "active":""?>">
            <a href="#"><span class="isw-text_document"></span><span class="text">PENGELUARAN PRODUK</span></a>
            <ul>
                <li><a href="{client_url}penjualan"><span class="icon-leaf"></span><span class="text">Penjualan Lokal</span></a></li>
                <li><a href="{client_url}ekspor"><span class="icon-leaf"></span><span class="text">Penjualan Ekspor</span></a></li>
            </ul>
        </li>
        <li class="openable <?= ($this->uri->segment(2) == 'ekspor' && in_array($this->uri->segment(3),array('draft','on_progress','rejected','approved','add','edit','detail','attachment','add_attachment','search')) || $this->uri->segment(2) == 'peb')? "active":""?>">
            <a href="#"><span class="isw-text_document"></span><span class="text">V-LEGAL</span></a>
            <ul>
                <li><a href="{client_url}ekspor/draft"><span class="icon-leaf"></span><span class="text">Draft</span></a></li>
                <li><a href="{client_url}ekspor/on_progress"><span class="icon-leaf"></span><span class="text">On Progress</span></a></li>
                <li><a href="{client_url}ekspor/rejected"><span class="icon-leaf"></span><span class="text">Rejected</span></a></li>
                <li><a href="{client_url}ekspor/approved"><span class="icon-leaf"></span><span class="text">Approved</span></a></li>
                <li><a href="{client_url}peb"><span class="icon-leaf"></span><span class="text">Rekapitulasi PEB</span></a></li>
            </ul>
        </li>
		<li class="openable <?= (in_array($this->uri->segment(1),array('Laporan')))? "active":""?>">
            <a href="#"><span class="isw-graph"></span><span class="text">REPORT</span></a>
            <ul>
                <li><a href="{client_url}laporan/lmko"><span class="icon-leaf"></span><span class="text">LMKO</span></a></li>
                <!--
                <li><a href="{client_url}project/"><span class="icon-leaf"></span><span class="text">Penggunaan Bahan Baku</span></a></li>
                <li><a href="{client_url}project/started"><span class="icon-leaf"></span><span class="text">Stok Bahan Baku</span></a></li>
                <li><a href="{client_url}project/open"><span class="icon-leaf"></span><span class="text">Produksi</span></a></li>
                <li><a href="{client_url}project/close"><span class="icon-leaf"></span><span class="text">Pengeluaran Produk Jadi</span></a></li>
                <li><a href="{client_url}project/finish"><span class="icon-leaf"></span><span class="text">Stok Produk Jadi</span></a></li>
                <li><a href="{client_url}project/finish"><span class="icon-leaf"></span><span class="text">Work In Procces (WIP)</span></a></li>
                <li><a href="{client_url}project/finish"><span class="icon-leaf"></span><span class="text">Penerbitan Dokumen V-Legal</span></a></li>
                <li><a href="{client_url}project/finish"><span class="icon-leaf"></span><span class="text">Rendemen</span></a></li>
                <li><a href="{client_url}project/finish"><span class="icon-leaf"></span><span class="text">Realisasi Jasa Proses</span></a></li>
                -->
            </ul>
        </li>
		
			        
		
        
    </ul>
