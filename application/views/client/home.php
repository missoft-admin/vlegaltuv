<div class="page-header">
	<h1>{first_title} <small>{second_title}</small></h1>
</div> 
<!-- 
<div class="row-fluid">
	<div class="span3">
        <div class="wBlock red clearfix">                        
            <div class="dSpace">
                <h3>Auditor Load</h3>
                <span class="mChartBar" sparkType="bar" sparkBarColor="white"></span>
                <span class="number">60%</span>                    
            </div>
            <div class="rSpace">
                <span>30% <b>Pre Audit</b></span>
                <span>50% <b>Open Audit</b></span>
                <span>20% <b>Pre Audit</b></span>
            </div>                          
        </div>                        
    </div>                        
    <div class="span3">
        <div class="wBlock green clearfix">                        
            <div class="dSpace">
                <h3>Project</h3>
                <span class="mChartBar" sparkType="bar" sparkBarColor="white"></span>
                <span class="number">131</span>                    
            </div>
            <div class="rSpace">
                <span>15 <b>in process</b></span>
                <span>99 <b>in queue</b></span>
                <span>16 <b>total</b></span>
            </div>                          
        </div>                                                                                                     
    </div>
    <div class="span6">

        <div class="wBlock red auto clearfix">
            <div class="dSpace">
                <h3>Total Project</h3>
                <span class="mChartBar" sparkType="bar" sparkBarColor="white"></span>
                <span class="number">250/23</span>                                                  
            </div>
        </div>                    

        <div class="wBlock green auto clearfix">
            <div class="dSpace">
                <h3>Pending</h3>
                <span class="mChartBar" sparkType="bar" sparkBarColor="white"></span>
                <span class="number">14,233</span>                                                  
            </div>
        </div>                    

        <div class="wBlock auto clearfix">
            <div class="dSpace">
                <h3>Started</h3>
                <span class="mChartBar" sparkType="bar" sparkBarColor="white"></span>
                <span class="number">$1,204</span>                                                
            </div>
        </div>    

        <div class="wBlock yellow auto clearfix">
            <div class="dSpace">
                <h3>Open Audit</h3>
                <span class="mChartBar" sparkType="bar" sparkBarColor="white"></span>
                <span class="number">60%</span>                                                  
            </div>
        </div>                        
    </div>
</div>    


<div class="row-fluid">
    <div class="span6">                       
        <div class="headInfo">
            <h2>Project Activity</h2>
            <div class="arrow_down"></div>
        </div>
        <div class="block messages scrollBox">

            <div class="scroll" style="height: 320px;">

                <div class="item clearfix">
                    <div class="image"><a href="#"><img src="{images_path}users/aqvatarius.jpg" class="img-polaroid"/></a></div>
                    <div class="info">
                        <a class="name" href="#">Aqvatarius</a>
                        <p>Lorem ipsum dolor. In id adipiscing diam. Sed lobortis dui ut odio tempor blandit. Suspendisse scelerisque mi nec nunc gravida quis mollis lacus dignissim.</p>
                        <span>19 feb 2012 12:45</span>
                    </div>
                </div>

                <div class="item clearfix">
                    <div class="image"><a href="#"><img src="{images_path}users/olga.jpg" class="img-polaroid"/></a></div>
                    <div class="info">
                        <a class="name" href="#">Olga</a>
                        <p>Cras nec risus dolor, ut tristique neque. Donec mauris sapien, pellentesque at porta id, varius eu tellus.</p>
                        <span>18 feb 2012 12:45</span>
                    </div>
                </div>                        

                <div class="item clearfix">
                    <div class="image"><a href="#"><img src="{images_path}users/dmitry.jpg" class="img-polaroid"/></a></div>
                    <div class="info">
                        <a class="name" href="#">Dmitry</a>
                        <p>In id adipiscing diam. Sed lobortis dui ut odio tempor blandit.</p>
                        <span>17 feb 2012 12:45</span>
                    </div>
                </div>                         

                <div class="item clearfix">
                    <div class="image"><a href="#"><img src="{images_path}users/helen.jpg" class="img-polaroid"/></a></div>
                    <div class="info">
                        <a class="name" href="#">Helen</a>
                        <p>Sed lobortis dui ut odio tempor blandit. Suspendisse scelerisque mi nec nunc gravida quis mollis lacus dignissim. Donec mauris sapien, pellentesque at porta id, varius eu tellus.</p>
                        <span>15 feb 2012 12:45</span>
                    </div>
                </div>                                  

                <div class="item clearfix">
                    <div class="image"><a href="#"><img src="{images_path}users/aqvatarius.jpg" class="img-polaroid"/></a></div>
                    <div class="info">
                        <a class="name" href="#">Aqvatarius</a>
                        <p>Lorem ipsum dolor. In id adipiscing diam. Sed lobortis dui ut odio tempor blandit. Suspendisse scelerisque mi nec nunc gravida quis mollis lacus dignissim.</p>
                        <span>19 feb 2012 12:45</span>
                    </div>
                </div>                        

            </div>
        </div>                    
    </div>
    <div class="span6">
        <div class="wBlock blue auto clearfix">
            <div class="dSpace">
                <h3>Close Audit</h3>
                <span class="mChartBar" sparkType="bar" sparkBarColor="white"></span>
                <span class="number">11,333</span>                                                  
            </div>
        </div>  

        <div class="wBlock gray auto clearfix">
            <div class="dSpace">
                <h3>Project Completed</h3>
                <span class="mChartBar" sparkType="bar" sparkBarColor="white"></span>
                <span class="number">2,513</span>                                                  
            </div>
        </div>  
        <div class="wBlock gray auto clearfix">
            <div class="dSpace">
                <h3>Percentage</h3>
                <span class="mChartBar" sparkType="bar" sparkBarColor="white"></span>
                <span class="number">2,513</span>                                                  
            </div>
        </div>                  

    </div>                
</div>
-->