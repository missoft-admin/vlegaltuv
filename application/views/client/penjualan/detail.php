<div class="page-header">
	<h1>{first_title} <small>{second_title}</small></h1>
</div>  
<?php echo error_success($this->session)?>
<?php if($error != '') echo error_message($error)?>
<div class="row-fluid">
	<div class="span12">
		<div class="head">
			<div class="isw-documents"></div>
			<h1>Form Informasi Data Ekspor</h1>
			<ul class="buttons">
				<li><a href="{client_url}ekspor" class="isw-list" title="List Pengajuan"></a></li>                                                        
			</ul>
			<div class="clear"></div>
		</div>
		<div class="block-fluid">                        
            <?php echo form_open('client/ekspor/save','id = "frmTransaksi"') ?>
			<div class="row-form">
				<div class="span2">No. Invoice & Tanggal Invoice:</div>
				<div class="span4">{invoice} & <?= human_date_short($tglinvoice) ?></div>
                <div class="span2">Loading / Discharge:</div>
				<div class="span4">{loading_name} / {discharge_name}</div>
				<div class="clear"></div>
			</div>
            <div class="row-form">
				<div class="span2">Pembeli / Negara / ISO:</div>
				<div class="span4">{namabuyer} / {country_name} / {iso}</div>
                <div class="span2">Alamat:</div>
				<div class="span4">{alamat}</div>
				<div class="clear"></div>
			</div>
            <div class="row-form">
				<div class="span2">No. PEB / No. B/L:</div>
				<div class="span4">{peb} / {bl}</div>
                <div class="span2">Packing List:</div>
				<div class="span4">{packinglist}</div>
				<div class="clear"></div>
			</div>
            <div class="row-form">
				<div class="span2">Tanggal Shipment:</div>
				<div class="span4"><?= human_date_short($tglship) ?></div>
                <div class="span2">Vessel:</div>
				<div class="span4"><?= define_vessel($vessel) ?> </div>
				<div class="clear"></div>
			</div>
            <div class="row-form">
				<div class="span2">ETPIK<br/>NPWP<br/>Sertifikat</div>
				<div class="span4">{etpik}<br/>{npwp}<br/>{sertifikat}</div>
                <div class="span2">Keterangan:</div>
				<div class="span4">{keterangan}</div>
				<div class="clear"></div>
			</div>
            <div class="row-form">
				<div class="span2">Status Dokumen LIU</div>
				<div class="span4">{status_liu}</div>
                <div class="span2">Mata Uang:</div>
				<div class="span4">{valuta}</div>
				<div class="clear"></div>
			</div>
            <div class="row-form">
				<div class="span2">Status Dokumen</div>
				<div class="span4">{status_dokumen}</div>
                <div class="span2">&nbsp;</div>
				<div class="span4">&nbsp;</div>
				<div class="clear"></div>
			</div>
            <div class="row-form">
				<div class="span2">Lokasi Stuffing</div>
				<div class="span4">{stuffing}</div>
                <div class="span2">&nbsp;</div>
				<div class="span4">&nbsp;</div>
				<div class="clear"></div>
			</div>
            <div class="row-form">
                <div class="span12" style="text-align:center"><h4>Data Detail Ekspor</h4></div>
                <div class="clear"></div>
            </div>
            <div class="row-form" style="padding:0px;">
				<div class="span12">
                        <table cellpadding="0" cellspacing="0" width="100%" class="table" id="detail_list">
                            <thead>
                                <tr>
                                    <th rowspan="2" width="5%">No.</th>
                                    <th rowspan="2" width="10%">FAKO</th>
                                    <th rowspan="2" width="20%">Nama Produk Jadi</th>
                                    <th rowspan="2" width="10%">HS CODE</th>
                                    <th colspan="3" width="10%">Jumlah</th>
                                    <th rowspan="2" width="15%">Jenis Kayu</th>
                                    <th rowspan="2" width="10%">Negara Asal</th>
                                    <th rowspan="2" width="10%">Nilai</th>
                                </tr>
                                <tr>                                    
                                    <th width="5%">m3</th>
                                    <th width="5%">kg</th>
                                    <th width="5%">unit</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                <?php $no = 0;?>
                                <?php foreach($detail_list as $row) { ?>
                                <?php $no++;?>
                                    <tr>
                                        <td width="5%"><?= $no ?></td>
                                        <td width="10%"><?= $row->fako ?></td>
                                        <td width="10%"><?= $row->namaproduk ?>)</td>
                                        <td width="10%"><?= $row->hscode ?></td>
                                        <td width="5%" class="angka_det"><?= titik_to_koma($row->volume) ?></td>
                                        <td width="5%" class="angka_det"><?= titik_to_koma($row->berat) ?></td>
                                        <td width="5%" class="angka_det"><?= titik_to_koma($row->jumlah) ?></td>
                                        <td width="10%"><?= $row->namakayu1."<br/>".$row->namakayu2."<br/>".$row->namakayu1 ?></td>
                                        <td width="10%"><?=  $row->asalnegara1."<br/>".$row->asalnegara2."<br/>".$row->asalnegara3 ?></td>
                                        <td width="5%" class="angka_det"><?= titik_to_koma($row->nilai) ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                </div>
				<div class="clear"></div>
			</div> 
            
			<?php echo form_hidden('idekspor', $idekspor); ?>
			<?php echo form_close() ?>
            
		</div>
	</div>
	
</div>
<link href="{css_path}chosen.css" rel="stylesheet" type="text/css" />
<script src="{js_path}chosen.jquery.js" type="text/javascript"></script>
<script src="{js_path}ajax-chosen.js" type="text/javascript"></script>
<script type="text/javascript">

$(document).ready(function(){
    $('.angka').number(true,3,',','.');
    $('.angka_det').number(true,3,',','.');
    
    
    $("input").bind("keypress", function(e) {
        if (e.keyCode == 13){
            return false;
        }
	});
    
    
    $("#idbuyer").ajaxChosen({
        method: 'GET',
        url: '{client_url}ekspor/find_buyer',
        dataType: 'json',
        minTermLength: 3,
        afterTypeDelay: 300
    },function (data) {
        var terms = {};
        
        $.each(data.buyer_list, function (i,buyer) {
            terms[buyer.idbuyer] = buyer.buyer;
        });
        
        return terms;
    });	

    $("#idbuyer").change(function(){
        $("#namabuyer").val($("#idbuyer option:selected").text());
        $.ajax({
          url: '{client_url}ekspor/buyer_detail/'+$(this).val(),
          dataType: 'json',
          success: function(data) {
            console.log(data);
            $("#namabuyer").val(data.buyer);
            $("#alamat").val(data.alamat);
            $("#idnegara").val(data.idnegara);
            $("#iso").val(data.idnegara);
            $("#country_name").val(data.country_name);
          }
        });
    });
    
    $("#valuta").ajaxChosen({
        method: 'GET',
        url: '{client_url}ekspor/find_valuta',
        dataType: 'json',
        minTermLength: 3,
        afterTypeDelay: 300
    },function (data) {
        var terms = {};
        
        $.each(data.valuta_list, function (i,valuta) {
            terms[valuta.kode] = valuta.kode+' - '+valuta.nama;
        });
        
        return terms;
    });	

    $("#valuta").change(function(){
        $("#nama_valuta").val($("#valuta option:selected").text())
    });
    
    
    $("#loading").ajaxChosen({
        method: 'GET',
        url: '{client_url}ekspor/find_loading',
        dataType: 'json',
        minTermLength: 3,
        afterTypeDelay: 300
    },function (data) {
        var terms = {};
        
        $.each(data.loading_list, function (i,loading) {
            terms[loading.idloading] = loading.kode+' ['+loading.uraian+']';
        });
        
        return terms;
    });	

    $("#loading").change(function(){
        $("#loading_name").val($("#loading option:selected").text())
    });
    
    
    $("#discharge").ajaxChosen({
        method: 'GET',
        url: '{client_url}ekspor/find_discharge',
        dataType: 'json',
        minTermLength: 1,
        afterTypeDelay: 300,
        beforeSend: function(xhr, opts) {
            opts.url += '&idnegara='+$("#iso").val();
        },
    },function (data) {
        var terms = {};
        
        $.each(data.discharge_list, function (i,discharge) {
            terms[discharge.iddischarge] = discharge.kode+' ['+discharge.uraian+']';
        });
        
        return terms;
    });	

    $("#discharge").change(function(){
        $("#discharge_name").val($("#discharge option:selected").text())
    });
    
    });
</script>