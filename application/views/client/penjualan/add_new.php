<div class="page-header">
	<h1>{first_title} <small>{second_title}</small></h1>
</div>  
<?php echo error_success($this->session)?>
<?php if($error != '') echo error_message($error)?>
<div class="row-fluid">
	<div class="span12">
		<div class="head">
			<div class="isw-documents"></div>
			<h1>Form Penjualan Lokal</h1>
			<ul class="buttons">
				<li><a href="{client_url}penjualan" class="isw-list" title="List Penjualan Lokal"></a></li>                                                        
			</ul>
			<div class="clear"></div>
		</div>
		<div class="block-fluid">                        
			<?php echo form_open('client/penjualan/save','id = "frmTransaksi"') ?>
			<div class="row-form">
				<div class="span2">Nama Pembeli:</div>
				<div class="span10">
                    <select name="idbuyer" id="idbuyer" class="validate[required]">
						<option value="{idbuyer}">{namabuyer}</option>
					</select>
                
                </div>
                
				<div class="clear"></div>
			</div> 
             <div class="row-form">
				<div class="span2">Alamat Pembeli:</div>
				<div class="span10"><textarea readonly name="alamat" id="alamat" placeholder="Alamat Pembeli">{alamat}</textarea></div>
				<div class="clear"></div>
			</div> 
            <div class="row-form">
                <div class="span2">Tanggal Invoice:</div>
				<div class="span4"><input class="<?= ($final == 0)? 'datepickertxt' : '' ?> validate[required]" type="text" name="periode_awal" placeholder="Tanggal Invoice" value="<?= human_date_short($periode_awal) ?>"/></div>
                <div class="span2">Tanggal Terbit:</div>
				<div class="span4"><input class="<?= ($final == 0)? 'datepickertxt' : '' ?> validate[required]" type="text" name="tglterbit" placeholder="Tanggal Terbit" value="<?= human_date_short($tglterbit) ?>"/></div>
                <div class="clear"></div>
			</div> 
            
            <div class="row-form">
                <div class="span2">No. Invoice:</div>
				<div class="span4"><input class="validate[required]" type="text" name="invoice" placeholder="No. Invoice" value="{invoice}"/></div>
                <div class="span2">No. Nota:</div>
				<div class="span4"><input class="validate[required]" type="text" name="nonota" placeholder="No. Nota" value="{nonota}"/></div>
                
				<div class="clear"></div>
			</div> 
            <div class="row-form">
                <div class="span2">No. PO:</div>
				<div class="span4"><input class="validate[required]" type="text" name="no_po" placeholder="No. PO" value="{no_po}"/></div>
                <div class="span2">Tanggal PO:</div>
				<div class="span4"><input  class="<?= ($final == 0)? 'datepickertxt' : '' ?> validate[required]" type="text" name="tgl_po" placeholder="Tanggal PO" value="<?= human_date_short($tgl_po) ?>"/></div>
                
				<div class="clear"></div>
			</div> 
            <div class="row-form">
				<div class="span2">Provinsi:</div>
				<div class="span4">
					<select name="idprovinsi" id="idprovinsi" data-placeholder="Provinsi">
						<option value="{idprovinsi}">{nama_provinsi}</option>
					</select>
				</div>	
                <div class="span2">Kota:</div>
				<div class="span4">
					<select class="validate[required]" name="idkota" id="idkota" data-placeholder="Kota">
						<option value="{idnegara}">{nama_kota}</option>
					</select>
				</div>				
				<div class="clear"></div>
			</div>
            <div class="row-form">
                <div class="span2">Nilai (Rp)</div>
				<div class="span4"><input class="angka" type="text" name="nilai" placeholder="Nilai (Rp)" value="{nilai}"/></div>
				<div class="clear"></div>
			</div> 
			
            <div class="row-form">
                <div class="span12" style="text-align:center"><h4>Data Detail Penjualan Lokal</h4></div>
                <div class="clear"></div>
            </div>
            <?php if($final == 0){ ?>
			<div class="row-form" style="padding:0px;">
                <div class="span12">
                    <div class="block-fluid"  style="position:relative;z-index:1;margin-bottom:0px;background-color:#F1F1F1">
                        <table cellpadding="0" cellspacing="0" width="100%" class="table">
                            <thead>
                                <tr>
                                    <th  width="15%">Product</th>
                                    <th colspan="6" style="vertical-align:middle">
                                        <input type="hidden" class="detail" id="rowindex"/>
                                        <input type="hidden"  class="detail" id="nomor"/>
                                        <input type="hidden"  class="detail" id="hscode"/>
                                        <select name="idproduct" id="idproduct">
                                            <option value="">Pilih Product</option>
                                        </select>
                                    </th>
                                    <th width="5%" rowspan="2" style="vertical-align:middle">
                                        <button class="btn" id="cmdAdd" type="button">Add</button>
                                        
                                    </th>                                                 
                                </tr>
                                <tr>
                                    <th  width="10%">Jenis Kayu</th>
                                    <th colspan="2"style="vertical-align:middle" >
                                        <select id="idkayu" data-placeholder="Jenis Kayu">
                                            <option value="">-Pilih Jenis Kayu-</option>
                                        </select>
                                    </th>
                                    <th colspan="2"style="vertical-align:middle" >
                                        <select id="idkayu2" data-placeholder="Jenis Kayu">
                                            <option value="">-Pilih Jenis Kayu-</option>
                                        </select>
                                    </th>
                                    <th colspan="2"style="vertical-align:middle" >
                                        <select id="idkayu3" data-placeholder="Jenis Kayu">
                                            <option value="">-Pilih Jenis Kayu-</option>
                                        </select>
                                    </th>
                                </tr>
                                <tr>
                                    <th  width="10%">&nbsp;</th>
                                    <th width="10%">Jumlah</th>
                                    <th width="10%">Satuan</th>
                                    <th width="10%">Volume</th>
                                    <th width="10%">Satuan</th>
                                    <th width="10%">Berat</th>		
                                    <th width="10%">Satuan</th> 
                                    <th width="5%" rowspan="2" style="vertical-align:middle">
                                        <button class="btn btn-danger btn-small" id="cmdClear" type="button">Clear</button>
                                    </th>
                                </tr>
                                <tr>
                                     
                                    <th>&nbsp;</th>
                                    <th><input style="width:100%" type="text" class="detail angka" id="jumlah"></th>                                        
                                    <th>
                                        <select style="width:100%" class="detail" id="sat_jumlah">
                                            <option value="">-</option>
                                            <?php foreach($satuan_jumlah as $row){?>
                                                <option value="<?= $row->satuan_id ?>"><?= $row->satuan_kode ?> </option>
                                            <?php }?>
                                        </select>
                                    </th>
                                    <th><input style="width:100%"  type="text" class="detail angka" id="volume"></th>                                        
                                    <th>
                                        <select style="width:100%" class="detail" id="sat_volume">
                                            <option value="">-</option>
                                            <?php foreach($satuan_volume as $row){?>
                                                <option value="<?= $row->satuan_id ?>"><?= $row->satuan_kode ?> </option>
                                            <?php }?>
                                        </select>
                                    </th>
                                    <th><input style="width:100%" type="text" class="detail angka" id="berat"></th>                                        
                                    <th style="border-right: 1px solid #DDD;">
                                        <select style="width:100%" class="detail" id="sat_berat">
                                            <option value="">-</option>
                                            <?php foreach($satuan_berat as $row){?>
                                                <option value="<?= $row->satuan_id ?>"><?= $row->satuan_kode ?> </option>
                                            <?php }?>
                                        </select>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="clear"></div>
			</div>
            <?php } ?>
            <div class="row-form" style="padding:0px;">
				<div class="span12">
                    <div class="block-fluid wrap-table" style="height:400px;overflow:auto;">
                        <table cellpadding="0" cellspacing="0" width="100%" class="table" id="detail_list">
                            <thead>
                                <tr>
                                    <th rowspan="2" width="5%">No.</th>
                                    <th rowspan="2" width="20%">Nama Produk Jadi</th>
                                    <th rowspan="2" width="10%">HS CODE</th>
                                    <th colspan="3">Jumlah</th>
                                    <th rowspan="2" width="5%">Jenis Kayu</th>
                                    <?php if($final == 0){ ?>
                                        <th rowspan="2" width="5%">Option</th>
                                    <?php } ?>
                                </tr>
                                <tr>                                    
                                    <th width="5%">m3</th>
                                    <th width="5%">kg</th>
                                    <th width="5%">unit</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php echo $detail_value_html ?>
                                <?php $no = 1;?>
                                <?php foreach($detail_list as $row) { ?>
                                    <tr>
                                        <td width="5%"><?= $no++ ?></td>
                                        <td style="display:none"><?= $row->idproduk ?></td>
                                        <td width="10%"><?= $row->namaproduk ?></td>
                                        <td width="10%"><?= $row->hscode ?></td>
                                        <td style="display:none"><?= $row->volume ?></td>
                                        <td width="5%" class="angka_det"><?= $row->volume ?></td>
                                        <td style="display:none"><?= $row->satuan_volume ?></td>
                                        <td style="display:none"><?= $row->berat ?></td>
                                        <td width="5%" class="angka_det"><?= $row->berat ?></td>
                                        <td style="display:none"><?= $row->satuan_berat ?></td>
                                        <td style="display:none"><?= $row->jumlah ?></td>
                                        <td width="5%" class="angka_det"><?= $row->jumlah ?></td>
                                        <td style="display:none"><?= $row->satuan_jumlah ?></td>
                                        <td style="display:none"><?= $row->idkayu ?></td>
                                        <td style="display:none"><?= $row->namakayu1 ?></td>
                                        <td style="display:none"><?= $row->idkayu2 ?></td>
                                        <td style="display:none"><?= $row->namakayu2 ?></td>
                                        <td style="display:none"><?= $row->idkayu3 ?></td>
                                        <td style="display:none"><?= $row->namakayu3 ?></td>
                                        <td width="10%"><?= $row->namakayu1."<br/>".$row->namakayu2."<br/>".$row->namakayu3 ?></td>
                                        <?php if($final == 0){ ?>
                                            <td width="5%">
                                                <a href="#" class="edit"><i class="icon-pencil"></i></a>
                                                <a href="#" class="hapus"><i class="icon-remove"></i></a> 
                                            </td>
                                        <?php } ?>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
				<div class="clear"></div>
			</div> 
            <?php if($final == 0){ ?>
            <div class="row-form">
				<div class="span12"><button class="btn" type="button" id="btnsimpan">Simpan</button></div>
				<div class="clear"></div>
			</div>
            <?php } ?>
			<input type="hidden" id="namabuyer" name="namabuyer" value="{namabuyer}"/>
			<input type="hidden" id="nama_provinsi" name="nama_provinsi" value="{nama_provinsi}"/>
			<input type="hidden" id="nama_kota" name="nama_kota" value="{nama_kota}"/>
            <input type="hidden" id="detail_value" name="detail_value"/>
            <input type="hidden" id="detail_value_html" name="detail_value_html"/>
			<?php echo form_hidden('idjual', $idjual); ?>
			<?php echo form_close() ?>
		</div>
	</div>
	
</div>
<script src="{js_path}jquery.floatThead.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
    <?php if($final == 1){ ?>
        $("input,select").prop( "disabled", true );
    <?php }?>
    
    $('.angka').number(true,3,',','.');
    $('.angka_det').number(true,3,',','.');
    var $table = $('table#detail_list');
    $table.floatThead({
        zIndex : 0,
        scrollContainer: function($table){
            return $table.closest('.wrap-table');
        }
    });
    
    
    $("#idkayu,#idkayu2,#idkayu3").select2({
         ajax: {
            url: '{client_url}penjualan/find_jenis_kayu',
            dataType: 'json',
            delay: 250,
            processResults: function (data, params) {
              return {
                    results: $.map(data, function (item) {
                        return {
                       id: item.idkayu,
                       text: item.nama
                        };
                    })
              };
            },
            cache: true
          },
          minimumInputLength: 3,
          theme: "bootstrap",
          allowClear: true
    }).on('select2:select', function(e) { $(this).focus(); });
    
    
    $("#idproduct").select2({
         ajax: {
            url: '{client_url}penjualan/find_product',
            dataType: 'json',
            delay: 250,
            processResults: function (data, params) {
              return {
                    results: $.map(data, function (item) {
                        return {
                       id: item.idproduk,
                       text: item.produk
                        };
                    })
              };
            },
            cache: true
          },
          minimumInputLength: 3,
          theme: "bootstrap",
          allowClear: true
    }).on('select2:select', function(e) { $(this).focus(); });
    
    $("#idproduct").change(function(){
        $.ajax({
          url: '{client_url}ekspor/find_hscode/'+$(this).val(),
          dataType: 'json',
          success: function(data) {
            $("#hscode").val(data);
          }
        });
    });
    
    
    $("#cmdAdd").click(function(){
        var valid = validate_detail();
        if(!valid) return false;
                
        //var duplicate = false;
        
        if($("#nomor").val() != ''){
            var no = $("#nomor").val();
            var content = "";
        }else{
            var no = $('#detail_list tr').length;
            var content = "<tr>";
            /*
            $('#detail_list tbody tr').filter(function (){
                var $cells = $(this).children('td');
                if($cells.eq(1).text() === $("#idkayu").val()){
                    alert("Jenis Kayu " + $("#idkayu option:selected").text() + " sudah ada didalam daftar di baris ke " + $cells.eq(0).text());
                    duplicate = true;
                }
            });	*/
        }
        
        //if(duplicate) return false;

        content += "<td >" + no + "</td>";  //0
        content += "<td style='display:none'>" + $("#idproduct").val(); + "</td>"; //1
        content += "<td >" + $("#idproduct option:selected").text(); + "</td>"; //2
        content += "<td>" + $("#hscode").val(); + "</td>"; //3
        content += "<td style='display:none'>" + $("#volume").val(); + "</td>"; //4
        content += "<td class='angka_det'>" + $("#volume").val(); + "</td>"; //5
        content += "<td style='display:none'>" + $("#sat_volume").val(); + "</td>"; //6
        content += "<td style='display:none'>" + $("#berat").val(); + "</td>"; //7
        content += "<td class='angka_det'>" + $("#berat").val(); + "</td>"; //8
        content += "<td style='display:none'>" + $("#sat_berat").val(); + "</td>"; //9
        content += "<td style='display:none'>" + $("#jumlah").val(); + "</td>"; //10
        content += "<td class='angka_det'>" + $("#jumlah").val(); + "</td>";//11 
        content += "<td style='display:none'>" + $("#sat_jumlah").val(); + "</td>"; //12
        content += "<td style='display:none'>" + $("#idkayu").val(); + "</td>"; //13
        content += "<td style='display:none'>" + (($("#idkayu").val() != '')? $("#idkayu option:selected").text() : '') + "</td>"; //14
        content += "<td style='display:none'>" + $("#idkayu2").val(); + "</td>"; //15
        content += "<td style='display:none'>" + (($("#idkayu2").val() != '')? $("#idkayu2 option:selected").text() : '') + "</td>"; //16
        content += "<td style='display:none'>" + $("#idkayu3").val(); + "</td>"; //17
        content += "<td style='display:none'>" + (($("#idkayu3").val() != '')? $("#idkayu3 option:selected").text() : '') + "</td>"; //18
        content += "<td >" + (($("#idkayu").val() != '')? $("#idkayu option:selected").text() : '') + (($("#idkayu2").val() != '')? "<br/>" + $("#idkayu2 option:selected").text() : '') + (($("#idkayu3").val() != '')? "<br/>" + $("#idkayu3 option:selected").text() : '') + "</td>"; //19
        content += "<td><a href='#' class='edit'><i class='icon-pencil'></i></a>&nbsp;&nbsp;<a href='#' class='hapus'><i class='icon-remove'></i></a> </td>";
        
        if($("#rowindex").val() != ''){
            $('#detail_list tbody tr:eq(' + $("#rowindex").val() + ')').html(content);
        }else{
            content += "</tr>";
            $('#detail_list tbody').append(content);
        }

        $("#cmdClear").trigger('click');
        $('.angka_det').number(true,3,',','.');
    });
    
    function validate_detail()
	{
		if($("#idproduct").val() == ""){
			alert("Produk Belum Dipilih");
			$("#idproduct").select2("open");
			return false;
		}
		
		if($("#jumlah").val() == "" && $("#volume").val() == "" && $("#berat").val() == ""){
			alert("Jumlah Penjualan Belum Diisi");
			$("#dok_jumlah").focus();
			return false;
		}
        
		
		return true;
	}
    
    
    $(document).on("click",".edit",function(){
			
        $("#rowindex").val($(this).closest('tr')[0].sectionRowIndex);
        $("#nomor").val($(this).closest('tr').find("td:eq(0)").html());
        $("#hscode").val($(this).closest('tr').find("td:eq(3)").html());
        $("#volume").val($(this).closest('tr').find("td:eq(4)").html());
        $("#sat_volume").val($(this).closest('tr').find("td:eq(6)").html());
        $("#berat").val($(this).closest('tr').find("td:eq(7)").html());
        $("#sat_berat").val($(this).closest('tr').find("td:eq(9)").html());
        $("#jumlah").val($(this).closest('tr').find("td:eq(10)").html());
        $("#sat_jumlah").val($(this).closest('tr').find("td:eq(12)").html());
        
        
        var idproduct = $(this).closest('tr').find("td:eq(1)").html();
        var nama_product = $(this).closest('tr').find("td:eq(2)").html();
        
        var idkayu = $(this).closest('tr').find("td:eq(13)").html();
        var nama_jenis = $(this).closest('tr').find("td:eq(14)").html();
        
        var idkayu2 = $(this).closest('tr').find("td:eq(15)").html();
        var nama_jenis2 = $(this).closest('tr').find("td:eq(16)").html();
        
        var idkayu3 = $(this).closest('tr').find("td:eq(17)").html();
        var nama_jenis3 = $(this).closest('tr').find("td:eq(18)").html();
        
        
        $('#idproduct').find('option').remove().end()
        .append('<option value="">- Pilih Produk -</option><option value="'+ idproduct +'">' + nama_product + '</option>')
        .val(idproduct);
        
        $('#idkayu').find('option').remove().end()
        .append('<option value="">- Pilih Jenis Kayu -</option><option value="'+ idkayu +'">' + nama_jenis + '</option>')
        .val(idkayu);
        
        $('#idkayu2').find('option').remove().end()
        .append('<option value="">- Pilih Jenis Kayu -</option><option value="'+ idkayu2 +'">' + nama_jenis2 + '</option>')
        .val(idkayu2);
        
        $('#idkayu3').find('option').remove().end()
        .append('<option value="">- Pilih Jenis Kayu -</option><option value="'+ idkayu3 +'">' + nama_jenis3 + '</option>')
        .val(idkayu3);
    
        $("#jumlah").focus();
        return false;
    });
    $(document).on("click",".hapus",function(){
        if(confirm("Hapus Data ?") == true){
            $(this).closest('td').parent().remove();
        }
        return false;
    });
    $("#cmdClear").click(function(){
        $(".detail").val('');
        $('#idkayu').find('option').remove().end().append('<option value="">- Pilih Jenis Kayu -</option>').val('');
        $('#idkayu2').find('option').remove().end().append('<option value="">- Pilih Jenis Kayu -</option>').val('');
        $('#idkayu3').find('option').remove().end().append('<option value="">- Pilih Jenis Kayu -</option>').val('');
        $('#idnegaraasal').find('option').remove().end().append('<option value="">- Pilih Negara Asal -</option>').val('');
        $('#idnegaraasal2').find('option').remove().end().append('<option value="">- Pilih Negara Asal -</option>').val('');
        $('#idnegaraasal3').find('option').remove().end().append('<option value="">- Pilih Negara Asal -</option>').val('');
        $('#idproduct').find('option').remove().end().append('<option value="">- Pilih Produk -</option>').val('');
        $("#idproduct").select2("open");
    });
    
    function renumbering(){
        $('#detail_list tbody tr').each(function(index, element) {
           $(this).find('td:eq(0)').text(element.sectionRowIndex + 1);		
        });
    }
    
    $(document).on("click","#btnsimpan",function(){
        var tbl = $('table#detail_list tbody tr').get().map(function(row) {
          return $(row).find('td').get().map(function(cell) {
            return $(cell).html();
          });
        });
        $("#detail_value").val(JSON.stringify(tbl));
        $("#frmTransaksi").submit();
    });
    
    
    $(document).on("click","#btnsimpan",function(){
        var rowCount = $('table#detail_list tr').length;
        if(rowCount <= 1){
           big_notification("Data Detail Penjualan Masih Kosong!",'error');
           return false;
        }else{
            var tbl = $('table#detail_list tbody tr').get().map(function(row) {
              return $(row).find('td').get().map(function(cell) {
                return $(cell).html();
              });
            });
            $("#detail_value").val(JSON.stringify(tbl));
            $("#detail_value_html").val($('table#detail_list tbody').html());
            $("#frmTransaksi").submit();
        }
    });
    
    
    
    $("input").bind("keypress", function(e) {
        if (e.keyCode == 13){
            return false;
        }
	});
    
    $("#idprovinsi").select2({
         ajax: {
            url: '{client_url}penerimaan/find_provinsi',
            dataType: 'json',
            delay: 250,
            processResults: function (data, params) {
              return {
                    results: $.map(data, function (item) {
                        return {
                       id: item.id,
                       text: item.nama_provinsi
                        };
                    })
              };
            },
            cache: true
          },
          minimumInputLength: 3,
          theme: "bootstrap",
          allowClear: true
    }).on('select2:select', function(e) { $(this).focus(); });
    
    
    
    $("#idprovinsi").change(function(){
        $('#idkota').find('option').remove().end().append('<option value="loading">Loading...</option>').val('loading').trigger("liszt:updated");
        $("#nama_provinsi").val($("#idprovinsi option:selected").text());
        $.ajax({
          url: '{client_url}penerimaan/find_kota/'+$(this).val(),
          dataType: 'json',
          success: function(data) {
            $('#idkota').find('option').remove().end().append('<option value="0">Pilih Kota</option>').val('0');
            $.each(data.kota, function (i,kot) {
                $('#idkota').append('<option value="'+kot.id+'">'+kot.nama_kota+'</option>');
            });
             
          }
        });
    });
    
    $("#idkota").select2({allowClear: true,theme: "bootstrap"});
    $("#idkota").change(function(){
        $("#nama_kota").val($("#idkota option:selected").text());
    });
    
    
    $("#idbuyer").select2({
         ajax: {
            url: '{client_url}penjualan/find_buyer',
            dataType: 'json',
            delay: 250,
            processResults: function (data, params) {
              return {
                    results: $.map(data, function (item) {
                        return {
                       id: item.idbuyer,
                       text: item.buyer
                        };
                    })
              };
            },
            cache: true
          },
          minimumInputLength: 3,
          theme: "bootstrap",
          allowClear: true
    }).on('select2:select', function(e) { $(this).focus(); });
    
    
    $("#idbuyer").change(function(){
        $("#namabuyer").val($("#idbuyer option:selected").text());
        $.ajax({
          url: '{client_url}penjualan/buyer_detail/'+$(this).val(),
          dataType: 'json',
          success: function(data) {
            console.log(data);
            $("#namabuyer").val(data.buyer);
            $("#alamat").val(data.alamat);
          }
        });
    });    
    });
</script>