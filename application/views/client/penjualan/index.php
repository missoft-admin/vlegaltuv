<div class="page-header">
	<h1>{first_title} <small>{second_title}</small></h1>
</div>  
<?php echo error_success($this->session)?>
<div class="row-fluid">
	<div class="span12">                    
		<div class="head">
			<div class="isw-grid"></div><h1> {table_title}</h1>
			<ul class="buttons">
				<li>
					<a href="#" class="isw-settings"></a>
					<ul class="dd-list">
						<li><a href="{client_url}penjualan/add"><span class="isw-plus"></span> Add Penjualan</a></li>
					</ul>
				</li>
			</ul>                        
			<div class="clear"></div>
		</div>
		<div class="block-fluid table-sorting">
			<div class="dataTables_wrapper">
            <div class="dataTables_filter" id="tSortable_filter" style="width:auto;">
				<form action="{client_url}penjualan/search" method="post" style="margin-bottom:0;">
				<label>Search: <input type="text" style="width:300px;" value="{searchText}" name="searchText" placeholder="Search By No. Invoice"></label>
				</form>
			</div>
			<table cellpadding="0" cellspacing="0" width="100%" class="table">
				<thead>
					<tr>                                    
						<th width="5%">No.</th>
						<th width="10%">Tanggal</th>
						<th width="10%">No. Invoice</th>
						<th width="10%">No. Nota</th>
						<th width="20%">Nama Pembeli</th>
						<th width="10%">Jumlah</th>
						<th width="10%">Volume</th>
						<th width="10%">Berat</th>
                        <th width="10%">Nilai</th>
                        <th width="10%">Option</th>                                                 
					</tr>
				</thead>
				<tbody>
					<?php foreach($query as $row){ ?>
					<?php $no = $no + 1 ?>
					<tr>                                    
						<td><?= $no?></td>
						<td><?php echo human_date_short($row->periode_awal)?></td>
						<td><?php echo $row->invoice ?></td>							
						<td><?php echo $row->nonota ?></td>							
						<td><?php echo $row->buyer ?></td>							
						<td><?php echo $row->jumlah_produk ?> Unit</td>
						<td><?php echo $row->volume_produk ?> m3</td>
						<td><?php echo $row->berat_produk ?> Kg</td>
						<td><?php echo format_angka_id($row->nilai,0) ?></td>
						<td>
                            <?php if($row->final == 0){ ?>
                               <div class="btn-group">                                        
                                <button data-toggle="dropdown" class="btn dropdown-toggle">Action <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="{site_url}client/penjualan/edit/<?= $row->idjual ?>">Edit</a></li>
                                    <li><a href="{site_url}client/penjualan/delete/<?= $row->idjual ?>" onClick="return confirm('Hapus Transaksi?')">Delete</a></li>
                                    <li><a href="{site_url}client/penjualan/finalize/<?= $row->idjual ?>" onClick="return confirm('Finalize Transaksi?')">Finalize</a></li>
                                </ul>
                            </div>								
                            <?php }else{ 
                                echo tool_finalized('client/penjualan/edit/'.$row->idjual,"Finalize By $row->finalize_user_name at ".human_date_time($row->finalize_date));										
                                  } ?>
                            
						</td>                                    
					</tr>                                
					<?php } ?>
				</tbody>
			</table>
			<?php echo $pagination ?>
			
		</div>
		<div class="clear"></div>
	</div>                                
</div>