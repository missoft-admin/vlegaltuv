<div class="page-header">
	<h1>{first_title} <small>{second_title}</small></h1>
</div>  
<?php echo error_success($this->session)?>
<div class="row-fluid">
	<div class="span12">                    
		<div class="head">
			<div class="isw-grid"></div><h1> {table_title}</h1>
			<ul class="buttons">
				<li>
					<a href="#" class="isw-settings"></a>
					<ul class="dd-list">
						<li><a href="{client_url}penerimaan/add"><span class="isw-plus"></span> Add Penerimaan</a></li>
					</ul>
				</li>
			</ul>                        
			<div class="clear"></div>
		</div>
		<div class="block-fluid table-sorting">
			<div class="dataTables_wrapper">
            <div class="dataTables_filter" id="tSortable_filter" style="width:auto;">
				<form action="{client_url}penerimaan/search" method="post" style="margin-bottom:0;">
				<label>Search: <input type="text" style="width:300px;" value="{searchText}" name="searchText" placeholder="Search Penerimaan By Nama Supplier"></label>
				</form>
			</div>
			<table cellpadding="0" cellspacing="0" width="100%" class="table">
				<thead>
					<tr>                                    
						<th width="5%">No.</th>
						<th width="10%">Tanggal</th>
						<th width="10%">Supplier</th>
						
						<th width="10%">Asal Kayu</th>
						<th width="10%">Sertifikasi</th>
						<th width="10%">Lokal/Import</th>
                        <th width="10%">Sortimen</th>
                        <th width="20%">Jenis Kayu</th>
                        <th width="15%">Option</th>                                                 
					</tr>
				</thead>
				<tbody>
					<?php foreach($query as $row){ ?>
					<?php $no = $no + 1 ?>
					<tr>                                    
						<td><?= $no?></td>
						<td><?php echo human_date_short($row->periode_awal) ?></td>
						<td><?php echo $row->nama_supplier ?></td>							
						<td><?php echo $row->asal_kayu ?></td>
						<td><?php echo $row->nama_sertifikasi ?></td>
						<td><?php echo strtoupper($row->type_penerimaan) ?></td>
                        <td><?php echo $row->nama_sortimen ?></td>
						<td><?php echo $row->nama_kayu ?></td>
						<td>
							<?php 
                                if($row->final == 0){
                                    echo tool_edit('client/penerimaan/edit/'.$row->idpenerimaan);
                                    echo tool_remove('client/penerimaan/delete/'.$row->idpenerimaan);								
                                    echo tool_finalize('client/penerimaan/finalize/'.$row->idpenerimaan);								
                                }else{
                                    echo tool_finalized('client/penerimaan/edit/'.$row->idpenerimaan,"Finalize By $row->finalize_user_name at ".human_date_time($row->finalize_date));										
                                }
							?>
						</td>                                    
					</tr>                                
					<?php } ?>
				</tbody>
			</table>
			<?php echo $pagination ?>
			
		</div>
		<div class="clear"></div>
	</div>                                
</div>