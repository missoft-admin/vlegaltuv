<div class="page-header">
	<h1>{first_title} <small>{second_title}</small></h1>
</div>  
<?php echo error_success($this->session)?>
<?php if($error != '') echo error_message($error)?>
<div class="row-fluid">
	<div class="span12">
		<div class="head">
			<div class="isw-documents"></div>
			<h1>Form Penerimaan Bahan Baku</h1>
			<ul class="buttons">
				<li><a href="{client_url}penerimaan" class="isw-list" title="List Penerimaan"></a></li>                                                        
			</ul>
			<div class="clear"></div>
		</div>
		<div class="block-fluid">                        
			<?php echo form_open('client/penerimaan/save','id = "frmTransaksi"') ?>
			<div class="row-form">
				<div class="span2">Tanggal:</div>
				<div class="span4"><input class="<?= ($final == 0)? 'datepickertxt' : '' ?> validate[required]" type="text" name="periode_awal" required placeholder="Tanggal" value="{periode_awal}"/></div>
                <!--<div class="span2">Periode Akhir:</div>
				<div class="span4"><input class="datepickertxt" type="text" name="periode_akhir" placeholder="Periode Akhir" value="{periode_akhir}"/></div>
				--><div class="clear"></div>
			</div> 
            <div class="row-form">
				<div class="span2">Jenis Penerimaan:</div>
				<div class="span4">
                    <label class="checkbox inline">
                        <input type="radio" id="lokal" class="validate[required]" value="LOKAL" name="type_penerimaan" <?= (strtoupper($type_penerimaan) == "LOKAL")? 'checked="checked"' : "" ?>/> LOKAL
                    </label>
                    <label class="checkbox inline">
                        <input type="radio" id="import" class="validate[required]" value="IMPORT" name="type_penerimaan" <?= (strtoupper($type_penerimaan) == "IMPORT")? 'checked="checked"' : "" ?>/> IMPORT
                    </label>
                </div>
				<div class="clear"></div>
			</div> 
			<div class="row-form">
				<div class="span2">No. Invoice:</div>
				<div class="span4">
						<input type="text" class="validate[required]" name="noinvoice" id="noinvoice" value="{noinvoice}"/>
				</div>	
                <div class="span2">Supplier:</div>
				<div class="span4">
					<select class="validate[required]" name="idsupplier" id="idsupplier" data-placeholder="Supplier">
						<option value="{idsupplier}">{nama_supplier}</option>
						
						
					</select>
				</div>	
				<div class="clear"></div>
			</div> 
			<div class="row-form lokal">
				<div class="span2">Status Hutan:</div>
				<div class="span2">
                    <select class="validate[required]" name="idhutan"/>
                        <option value="">Pilih Status Hutan</option>
                        <?php foreach($hutan_list as $row){ ?>
                            <option <?= ($row->idhutan == $idhutan)? 'selected' : '' ?> value="<?= $row->idhutan ?>"><?= $row->hutan ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="span2">Jenis Dokumen:</div>
				<div class="span2">
                    <select  name="iddokumen"/>
                        <option value="">Pilih Jenis Dokumen</option>
                        <?php foreach($jenisdokumen_list as $row){ ?>
                            <option <?= ($row->iddok == $iddokumen)? 'selected' : '' ?> value="<?= $row->iddok ?>"><?= $row->dok ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="span2">Dokumen Berlaku Sampai:</div>
                <div class="span2">
					<input type="text" class="<?= ($final == 0)? 'datepickertxt datepickertxt80' : '' ?> validate[required]" placeholder="Berlaku Sampai"  id="tgl_berlaku_dokumen" value="{tgl_berlaku_dokumen}" name="tgl_berlaku_dokumen"/>
				</div>	
				<div class="clear"></div>
			</div> 
            <div class="row-form lokal">
				<div class="span2">Status Sertifikasi:</div>
				<div class="span2">
                    <select name="idsertifikasi"/>
                        <option value="">Pilih Status Sertifikasi</option>
                        <?php foreach($sertifikasi_list as $row){ ?>
                            <option <?= ($row->id == $idsertifikasi)? 'selected' : '' ?> value="<?= $row->id ?>"><?= $row->deskripsi ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="span2">No. Sertifikat:</div>
                <div class="span2">
					<input type="text" id="no_sertifikat" value="{no_sertifikat}" placeholder="No. Sertifikat" name="no_sertifikat"/>
				</div>	
                <div class="span2">Sertifikat Berlaku Sampai:</div>
               <div class="span2">
					<input type="text" class="<?= ($final == 0)? 'datepickertxt datepickertxt80' : '' ?> validate[required]" placeholder="Berlaku Sampai"  id="tgl_berlaku_sertifikat" value="{tgl_berlaku_sertifikat}" name="tgl_berlaku_sertifikat"/>
				</div>
                
				<div class="clear"></div>
			</div>	
            <div class="row-form lokal">
				<div class="span2">Provinsi:</div>
				<div class="span4">
					<select name="idprovinsi" id="idprovinsi" data-placeholder="Provinsi">
						<option value="{idprovinsi}">{nama_provinsi}</option>
						
						
					</select>
				</div>	
                <div class="span2">Kota:</div>
				<div class="span4">
					<select class="validate[required]" name="idkota" id="idkota" data-placeholder="Kota">
						<option value="{idnegara}">{nama_kota}</option>
					</select>
				</div>				
				<div class="clear"></div>
			</div>
            <div class="row-form import">
				<div class="span2">No. Deklarasi Import:</div>
                <div class="span2">
                    <select class="validate[required]" name="id_deklarasi_import" id="id_deklarasi_import" data-placeholder="No. Deklarasi Import">
						<option value="{id_deklarasi_import}">{no_deklarasi_import}</option>
					</select>
				</div>	
                <div class="span2">Berlaku Sampai:</div>
                <div class="span2">
					<input type="text" class="validate[required]" readonly id="tgl_berlaku_deklarasi" value="{tgl_berlaku_deklarasi}" name="tgl_berlaku_deklarasi"/>
				</div>	
                <div class="span2">Kuota Import:</div>
                <div class="span2">
					<input type="text" class="validate[required]" readonly id="kuota_import" value="{kuota_import}" name="kuota_import" class="angka_std"/>
				</div>	
				<div class="clear"></div>
			</div>	
            <div class="row-form import">
				<div class="span2">Asal Kayu:</div>
				<div class="span4">
					<select class="validate[required]" name="idnegara" id="idnegara" data-placeholder="Asal Kayu">
						<option value="{idnegara}">{nama_negara}</option>
					</select>
				</div>	
				<div class="clear"></div>
			</div>	
            <div class="row-form">
                <div class="span12" style="text-align:center"><h4>Data Detail Penerimaan</h4></div>
                <div class="clear"></div>
            </div>
            <?php if($final == 0){ ?>
			<div class="row-form" style="padding:0px;">
                <div class="span12">
                    <div class="block-fluid"  style="position:relative;z-index:1;padding-right:17px;margin-bottom:0px;background-color:#F1F1F1">
                        <table cellpadding="0" cellspacing="0" width="100%" class="table">
                            <thead>
                                <tr>
                                    <th  width="25%">Sortimen &amp; Jenis Kayu</th>
                                    <th width="10%">Data</th>
                                    <th width="10%">Jumlah</th>
                                    <th width="10%">Satuan</th>
                                    <th width="10%">Volume</th>
                                    <th width="10%">Satuan</th>
                                    <th width="10%">Berat</th>		
                                    <th width="10%">Satuan</th> 
                                    <th width="5%" rowspan="2" style="vertical-align:middle"><button class="btn" id="cmdAdd" type="button">Add</button></th>                                                 
                                </tr>
                                <tr>
                                     <th style="vertical-align:middle">
                                        <input type="hidden" class="detail" id="rowindex"/>
                                        <input type="hidden"  class="detail" id="nomor" />
                                        <select name="idsortimen" id="idsortimen" data-placeholder="Sortimen">
                                            <option value="">Pilih Sortimen</option>
                                        </select>
                                    </th>
                                    
                                    <th>Dokumen</th>
                                    <th><input style="width:100%" type="text" class="detail angka" id="dok_jumlah"></th>                                        
                                    <th>
                                        <select style="width:100%" class="detail" id="dok_sat_jumlah">
                                            <option value="">-</option>
                                            <?php foreach($satuan_jumlah as $row){?>
                                                <option value="<?= $row->satuan_id ?>"><?= $row->satuan_kode ?> </option>
                                            <?php }?>
                                        </select>
                                    </th>
                                    <th><input style="width:100%"  type="text" class="detail angka" id="dok_volume"></th>                                        
                                    <th>
                                        <select style="width:100%" class="detail" id="dok_sat_volume">
                                            <option value="">-</option>
                                            <?php foreach($satuan_volume as $row){?>
                                                <option value="<?= $row->satuan_id ?>"><?= $row->satuan_kode ?> </option>
                                            <?php }?>
                                        </select>
                                    </th>
                                    <th><input style="width:100%" type="text" class="detail angka" id="dok_berat"></th>                                        
                                    <th style="border-right: 1px solid #DDD;">
                                        <select style="width:100%" class="detail" id="dok_sat_berat">
                                            <option value="">-</option>
                                            <?php foreach($satuan_berat as $row){?>
                                                <option value="<?= $row->satuan_id ?>"><?= $row->satuan_kode ?> </option>
                                            <?php }?>
                                        </select>
                                    </th>
                                    
                                </tr>
                                <tr>
                                   <th style="vertical-align:middle">
                                        <select id="idkayu" data-placeholder="Jenis Kayu">
                                            <option value="">Pilih Jenis Kayu</option>
                                        </select>
                                    </th>
                                    <th>Fisik</th>
                                    <th><input style="width:100%" type="text" class="detail angka"  id="fisik_jumlah"></th>   
                                    <th>
                                        <select style="width:100%" class="detail" id="fisik_sat_jumlah">
                                            <option value="">-</option>
                                            <?php foreach($satuan_jumlah as $row){?>
                                                <option value="<?= $row->satuan_id ?>"><?= $row->satuan_kode ?> </option>
                                            <?php }?>
                                        </select>
                                    </th>
                                    <th><input style="width:100%" type="text" class="detail angka" id="fisik_volume"></th>                                        
                                    <th>
                                        <select style="width:100%" class="detail" id="fisik_sat_volume">
                                            <option value="">-</option>
                                            <?php foreach($satuan_volume as $row){?>
                                                <option value="<?= $row->satuan_id ?>"><?= $row->satuan_kode ?> </option>
                                            <?php }?>
                                        </select>
                                    </th>
                                    <th><input style="width:100%" type="text" class="detail angka" id="fisik_berat"></th>                                        
                                    <th style="border-right: 1px solid #DDD;">
                                        <select style="width:100%" class="detail" id="fisik_sat_berat">
                                            <option value="">-</option>
                                            <?php foreach($satuan_berat as $row){?>
                                                <option value="<?= $row->satuan_id ?>"><?= $row->satuan_kode ?> </option>
                                            <?php }?>
                                        </select>
                                    </th>
                                    <th width="5%" style="vertical-align:middle"><button class="btn btn-danger btn-small" id="cmdClear" type="button">Clear</button></th>                                                 
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="clear"></div>
			</div>
            <?php } ?>
			<div class="row-form" style="padding:0px;">
				<div class="span12">
                    <div class="block-fluid wrap-table" style="height:400px;overflow:auto;">
                        <table cellpadding="0" cellspacing="0" width="100%" class="table" id="detail_list">
                            <thead>
                                <tr>
                                    <th rowspan="2" width="5%">No.</th>
                                    <th rowspan="2" width="10%">Sortimen</th>
                                    <th rowspan="2" width="10%">Jenis Kayu</th>
                                    <th colspan="6">Data Dokumen Penerimaan</th>
                                    <th colspan="6">Data Fisik Penerimaan</th>
                                    <?php if($final == 0){ ?>
                                    <th width="5%">&nbsp;</th>
                                    <?php } ?>
                                </tr>
                                <tr>                                    
                                    <th width="5%">Jumlah</th>
                                    <th width="5%">Satuan</th>
                                    <th width="5%">Volume</th>
                                    <th width="5%">Satuan</th>
                                    <th width="5%">Berat</th>		
                                    <th width="5%">Satuan</th>  
                                    <th width="5%">Jumlah</th>
                                    <th width="5%">Satuan</th>
                                    <th width="5%">Volume</th>
                                    <th width="5%">Satuan</th>
                                    <th width="5%">Berat</th>		
                                    <th width="5%">Satuan</th>                                                 
                                    <?php if($final == 0){ ?>
                                    <th>Option</th>
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php echo $detail_value_html ?>
                                <?php foreach($detail_list as $row) { ?>
                                    <tr>
                              <!-- 0 --><td width="5%"><?= $row->urutan ?></td>
                              <!-- 1 --><td style="display:none"><?= $row->idsortimen ?></td>
                              <!-- 2 --><td width="10%"><?= $row->sortimen ?></td>
                              <!-- 3 --><td style="display:none"><?= $row->idkayu ?></td>
                              <!-- 4 --><td width="10%"><?= $row->nama_kayu ?></td>
                              <!-- 5 --><td style="display:none"><?= $row->datadokumen_jumlah ?></td>
                              <!-- 6 --><td width="5%" class="angka_det"><?= $row->datadokumen_jumlah ?></td>
                              <!-- 7 --><td style="display:none"><?= $row->datadokumen_satuan_jumlah ?></td>
                              <!-- 8 --><td width="5%"><?= $row->datadok_satuan_jml ?></td>
                              <!-- 9 --><td style="display:none"><?= $row->datadokumen_volume ?></td>
                              <!-- 10--><td width="5%" class="angka_det"><?= $row->datadokumen_volume ?></td>
                              <!-- 11--><td style="display:none"><?= $row->datadokumen_satuan_volume ?></td>
                              <!-- 12--><td width="5%"><?= $row->datadok_satuan_vol ?></td>
                              <!-- 13--><td style="display:none"><?= $row->datadokumen_berat ?></td>
                              <!-- 14--><td width="5%" class="angka_det"><?= $row->datadokumen_berat ?></td>
                              <!-- 15--><td style="display:none"><?= $row->datadokumen_satuan_berat ?></td>
                              <!-- 16--><td width="5%"><?= $row->datadok_satuan_brt ?></td>
                              <!-- 17--><td style="display:none"><?= $row->datafisik_jumlah ?></td>
                              <!-- 18--><td width="5%" class="angka_det"><?= $row->datafisik_jumlah ?></td>
                              <!-- 19--><td style="display:none"><?= $row->datafisik_satuan_jumlah ?></td>
                              <!-- 20--><td width="5%"><?= $row->datafis_satuan_jml ?></td>
                              <!-- 21--><td style="display:none"><?= $row->datafisik_volume ?></td>
                              <!-- 22--><td width="5%" class="angka_det"><?= $row->datafisik_volume ?></td>
                              <!-- 23--><td style="display:none"><?= $row->datafisik_satuan_volume ?></td>
                              <!-- 24--><td width="5%"><?= $row->datafis_satuan_vol ?></td>
                              <!-- 25--><td style="display:none"><?= $row->datafisik_berat ?></td>
                              <!-- 26--><td width="5%"  class="angka_det"><?= $row->datafisik_berat ?></td>
                              <!-- 27--><td style="display:none"><?= $row->datafisik_satuan_berat ?></td>
                              <!-- 28--><td width="5%"><?= $row->datafis_satuan_brt ?></td>
                                        <?php if($final == 0){ ?>
                                        <td width="5%">
                                            <a href="#" class="edit"><i class="icon-pencil"></i></a>&nbsp;&nbsp;<a href="#" class="hapus"><i class="icon-remove"></i></a> 
                                        </td>
                                        <?php } ?>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
				<div class="clear"></div>
			</div> 
            <?php if($final == 0){ ?>
            <div class="row-form">
				<div class="span12"><button class="btn" type="button" id="btnsimpan">Simpan</button></div>
				<div class="clear"></div>
			</div> 
            <?php } ?>
			<input type="hidden" id="detail_value" name="detail_value"/>
			<input type="hidden" id="detail_value_html" name="detail_value_html"/>
			<input type="hidden" id="nama_supplier" name="nama_supplier" value="{nama_supplier}"/>
			<input type="hidden" id="nama_provinsi" name="nama_provinsi" value="{nama_provinsi}"/>
			<input type="hidden" id="nama_kota" name="nama_kota" value="{nama_kota}"/>
			<input type="hidden" id="no_deklarasi_import" name="no_deklarasi_import" value="{no_deklarasi_import}"/>
			<input type="hidden" id="nama_negara" name="nama_negara" value="{nama_negara}"/>
			<input type="hidden" id="final" name="final" value="{final}"/>
			<?php echo form_hidden('idpenerimaan', $idpenerimaan); ?>
			<?php echo form_close() ?>
		</div>
	</div>
	
</div>
<script src="{js_path}jquery.floatThead.js" type="text/javascript"></script>
<script type="text/javascript">

$(document).ready(function(){
     <?php if($final == 1){ ?>
        $("input,select").prop( "disabled", true );
    <?php }?>
    var $table = $('table#detail_list');
    // $table.floatThead({
        // position: 'absolute',
        // scrollContainer: true
    // });
    
    $table.floatThead({
        zIndex : 0,
        scrollContainer: function($table){
            return $table.closest('.wrap-table');
        }
    });
    
    //$table.floatThead();

    $('.angka').number(true,3,',','.');
    $('.angka_det').number(true,3,',','.');
    $('.angka_std').number(true,0,',','.');
    
    
    $("input").bind("keypress", function(e) {
        if (e.keyCode == 13){
            return false;
        }
	});
    
    $("#idhutan").select2();
    
    
    
    $("#lokal").click(function(){
        $(".lokal").show();
        $(".import").hide();
    });
    
    $("#import").click(function(){
        $(".lokal").hide();
        $(".import").show();
    });
    
    $("#idkayu").select2({
         ajax: {
            url: '{client_url}penerimaan/find_jenis_kayu',
            dataType: 'json',
            delay: 250,
            processResults: function (data, params) {
              return {
                    results: $.map(data, function (item) {
                        return {
                       id: item.idkayu,
                       text: item.nama
                        };
                    })
              };
            },
            cache: true
          },
          minimumInputLength: 3,
          theme: "bootstrap",
          allowClear: true
    }).on('select2:select', function(e) { $(this).focus(); });
    
    $("#idsortimen").select2({
         ajax: {
            url: '{client_url}penerimaan/find_sortimen',
            dataType: 'json',
            delay: 250,
            processResults: function (data, params) {
              return {
                    results: $.map(data, function (item) {
                        return {
                       id: item.idsortimen,
                       text: item.sortimen
                        };
                    })
              };
            },
            cache: true
          },
          minimumInputLength: 3,
          theme: "bootstrap",
          allowClear: true
    }).on('select2:select', function(e) { $(this).focus(); });
    
    $("#idsupplier").select2({
         ajax: {
            url: '{client_url}penerimaan/find_supplier',
            dataType: 'json',
            delay: 250,
            processResults: function (data, params) {
              return {
                    results: $.map(data, function (item) {
                        return {
                       id: item.idsupplier,
                       text: item.supplier
                        };
                    })
              };
            },
            cache: true
          },
          minimumInputLength: 3,
          theme: "bootstrap",
          allowClear: true
    }).on('select2:select', function(e) { $(this).focus(); });
    
    $("#idsupplier").change(function(){
        $("#nama_supplier").val($("#idsupplier option:selected").text());
    });
    
    
     $("#idnegara").select2({
         ajax: {
            url: '{client_url}penerimaan/find_negara',
            dataType: 'json',
            delay: 250,
            processResults: function (data, params) {
              return {
                    results: $.map(data, function (item) {
                        return {
                       id: item.idnegara,
                       text: item.negara + " (" + item.idnegara + ")"
                        };
                    })
              };
            },
            cache: true
          },
          minimumInputLength: 3,
          theme: "bootstrap",
          allowClear: true
    }).on('select2:select', function(e) { $(this).focus(); });
    
    $("#idnegara").change(function(){
        $("#nama_negara").val($("#idnegara option:selected").text());
    });
    
    $("#idprovinsi").select2({
         ajax: {
            url: '{client_url}penerimaan/find_provinsi',
            dataType: 'json',
            delay: 250,
            processResults: function (data, params) {
              return {
                    results: $.map(data, function (item) {
                        return {
                       id: item.id,
                       text: item.nama_provinsi
                        };
                    })
              };
            },
            cache: true
          },
          minimumInputLength: 3,
          theme: "bootstrap",
          allowClear: true
    }).on('select2:select', function(e) { $(this).focus(); });
    
    
    
    $("#idprovinsi").change(function(){
        $('#idkota').find('option').remove().end().append('<option value="loading">Loading...</option>').val('loading').trigger("liszt:updated");
        $("#nama_provinsi").val($("#idprovinsi option:selected").text());
        $.ajax({
          url: '{client_url}penerimaan/find_kota/'+$(this).val(),
          dataType: 'json',
          success: function(data) {
            $('#idkota').find('option').remove().end().append('<option value="0">Pilih Kota</option>').val('0');
            $.each(data.kota, function (i,kot) {
                $('#idkota').append('<option value="'+kot.id+'">'+kot.nama_kota+'</option>');
            });
             
          }
        });
    });
    
    $("#idkota").select2({allowClear: true,theme: "bootstrap"});
    $("#idkota").change(function(){
        $("#nama_kota").val($("#idkota option:selected").text());
    });
    
    $("#id_deklarasi_import").select2({
         ajax: {
            url: '{client_url}penerimaan/find_deklarasi_import',
            dataType: 'json',
            delay: 250,
            processResults: function (data, params) {
              return {
                    results: $.map(data, function (item) {
                        return {
                       id: item.id,
                       text: item.no_deklarasi_import
                        };
                    })
              };
            },
            cache: true
          },
          minimumInputLength: 3,
          theme: "bootstrap",
          allowClear: true
    }).on('select2:select', function(e) { $(this).focus(); });
        
    $("#id_deklarasi_import").change(function(){
        $("#tgl_berlaku_deklarasi").val("Loading Data...");
        $("#kuota_import").val("Loading Data...");
        $("#no_deklarasi_import").val($("#id_deklarasi_import option:selected").text());
        $.ajax({
          url: '{client_url}penerimaan/find_deklarasi_import_detail/'+$(this).val(),
          dataType: 'json',
          success: function(data) {
            if(data != null){
            $("#tgl_berlaku_deklarasi").val(data.berlaku_sampai);
            $("#kuota_import").val(data.kuota);
            }else{
                $("#tgl_berlaku_deklarasi").val('');
                $("#kuota_import").val('');
            }
             
          }
        });
    });
    
    
    $("#cmdAdd").click(function(){
        var valid = validate_detail();
        if(!valid) return false;
                
        //var duplicate = false;
        
        if($("#nomor").val() != ''){
            var no = $("#nomor").val();
            var content = "";
        }else{
            var no = $('#detail_list tr').length;
            var content = "<tr>";
            /*
            $('#detail_list tbody tr').filter(function (){
                var $cells = $(this).children('td');
                if($cells.eq(1).text() === $("#idkayu").val()){
                    alert("Jenis Kayu " + $("#idkayu option:selected").text() + " sudah ada didalam daftar di baris ke " + $cells.eq(0).text());
                    duplicate = true;
                }
            });	*/
        }
        
        //if(duplicate) return false;
        content += "<td width='5%'>" + no + "</td>"; /* 0 */
        content += "<td style='display:none'>" + $("#idsortimen").val(); + "</td>"; /* 1 */
        content += "<td width='10%'>" + $("#idsortimen option:selected").text(); + "</td>"; /* 2 */
        content += "<td style='display:none'>" + $("#idkayu").val(); + "</td>"; /* 3 */
        content += "<td width='10%'>" + $("#idkayu option:selected").text(); + "</td>"; /* 4 */
        content += "<td style='display:none'>" + $("#dok_jumlah").val(); + "</td>"; /* 5 */
        content += "<td width='5%' class='angka_det'>" + $("#dok_jumlah").val(); + "</td>"; /* 6 */
        content += "<td style='display:none'>" + $("#dok_sat_jumlah").val(); + "</td>"; /* 7 */
        content += "<td width='5%'>" + $("#dok_sat_jumlah option:selected").text(); + "</td>"; /* 8 */
        content += "<td style='display:none'>" + $("#dok_volume").val(); + "</td>"; /* 9 */
        content += "<td width='5%' class='angka_det'>" + $("#dok_volume").val(); + "</td>"; /* 10 */
        content += "<td style='display:none'>" + $("#dok_sat_volume").val(); + "</td>"; /* 11 */
        content += "<td width='5%'>" + $("#dok_sat_volume option:selected").text(); + "</td>"; /* 12 */
        content += "<td style='display:none'>" + $("#dok_berat").val(); + "</td>"; /* 13 */
        content += "<td width='5%' class='angka_det'>" + $("#dok_berat").val(); + "</td>"; /* 14 */
        content += "<td style='display:none'>" + $("#dok_sat_berat").val(); + "</td>"; /* 15 */
        content += "<td width='5%'>" + $("#dok_sat_berat option:selected").text(); + "</td>"; /* 16 */
        content += "<td style='display:none'>" + $("#fisik_jumlah").val(); + "</td>"; /* 17 */
        content += "<td width='5%' class='angka_det'>" + $("#fisik_jumlah").val(); + "</td>"; /* 18 */
        content += "<td style='display:none'>" + $("#fisik_sat_jumlah").val(); + "</td>"; /* 19 */
        content += "<td width='5%'>" + $("#fisik_sat_jumlah option:selected").text(); + "</td>"; /* 20 */
        content += "<td style='display:none'>" + $("#fisik_volume").val(); + "</td>"; /* 21 */
        content += "<td width='5%' class='angka_det'>" + $("#fisik_volume").val(); + "</td>"; /* 22 */
        content += "<td style='display:none'>" + $("#fisik_sat_volume").val(); + "</td>"; /* 23 */
        content += "<td width='5%'>" + $("#fisik_sat_volume option:selected").text(); + "</td>"; /* 24 */
        content += "<td style='display:none'>" + $("#fisik_berat").val(); + "</td>"; /* 25 */
        content += "<td width='5%' class='angka_det'>" + $("#fisik_berat").val(); + "</td>"; /* 26 */
        content += "<td style='display:none'>" + $("#fisik_sat_berat").val(); + "</td>"; /* 27 */
        content += "<td width='5%'>" + $("#fisik_sat_berat option:selected").text(); + "</td>"; /* 28 */
        content += "<td width='5%'><a href='#' class='edit'><i class='icon-pencil'></i></a>&nbsp;&nbsp;<a href='#' class='hapus'><i class='icon-remove'></i></a> </td>";
        
        
        if($("#rowindex").val() != ''){
            $('#detail_list tbody tr:eq(' + $("#rowindex").val() + ')').html(content);
        }else{
            content += "</tr>";
            $('#detail_list tbody').append(content);
        }

        $("#cmdClear").trigger('click');
        $('.angka_det').number(true,3,',','.');
        $table.floatThead("reflow");
    });
    
    function validate_detail()
	{
		if($("#idsortimen").val() == ""){
			big_notification("Sortimen Belum Dipilih","error");
			$("#idsortimen").select2("open");
			return false;
		}
        
       
		
		if($("#dok_jumlah").val() == "" && $("#dok_volume").val() == "" && $("#dok_berat").val() == ""){
			big_notification("Jumlah Penerimaan (Document) Harus Diisi","error");
			$("#dok_jumlah").focus();
			return false;
		}
        
        if($("#fisik_jumlah").val() == "" && $("#fisik_volume").val() == "" && $("#fisik_berat").val() == ""){
			big_notification("Jumlah Penerimaan (Fisik) Harus Diisi","error");
			$("#fisik_jumlah").focus();
			return false;
		}
		
		
		return true;
	}
    
    
    $(document).on("click",".edit",function(){
			
        $("#rowindex").val($(this).closest('tr')[0].sectionRowIndex);
        $("#nomor").val($(this).closest('tr').find("td:eq(0)").html());
        
        $("#dok_jumlah").val($(this).closest('tr').find("td:eq(5)").html());
        $("#dok_sat_jumlah").val($(this).closest('tr').find("td:eq(7)").html());
        $("#dok_volume").val($(this).closest('tr').find("td:eq(9)").html());
        $("#dok_sat_volume").val($(this).closest('tr').find("td:eq(11)").html());
        $("#dok_berat").val($(this).closest('tr').find("td:eq(13)").html());
        $("#dok_sat_berat").val($(this).closest('tr').find("td:eq(15)").html());
        $("#fisik_jumlah").val($(this).closest('tr').find("td:eq(17)").html());
        $("#fisik_sat_jumlah").val($(this).closest('tr').find("td:eq(19)").html());
        $("#fisik_volume").val($(this).closest('tr').find("td:eq(21)").html());
        $("#fisik_sat_volume").val($(this).closest('tr').find("td:eq(23)").html());
        $("#fisik_berat").val($(this).closest('tr').find("td:eq(25)").html());
        $("#fisik_sat_berat").val($(this).closest('tr').find("td:eq(27)").html());
        
        var idsortimen = $(this).closest('tr').find("td:eq(1)").html();
        var nama_sortimen = $(this).closest('tr').find("td:eq(2)").html();
        
        var idkayu = $(this).closest('tr').find("td:eq(3)").html();
        var nama_jenis = $(this).closest('tr').find("td:eq(4)").html();
        
        $('#idsortimen').find('option').remove().end()
        .append('<option value="">- Pilih Jenis Sortimen -</option><option value="'+ idsortimen +'">' + nama_sortimen + '</option>')
        .val(idsortimen)
        .trigger("liszt:updated");
        
        $('#idkayu').find('option').remove().end()
        .append('<option value="">- Pilih Jenis Kayu -</option><option value="'+ idkayu +'">' + nama_jenis + '</option>')
        .val(idkayu)
        .trigger("liszt:updated");
    
        $("#dok_jumlah").focus();
        return false;
    });
    $(document).on("click",".hapus",function(){
        if(confirm("Hapus Data ?") == true){
            $(this).closest('td').parent().remove();
            $("#idkayu").trigger("liszt:activate");
            $("#idsortimen").trigger("liszt:activate");
        }
        renumbering();
        return false;
    });
    $("#cmdClear").click(function(){
        $(".detail").val('');
        $('#idkayu').find('option').remove().end().append('<option value="">- Pilih Jenis Kayu -</option>').val('').trigger("liszt:updated");
        $('#idsortimen').find('option').remove().end().append('<option value="">- Pilih Jenis Sortimen -</option>').val('').trigger("liszt:updated");
        $("#idkayu").trigger("liszt:activate");
    });
    
    $(document).on("click","#btnsimpan",function(){
        var rowCount = $('table#detail_list tr').length;
        if(rowCount <= 1){
           big_notification("Data Detail Penerimaan Masih Kosong!",'error');
           return false;
        }else{
            var tbl = $('table#detail_list tbody tr').get().map(function(row) {
              return $(row).find('td').get().map(function(cell) {
                return $(cell).html();
              });
            });
            $("#detail_value").val(JSON.stringify(tbl));
            $("#detail_value_html").val($('table#detail_list tbody').html());
            $("#frmTransaksi").submit();
        }
    });
    
    function renumbering(){
        $('#detail_list tbody tr').each(function(index, element) {
           $(this).find('td:eq(0)').text(element.sectionRowIndex + 1);		
        });
    }

    <?php if( strtoupper($type_penerimaan) == "LOKAL"){ ?>
        $(".lokal").show();
        $(".import").hide();
    <?php }else{ ?>
        $(".lokal").hide();
        $(".import").show();
    <?php } ?>

    
    /*
    $(document).on("click","#cmdSave",function(){
        var tbl = $('table#detail_list tbody tr').get().map(function(row) {
          return $(row).find('td').get().map(function(cell) {
            return $(cell).html();
          });
        });
        //console.log(tbl);
        $("#detail_value").val(JSON.stringify(tbl));
        $("#frmTransaksi").submit();
    });*/
});
</script>