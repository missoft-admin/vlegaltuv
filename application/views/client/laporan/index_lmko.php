<div class="page-header">
	<h1>{first_title} <small>{second_title}</small></h1>
</div>  
<?php echo error_success($this->session)?>
<div class="row-fluid">
	<div class="span12">                    
		<div class="head">
			<div class="isw-grid"></div><h1> {table_title}</h1>
			<div class="clear"></div>
		</div>
		<div class="block-fluid table-sorting">
			<div class="dataTables_wrapper">
            <div class="dataTables_filter" id="tSortable_filter" style="width:auto;">
				<form action="{client_url}laporan/search_lmko" method="post" style="margin-bottom:0;">
				<label>Search: <input type="text" style="width:300px;" value="{searchText}" name="searchText" placeholder="Search LMKO "></label>
				</form>
			</div>
			<table cellpadding="0" cellspacing="0" width="100%" class="table">
				<thead>
					<tr>                                    
						<th width="5%">No.</th>
						<th width="30%">Jenis Kayu Olahan</th>
						<th width="10%">Persediaan Awal</th>
						<th width="10%">Perolehan</th>
						<th width="10%">Dipakai / Diolah Sendiri</th>
						<th width="10%">Pemasaran dalam Negeri dan Ekspor</th>
						<th width="10%">Persediaan Akhir</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($query as $row){ ?>
					<?php $no = $no + 1 ?>
					<tr>                                    
						<td><?= $no?></td>
						<td><?php echo $row->nama ?></td>							
						<td><?php echo format_angka_id($row->persediaan_awal,3) ?></td>							
						<td><?php echo format_angka_id($row->perolehan,3) ?></td>							
						<td><?php echo format_angka_id($row->produksi,3) ?></td>							
						<td><?php echo format_angka_id($row->penjualan,3) ?></td>							
						<td><?php echo format_angka_id($row->persediaan_awal + $row->perolehan - $row->produksi - $row->penjualan,3) ?></td>							
					</tr>                                
					<?php } ?>
                    <tr>                                    
						<td colspan="7">&nbsp;</td>
                    </tr>    
                    <?php $no = 0 ?>
                    <?php foreach($query_fg as $row){ ?>
					<?php $no = $no + 1 ?>
					<tr>                                    
						<td><?= $no?></td>
						<td><?php echo $row->produk ?></td>							
						<td><?php echo format_angka_id($row->persediaan_awal,3) ?></td>							
						<td><?php echo format_angka_id($row->perolehan,3) ?></td>							
						<td><?php echo $row->produksi ?></td>							
						<td><?php echo format_angka_id($row->penjualan,3) ?></td>							
						<td><?php echo format_angka_id($row->persediaan_awal + $row->perolehan - $row->penjualan,3) ?></td>							
					</tr>                                
					<?php } ?>
				</tbody>
			</table>
			<?php echo $pagination ?>
			
		</div>
		<div class="clear"></div>
	</div>                                
</div>