<div class="page-header">
	<h1>{first_title} <small>{second_title}</small></h1>
</div>  
<?php echo error_success($this->session)?>
<div class="row-fluid">
	<div class="span12">                    
		<div class="head">
			<div class="isw-grid"></div><h1> {table_title}</h1>
			<ul class="buttons">
                <li><a href="{client_url}ekspor/{ref_url}" class="isw-list" title="List Attachment"></a></li>
				<li>
					<a href="#" class="isw-settings"></a>
					<ul class="dd-list">
						<li><a href="{client_url}ekspor/add_attachment/{idekspor}/{ref_url}"><span class="isw-plus"></span> Add Attachment</a></li>
					</ul>
				</li>
			</ul>                        
			<div class="clear"></div>
		</div>
		<div class="block-fluid table-sorting">
			<div class="dataTables_wrapper">
			<table cellpadding="0" cellspacing="0" width="100%" class="table">
				<thead>
					<tr>                                    
						<th width="5%">No.</th>
						<th width="10%">Jenis Dokumen</th>
						<th width="10%">No. Dokumen</th>
						<th width="10%">Keterangan</th>
						<th width="10%">Upload Date</th>
						<th width="10%">Download Attachment</th>
						<th width="10%">Status</th>
                        <th width="10%">Option</th>                                                 
					</tr>
				</thead>
				<tbody>
					<?php foreach($query as $row){ ?>
					<?php $no = $no + 1 ?>
					<tr>                                    
						<td><?= $no?></td>
						<td><?php echo define_jenis_dokumen($row->attach_doc)?></td>
						<td><?php echo $row->no_doc ?></td>							
						<td><?php echo $row->attach_keterangan ?></td>							
						<td><?php echo human_date_time($row->attach_date) ?></td>							
						<td><?php echo tool_download('assets/lampiran/'.$row->client_id."/".$row->attach_file) ?></td>							
						<td><?php echo ($row->attach_status == 1)? "Active" : "Inactive" ?></td>
						<td>
                            <div class="btn-group">                                        
                                <button data-toggle="dropdown" class="btn dropdown-toggle">Action <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="{site_url}client/ekspor/edit_attachment/<?= $row->attach_id ?>/{ref_url}">Edit</a></li>
                                    <li><a href="{site_url}client/ekspor/delete_attachment/<?= $row->attach_id ?>/{ref_url}" onClick="return confirm('Delete Attachment?')">Delete</a></li>
                                </ul>
                            </div>
						</td>                                    
					</tr>                                
					<?php } ?>
				</tbody>
			</table>
			
		</div>
		<div class="clear"></div>
	</div>                                
</div>