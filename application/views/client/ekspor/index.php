<div class="page-header">
	<h1>{first_title} <small>{second_title}</small></h1>
</div>  
<?php echo error_success($this->session)?>
<div class="row-fluid">
	<div class="span12">                    
		<div class="head">
			<div class="isw-grid"></div><h1> {table_title}</h1>
			<ul class="buttons">
				<li>
					<a href="#" class="isw-settings"></a>
					<ul class="dd-list">
						<li><a href="{client_url}ekspor/add"><span class="isw-plus"></span> Add Ekspor</a></li>
					</ul>
				</li>
			</ul>                        
			<div class="clear"></div>
		</div>
		<div class="block-fluid table-sorting">
			<div class="dataTables_wrapper">
            <div class="dataTables_filter" id="tSortable_filter" style="width:auto;">
				<form action="{client_url}ekspor/search" method="post" style="margin-bottom:0;">
				<label>Search: <input type="text" style="width:300px;" value="{searchText}" name="searchText" placeholder="Search By No. Invoice"></label>
                </form>
			</div>
			<table cellpadding="0" cellspacing="0" width="100%" class="table">
				<thead>
					<tr>                                    
						<th width="5%">No.</th>
						<th width="10%">Invoice/TGL</th>
						<th width="7%">No. Urut</th>
						<th width="20%">Nama Pembeli / Negara</th>
						<th width="5%">ISO</th>
						<th width="10%">Loading</th>
                        <th width="10%">Discharge</th>
                        <th width="7%">Dokumen</th>
                        <th width="7%">Status Dokumen</th>
                        <th width="7%">Tgl.Shipment</th>
                        <th width="10%">Option</th>                                                 
					</tr>
				</thead>
				<tbody>
					<?php foreach($query as $row){ ?>
					<?php $no = $no + 1 ?>
					<tr>                                    
						<td><?= $no?></td>
						<td><?php echo $row->invoice." / ".human_date_short($row->tglinvoice)?></td>
						<td><?php echo $row->no_urut ?></td>							
						<td><?php echo $row->buyer ?></td>							
						<td><?php echo $row->iso ?></td>							
						<td><?php echo $row->loading_name ?></td>
						<td><?php echo $row->discharge_name ?></td>
						<td><?php echo $row->status ?></td>
						<td><?php echo $row->status_liu ?></td>
						<td><?php echo human_date_short($row->tglship) ?></td>
						<td>
                            <div class="btn-group">                                        
                                <button data-toggle="dropdown" class="btn dropdown-toggle">Action <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="{site_url}client/ekspor/edit/<?= $row->idekspor ?>">Edit</a></li>
                                    <li><a href="{site_url}client/ekspor/delete/<?= $row->idekspor ?>" onClick="return confirm('Delete V-Legal?')">Delete</a></li>
                                    <li class="divider"></li>
                                    <li><a href="{site_url}client/ekspor/detail/<?= $row->idekspor ?>">View Ekspor</a></li>
                                </ul>
                            </div>
						</td>                                    
					</tr>                                
					<?php } ?>
				</tbody>
			</table>
			<?php echo $pagination ?>
			
		</div>
		<div class="clear"></div>
	</div>                                
</div>