<div class="page-header">
	<h1>{first_title} <small>{second_title}</small></h1>
</div>  
<?php echo error_success($this->session)?>
<?php if($error != '') echo error_message($error)?>
<div class="row-fluid">
	<div class="span12">
		<div class="head">
			<div class="isw-documents"></div>
			<h1>{table_title}</h1>
			<ul class="buttons">
				<li><a href="{client_url}ekspor/attachment/{idekspor}/{ref_url}" class="isw-list" title="List Attachment"></a></li>                                                        
			</ul>
			<div class="clear"></div>
		</div>
		<div class="block-fluid">                        
			<?php echo form_open_multipart('client/ekspor/save_attachment','id = "frmTransaksi"') ?>
			<div class="row-form">
				<div class="span2">Jenis Dokumen:</div>
				<div class="span10"> 
                       
                    <select name="attach_doc" id="attach_doc">
                        <option value="" <?php if($attach_doc=='') echo 'selected'; ?>>Pilih Jenis Dokumen</option>
                        <optgroup label="Lampiran Data Bulanan">
                            <option class="bulanan" value="lmk" <?php if($attach_doc=='lmk') echo 'selected'; ?>>LMK</option>
                            <option class="bulanan" value="lmko" <?php if($attach_doc=='lmko') echo 'selected'; ?>>LMKO</option>
                            <option class="bulanan" value="dkp" <?php if($attach_doc=='dkp') echo 'selected'; ?>>DKP</option>
                        </optgroup>
                        <optgroup label="Lampiran saat permohonan V-Legal diajukan">
                            <option value="vlegal-request" <?php if($attach_doc=='vlegal-request') echo 'selected'; ?>>Form Permohonan</option>
                            <option value="vlegal-po" <?php if($attach_doc=='vlegal-po') echo 'selected'; ?>>Dokumen PO</option>
                            <option value="vlegal-invoice" <?php if($attach_doc=='vlegal-invoice') echo 'selected'; ?>>Dokumen Invoice</option>
                            <option value="vlegal-packing" <?php if($attach_doc=='vlegal-packing') echo 'selected'; ?>>Dokumen Packing List</option>
                        </optgroup>
                        <optgroup label="Lampiran pasca eksport">
                            <option value="vlegal-peb" <?php if($attach_doc=='vlegal-peb') echo 'selected'; ?>>Dokumen PEB</option>
                            <option value="vlegal-bl-pascaekspor" <?php if($attach_doc=='vlegal-bl-pascaekspor') echo 'selected'; ?>>Dokumen B/L</option>
                            <option value="vlegal-invoice-pascaekspor" <?php if($attach_doc=='vlegal-invoice-pascaekspor') echo 'selected'; ?>>Dokumen Invoice</option>
                            <option value="vlegal-packing-pascaekspor" <?php if($attach_doc=='vlegal-packing-pascaekspor') echo 'selected'; ?>>Dokumen Packing List</option>
                        </optgroup>
                        <optgroup label="Others">
                                <option value="others" <?php if($attach_doc=='others') echo 'selected'; ?>>Others</option>
                            </optgroup>
                    </select>
                </div>
                <div class="clear"></div>
            </div>
            <div class="row-form">
                <div class="span2">No. Invoice:</div>
				<div class="span10"><input type="text" readonly value="{attach_invoice}"/><input type="hidden" name="attach_invoice" id="attach_invoice" value="{attach_invoice}"/></div>
				<div class="clear"></div>
			</div> 
            <div class="row-form">
                <div class="span2">No. Dokumen:</div>
				<div class="span10"><input type="text" name="no_doc" value="{no_doc}"/></div>
				<div class="clear"></div>
			</div> 
            <div class="row-form data-bulanan">
                <div class="span2">Periode:</div>
				<div class="span3">
                    <select size="1" name="attach_bln" id="attach_bln">
                        <option <?= ($attach_bln == '')? 'selected="selected"' : ''?> value="0">-- Bulan --</option>
                        <option <?= ($attach_bln == '1')? 'selected="selected"' : ''?> value="1">Januari</option>
                        <option <?= ($attach_bln == '2')? 'selected="selected"' : ''?> value="2">Februari</option>
                        <option <?= ($attach_bln == '3')? 'selected="selected"' : ''?> value="3">Maret</option>
                        <option <?= ($attach_bln == '4')? 'selected="selected"' : ''?> value="4">April</option>
                        <option <?= ($attach_bln == '5')? 'selected="selected"' : ''?> value="5">Mei</option>
                        <option <?= ($attach_bln == '6')? 'selected="selected"' : ''?> value="6">Juni</option>
                        <option <?= ($attach_bln == '7')? 'selected="selected"' : ''?> value="7">Juli</option>
                        <option <?= ($attach_bln == '8')? 'selected="selected"' : ''?> value="8">Agustus</option>
                        <option <?= ($attach_bln == '9')? 'selected="selected"' : ''?> value="9">September</option>
                        <option <?= ($attach_bln == '10')? 'selected="selected"' : ''?> value="10">Oktober</option>
                        <option <?= ($attach_bln == '11')? 'selected="selected"' : ''?> value="11">November</option>
                        <option <?= ($attach_bln == '12')? 'selected="selected"' : ''?> value="12">Desember</option>
                    </select>
                </div>
                <div class="span2">
                    <select size="1" name="attach_thn" id="attach_thn">
                        <option value="0" <?= ($attach_thn == '')? 'selected="selected"' : ''?>>-- Tahun --</option>
                        <?php for($i=2007;$i<2100;$i++){?>
                            <option <?= ($i == $attach_thn)? 'selected="selected"' : ''?> value="<?= $i ?>"><?= $i ?></option>
                        <?php } ?>
                    </select>
                </div>
				<div class="clear"></div>
			</div> 
            <?php if($attach_file != ''){?>
            <div class="row-form">
                <div class="span2">Attachment File:</div>
				<div class="span10"><?php echo tool_download('assets/lampiran/'.$client_id."/".$attach_file) ?></div>
				<div class="clear"></div>
			</div>
            <?php }?>
            <div class="row-form">
                <div class="span2">File:</div>
				<div class="span10"><input type="file" name="doc_file" /></div>
				<div class="clear"></div>
			</div> 
            <div class="row-form">
				<div class="span2">Keterangan:</div>
				<div class="span10"><textarea name="attach_keterangan" id="attach_keterangan" placeholder="Keterangan">{attach_keterangan}</textarea></div>
				<div class="clear"></div>
			</div> 
			<div class="row-form">
				<div class="span2">Status:</div>
				<div class="span10">
                        <select name="attach_status" id="attach_status">
                            <option value="1" <?= ($attach_status == 1)? 'selected':'' ?>>Active</option>
                            <option value="0" <?= ($attach_status == 0)? 'selected':'' ?>>Inactive</option>
                        </select>
				</div>	
				<div class="clear"></div>
			</div> 
            <div class="row-form">
				<div class="span12"><button class="btn" type="submit" id="btnsimpan">Simpan</button></div>
				<div class="clear"></div>
			</div>
			<?php echo form_hidden('attach_file', $attach_file); ?>
			<?php echo form_hidden('attach_invoice', $attach_invoice); ?>
			<?php echo form_hidden('idekspor', $idekspor); ?>
			<?php echo form_hidden('attach_id', $attach_id); ?>
			<?php echo form_hidden('ref_url', $ref_url); ?>
			<?php echo form_close() ?>
		</div>
	</div>
	
</div>
<script type="text/javascript">

$(document).ready(function(){  
    $('.data-bulanan').hide();
    $("#attach_doc").change(function(){
        if($("#attach_doc option:selected").attr('class') == 'bulanan'){
            $('.data-bulanan').show();
        }else{
            $('.data-bulanan').hide();
        }
    });
});
</script>