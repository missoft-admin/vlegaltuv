<div class="page-header">
	<h1>{first_title} <small>{second_title}</small></h1>
</div>  
<?php echo error_success($this->session)?>
<?php if($error != '') echo error_message($error)?>
<div class="row-fluid">
	<div class="span12">
		<div class="head">
			<div class="isw-documents"></div>
			<h1>Form Penjualan Ekspor</h1>
			<ul class="buttons">
				<li><a href="{client_url}ekspor" class="isw-list" title="List Pengajuan"></a></li>                                                        
			</ul>
			<div class="clear"></div>
		</div>
		<div class="block-fluid">                        
			<?php echo form_open('client/ekspor/save','id = "frmTransaksi"') ?>
			<div class="row-form">
				<div class="span2">Nama Pembeli:</div>
				<div class="span4">
                    <select name="idbuyer" id="idbuyer" class="validate[required]">
						<option value="{idbuyer}">{namabuyer}</option>
					</select>
                
                </div>
                <div class="span2">No. Invoice:</div>
				<div class="span4"><input class="validate[required]" type="text" name="invoice" placeholder="No. Invoice" value="{invoice}"/></div>
				<div class="clear"></div>
			</div> 
            
            <div class="row-form">
				<div class="span2">Alamat Pembeli:</div>
				<div class="span4"><textarea readonly name="alamat" id="alamat" placeholder="Alamat Pembeli">{alamat}</textarea></div>
                 <div class="span2">Tanggal Invoice:</div>
				<div class="span4"><input class="datepickertxt validate[required]" type="text" name="tglinvoice" placeholder="Tanggal Invoice" value="<?= human_date_short($tglinvoice) ?>"/></div>
				<div class="clear"></div>
			</div> 
			<div class="row-form">
				<div class="span2">Negara Tujuan:</div>
				<div class="span4">
                        <select name="idnegara" id="idnegara" class="validate[required]">
                            <option value="{idnegara}">{country_name}</option>
                        </select>
						<input type="hidden" name="country_name" id="country_name" value="{country_name}"/>
				</div>	
                <div class="span2">ISO Code:</div>
				<div class="span4">
						<input type="text" name="iso" readonly id="iso" value="{iso}"/>
				</div>	
				<div class="clear"></div>
			</div> 
			
            <div class="row-form lokal">
				<div class="span2">Mata Uang:</div>
				<div class="span4">
					<select name="valuta" id="valuta" class="validate[required]">
						<option value="{valuta}">{nama_valuta}</option>
					</select>
				</div>	
                <div class="span2">Alat Angkut:</div>
				<div class="span4">
					<select name="vessel" id="vessel" class="validate[required]">
						<option value="">- Pilih Alat Angkut-</option>
						<option value="1" <?= ($vessel == 1)? 'selected' : ''?>>By Sea</option>
						<option value="2" <?= ($vessel == 2)? 'selected' : ''?>>By Air</option>
						<option value="3" <?= ($vessel == 3)? 'selected' : ''?>>By Land</option>
					</select>
				</div>				
				<div class="clear"></div>
			</div>
            
            <div class="row-form import">
				<div class="span2">Port of Loading:</div>
				<div class="span4">
					<select name="loading" id="loading" class="validate[required]">
						<option value="{loading}">{loading_name}</option>
					</select>
				</div>	
                <div class="span2">Tanggal Shipment:</div>
				<div class="span4"><input class="datepickertxt validate[required]" type="text" name="tglship" placeholder="Tanggal Shipment" value="<?= human_date_short($tglship) ?>"/></div>	
				<div class="clear"></div>
			</div>
            <div class="row-form import">
				<div class="span2">Port of Discharge:</div>
				<div class="span4">
					<select name="discharge" id="discharge" class="validate[required]">
						<option value="{discharge}">{discharge_name}</option>
					</select>
				</div>	
                <div class="span2">No. Sertifikat:</div>
				<div class="span4"><input type="text" readonly name="sertifikat" value="{sertifikat}"/></div>
				<div class="clear"></div>
			</div>	
            <div class="row-form lokal">
				<div class="span2">No. ETPIK:</div>
				<div class="span4">
						<input type="text" name="etpik" readonly id="etpik" value="{etpik}"/>
				</div>	
                <div class="span2">NPWP:</div>
				<div class="span4">
						<input type="text" name="npwp" readonly id="npwp" value="{npwp}"/>
				</div>	
                
				<div class="clear"></div>
			</div>
            <div class="row-form lokal">
				<div class="span2">Lokasi Stuffing:</div>
				<div class="span4">
                    <textarea readonly name="stuffing" id="stuffing" placeholder="Lokasi Stuffing">{stuffing}</textarea>
				</div>	
                <div class="span2">Status Dokumen:</div>
				<div class="span4">
					<select name="status" id="status" class="validate[required]">
						<option value="">- Pilih Status Dokumen-</option>
						<option value="vlegal" <?= ($status == 'vlegal')? 'selected' : ''?>>Dokumen V-Legal</option>
						<option value="non-vlegal" <?= ($status == 'non-vlegal')? 'selected' : ''?>>Dokumen Non V-Legal</option>
					</select>
				</div>
                
				<div class="clear"></div>
			</div>
            <div class="row-form">
                <div class="span2">No. PO:</div>
				<div class="span4"><input class="validate[required]" type="text" name="no_po" placeholder="No. PO" value="{no_po}"/></div>
                <div class="span2">Tanggal PO:</div>
				<div class="span4"><input  class="<?= ($final == 0)? 'datepickertxt' : '' ?> [required]" type="text" name="tgl_po" placeholder="Tanggal PO" value="<?= human_date_short($tgl_po) ?>"/></div>
                
				<div class="clear"></div>
			</div> 
            <div class="row-form">
				<div class="span2">Keterangan:</div>
				<div class="span4"><textarea name="keterangan">{keterangan}</textarea></div>	
                <div class="span2">V-Legal Paper:</div>
				<div class="span4">
                   <input type="text" id="paper_number_search"/>
                </div>	
				<div class="clear"></div>
			</div>
            <?php if($idekspor != ''){ ?>
            <div class="row-form">
                <div class="span2">Status Edit Dokumen:</div>
				<div class="span4">
                    <select name="status_edit" id="status_edit" class="medium-input validate[required]">
                        <option value="">-- Pilih Status Edit Dokumen --</option>
                        <option value="edit">Penambahan/Perubahan Data</option>
                        <option value="replace">Perubahan Dokumen V-Legal</option>
                        <option value="extend">Perpanjangan Dokumen V-Legal</option>
                        <option value="cancel">Pembatalan Dokumen V-Legal</option>
                    </select>
                </div>	
				<div class="clear"></div>
			</div>
            <?php } ?>
            <div class="row-form">
                <div class="span12" style="text-align:center"><h4>Data Detail Ekspor</h4></div>
                <div class="clear"></div>
            </div>
			<div class="row-form" style="padding:0px;">
                <div class="span12">
                    <div class="block-fluid"  style="position:relative;z-index:1;margin-bottom:0px;background-color:#F1F1F1">
                        <table cellpadding="0" cellspacing="0" width="100%" class="table">
                            <thead>
                                <tr>
                                    <th  width="15%">Produk</th>
                                    <th colspan="9" style="vertical-align:middle">
                                        <input type="hidden" class="detail" id="rowindex"/>
                                        <input type="hidden"  class="detail" id="nomor"/>
                                        <input type="hidden"  class="detail" id="hscode"/>
                                        <select name="idproduct" id="idproduct">
                                            <option value="">Pilih Produk</option>
                                        </select>
                                    </th>
                                    <th width="5%" rowspan="3" style="vertical-align:middle">
                                        <button class="btn" id="cmdAdd" type="button">Add</button>
                                    </th>                                                 
                                </tr>
                                <tr>
                                    <th  width="10%">Jenis Kayu</th>
                                    <th colspan="9"style="vertical-align:middle" >
                                        <input type="text" id="jenis_kayu" name="jenis_kayu" />
                                    </th>
                                    
                                </tr>
                                <tr>    
                                    <th  width="10%">Negara Asal</th>
                                    <th colspan="9"style="vertical-align:middle" >
                                        <input type="text" id="negara_asal" name="negara_asal" />
                                    </th>
                                </tr>
                                <tr>
                                    <th  width="10%">&nbsp;</th>
                                    <th width="10%">Jumlah</th>
                                    <th width="10%">Satuan</th>
                                    <th width="10%">Volume</th>
                                    <th width="10%">Satuan</th>
                                    <th width="10%">Berat</th>		
                                    <th width="10%">Satuan</th> 
                                    <th width="10%">Nilai (FOB)</th>
                                    <th width="10%">Nilai (CNF)</th>
                                    <th width="10%">Nilai (CIF)</th>
                                    <th width="5%" rowspan="2" style="vertical-align:middle">
                                        <button class="btn btn-danger btn-small" id="cmdClear" type="button">Clear</button>
                                    </th>
                                </tr>
                                <tr>
                                     
                                    <th>&nbsp;</th>
                                    <th><input style="width:100%" type="text" class="detail angka" id="jumlah"></th>                                        
                                    <th>
                                        <select style="width:100%" class="detail" id="sat_jumlah">
                                            <option value="">-</option>
                                            <?php foreach($satuan_jumlah as $row){?>
                                                <option value="<?= $row->satuan_id ?>"><?= $row->satuan_kode ?> </option>
                                            <?php }?>
                                        </select>
                                    </th>
                                    <th><input style="width:100%"  type="text" class="detail angka" id="volume"></th>                                        
                                    <th>
                                        <select style="width:100%" class="detail" id="sat_volume">
                                            <option value="">-</option>
                                            <?php foreach($satuan_volume as $row){?>
                                                <option value="<?= $row->satuan_id ?>"><?= $row->satuan_kode ?> </option>
                                            <?php }?>
                                        </select>
                                    </th>
                                    <th><input style="width:100%" type="text" class="detail angka" id="berat"></th>                                        
                                    <th style="border-right: 1px solid #DDD;">
                                        <select style="width:100%" class="detail" id="sat_berat">
                                            <option value="">-</option>
                                            <?php foreach($satuan_berat as $row){?>
                                                <option value="<?= $row->satuan_id ?>"><?= $row->satuan_kode ?> </option>
                                            <?php }?>
                                        </select>
                                    </th>
                                    <th><input style="width:100%" type="text" class="detail angka"  id="fob"></th>   
                                    <th><input style="width:100%" type="text" class="detail angka" id="cnf"></th>                                        
                                    <th><input style="width:100%" type="text" class="detail angka" id="cif"></th>                                        
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="clear"></div>
			</div>
            <div class="row-form" style="padding:0px;">
				<div class="span12">
                     <div class="block-fluid wrap-table" style="height:400px;overflow:auto;">
                        <table cellpadding="0" cellspacing="0" width="100%" class="table" id="detail_list">
                            <thead>
                                <tr>
                                    <th rowspan="2" width="5%">No.</th>
                                    <th rowspan="2" width="20%">Nama Produk Jadi</th>
                                    <th rowspan="2" width="10%">HS CODE</th>
                                    <th colspan="3">Jumlah</th>
                                    <th rowspan="2" width="10%">Jenis Kayu</th>
                                    <th rowspan="2" width="10%">Negara Asal</th>
                                    <th rowspan="2" width="10%">Nilai</th>
                                    <th rowspan="2" width="5%" style="border-right: 1px solid #DDD;">Option</th>
                                </tr>
                                <tr>                                    
                                    <th width="5%">m3</th>
                                    <th width="5%">kg</th>
                                    <th width="5%">unit</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(isset($detail_list)){?>
                                <?php $no = 0;?>
                                <?php foreach($detail_list as $row) { ?>
                                <?php $no++;?>
                                    <tr>
                                        <!-- 0  --><td width="5%"><?= $no ?></td>
                                        <!-- 1  --><td style="display:none"><?= $row->idproduk ?></td>
                                        <!-- 2  --><td width="10%"><?= $row->namaproduk ?></td>
                                        <!-- 3  --><td width="10%"><?= $row->hscode ?></td>
                                        <!-- 4  --><td style="display:none"><?= $row->volume ?></td>
                                        <!-- 5  --><td width="5%" class="angka_det"><?= titik_to_koma($row->volume) ?></td>
                                        <!-- 6  --><td style="display:none"><?= $row->satuan_volume ?></td>
                                        <!-- 7  --><td width="5%" class="angka_det"><?= titik_to_koma($row->berat) ?></td>
                                        <!-- 8  --><td style="display:none"><?= $row->berat ?></td>
                                        <!-- 9  --><td style="display:none"><?= $row->satuan_berat ?></td>
                                        <!-- 10 --><td width="5%" class="angka_det"><?= titik_to_koma($row->jumlah) ?></td>
                                        <!-- 11 --><td style="display:none"><?= $row->jumlah ?></td>
                                        <!-- 12 --><td style="display:none"><?= $row->satuan_jumlah ?></td>
                                        <!-- 13 --><td style="display:none"><?= $row->jenis_kayu ?></td>
                                        <!-- 14 --><td><?= implode(array_column(json_decode($row->jenis_kayu,true),'name'),"<br/>"); ?></td>
                                        <!-- 15 --><td style="display:none"><?= $row->negara_asal?></td>
                                        <!-- 16 --><td><?= implode(array_column(json_decode($row->negara_asal,true),'name'),"<br/>"); ?></td>
                                        <!-- 17 --><td width="5%" class="angka_det"><?= titik_to_koma($row->nilai) ?></td>
                                        <!-- 18 --><td style="display:none"><?= $row->nilai ?></td>
                                        <!-- 19 --><td style="display:none"><?= $row->nilai_cnf ?></td>
                                        <!-- 20 --><td style="display:none"><?= $row->nilai_cif ?></td>
                                        <td width="5%">
                                            <a href="#" class="edit"><i class="icon-pencil"></i></a>
                                            <a href="#" class="hapus"><i class="icon-remove"></i></a> 
                                        </td>
                                    </tr>
                                <?php } ?>
                                <?php } ?>
                                
                                <?php if(isset($detail_value)){?>
                                <?php $detail_value = json_decode($detail_value)?>
                                <?php foreach($detail_value as $row) { ?>
                                    <tr>
                                        <td width="5%"><?= $row[0] ?></td>
                                        <td style="display:none"><?= $row[1] ?></td>
                                        <td width="10%"><?= $row[2] ?></td>
                                        <td width="10%"><?= $row[3] ?></td>
                                        <td style="display:none"><?= $row[4] ?></td>
                                        <td width="5%" class="angka_det"><?= $row[4] ?></td>
                                        <td style="display:none"><?= $row[6] ?></td>
                                        <td width="5%" class="angka_det"><?= $row[8] ?></td>
                                        <td style="display:none"><?= $row[8] ?></td>
                                        <td style="display:none"><?= $row[9] ?></td>
                                        <td width="5%" class="angka_det"><?= $row[11]?></td>
                                        <td style="display:none"><?= $row[11] ?></td>
                                        <td style="display:none"><?= $row[12] ?></td>
                                        <td style="display:none"><?= $row[13] ?></td>
                                        <td><?= $row[14] ?></td>
                                        <td style="display:none"><?= $row[15] ?></td>
                                        <td><?= $row[16] ?></td>
                                        <td width="5%" class="angka_det"><?= $row[18] ?></td>
                                        <td style="display:none"><?= $row[18] ?></td>
                                        <td style="display:none"><?= $row[19] ?></td>
                                        <td style="display:none"><?= $row[20] ?></td>
                                        <td width="5%">
                                            <a href="#" class="edit"><i class="icon-pencil"></i></a>
                                            <a href="#" class="hapus"><i class="icon-remove"></i></a> 
                                        </td>
                                    </tr>
                                <?php } ?>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
				<div class="clear"></div>
			</div> 
            <div class="row-form">
				<div class="span12"><button class="btn" type="button" id="btnsimpan">Simpan</button></div>
				<div class="clear"></div>
			</div>
			<input type="hidden" id="namabuyer" name="namabuyer" value="{namabuyer}"/>
			<input type="hidden" id="nama_valuta" name="nama_valuta" value="{nama_valuta}"/>
			<input type="hidden" id="loading_name" name="loading_name" value="{loading_name}"/>
			<input type="hidden" id="discharge_name" name="discharge_name" value="{discharge_name}"/>
			<input type="hidden" id="paper_number" name="paper_number" value="{paper_number}"/>
            <input type="hidden" id="detail_value" name="detail_value"/>
			<?php echo form_hidden('idekspor', $idekspor); ?>
			<?php echo form_hidden('ref_url', $ref_url); ?>
			<?php echo form_close() ?>
		</div>
	</div>
	
</div>
<script src="{js_path}jquery.floatThead.js" type="text/javascript"></script>
<script type="text/javascript">

$(document).ready(function(){
    $('.angka').number(true,3,',','.');
    $('.angka_det').number(true,3,',','.');
    
    var $table = $('table#detail_list');
    
    // $table.floatThead({
        // position: 'absolute',
        // scrollContainer: true
    // });
    
    $table.floatThead({
        zIndex : 0,
        scrollContainer: function($table){
            return $table.closest('.wrap-table');
        }
    });
    
    
    $("#jenis_kayu").tokenInput("{client_url}ekspor/find_jenis_kayu",{theme: "facebook"});
    $("#negara_asal").tokenInput("{client_url}ekspor/find_negara_token",{theme: "facebook"});
    $("#paper_number_search").tokenInput("{client_url}ekspor/find_vlegal_paper",{theme: "facebook",preventDuplicates: true,prePopulate: <?= is_empty($paper_number,'[]')?> });

    $("#idproduct").select2({
         ajax: {
            url: '{client_url}ekspor/find_product',
            dataType: 'json',
            delay: 250,
            processResults: function (data, params) {
              return {
                    results: $.map(data, function (item) {
                        return {
                       id: item.idproduk,
                       text: item.produk
                        };
                    })
              };
            },
            cache: true
          },
          minimumInputLength: 3,
          theme: "bootstrap",
          allowClear: true
    }).on('select2:select', function(e) { $(this).focus(); });
    
    $("#idproduct").change(function(){
        $.ajax({
          url: '{client_url}ekspor/find_hscode/'+$(this).val(),
          dataType: 'json',
          success: function(data) {
            $("#hscode").val(data);
          }
        });
    });
    
    
    $("#cmdAdd").click(function(){
        var valid = validate_detail();
        if(!valid) return false;
                
        //var duplicate = false;
        
        if($("#nomor").val() != ''){
            var no = $("#nomor").val();
            var content = "";
        }else{
            var no = $('#detail_list tr').length;
            var content = "<tr>";
            /*
            $('#detail_list tbody tr').filter(function (){
                var $cells = $(this).children('td');
                if($cells.eq(1).text() === $("#idkayu").val()){
                    alert("Jenis Kayu " + $("#idkayu option:selected").text() + " sudah ada didalam daftar di baris ke " + $cells.eq(0).text());
                    duplicate = true;
                }
            });	*/
        }
        
        //if(duplicate) return false;
        // console.log($("#idkayu2 option:selected").text());
        // console.log($("#idkayu3 option:selected").text());
        // console.log($("#idnegaraasal2 option:selected").text());
        // console.log($("#idnegaraasal3 option:selected").text());
        
        var jenis_kayu =  $("#jenis_kayu").tokenInput("get");
        var jenis_kayu_text = '';
        $.each( jenis_kayu, function( key, value ) {
            jenis_kayu_text = jenis_kayu_text + value.name + "<br/>";
            
        });
        
        var negara_asal =  $("#negara_asal").tokenInput("get");
        var negara_asal_text = '';
        $.each(negara_asal, function( key, value ) {
            negara_asal_text = negara_asal_text + value.name + "<br/>";
            
        }); 
        
        content += "<td width='5%'>" + no + "</td>";  //0
        content += "<td style='display:none'>" + $("#idproduct").val(); + "</td>"; //1
        content += "<td width='10%'>" + $("#idproduct option:selected").text(); + "</td>"; //2
        content += "<td width='10%'>" + $("#hscode").val(); + "</td>"; //3
        content += "<td style='display:none'>" + $("#volume").val(); + "</td>"; //4
        content += "<td width='5%' class='angka_det'>" + $("#volume").val(); + "</td>"; //5
        content += "<td style='display:none'>" + $("#sat_volume").val(); + "</td>"; //6
        content += "<td width='5%' class='angka_det'>" + $("#berat").val(); + "</td>"; //7
        content += "<td style='display:none'>" + $("#berat").val(); + "</td>"; //8
        content += "<td style='display:none'>" + $("#sat_berat").val(); + "</td>"; //9
        content += "<td width='5%'  class='angka_det'>" + $("#jumlah").val(); + "</td>";//10 
        content += "<td style='display:none'>" + $("#jumlah").val(); + "</td>"; //11
        content += "<td style='display:none'>" + $("#sat_jumlah").val(); + "</td>"; //12
        content += "<td style='display:none'>" + JSON.stringify(jenis_kayu); + "</td>"; //13
        content += "<td>" + jenis_kayu_text + "</td>"; //14
        content += "<td style='display:none'>" + JSON.stringify(negara_asal); + "</td>"; //15
        content += "<td>" + negara_asal_text + "</td>"; //16
        content += "<td width='5%' class='angka_det'>" + $("#fob").val(); + "</td>"; //17
        content += "<td style='display:none'>" + $("#fob").val(); + "</td>"; //18
        content += "<td style='display:none'>" + $("#cnf").val(); + "</td>"; //19
        content += "<td style='display:none'>" + $("#cif").val(); + "</td>"; //20
        content += "<td width='5%'><a href='#' class='edit'><i class='icon-pencil'></i></a>&nbsp;&nbsp;<a href='#' class='hapus'><i class='icon-remove'></i></a> </td>";
        
        
        if($("#rowindex").val() != ''){
            $('#detail_list tbody tr:eq(' + $("#rowindex").val() + ')').html(content);
        }else{
            content += "</tr>";
            $('#detail_list tbody').append(content);
        }

        $("#cmdClear").trigger('click');
        $('.angka_det').number(true,3,',','.');
        $("#idproduct").focus();
        return false;
    });
    
    function validate_detail()
	{
		if($("#idproduct").val() == ""){
			big_notification("Produk belum dipilih!","error");
			$("#idproduct").trigger("liszt:activate");
			return false;
		}
        
        if($("#jenis_kayu").val() == ""){
			big_notification("Jenis kayu harus diisi!","error");
			$("#jenis_kayu").focus();
			return false;
		} 
        
        if($("#negara_asal").val() == ""){
			big_notification("Negara Asal kayu harus diisi!","error");
			$("#negara_asal").focus();
			return false;
		}
		
		if($("#jumlah").val() == "" && $("#volume").val() == "" && $("#berat").val() == ""){
			big_notification("Jumlah, atau Volume , atau Berat harus diisi!","error");
			$("#jumlah").focus();
			return false;
		}
        
        if($("#sat_jumlah").val() == "" && $("#sat_volume").val() == "" && $("#sat_berat").val() == ""){
			big_notification("Satuan untuk Jumlah, atau Volume , atau Berat harus diisi!","error");
			$("#sat_jumlah").focus();
			return false;
		}
		
		
		return true;
	}
    
    
    $(document).on("click",".edit",function(){
			
        $("#rowindex").val($(this).closest('tr')[0].sectionRowIndex);
        $("#nomor").val($(this).closest('tr').find("td:eq(0)").html());
        $("#hscode").val($(this).closest('tr').find("td:eq(3)").html());
        $("#volume").val($(this).closest('tr').find("td:eq(4)").html());
        $("#sat_volume").val($(this).closest('tr').find("td:eq(6)").html());
        $("#berat").val($(this).closest('tr').find("td:eq(8)").html());
        $("#sat_berat").val($(this).closest('tr').find("td:eq(9)").html());
        $("#jumlah").val($(this).closest('tr').find("td:eq(11)").html());
        $("#sat_jumlah").val($(this).closest('tr').find("td:eq(12)").html());
        
        $("#fob").val($(this).closest('tr').find("td:eq(18)").html());
        $("#cnf").val($(this).closest('tr').find("td:eq(19)").html());
        $("#cif").val($(this).closest('tr').find("td:eq(20)").html());
        
        var idproduct = $(this).closest('tr').find("td:eq(1)").html();
        var nama_product = $(this).closest('tr').find("td:eq(2)").html(); 
        $('#idproduct').find('option').remove().end()
        .append('<option value="">- Pilih Produk -</option><option value="'+ idproduct +'">' + nama_product + '</option>')
        .val(idproduct)
        .trigger("liszt:updated");
        
        var jenis_kayu  = JSON.parse($(this).closest('tr').find("td:eq(13)").html());
        
        
        $.each( jenis_kayu, function( key, value ) {
            $("#jenis_kayu").tokenInput("add", value);
        });
        
        var negara_asal = JSON.parse($(this).closest('tr').find("td:eq(15)").html());
        
        $.each(negara_asal, function( key, value ) {
            $("#negara_asal").tokenInput("add", value);
            
        });
    
        $("#jumlah").focus();
        return false;
    });
    $(document).on("click",".hapus",function(){
        if(confirm("Hapus Data ?") == true){
            $(this).closest('td').parent().remove();
        }
        return false;
    });
    $("#cmdClear").click(function(){
        $(".detail").val('');
        $('#idproduct').find('option').remove().end().append('<option value="">- Pilih Produk -</option>').val('');
        $("#jenis_kayu").tokenInput("clear");
        $("#negara_asal").tokenInput("clear");
    });
    
    $(document).on("click","#btnsimpan",function(){
        var tbl = $('table#detail_list tbody tr').get().map(function(row) {
          return $(row).find('td').get().map(function(cell) {
            return $(cell).html();
          });
        });
        $("#detail_value").val(JSON.stringify(tbl));
        
        paper_number_search
        $("#paper_number").val(JSON.stringify($("#paper_number_search").tokenInput("get")));
        $("#frmTransaksi").submit();
    });
    
    
    $("input").bind("keypress", function(e) {
        if (e.keyCode == 13){
            return false;
        }
	});
    
    $("#idnegara").select2({
         ajax: {
            url: '{client_url}ekspor/find_negara',
            dataType: 'json',
            delay: 250,
            processResults: function (data, params) {
              return {
                    results: $.map(data, function (item) {
                        return {
                       id: item.idnegara,
                       text: item.negara
                        };
                    })
              };
            },
            cache: true
          },
          minimumInputLength: 3,
          theme: "bootstrap",
          allowClear: true
    }).on('select2:select', function(e) { $(this).focus(); });


    $("#idnegara").change(function(){
        $("#country_name").val($("#idnegara option:selected").text());
        $("#iso").val($("#idnegara").val());
    });
    
     $("#idbuyer").select2({
         ajax: {
            url: '{client_url}ekspor/find_buyer',
            dataType: 'json',
            delay: 250,
            processResults: function (data, params) {
              return {
                    results: $.map(data, function (item) {
                        return {
                       id: item.idbuyer,
                       text: item.buyer
                        };
                    })
              };
            },
            cache: true
          },
          minimumInputLength: 3,
          theme: "bootstrap",
          allowClear: true
    }).on('select2:select', function(e) { $(this).focus(); });
    
    
    $("#idbuyer").change(function(){
        $("#namabuyer").val($("#idbuyer option:selected").text());
        $.ajax({
          url: '{client_url}ekspor/buyer_detail/'+$(this).val(),
          dataType: 'json',
          success: function(data) {
            $("#namabuyer").val(data.buyer);
            $("#alamat").val(data.alamat);
          }
        });
    });    
    
    $("#valuta").select2({
         ajax: {
            url: '{client_url}ekspor/find_valuta',
            dataType: 'json',
            delay: 250,
            processResults: function (data, params) {
              return {
                    results: $.map(data, function (item) {
                        return {
                       id: item.kode,
                       text: item.nama
                        };
                    })
              };
            },
            cache: true
          },
          minimumInputLength: 3,
          theme: "bootstrap",
          allowClear: true
    }).on('select2:select', function(e) { $(this).focus(); });

    $("#valuta").change(function(){
        $("#nama_valuta").val($("#valuta option:selected").text())
    });
    
    $("#loading").select2({
         ajax: {
            url: '{client_url}ekspor/find_loading',
            dataType: 'json',
            delay: 250,
            processResults: function (data, params) {
              return {
                    results: $.map(data, function (item) {
                        return {
                       id: item.idloading,
                       text: item.uraian + ' [' + item.kode + ']'
                        };
                    })
              };
            },
            cache: true
          },
          minimumInputLength: 3,
          theme: "bootstrap",
          allowClear: true
    }).on('select2:select', function(e) { $(this).focus(); });
    
    $("#loading").change(function(){
        $("#loading_name").val($("#loading option:selected").text())
    });
    
    $("#discharge").select2({
         ajax: {
            
            url: function() { return '{client_url}ekspor/find_discharge/?idnegara='+$("#iso").val() },
            dataType: 'json',
            delay: 250,
            processResults: function (data, params) {
              return {
                    results: $.map(data, function (item) {
                        return {
                       id: item.iddischarge,
                       text: item.uraian + ' [' + item.kode + ']'
                        };
                    })
              };
            },
            cache: true
          },
          minimumInputLength: 3,
          theme: "bootstrap",
          allowClear: true
    }).on('select2:select', function(e) { $(this).focus(); });

    $("#discharge").change(function(){
        $("#discharge_name").val($("#discharge option:selected").text())
    });
    
    });
</script>