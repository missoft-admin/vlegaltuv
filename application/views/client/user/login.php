﻿<!DOCTYPE html>
<html lang="en">
<head>        
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <!--[if gt IE 8]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <![endif]-->
    
    <title>Client Login - V-Legal Indonesia TÜV Rheinland Indonesia </title>

    <link rel="icon" type="image/ico" href="{images_path}tuvicon.ico"/>
    
    <link href="{css_path}stylesheets.css" rel="stylesheet" type="text/css" />
    <!--[if lt IE 8]>
        <link href="{css_path}ie7.css" rel="stylesheet" type="text/css" />
    <![endif]-->    
    
    <script type='text/javascript' src='{js_path}plugins/jquery/jquery-1.10.2.min.js'></script>
    <script type='text/javascript' src='{js_path}plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='{js_path}plugins/jquery/jquery-migrate-1.2.1.min.js'></script>
    <script type='text/javascript' src='{js_path}plugins/jquery/jquery.mousewheel.min.js'></script>
    
    <script type='text/javascript' src='{js_path}plugins/cookie/jquery.cookies.2.2.0.min.js'></script>
    
    <script type='text/javascript' src='{js_path}plugins/bootstrap.min.js'></script>
        
    <script type='text/javascript' src='{js_path}plugins/sparklines/jquery.sparkline.min.js'></script>
    
    <script type='text/javascript' src='{js_path}plugins/uniform/uniform.js'></script>
    
    
    <script type='text/javascript' src='{js_path}plugins/validation/languages/jquery.validationEngine-id.js' charset='utf-8'></script>
    <script type='text/javascript' src='{js_path}plugins/validation/jquery.validationEngine.js' charset='utf-8'></script>
    
    <script type='text/javascript' src='{js_path}plugins/scrollup/jquery.scrollUp.min.js'></script>
    
    <script type='text/javascript' src='{js_path}cookies.js'></script>
    <script type='text/javascript'>
        $(document).ready(function(){
            $("#frmTransaksi").validationEngine({promptPosition : "topLeft", scroll: true});        
        });
    </script>
</head>
<body>
    
    <div class="loginBlock" id="login" style="display: block;background-color:#F9F9F9;text-align:center">
        <img src="{images_path}logo_tuv.png" title="PT TÜV Rheinland Indonesia" alt="PT TÜV Rheinland Indonesia"/>
        <h1>V-Legal - Client Login</h1>
        <div class="loginForm">
            <form class="form-horizontal" action="{site_url}client/user/do_login" method="POST" id="frmTransaksi">
                <div class="control-group">
                    <div class="input-prepend">
                        <span class="add-on"><span class="icon-user"></span></span>
                        <input type="text" id="username" name="username" placeholder="Username" class="validate[required]"/>
                    </div>                
                </div>
                <div class="control-group">
                    <div class="input-prepend">
                        <span class="add-on"><span class="icon-lock"></span></span>
                        <input type="password" id="passkey" name="passkey" placeholder="Password" class="validate[required]"/>
                    </div>
                </div>
                <div class="row-fluid">
                    <div style="text-align:center" class="span12">
                        <div ><span style="color:red; font-weight:bold">{error}</span></div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span4">
                        <button type="submit" class="btn btn-block">Login</button>       
                    </div>
                </div>
            </form>  
        </div>
    </div>    
       
</html>
