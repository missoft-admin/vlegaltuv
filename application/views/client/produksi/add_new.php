<div class="page-header">
	<h1>{first_title} <small>{second_title}</small></h1>
</div>  
<?php echo error_success($this->session)?>
<?php if($error != '') echo error_message($error)?>
<div class="row-fluid">
	<div class="span12">
		<div class="head">
			<div class="isw-documents"></div>
			<h1>Form Realisasi Produksi</h1>
			<ul class="buttons">
				<li><a href="{client_url}produksi" class="isw-list" title="List Realisasi Produksi"></a></li>                                                        
			</ul>
			<div class="clear"></div>
		</div>
		<div class="block-fluid">                        
			<?php echo form_open('client/produksi/save','id = "frmTransaksi"') ?>
			<div class="row-form">
				<div class="span2">Tanggal Produksi:</div>
				<div class="span4"><input class="<?= ($final == 0)? 'datepickertxt' : '' ?> validate[required]" type="text" name="production_date" required placeholder="Periode Produksi" value="{production_date}"/></div>
                <div class="clear"></div>
            </div> 
            
            <div class="row-form">
				<div class="span2">Produk:</div>
				<div class="span10">
					<select name="idproduk" id="idproduk" data-placeholder="Produk">
						<option value="{idproduk}">{nama_produk}</option>
						
						
					</select>
				</div>	
				<div class="clear"></div>
			</div>
            <div class="row-form lokal">
				<div class="span2">Kode HS:</div>
				<div class="span2">
					<input type="text" readonly id="kodehs" value="{kodehs}" name="kodehs"/>
				</div>	
				<div class="clear"></div>
			</div>
            <div class="row-form lokal">
				<div class="span2">Kapasitas Produksi:</div>
				<div class="span2">
					<input type="text" readonly id="kapasitas_produksi" name="kapasitas_produksi" value="{kapasitas_produksi}" class="angka_std"/>
				</div>	
                <div class="span2">Saldo Kapasitas Produksi:</div>
				<div class="span2">
					<input type="text" readonly id="saldo_kapasitas" name="saldo_kapasitas" value="{saldo_kapasitas}" class="angka_std"/>
				</div>	
				<div class="clear"></div>
			</div>
            <div class="row-form lokal">
				<div class="span2">Jumlah Produksi:</div>
				<div class="span2">
					<input type="text" id="jumlah_produksi" name="jumlah_produksi" value="{jumlah_produksi}" style="width:70%" class="angka_std"/> Pcs
				</div>	
                <div class="span2">Volume Produksi:</div>
				<div class="span2">
					<input type="text" id="volume_produksi" name="volume_produksi" value="{volume_produksi}" style="width:70%" class="angka"/> m3
				</div>	
                <div class="span2">Berat Produksi:</div>
				<div class="span2">
					<input type="text" id="berat_produksi" name="berat_produksi" value="{berat_produksi}" style="width:70%" class="angka"/> kg
				</div>	
				<div class="clear"></div>
			</div>
            <div class="row-form lokal">
				<div class="span2">Rendemen:</div>
				<div class="span2">&nbsp;</div>	
                <div class="span2">Volume:</div>
				<div class="span2">
					<input type="text" id="rendemen_volume" readonly name="rendemen_volume" value="{rendemen_volume}" style="width:70%" class="angka"/> %
				</div>	
                <div class="span2">Berat:</div>
				<div class="span2">
					<input type="text" id="rendemen_berat" readonly name="rendemen_berat" value="{rendemen_berat}" style="width:70%" class="angka"/> %
				</div>	
				<div class="clear"></div>
			</div>
            <div class="row-form lokal">
				<div class="span2">Asal Kayu (Supplier):</div>
				<div class="span10">
                    
					<select  id="idsupplier" name="idsupplier[]" multiple data-placeholder="Supplier">
                         <?php foreach($detail_list_sup as $row) { ?>
                                   <option value="<?= $row->idsupplier ?>" selected><?= $row->supplier ?></option> 
                         <?php } ?>
                    </select>
				</div>	
				<div class="clear"></div>
			</div>
            <div class="row-form">
                <div class="span12" style="text-align:center"><h4>Data Detail Produksi</h4></div>
                <div class="clear"></div>
            </div>
			<div class="row-form" style="padding:0px;">
				<div class="span12">
                    <div class="block-fluid wrap-table" style="height:400px;overflow:auto;">
                        <table cellpadding="0" cellspacing="0" width="100%" class="table" id="detail_list">
                            <thead>
                                <tr>
                                    <th width="5%">No.</th>
                                    <th width="20%">Sortimen</th>
                                    <th width="20%">Jenis Kayu</th>
                                    <th width="10%">Jumlah</th>
                                    <th width="10%">Volume</th>
                                    <th width="10%">Berat</th>
                                    <th width="7%">Jumlah WIP</th>
                                    <th width="7%">Volume WIP</th>
                                    <th width="7%">Berat <br/>WIP</th>
                                    <?php if($final == 0){ ?>
                                    <th width="10%">Action</th>
                                    <?php } ?>
                                </tr>
                                <?php if($final == 0){ ?>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th width="20%" style="vertical-align:middle" >
                                        <input type="hidden" class="detail" id="rowindex"/>
                                        <input type="hidden"  class="detail" id="nomor" />
                                        <input type="hidden"  class="detail" id="stok_jumlah" />
                                        <input type="hidden"  class="detail" id="stok_volume" />
                                        <input type="hidden"  class="detail" id="stok_berat" />
                                        <select name="idsortimen" id="idsortimen" data-placeholder="Sortimen" class="detail">
                                            <option value="">Pilih Sortimen</option>
                                        </select>

                                    </th>
                                    <th  style="vertical-align:middle">
                                        <select id="idkayu" data-placeholder="Jenis Kayu" class="detail">
                                            <option value="">Pilih Jenis Kayu</option>
                                        </select></th>
                                    <th  style="vertical-align:middle"><input style="width:100%"  type="text" class="detail angka" id="jumlah"></th>
                                    <th  style="vertical-align:middle"><input style="width:100%"  type="text" class="detail angka" id="volume"></th>
                                    <th  style="vertical-align:middle"><input style="width:100%"  type="text" class="detail angka" id="berat"></th>
                                    <th  style="vertical-align:middle"><input style="width:100%"  type="text" class="detail angka" id="jumlah_wip"></th>
                                    <th  style="vertical-align:middle"><input style="width:100%"  type="text" class="detail angka" id="volume_wip"></th>
                                    <th  style="vertical-align:middle"><input style="width:100%"  type="text" class="detail angka" id="berat_wip"></th>
                                    <th><button class="btn" id="cmdAdd" type="button">Add</button>&nbsp;&nbsp;<button class="btn btn-danger" id="cmdClear" type="button">Clear</button></th>
                                </tr>
                                <?php } ?>
                            </thead>
                            <tbody>
                                <?php echo $detail_value_html ?>
                                <?php $no = 1; ?>
                                <?php foreach($detail_list as $row) { ?>
                                    <tr>
                              <!-- 0 --><td><?= $no++; ?></td>
                              <!-- 1 --><td style="display:none"><?= $row->idsortimen ?></td>
                              <!-- 2 --><td><?= $row->sortimen ?></td>
                              <!-- 3 --><td style="display:none"><?= $row->idkayu ?></td>
                              <!-- 4 --><td><?= $row->nama_kayu ?></td>
                              <!-- 5 --><td style="display:none"><?= $row->jumlah ?></td>
                              <!-- 6 --><td class="angka_det"><?= $row->jumlah ?></td>
                              <!-- 7 --><td style="display:none"><?= $row->volume ?></td>
                              <!-- 8 --><td class="angka_det"><?= $row->volume ?></td>
                              <!-- 9 --><td style="display:none"><?= $row->berat ?></td>
                              <!-- 10 --><td class="angka_det"><?= $row->berat ?></td>
                              <!-- 11 --><td style="display:none"><?= $row->jumlah_wip ?></td>
                              <!-- 12--><td class="angka_det"><?= $row->jumlah_wip ?></td>
                              <!-- 13--><td style="display:none"><?= $row->volume_wip ?></td>
                              <!-- 14--><td><?= $row->volume_wip ?></td>
                              <!-- 15--><td style="display:none"><?= $row->berat_wip ?></td>
                              <!-- 16--><td><?= $row->berat_wip ?></td>
                              <!-- 17--><td style="display:none"><?= $row->stok_jumlah ?></td>
                              <!-- 18--><td style="display:none"><?= $row->stok_volume ?></td>
                              <!-- 19--><td style="display:none"><?= $row->stok_berat ?></td>
                                        <?php if($final == 0){ ?>
                                        <td >
                                            <a href="#" class="edit"><i class="icon-pencil"></i></a>&nbsp;&nbsp;<a href="#" class="hapus"><i class="icon-remove"></i></a> 
                                        </td>
                                        <?php } ?>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
				<div class="clear"></div>
			</div> 
            <?php if($final == 0){ ?>
            <div class="row-form">
				<div class="span12"><button class="btn" type="button" id="btnsimpan">Simpan</button></div>
				<div class="clear"></div>
			</div> 
            <?php } ?>
			<input type="hidden" id="detail_value" name="detail_value"/>
			<input type="hidden" id="detail_value_html" name="detail_value_html"/>
			<input type="hidden" id="detail_list_sup" name="detail_list_sup"/>
			<input type="hidden" id="nama_produk" name="nama_produk" value="{nama_produk}"/>
			<input type="hidden" id="kapasitas_id" name="kapasitas_id" value="{kapasitas_id}"/>
			<input type="hidden" id="final" name="final" value="{final}"/>
			<?php echo form_hidden('idproduksi', $idproduksi); ?>
			<?php echo form_close() ?>
		</div>
	</div>
	
</div>
<script src="{js_path}jquery.floatThead.js" type="text/javascript"></script>
<script type="text/javascript">

$(document).ready(function(){
     <?php if($final == 1){ ?>
        $("input,select").prop( "disabled", true );
    <?php }?>
    var $table = $('table#detail_list');
    
    // $table.floatThead({
        // position: 'absolute',
        // scrollContainer: true
    // });
    
    $table.floatThead({
        zIndex : 0,
        scrollContainer: function($table){
            return $table.closest('.wrap-table');
        }
    });
    
    //$table.floatThead();

    $('.angka').number(true,3,',','.');
    $('.angka_det').number(true,3,',','.');
    $('.angka_std').number(true,0,',','.');
    hitung_rendemen();
    
    $("input").bind("keypress", function(e) {
        if (e.keyCode == 13){
            return false;
        }
	});
    
    
    
    $("#idsortimen").select2({
         ajax: {
            url: '{client_url}produksi/find_sortimen',
            dataType: 'json',
            delay: 250,
            processResults: function (data, params) {
              return {
                    results: $.map(data, function (item) {
                        return {
                       id: item.idsortimen,
                       text: item.sortimen
                        };
                    })
              };
            },
            cache: true
          },
          minimumInputLength: 3,
          theme: "bootstrap",
          allowClear: true
    }).on('select2:select', function(e) { $(this).focus(); });
    
    
    $("#idsortimen").change(function(){
        $('#idkayu').find('option').remove().end();
        
    });
    
    $("#idkayu").select2({
         ajax: {
            url: function() { return '{client_url}produksi/find_jenis_kayu_bahan_baku/' + $("#idsortimen").val() },
            dataType: 'json',
            delay: 250,
            processResults: function (data, params) {
              return {
                    results: $.map(data, function (item) {
                        return {
                       id: item.idkayu,
                       text: item.nama
                        };
                    })
              };
            },
            cache: true
          },
          minimumInputLength: 3,
          theme: "bootstrap",
          allowClear: true
    }).on('select2:select', function(e) { $(this).focus(); });
    
    $("#idkayu,#idsortimen").change(function(){
        $.ajax({
          url: '{client_url}produksi/find_stok_bahan_baku/'+ $("#idsortimen").val() +'/'+$("#idkayu").val(),
          dataType: 'json',
          success: function(data) {
            
            if(data != null){
                console.log(data);
                $("#stok_jumlah").val(data.jumlah);
                $("#stok_volume").val(data.volume);
                $("#stok_berat").val(data.berat);
            }
          }
        });
    });
    
    
    
    $("#idsupplier").select2({
         ajax: {
            url: '{client_url}produksi/find_supplier',
            dataType: 'json',
            delay: 250,
            processResults: function (data, params) {
              return {
                    results: $.map(data, function (item) {
                        return {
                       id: item.idsupplier,
                       text: item.supplier
                        };
                    })
              };
            },
            cache: true
          },
          minimumInputLength: 3,
          theme: "bootstrap",
          allowClear: true,
    }).on('select2:select', function(e) { $(this).focus(); });
    
    $("#idproduk").select2({
         ajax: {
            url: '{client_url}produksi/find_produk',
            dataType: 'json',
            delay: 250,
            processResults: function (data, params) {
              return {
                    results: $.map(data, function (item) {
                        return {
                       id: item.idproduk,
                       text: item.produk
                        };
                    })
              };
            },
            cache: true
          },
          minimumInputLength: 3,
          theme: "bootstrap",
          allowClear: true
    }).on('select2:select', function(e) { $(this).focus(); }).on('select2:unselecting', function(e) { $("#kodehs").val('') });
    
    
    
    $("#idproduk").change(function(){
        $("#nama_produk").val($("#idproduk option:selected").text());
        $.ajax({
          url: '{client_url}produksi/find_produk_hscode/'+$(this).val(),
          dataType: 'json',
          success: function(data) {
            $("#kodehs").val(data.kodehs)
            $("#ikp_id").val(data.ikp_id)
            $("#kapasitas_produksi").val(data.kapasitas_produksi)
            $("#saldo_kapasitas").val(data.saldo_kapasitas)
          }
        });
    });
    
    
    $("#cmdAdd").click(function(){
        var valid = validate_detail();
        if(!valid) return false;
                
        var duplicate = false;
        
        if($("#nomor").val() != ''){
            var no = $("#nomor").val();
            var content = "";
        }else{
            var no = $('#detail_list tr').length;
            var content = "<tr>";
            
            $('#detail_list tbody tr').filter(function (){
                var $cells = $(this).children('td');
                if($cells.eq(1).text() === $("#idsortimen").val() && $cells.eq(3).text() === $("#idkayu").val()){
                    big_notification("Sortimen " + $("#idsortimen option:selected").text() +" dengan Jenis Kayu " + $("#idkayu option:selected").text() + " sudah ada didalam daftar di baris ke " + $cells.eq(0).text(),"Error");
                    duplicate = true;
                }
            });	
        }
        
        if(duplicate) return false;
        content += "<td>" + no + "</td>"; /* 0 */
        content += "<td style='display:none'>" + $("#idsortimen").val(); + "</td>"; /* 1 */
        content += "<td>" + $("#idsortimen option:selected").text(); + "</td>"; /* 2 */
        content += "<td style='display:none'>" + $("#idkayu").val(); + "</td>"; /* 3 */
        content += "<td>" + $("#idkayu option:selected").text(); + "</td>"; /* 4 */
        content += "<td style='display:none'>" + $("#jumlah").val(); + "</td>"; /* 5 */
        content += "<td class='angka_det'>" + $("#jumlah").val(); + "</td>"; /* 6 */
        content += "<td style='display:none'>" + $("#volume").val(); + "</td>"; /* 7 */
        content += "<td class='angka_det'>" + $("#volume").val(); + "</td>"; /* 8 */
        content += "<td style='display:none'>" + $("#berat").val(); + "</td>"; /* 9 */
        content += "<td class='angka_det'>" + $("#berat").val(); + "</td>"; /* 10 */
        content += "<td style='display:none'>" + $("#jumlah_wip").val(); + "</td>"; /* 11 */
        content += "<td class='angka_det'>" + $("#jumlah_wip").val(); + "</td>"; /* 12 */
        content += "<td style='display:none'>" + $("#volume_wip").val(); + "</td>"; /* 13 */
        content += "<td class='angka_det'>" + $("#volume_wip").val(); + "</td>"; /* 14 */
        content += "<td style='display:none'>" + $("#berat_wip").val(); + "</td>"; /* 15 */
        content += "<td class='angka_det'>" + $("#berat_wip").val(); + "</td>"; /* 16 */
        content += "<td style='display:none'>" + $("#stok_jumlah").val(); + "</td>"; /* 17 */
        content += "<td style='display:none'>" + $("#stok_volume").val(); + "</td>"; /* 18 */
        content += "<td style='display:none'>" + $("#stok_berat").val(); + "</td>"; /* 19 */
        content += "<td width='5%'><a href='#' class='edit'><i class='icon-pencil'></i></a>&nbsp;&nbsp;<a href='#' class='hapus'><i class='icon-remove'></i></a> </td>";
        
        
        if($("#rowindex").val() != ''){
            $('#detail_list tbody tr:eq(' + $("#rowindex").val() + ')').html(content);
        }else{
            content += "</tr>";
            $('#detail_list tbody').append(content);
        }

        $("#cmdClear").trigger('click');
        $('.angka_det').number(true,3,',','.');
        $table.floatThead("reflow");
        hitung_rendemen();
    });
   
    
    function validate_detail()
	{
		if($("#idsortimen").val() == ""){
			big_notification("Sortimen Belum Dipilih","error");
			$("#idsortimen").select2("open");
			return false;
		}
		
		if($("#jumlah").val() == '' && $("#volume").val() == '' && $("#berat").val() == ''){
			big_notification("Jumlah atau Volume atau Berat Bahan Baku Harus Diisi","error");
			$("#jumlah").focus();
			return false;
		}
        
		if($("#jumlah_wip").val() == '' && $("#volume_wip").val() == '' && $("#berat_wip").val() == ''){
			big_notification("Jumlah WIP atau Volume WIP atau Berat WIP Bahan Baku Harus Diisi","error");
			$("#jumlah_wip").focus();
			return false;
		}
        
        
		
		return true;
	}
    
    $(document).on("click",".edit",function(){
			
        $("#rowindex").val($(this).closest('tr')[0].sectionRowIndex);
        $("#nomor").val($(this).closest('tr').find("td:eq(0)").html());
        
        $("#jumlah").val($(this).closest('tr').find("td:eq(5)").html());
        $("#volume").val($(this).closest('tr').find("td:eq(7)").html());
        $("#berat").val($(this).closest('tr').find("td:eq(9)").html());
        $("#jumlah_wip").val($(this).closest('tr').find("td:eq(11)").html());
        $("#volume_wip").val($(this).closest('tr').find("td:eq(13)").html());
        $("#berat_wip").val($(this).closest('tr').find("td:eq(15)").html());
        $("#stok_jumlah").val($(this).closest('tr').find("td:eq(17)").html());
        $("#stok_volume").val($(this).closest('tr').find("td:eq(18)").html());
        $("#stok_berat").val($(this).closest('tr').find("td:eq(19)").html());
        
        var idsortimen = $(this).closest('tr').find("td:eq(1)").html();
        var nama_sortimen = $(this).closest('tr').find("td:eq(2)").html();
        
        var idkayu = $(this).closest('tr').find("td:eq(3)").html();
        var nama_jenis = $(this).closest('tr').find("td:eq(4)").html();
        
        $('#idsortimen').find('option').remove().end()
        .append('<option value="">- Pilih Jenis Sortimen -</option><option value="'+ idsortimen +'">' + nama_sortimen + '</option>')
        .val(idsortimen)
        .trigger("liszt:updated");
        
        $('#idkayu').find('option').remove().end()
        .append('<option value="">- Pilih Jenis Kayu -</option><option value="'+ idkayu +'">' + nama_jenis + '</option>')
        .val(idkayu)
        .trigger("liszt:updated");
    
        $("#dok_jumlah").focus();
        return false;
    });
    
    $(document).on("click",".hapus",function(){
        if(confirm("Hapus Data ?") == true){
            $(this).closest('td').parent().remove();
            $("#idkayu").trigger("liszt:activate");
            $("#idsortimen").trigger("liszt:activate");
        }
        renumbering();
        hitung_rendemen();
        return false;
    });
   
    $("#cmdClear").click(function(){
        $(".detail").val('');
        $('#idkayu').find('option').remove().end().append('<option value="">- Pilih Jenis Kayu -</option>').val('').trigger("liszt:updated");
        $('#idsortimen').find('option').remove().end().append('<option value="">- Pilih Jenis Sortimen -</option>').val('').trigger("liszt:updated");
        $("#idkayu").trigger("liszt:activate");
    });
    
    function renumbering(){
        $('#detail_list tbody tr').each(function(index, element) {
           $(this).find('td:eq(0)').text(element.sectionRowIndex + 1);		
        });
    }
    
    $("#volume_produksi,#berat_produksi").change(function(){
        hitung_rendemen();
    });
    
    function hitung_rendemen(){
        var total_volume    = parseFloat($("#volume_produksi").val());
        var total_berat     = parseFloat($("#berat_produksi").val());
        
        var produksi_volume    = 0;
        var produksi_berat     = 0;
        
        $('#detail_list tbody tr').each(function(index, element) {
            produksi_volume += parseFloat($(this).find('td:eq(7)').text());
            produksi_berat += parseFloat($(this).find('td:eq(9)').text());
        });
        
        var rendemen_volume = (total_volume / produksi_volume) * 100;
        var rendemen_berat  = (total_berat / produksi_berat) * 100;
        
        $("#rendemen_volume").val(rendemen_volume);
        $("#rendemen_berat").val(rendemen_berat);
        
        if(rendemen_volume >= 60 && rendemen_volume <=70){
            $("#rendemen_volume").css('background-color','#adff2f');
        }else if(rendemen_volume < 60){
            $("#rendemen_volume").css('background-color','#ffff00');
        }else if(rendemen_volume > 70){
            $("#rendemen_volume").css('background-color','#ff0000');
        }
        
        if(rendemen_berat >= 60 && rendemen_berat <=70){
            $("#rendemen_berat").css('background-color','#adff2f');
        }else if(rendemen_berat < 60){
            $("#rendemen_berat").css('background-color','#ffff00');
        }else if(rendemen_berat > 70){
            $("#rendemen_berat").css('background-color','#ff0000');
        }
    }
    
    $(document).on("click","#btnsimpan",function(){
        var rowCount = $('table#detail_list tr').length;
        if(rowCount <= 1){
           big_notification("Data Detail Penerimaan Masih Kosong!",'error');
           return false;
        }else{
            var tbl = $('table#detail_list tbody tr').get().map(function(row) {
              return $(row).find('td').get().map(function(cell) {
                return $(cell).html();
              });
            });
            $("#detail_value").val(JSON.stringify(tbl));
            $("#detail_value_html").val($('table#detail_list tbody').html());
            var supplier_list = [];
            $.each( $("#idsupplier").select2("data"), function( key, value ) {
                supplier_list.push({idsupplier: value.id, supplier: value.text});
            });
            $("#detail_list_sup").val(JSON.stringify(supplier_list));
            $("#frmTransaksi").submit();
        }
    });
    
    

  
    
    /*
    $(document).on("click","#cmdSave",function(){
        var tbl = $('table#detail_list tbody tr').get().map(function(row) {
          return $(row).find('td').get().map(function(cell) {
            return $(cell).html();
          });
        });
        //console.log(tbl);
        $("#detail_value").val(JSON.stringify(tbl));
        $("#frmTransaksi").submit();
    });*/
});
</script>