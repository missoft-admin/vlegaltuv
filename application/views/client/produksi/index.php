<div class="page-header">
	<h1>{first_title} <small>{second_title}</small></h1>
</div>  
<?php echo error_success($this->session)?>
<div class="row-fluid">
	<div class="span12">                    
		<div class="head">
			<div class="isw-grid"></div><h1> {table_title}</h1>
			<ul class="buttons">
				<li>
					<a href="#" class="isw-settings"></a>
					<ul class="dd-list">
						<li><a href="{client_url}produksi/add"><span class="isw-plus"></span> Add Produksi</a></li>
					</ul>
				</li>
			</ul>                        
			<div class="clear"></div>
		</div>
		<div class="block-fluid table-sorting">
			<div class="dataTables_wrapper">
            <div class="dataTables_filter" id="tSortable_filter" style="width:auto;">
				<form action="{client_url}produksi/search" method="post" style="margin-bottom:0;">
				<label>Search: <input type="text" style="width:300px;" value="{searchText}" name="searchText" placeholder="Search Produksi By Supplier, Jenis Kayu, Sortimen"></label>
				</form>
			</div>
			<table cellpadding="0" cellspacing="0" width="100%" class="table">
				<thead>
					<tr>                                    
						<th width="5%">No.</th>
						<th width="10%">Tanggal</th>
						<th width="10%">Produk</th>
						<th width="10%">Kode HS</th>
						<th width="10%">Jumlah Produksi</th>
						<th width="10%">Volume Produksi</th>
                        <th width="10%">Sortimen</th>
                        <th width="20%">Jenis Kayu</th>
						<!--<th width="10%">Asal Kayu</th>-->
                        <th width="15%">Option</th>                                                 
					</tr>
				</thead>
				<tbody>
					<?php foreach($query as $row){ ?>
					<?php $no = $no + 1 ?>
					<tr>                                    
						<td><?= $no?></td>
						<td><?php echo human_date_short($row->production_date) ?></td>
						<td><?php echo $row->nama_produk ?></td>							
						<td><?php echo $row->kodehs ?></td>
						<td><?php echo $row->jumlah_produksi ?></td>
						<td><?php echo $row->volume_produksi ?></td>
                        <td><?php echo $row->nama_sortimen ?></td>
						<td><?php echo $row->nama_kayu ?></td>
						<!--<td><?php echo $row->nama_supplier ?></td>-->
						<td>
							<?php 
                                if($row->final == 0){
                                    echo tool_edit('client/produksi/edit/'.$row->idproduksi);
                                    echo tool_remove('client/produksi/delete/'.$row->idproduksi);								
                                    echo tool_finalize('client/produksi/finalize/'.$row->idproduksi);								
                                }else{
                                    echo tool_finalized('client/produksi/edit/'.$row->idproduksi,"Finalize By $row->finalize_user_name at ".human_date_time($row->finalize_date));										
                                }
							?>
						</td>                                    
					</tr>                                
					<?php } ?>
				</tbody>
			</table>
			<?php echo $pagination ?>
			
		</div>
		<div class="clear"></div>
	</div>                                
</div>