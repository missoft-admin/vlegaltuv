<div class="page-header">
	<h1>{first_title} <small>{second_title}</small></h1>
</div>  
<?php echo error_success($this->session)?>
<?php if($error != '') echo error_message($error)?>
<div class="row-fluid">
	<div class="span12">
		<div class="head">
			<div class="isw-documents"></div>
			<h1>BUYER FORM</h1>
			<ul class="buttons">
				<li><a href="{client_url}buyer" class="isw-list" title="Buyer List"></a></li>                                                        
			</ul>
			<div class="clear"></div>
		</div>
		<div class="block-fluid">                        
			<?php echo form_open('client/buyer/save','id = "adminform"') ?>
			<div class="row-form">
				<div class="span2">Negara:</div>
				<div class="span10">
					<select name="idnegara">
						<option value="0">Pilih Negara</option>
						<?php foreach ($negara_list as $row){ ?>
							<option <?=($row->idnegara==$idnegara ? 'selected':'') ?> value=<?=$row->idnegara ?>><?=$row->negara ?></option>
						<?php } ?>
						
					</select>
				</div>				
				<div class="clear"></div>
			</div> 
			<div class="row-form">
				<div class="span2">Nama Buyer:</div>
				<div class="span10"><input type="text" name="buyer" placeholder="Nama Buyer" value="{buyer}"/></div>
				<div class="clear"></div>
			</div> 
			<div class="row-form">
				<div class="span2">Alamat:</div>
				<div class="span10"><input type="text" name="alamat" placeholder="Alamat Buyer" value="{alamat}"/></div>
				<div class="clear"></div>
			</div>
			<div class="row-form">
				<div class="span2">Status:</div>
				<div class="span10"><input type="checkbox" name="status" value="{status}"  checked>Active</div>
				<div class="clear"></div>
			</div>			
			<div class="row-form">
				<div class="span12"><button class="btn" type="submit" id="btnsimpan">Simpan</button></div>
				<div class="clear"></div>
			</div> 
			
			<input type="hidden" name="client_id" id="client_id" value="{client_id}"/>
			<input type="hidden" name="ket" id="ket" value="{ket}"/>
			<input type="hidden" name="negara" id="negara" value="{negara}"/>
			<?php echo form_hidden('idbuyer', $idbuyer); ?>
			<?php echo form_close() ?>
		</div>
	</div>
	
</div>
