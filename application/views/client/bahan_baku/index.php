<div class="page-header">
	<h1>{first_title} <small>{second_title}</small></h1>
</div>  
<?php echo error_success($this->session)?>
<div class="row-fluid">
	<div class="span12">                    
		<div class="head">
			<div class="isw-grid"></div><h1> {table_title}</h1>
			<div class="clear"></div>
		</div>
		<div class="block-fluid table-sorting">
			<div class="dataTables_wrapper">
            <div class="dataTables_filter" id="tSortable_filter" style="width:auto;">
				<form action="{client_url}bahan_baku/search" method="post" style="margin-bottom:0;">
				<label>Search: <input type="text" style="width:300px;" value="{searchText}" name="searchText" placeholder="Search Stok Bahan Baku "></label>
				</form>
			</div>
			<table cellpadding="0" cellspacing="0" width="100%" class="table">
				<thead>
					<tr>                                    
						<th width="5%">No.</th>
						<th width="20%">Nama Sortimen</th>
						<th width="20%">Jenis Kayu</th>
						<th width="10%">Jumlah</th>
						<th width="10%">Volume</th>
						<th width="10%">Berat</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($query as $row){ ?>
					<?php $no = $no + 1 ?>
					<tr>                                    
						<td><?= $no?></td>
						<td><?php echo $row->sortimen ?></td>							
						<td><?php echo $row->nama_jenis_kayu ?></td>							
						<td><?php echo format_angka_id($row->jumlah,3) ?></td>							
						<td><?php echo format_angka_id($row->volume,3) ?></td>							
						<td><?php echo format_angka_id($row->berat,3) ?></td>							
					</tr>                                
					<?php } ?>
				</tbody>
			</table>
			<?php echo $pagination ?>
			
		</div>
		<div class="clear"></div>
	</div>                                
</div>