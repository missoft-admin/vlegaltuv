<div class="page-header">
	<h1>{first_title} <small>{second_title}</small></h1>
</div>  
<?php echo error_success($this->session)?>
<?php if($error != '') echo error_message($error)?>
<div class="row-fluid">
	<div class="span12">
		<div class="head">
			<div class="isw-documents"></div>
			<h1>Form Pengeluaran Bahan Baku</h1>
			<ul class="buttons">
				<li><a href="{client_url}pengeluaran" class="isw-list" title="List Pengeluaran"></a></li>                                                        
			</ul>
			<div class="clear"></div>
		</div>
		<div class="block-fluid">                        
			<?php echo form_open('client/pengeluaran/save','id = "frmTransaksi"') ?>
			<div class="row-form">
				<div class="span2">Tanggal:</div>
				<div class="span3"><input tabindex="1" class="<?= ($final == 0)? 'datepickertxt' : '' ?> validate[required]" type="text" name="periode_awal" placeholder="Tanggal" value="{periode_awal}"/></div>
				<div class="clear"></div>
			</div> 
            <div class="row-form">
				<div class="span2">Type Pengeluaran:</div>
				<div class="span4">
                    <label class="checkbox inline">
                        <input class="validate[required]" tabindex="2"  type="radio" id="lokal" value="LOKAL" name="type_pengeluaran" <?= (strtoupper($type_pengeluaran) == "LOKAL")? 'checked="checked"' : "" ?>/> LOKAL
                    </label>
                    <label class="checkbox inline">
                        <input class="validate[required]" tabindex="2"  type="radio" id="import" value="IMPORT" name="type_pengeluaran" <?= (strtoupper($type_pengeluaran) == "IMPORT")? 'checked="checked"' : "" ?>/> IMPORT
                    </label>
                </div>
				<div class="clear"></div>
			</div> 
			<div class="row-form lokal">
				<div class="span2">Jenis Pengeluaran:</div>
				<div class="span4">
                    <select tabindex="3"  name="jenis_pengeluaran" class="validate[required]"/>
                        <option value="">- Pilih Jenis Pengeluaran -</option>
                        <?php foreach($jenis_pengeluaran_list as $row){ ?>
                            <option value="<?= $row->id ?>" <?= ($row->id == $jenis_pengeluaran)? "selected" : "" ?>><?= $row->deskripsi ?></option>
                        <?php } ?>
                    </select>
                </div>
				<div class="clear"></div>
			</div> 
            
            <div class="row-form">
                <div class="span12" style="text-align:center"><h4>Data Detail Pengeluaran</h4></div>
                <div class="clear"></div>
            </div>
			<div class="row-form" style="padding:0px;">
				<div class="span12">
                    <div class="block-fluid wrap-table" style="height:400px;overflow:auto;">
                        <table cellpadding="0" cellspacing="0" width="100%" class="table" id="detail_list">
                            <thead>
                               <tr>
                                    <th <?= ($final == 0)? 'rowspan="2"' : '' ?> width="5%">No.</th>
                                    <th width="20%">Sortimen</th>
                                    <th width="20%">Jenis Kayu</th>
                                    <th width="7%">Jumlah</th>
                                    <th width="9%">Satuan</th>
                                    <th width="7%">Volume</th>
                                    <th width="7%">Satuan</th>
                                    <th width="7%">Berat</th>		
                                    <th width="7%">Satuan</th> 
                                    <?php if($final == 0){ ?>
                                    <th width="5%" style="vertical-align:middle"><button tabindex="-1" class="btn btn-danger btn-small" id="cmdClear" type="button">Clear</button></th>                                                 
                                    <?php } ?>
                                </tr>
                                <?php if($final == 0){ ?>
                                <tr>
                                     <th>
                                        <input type="hidden" class="detail" id="rowindex"/>
                                        <input type="hidden"  class="detail" id="nomor"/>
                                        <select tabindex="4"  id="idsortimen" data-placeholder="Sortimen">
                                            <option value="">Pilih Sortimen</option>
                                        </select>
                                    </th>
                                    
                                    <th>
                                        <select id="idkayu" tabindex="5"  data-placeholder="Jenis Kayu">
                                            <option value="">Pilih Jenis Kayu</option>
                                        </select>
                                    </th>
                                    <th><input  tabindex="6"  style="width:100%" type="text" class="detail angka" id="jumlah"></th>                                        
                                    <th>
                                        <select style="width:100%" class="detail" id="sat_jumlah" tabindex="7" >
                                            <option value="">-</option>
                                            <?php foreach($satuan_jumlah as $row){?>
                                                <option value="<?= $row->satuan_id ?>"><?= $row->satuan_kode ?> </option>
                                            <?php }?>
                                        </select>
                                    </th>
                                    <th><input style="width:100%"  type="text" class="detail angka" id="volume" tabindex="8" ></th>                                        
                                    <th>
                                        <select style="width:100%" class="detail" id="sat_volume" tabindex="9" >
                                            <option value="">-</option>
                                            <?php foreach($satuan_volume as $row){?>
                                                <option value="<?= $row->satuan_id ?>"><?= $row->satuan_kode ?> </option>
                                            <?php }?>
                                        </select>
                                    </th>
                                    <th><input style="width:100%" type="text" class="detail angka" id="berat" tabindex="10" ></th>                                        
                                    <th style="border-right: 1px solid #DDD;">
                                        <select style="width:100%" class="detail" id="sat_berat" tabindex="11" >
                                            <option value="">-</option>
                                            <?php foreach($satuan_berat as $row){?>
                                                <option value="<?= $row->satuan_id ?>"><?= $row->satuan_kode ?> </option>
                                            <?php }?>
                                        </select>
                                    </th>
                                    <th width="5%" style="vertical-align:middle"><button tabindex="12"  class="btn" id="cmdAdd" type="button">Add</button></th>                                                 
                                </tr>
                                <?php } ?>
                            </thead>
                            <tbody>
                                <?php echo $detail_value_html ?>
                                <?php foreach($detail_list as $row) { ?>
                                    <tr>
                                        <!-- 0  --><td><?= $row->urutan ?></td>
                                        <!-- 1  --><td style="display:none"><?= $row->idsortimen ?></td>
                                        <!-- 2  --><td><?= $row->sortimen ?></td>
                                        <!-- 3  --><td style="display:none"><?= $row->idkayu ?></td>
                                        <!-- 4  --><td><?= $row->nama_kayu ?></td>
                                        <!-- 5  --><td style="display:none"><?= $row->jumlah ?></td>
                                        <!-- 6  --><td class="angka_det"><?= $row->jumlah ?></td>
                                        <!-- 7  --><td style="display:none"><?= $row->satuan_jumlah ?></td>
                                        <!-- 8  --><td><?= $row->sat_jumlah_kode ?></td>
                                        <!-- 9  --><td style="display:none"><?= $row->volume ?></td>
                                        <!-- 10 --><td class="angka_det"><?= $row->volume ?></td>
                                        <!-- 11 --><td style="display:none"><?= $row->satuan_volume ?></td>
                                        <!-- 12 --><td><?= $row->sat_volume_kode ?></td>
                                        <!-- 13 --><td style="display:none"><?= $row->berat ?></td>
                                        <!-- 14 --><td class="angka_det"><?= $row->berat ?></td>
                                        <!-- 15 --><td style="display:none"><?= $row->satuan_berat ?></td>
                                        <!-- 16 --><td><?= $row->sat_berat_kode ?></td>
                                        <?php if($final == 0){ ?>
                                        <td>
                                            <a href="#" class="edit"><i class="icon-pencil"></i></a>
                                            <a href="#" class="hapus"><i class="icon-remove"></i></a> 
                                        </td>
                                        <?php } ?>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
				<div class="clear"></div>
			</div> 
            <?php if($final == 0){ ?>
            <div class="row-form">
				<div class="span12"><button class="btn" type="button" id="btnsimpan">Simpan</button></div>
				<div class="clear"></div>
			</div> 
            <?php } ?>
			<input type="hidden" id="detail_value" name="detail_value"/>
            <input type="hidden" id="detail_value_html" name="detail_value_html"/>
            <input type="hidden" id="final" name="final" value="{final}"/>
			<?php echo form_hidden('idpengeluaran', $idpengeluaran); ?>
			<?php echo form_close() ?>
		</div>
	</div>
	
</div>

<script src="{js_path}jquery.floatThead.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
    <?php if($final == 1){ ?>
        $("input,select").prop( "disabled", true );
    <?php }?>
    var $table = $('table#detail_list');

    $table.floatThead({
        scrollContainer: function($table){
            return $table.closest('.wrap-table');
        }
    });
    
    $("input").bind("keypress", function(e) {
        if (e.keyCode == 13){
            return false;
        }
	});
    
    $('.angka').number(true,3,',','.');
    $('.angka_det').number(true,3,',','.');
    
    
    $("#idkayu").select2({
         ajax: {
            url: '{client_url}pengeluaran/find_jenis_kayu',
            dataType: 'json',
            delay: 250,
            processResults: function (data, params) {
              return {
                    results: $.map(data, function (item) {
                        return {
                       id: item.idkayu,
                       text: item.nama
                        };
                    })
              };
            },
            cache: true
          },
          minimumInputLength: 3,
          theme: "bootstrap",
          allowClear: true
    }).on('select2:select', function(e) { $(this).focus(); });
    
    $("#idsortimen").select2({
         ajax: {
            url: '{client_url}pengeluaran/find_sortimen',
            dataType: 'json',
            delay: 250,
            processResults: function (data, params) {
              return {
                    results: $.map(data, function (item) {
                        return {
                       id: item.idsortimen,
                       text: item.sortimen
                        };
                    })
              };
            },
            cache: true
          },
          minimumInputLength: 3,
          theme: "bootstrap",
          allowClear: true
    }).on('select2:select', function(e) { $(this).focus(); });
    
     
    $("#cmdAdd").click(function(){
        var valid = validate_detail();
        if(!valid) return false;
                
        //var duplicate = false;
        
        if($("#nomor").val() != ''){
            var no = $("#nomor").val();
            var content = "";
        }else{
            var no = $('#detail_list tr').length;
            var content = "<tr>";
            /*
            $('#detail_list tbody tr').filter(function (){
                var $cells = $(this).children('td');
                if($cells.eq(1).text() === $("#idkayu").val()){
                    alert("Jenis Kayu " + $("#idkayu option:selected").text() + " sudah ada didalam daftar di baris ke " + $cells.eq(0).text());
                    duplicate = true;
                }
            });	*/
        }
        
        //if(duplicate) return false;

        content += "<td width='5%'>" + no + "</td>"; /*  0  */
        content += "<td style='display:none'>" + $("#idsortimen").val(); + "</td>"; /*  1  */
        content += "<td width='10%'>" + $("#idsortimen option:selected").text(); + "</td>"; /*  2  */
        content += "<td style='display:none'>" + $("#idkayu").val(); + "</td>"; /*  3  */
        content += "<td width='10%'>" + $("#idkayu option:selected").text(); + "</td>"; /*  4  */
        content += "<td style='display:none'>" + $("#jumlah").val(); + "</td>"; /*  5  */
        content += "<td width='5%' class='angka_det'>" + $("#jumlah").val(); + "</td>"; /*  6  */
        content += "<td style='display:none'>" + $("#sat_jumlah").val(); + "</td>"; /*  7  */
        content += "<td width='5%'>" + $("#sat_jumlah option:selected").text(); + "</td>"; /*  8  */
        content += "<td style='display:none'>" + $("#volume").val(); + "</td>"; /*  9  */
        content += "<td width='5%' class='angka_det'>" + $("#volume").val(); + "</td>"; /*  10  */
        content += "<td style='display:none'>" + $("#sat_volume").val(); + "</td>"; /*  11  */
        content += "<td width='5%'>" + $("#sat_volume option:selected").text(); + "</td>"; /*  12  */
        content += "<td style='display:none'>" + $("#berat").val(); + "</td>"; /*  13 */
        content += "<td width='5%' class='angka_det'>" + $("#berat").val(); + "</td>";/*  14  */
        content += "<td style='display:none'>" + $("#sat_berat").val(); + "</td>"; /*  15  */
        content += "<td width='5%'>" + $("#sat_berat option:selected").text(); + "</td>"; /*  16  */
        content += "<td width='5%'><a href='#' class='edit'><i class='icon-pencil'></i></a>&nbsp;&nbsp;<a href='#' class='hapus'><i class='icon-remove'></i></a> </td>";
        
        
        if($("#rowindex").val() != ''){
            $('#detail_list tbody tr:eq(' + $("#rowindex").val() + ')').html(content);
        }else{
            content += "</tr>";
            $('#detail_list tbody').append(content);
        }

        $("#cmdClear").trigger('click');
        $('.angka_det').number(true,3,',','.');
        $table.floatThead("reflow");
        $("#idsortimen").select2("open");
    });
    
    function validate_detail()
	{
		if($("#idsortimen").val() == ""){
			big_notification("Sortimen Belum Dipilih","error");
            $("#idsortimen").select2("open");
			return false;
		}
        
        if($("#idkayu").val() == ""){
			big_notification("Jenis Kayu Belum Dipilih","error");
			 $("#idkayu").select2("open");
			return false;
		}
		
		if($("#jumlah").val() == "" && $("#volume").val() == "" && $("#berat").val() == ""){
			big_notification("Jumlah, Volume, dan Berat Pengeluaran Harus Diisi","error");
			$("#jumlah").focus();
			return false;
		}
        
        
		
		return true;
	}
    
    $(document).on("click",".edit",function(){
			
        $("#rowindex").val($(this).closest('tr')[0].sectionRowIndex);
        $("#nomor").val($(this).closest('tr').find("td:eq(0)").html());
        
        
        $("#jumlah").val($(this).closest('tr').find("td:eq(5)").html());
        $("#sat_jumlah").val($(this).closest('tr').find("td:eq(7)").html());
        $("#volume").val($(this).closest('tr').find("td:eq(9)").html());
        $("#sat_volume").val($(this).closest('tr').find("td:eq(11)").html());
        $("#berat").val($(this).closest('tr').find("td:eq(13)").html());
        $("#sat_berat").val($(this).closest('tr').find("td:eq(15)").html());
        
        var idsortimen = $(this).closest('tr').find("td:eq(1)").html();
        var nama_sortimen = $(this).closest('tr').find("td:eq(2)").html();
        
        var idkayu = $(this).closest('tr').find("td:eq(3)").html();
        var nama_jenis = $(this).closest('tr').find("td:eq(4)").html();
        
        $('#idsortimen').find('option').remove().end()
        .append('<option value="">- Pilih Jenis Sortimen -</option><option value="'+ idsortimen +'">' + nama_sortimen + '</option>')
        .val(idsortimen)
        .trigger("liszt:updated");
        
        $('#idkayu').find('option').remove().end()
        .append('<option value="">- Pilih Jenis Kayu -</option><option value="'+ idkayu +'">' + nama_jenis + '</option>')
        .val(idkayu)
        .trigger("liszt:updated");
    
        $("#jumlah").focus();
        return false;
    });
    $(document).on("click",".hapus",function(){
        if(confirm("Hapus Data ?") == true){
            $(this).closest('td').parent().remove();
            $("#idkayu").trigger("liszt:activate");
            $("#idsortimen").trigger("liszt:activate");
        }
        renumbering();
        return false;
    });
    $("#cmdClear").click(function(){
        $(".detail").val('');
        $('#idkayu').find('option').remove().end().append('<option value="">- Pilih Jenis Kayu -</option>').val('').trigger("liszt:updated");
        $('#idsortimen').find('option').remove().end().append('<option value="">- Pilih Jenis Sortimen -</option>').val('').trigger("liszt:updated");
        $("#idkayu").trigger("liszt:activate");
    });
    
    $(document).on("click","#btnsimpan",function(){
        var rowCount = $('table#detail_list tr').length;
        if(rowCount <= 1){
           big_notification("Data Detail Pengeluaran Masih Kosong!",'error');
           return false;
        }else{
            var tbl = $('table#detail_list tbody tr').get().map(function(row) {
              return $(row).find('td').get().map(function(cell) {
                return $(cell).html();
              });
            });
            $("#detail_value").val(JSON.stringify(tbl));
            $("#detail_value_html").val($('table#detail_list tbody').html());
            $("#frmTransaksi").submit();
        }
    });

    function renumbering(){
        $('#detail_list tbody tr').each(function(index, element) {
           $(this).find('td:eq(0)').text(element.sectionRowIndex + 1);		
        });
    }

    
    /*
    $(document).on("click","#cmdSave",function(){
        var tbl = $('table#detail_list tbody tr').get().map(function(row) {
          return $(row).find('td').get().map(function(cell) {
            return $(cell).html();
          });
        });
        //console.log(tbl);
        $("#detail_value").val(JSON.stringify(tbl));
        $("#frmTransaksi").submit();
    });*/
});
</script>