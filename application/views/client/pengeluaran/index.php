<div class="page-header">
	<h1>{first_title} <small>{second_title}</small></h1>
</div>  
<?php echo error_success($this->session)?>
<div class="row-fluid">
	<div class="span12">                    
		<div class="head">
			<div class="isw-grid"></div><h1> {table_title}</h1>
			<ul class="buttons">
				<li>
					<a href="#" class="isw-settings"></a>
					<ul class="dd-list">
						<li><a href="{client_url}pengeluaran/add"><span class="isw-plus"></span> Add Pengeluaran</a></li>
					</ul>
				</li>
			</ul>                        
			<div class="clear"></div>
		</div>
		<div class="block-fluid table-sorting">
			<div class="dataTables_wrapper">
            <div class="dataTables_filter" id="tSortable_filter" style="width:auto;">
				<form action="{client_url}pengeluaran/search" method="post" style="margin-bottom:0;">
				<label>Search: <input type="text" style="width:300px;" value="{searchText}" name="searchText" placeholder="Search Pengeluaran"></label>
				</form>
			</div>
			<table cellpadding="0" cellspacing="0" width="100%" class="table">
				<thead>
					<tr>                                    
						<th width="5%">No.</th>
						<th width="10%">Tanggal</th>
						<th width="10%">Jenis Pengeluaran</th>
						<th width="10%">Type Pengeluaran</th>
                        <th width="10%">Sortimen</th>
                        <th width="25%">Jenis Kayu</th>
                        <th width="10%">Option</th>                                                 
					</tr>
				</thead>
				<tbody>
					<?php foreach($query as $row){ ?>
					<?php $no = $no + 1 ?>
					<tr>                                    
						<td><?= $no?></td>
						<td><?php echo human_date_short($row->periode_awal) ?></td>
						<td><?php echo $row->nama_jenis_pengeluaran ?></td>							
						<td><?php echo strtoupper($row->type_pengeluaran) ?></td>
                        <td><?php echo $row->nama_sortimen ?></td>
						<td><?php echo $row->nama_kayu ?></td>
						<td>
                            <?php 
                                if($row->final == 0){
                                    echo tool_edit('client/pengeluaran/edit/'.$row->idpengeluaran);
                                    echo tool_remove('client/pengeluaran/delete/'.$row->idpengeluaran);								
                                    echo tool_finalize('client/pengeluaran/finalize/'.$row->idpengeluaran);										
                                }else{
                                    echo tool_finalized('client/pengeluaran/edit/'.$row->idpengeluaran,"Finalize By $row->finalize_user_name at ".human_date_time($row->finalize_date));										
                                }
							?>
						</td>                                    
					</tr>                                
					<?php } ?>
				</tbody>
			</table>
			<?php echo $pagination ?>
			
		</div>
		<div class="clear"></div>
	</div>                                
</div>