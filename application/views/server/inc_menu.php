	

	<ul class="navigation">            
		<li class="<?= (in_array($this->uri->segment(1),array('home')))? "active":""?>">
			<a href="{admin_url}">
				<span class="isw-grid"></span><span class="text">DASHBOARD</span>
			</a>
		</li>
		
        <!--
        <li class="openable <?= (in_array($this->uri->segment(2),array('report')))? "active":""?>">
		<a href="#"><span class="isw-bookmark"></span><span class="text">PUBLIKASI</span></a>
            <ul>
                <li><a href="{admin_url}report"><span class="icon-road"></span><span class="text">Lap. Penerbitan Dok V-Legal</span></a></li>                
                <li><a href="{admin_url}report/vlk_certified"><span class="icon-road"></span><span class="text">List of Certified Client </span></a></li>                
            </ul>
        </li>	
        -->
        
        <li class="openable <?= (in_array($this->uri->segment(2),array('discharge','matauang','negara','buyer','supplier','kayu','produk','pelabuhan','sortimen','hutan')))? "active":""?>">
		<a href="#"><span class="isw-bookmark"></span><span class="text">MASTER</span></a>
            <ul>
                <li><a href="{admin_url}negara"><span class="icon-road"></span><span class="text">Negara</span></a></li>
                <li><a href="{admin_url}buyer"><span class="icon-flag"></span><span class="text">Buyer</span></a></li>
                <li><a href="{admin_url}supplier"><span class="icon-qrcode"></span><span class="text">Supplier</span></a></li>
                <li><a href="{admin_url}kayu"><span class="icon-asterisk"></span><span class="text">Data Jenis Kayu </span></a></li>
                <li><a href="{admin_url}produk"><span class="icon-tag"></span><span class="text">Data Produk </span></a></li>
                <li><a href="{admin_url}pelabuhan"><span class="icon-th-large"></span><span class="text">Data Pelabuhan </span></a></li>
                <li><a href="{admin_url}sortimen"><span class="icon-eye-close"></span><span class="text">Sortimen </span></a></li>
                <li><a href="{admin_url}hutan"><span class="icon-leaf"></span><span class="text">Data Hutan </span></a></li>
                <li><a href="{admin_url}matauang"><span class="icon-leaf"></span><span class="text">Mata Uang</span></a></li>
                <li><a href="{admin_url}discharge"><span class="icon-leaf"></span><span class="text">Discharge</span></a></li>
                
            </ul>
        </li>	 
        <li class="openable <?= (in_array($this->uri->segment(2),array('vlegal','peb')))? "active":""?>">
            <a href="#"><span class="isw-text_document"></span><span class="text">V-LEGAL</span></a>
            <ul>
                <li><a href="{admin_url}vlegal/"><span class="icon-leaf"></span><span class="text">Pengajuan</span></a></li>
                <li><a href="{admin_url}vlegal/rejected"><span class="icon-leaf"></span><span class="text">Rejected</span></a></li>
                <li><a href="{admin_url}vlegal/approved"><span class="icon-leaf"></span><span class="text">Approved</span></a></li>
                <li><a href="{admin_url}peb"><span class="icon-leaf"></span><span class="text">Verifikasi PEB</span></a></li>
                <li><a href="{admin_url}peb/approved"><span class="icon-leaf"></span><span class="text">Verified PEB</span></a></li>
            </ul>
        </li>
		         
		<li class="openable <?= (in_array($this->uri->segment(1),array('Report')))? "active":""?>">
            <a href="#"><span class="isw-graph"></span><span class="text">REPORT</span></a>
            <ul>
                <li><a href="{admin_url}vlegal/report"><span class="icon-leaf"></span><span class="text">Laporan Penerbitan V-Legal</span></a></li>
                <li><a href="{admin_url}vlegal/report_penerbitan"><span class="icon-leaf"></span><span class="text">Laporan Penerbitan dan Ketidaksesuaian</span></a></li>
            </ul>
        </li>
		
		
        
    </ul>
