<div class="page-header">
	<h1>{first_title} <small>{second_title}</small></h1>
</div>  
<?php echo error_success($this->session)?>
<?php if($error != '') echo error_message($error)?>
<div class="row-fluid">
	<div class="span12">
		<div class="head">
			<div class="isw-documents"></div>
			<h1>HUTAN FORM</h1>
			<ul class="buttons">
				<li><a href="{admin_url}hutan" class="isw-list" title="Hutan List"></a></li>                                                        
			</ul>
			<div class="clear"></div>
		</div>
		<div class="block-fluid">                        
			<?php echo form_open('server/hutan/save','id = "adminform"') ?>
			
			
			<div class="row-form">
				<div class="span2">Nama Hutan:</div>
				<div class="span10"><input type="text" name="hutan" placeholder="Nama Hutan" value="{hutan}"/></div>
				<div class="clear"></div>
			</div> 
			
			<div class="row-form">
				<div class="span2">Status:</div>
				<div class="span10"><input type="checkbox" name="status" value="{status}"  checked>Active</div>
				<div class="clear"></div>
			</div>			
			<div class="row-form">
				<div class="span12"><button class="btn" type="submit" id="btnsimpan">Simpan</button></div>
				<div class="clear"></div>
			</div> 
			
			<input type="hidden" name="negara" id="negara" value="{negara}"/>
			<?php echo form_hidden('idhutan', $idhutan); ?>
			<?php echo form_close() ?>
		</div>
	</div>
	
</div>
