<!DOCTYPE html>
<html lang="en">
<head>        
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <!--[if gt IE 8]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <![endif]-->
    
    <title>Login - V-Legal TUV Rheinland Indonesia</title>

    <link rel="icon" type="image/ico" href="favicon.ico"/>
    
    <link href="{css_path}stylesheets.css" rel="stylesheet" type="text/css" />
    <!--[if lt IE 8]>
        <link href="css/ie7.css" rel="stylesheet" type="text/css" />
    <![endif]-->    
    <link rel='stylesheet' type='text/css' href='{css_path}fullcalendar.print.css' media='print' />
    
    <script type='text/javascript' src='{js_path}plugins/jquery/jquery-1.10.2.min.js'></script>
    <script type='text/javascript' src='{js_path}plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='{js_path}plugins/jquery/jquery-migrate-1.2.1.min.js'></script>
    <script type='text/javascript' src='{js_path}plugins/jquery/jquery.mousewheel.min.js'></script>
    
    <script type='text/javascript' src='{js_path}plugins/cookie/jquery.cookies.2.2.0.min.js'></script>
    
    <script type='text/javascript' src='{js_path}plugins/bootstrap.min.js'></script>
    
    <script type='text/javascript' src='{js_path}plugins/charts/excanvas.min.js'></script>
    <script type='text/javascript' src='{js_path}plugins/charts/jquery.flot.js'></script>    
    <script type='text/javascript' src='{js_path}plugins/charts/jquery.flot.stack.js'></script>    
    <script type='text/javascript' src='{js_path}plugins/charts/jquery.flot.pie.js'></script>
    <script type='text/javascript' src='{js_path}plugins/charts/jquery.flot.resize.js'></script>
    
    <script type='text/javascript' src='{js_path}plugins/sparklines/jquery.sparkline.min.js'></script>
    
    <script type='text/javascript' src='{js_path}plugins/fullcalendar/fullcalendar.min.js'></script>
    
    <script type='text/javascript' src='{js_path}plugins/select2/select2.min.js'></script>
    
    <script type='text/javascript' src='{js_path}plugins/uniform/uniform.js'></script>
    
    <script type='text/javascript' src='{js_path}plugins/maskedinput/jquery.maskedinput-1.3.min.js'></script>
    
    <script type='text/javascript' src='{js_path}plugins/validation/languages/jquery.validationEngine-en.js' charset='utf-8'></script>
    <script type='text/javascript' src='{js_path}plugins/validation/jquery.validationEngine.js' charset='utf-8'></script>
    
    <script type='text/javascript' src='{js_path}plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
    <script type='text/javascript' src='{js_path}plugins/animatedprogressbar/animated_progressbar.js'></script>
    
    <script type='text/javascript' src='{js_path}plugins/qtip/jquery.qtip-1.0.0-rc3.min.js'></script>
    
    <script type='text/javascript' src='{js_path}plugins/cleditor/jquery.cleditor.js'></script>
    
    <script type='text/javascript' src='{js_path}plugins/dataTables/jquery.dataTables.min.js'></script>    
    
    <script type='text/javascript' src='{js_path}plugins/fancybox/jquery.fancybox.pack.js'></script>
    
    <script type='text/javascript' src='{js_path}plugins/pnotify/jquery.pnotify.min.js'></script>
    <script type='text/javascript' src='{js_path}plugins/ibutton/jquery.ibutton.min.js'></script>
    
    <script type='text/javascript' src='{js_path}plugins/scrollup/jquery.scrollUp.min.js'></script>
    
    <script type='text/javascript' src='{js_path}cookies.js'></script>
    <script type='text/javascript' src='{js_path}actions.js'></script>
    <script type='text/javascript' src='{js_path}charts.js'></script>
    <script type='text/javascript' src='{js_path}plugins.js'></script>
    
</head>
<body>
    
    <div class="loginBlock" id="login" style="display: block;">
        <h1>Welcome, Please Login</h1>
        <div class="dr"><span></span></div>
        <div class="loginForm">
            <form class="form-horizontal" action="{admin_url}user/do_login" method="POST" id="validation">
                <div class="control-group">
                    <div class="input-prepend">
                        <span class="add-on"><span class="icon-envelope"></span></span>
                        <input type="text" id="username" name="username" placeholder="Username" class="validate[required]"/>
                    </div>                
                </div>
                <div class="control-group">
                    <div class="input-prepend">
                        <span class="add-on"><span class="icon-lock"></span></span>
                        <input type="password" id="password" name="password" placeholder="Password" class="validate[required]"/>
                    </div>
                </div>
                <div class="row-fluid">
                    
                    <div class="span4">
                        <button type="submit" class="btn btn-block">Sign in</button>       
                    </div>
                </div>
            </form>  
            <div class="dr"><span></span></div>
            <div class="controls">
                <div class="row-fluid">
                    <div class="span6">
                        <!--<button class="btn btn-link btn-block" onClick="loginBlock('#forgot');">Forgot your password?</button>-->
                    </div>
                    <div class="span2"></div>
                    <div class="span4">
                        <!--<button class="btn btn-link btn-block" onClick="loginBlock('#reg');">Registration</button>-->
                    </div>
                </div>
            </div>
        </div>
    </div>    
</html>
