<div class="page-header">
	<h1>{first_title} <small>{second_title}</small></h1>
</div>  
<?php echo error_success($this->session)?>
<div class="row-fluid">
	<div class="span12">                    
		<div class="head">
			<div class="isw-grid"></div><h1> {table_title}</h1>
			<ul class="buttons">
				<li>
					<a href="#" class="isw-settings"></a>
					<ul class="dd-list">
						<li><a href="{admin_url}negara/add"><span class="isw-plus"></span> Add Negara</a></li>
					</ul>
				</li>
			</ul>                        
			<div class="clear"></div>
		</div>
		<div class="block-fluid table-sorting">
		
			<div class="dataTables_wrapper">
            <div class="dataTables_filter" id="tSortable_filter" style="width:auto;">
				<form action="{admin_url}negara/search" method="post" style="margin-bottom:0;">
				<label>Search: <input type="text" style="width:300px;" value="{searchText}" name="searchText" placeholder="Search Negara By Nama Negara"></label>
				</form>
			</div>
			<table cellpadding="0" cellspacing="0" width="100%" class="table">
				<thead>
					<tr>                                    
						<th width="5%">No.</th>
						<th width="10%">ID Negara</th>
						<th width="60%">Nama Negara </th>						
						<th width="10%">Status </th>						
                        <th width="15%">Action</th>                                                 
					</tr>
				</thead>
				<tbody>
					<?php foreach($query as $row){ ?>
					<?php $no = $no + 1 ?>
					<tr>                                    
						<td><?= $no .'.'?></td>
						<td><?php echo anchor('server/negara/update/'.$row->idnegara,$row->idnegara) ?></td>						
						<td><?php echo anchor('server/negara/update/'.$row->idnegara,$row->negara) ?></td>
						<td align="center"><?=($row->status ? 'Active':'Not Active'); ?></td>
						<td>
							<?php 
								echo tool_edit('server/negara/update/'.$row->idnegara);
								echo tool_remove('server/negara/delete/'.$row->idnegara);								
							?>
						</td>                                    
					</tr>                                
					<?php } ?>
				</tbody>
			</table>
			<?php echo $pagination ?>
			
		</div>
		<div class="clear"></div>
	</div>                                
</div>