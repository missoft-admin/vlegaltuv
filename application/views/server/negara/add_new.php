<div class="page-header">
	<h1>{first_title} <small>{second_title}</small></h1>
</div>  
<?php echo error_success($this->session)?>
<?php if($error != '') echo error_message($error)?>
<div class="row-fluid">
	<div class="span12">
		<div class="head">
			<div class="isw-documents"></div>
			<h1>Negara Form</h1>
			<ul class="buttons">
				<li><a href="{admin_url}negara" class="isw-list" title="Negara List"></a></li>                                                        
			</ul>
			<div class="clear"></div>
		</div>
		<div class="block-fluid">                        
			<?php echo form_open('server/negara/save','id = "adminform"') ?>
			<div class="row-form">
				<div class="span2">ID Negara:</div>
				<div class="span2"><input type="text" <?php if ($st_insert==0){ echo "readonly"; }?> name="idnegara" placeholder="ID Negara" value="{idnegara}"/></div>
				<div class="clear"></div>
			</div> 
			<div class="row-form">
				<div class="span2">Nama Negara:</div>
				<div class="span10"><input type="text" name="negara" placeholder="Nama Negara" value="{negara}"/></div>
				<div class="clear"></div>
			</div> 
			<div class="row-form">
				<div class="span12"><button class="btn" type="submit" id="btnsimpan">Simpan</button></div>
				<div class="clear"></div>
			</div> 
			<input type="hidden" name="st_insert" id="st_insert" value="{st_insert}"/>
			<?php echo form_close() ?>
		</div>
	</div>
	
</div>
