<div class="page-header">
	<h1>{first_title} <small>{second_title}</small></h1>
</div>  
<?php echo error_success($this->session)?>
<?php if($error != '') echo error_message($error)?>
<div class="row-fluid">
	<div class="span12">
		<div class="head">
			<div class="isw-documents"></div>
			<h1>PEB FORM</h1>
			<ul class="buttons">
				<li><a href="{client_url}peb" class="isw-list" title="Rekapitulasi PEB List"></a></li>                                                        
			</ul>
			<div class="clear"></div>
		</div>
		<div class="block-fluid">                        
			<?php echo form_open('client/peb/save','id = "adminform"') ?>
			<div class="row-form">
				<div class="span2">No V-Legal:</div>
				<div class="span10">
                    <select class="validate[required]" name="vid" id="vid" data-placeholder="No. V-Legal">
						<option value="{vid}">{no_vlegal}</option>
					</select>
                </div>
				<div class="clear"></div>
			</div> 
            <div class="row-form">
				<div class="span2">Tanggal Terbit V-Legal:</div>
				<div class="span2"><input readonly type="text" name="tgl_vlegal" id="tgl_vlegal" required placeholder="Tanggal V-Legal" value="{tgl_vlegal}"/></div>
                <div class="clear"></div>
			</div> 
            <div class="row-form">
				<div class="span2">No. PEB:</div>
				<div class="span10"><input type="text" name="nomor_peb" placeholder="No. PEB" value="{nomor_peb}"/></div>
				<div class="clear"></div>
			</div>
             <div class="row-form">
				<div class="span2">Tanggal PEB:</div>
				<div class="span3"><input class="datepickertxt validate[required]" type="text" name="tgl_peb" required placeholder="Tanggal PEB" value="{tgl_peb}"/></div>
                <div class="clear"></div>
			</div> 
            <div class="row-form">
				<div class="span2">Lokasi Stuffing:</div>
				<div class="span10">
                    <select class="validate[required]" name="stuffing" id="stuffing" data-placeholder="Lokasi Stuffing">
						<option value="{stuffing}">{stuffing}</option>
					</select>
                </div>
				<div class="clear"></div>
			</div> 
            <div class="row-form">
				<div class="span2">Volume:</div>
				<div class="span2"><input class="validate[required] angka" type="text" name="volume" placeholder="Volume" value="{volume}"/></div>
				<div class="span1">m3</div>
                <div class="clear"></div>
			</div>
            <div class="row-form">
				<div class="span2">Netto:</div>
				<div class="span2"><input class="validate[required] angka" type="text" name="netto" placeholder="Netto" value="{netto}"/></div>
				<div class="span1">Kg</div>
                <div class="clear"></div>
			</div>
            <div class="row-form">
				<div class="span2">Jumlah:</div>
				<div class="span2"><input class="validate[required] angka" type="text" name="jumlah" placeholder="Jumlah" value="{jumlah}"/></div>
				<div class="span1">Unit</div>
                <div class="clear"></div>
			</div>
            <div class="row-form">
				<div class="span2">Nilai:</div>
				<div class="span3"><input class="validate[required] angka" type="text" name="nilai" placeholder="Nilai" value="{nilai}"/></div>
				<div class="clear"></div>
			</div>
            <div class="row-form">
				<div class="span2">Nilai USD:</div>
				<div class="span3"><input class="validate[required] angka" type="text" name="nilai_usd" placeholder="Nilai USD" value="{nilai_usd}"/></div>
				<div class="clear"></div>
			</div>
			<div class="row-form">
				<div class="span12"><button class="btn" type="submit" id="btnsimpan">Simpan</button></div>
				<div class="clear"></div>
			</div> 
			
			<input type="hidden" name="no_vlegal" id="no_vlegal" value="{no_vlegal}"/>
			
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
		</div>
	</div>
	
</div>
<script type="text/javascript">

$(document).ready(function(){
    $('.angka').number(true,3,',','.');
    $("#vid").select2({
         ajax: {
            url: '{client_url}peb/find_vlegal',
            dataType: 'json',
            delay: 250,
            processResults: function (data, params) {
              return {
                    results: $.map(data, function (item) {
                        return {
                       id: item.vid,
                       text: item.no_dokumen
                        };
                    })
              };
            },
            cache: true
          },
          minimumInputLength: 5,
          theme: "bootstrap",
          allowClear: true
    }).on('select2:select', function(e) { $(this).focus(); });
    
    $("#vid").change(function(){
        $("#no_vlegal").val($("#vid option:selected").text());
        $.ajax({
          url: '{client_url}peb/vlegal_detail/'+$(this).val(),
          dataType: 'json',
          success: function(data) {
            $('#tgl_vlegal').val(data);
          }
        });
    });
    
    $("#stuffing").select2({
         ajax: {
            url: '{client_url}peb/find_stuffing',
            dataType: 'json',
            delay: 250,
            processResults: function (data, params) {
              return {
                    results: $.map(data, function (item) {
                        return {
                       id: item.stuffing,
                       text: item.stuffing
                        };
                    })
              };
            },
            cache: true
          },
          minimumInputLength: 3,
          theme: "bootstrap",
          allowClear: true
    }).on('select2:select', function(e) { $(this).focus(); });
    
});
</script>
    
