<div class="page-header">
	<h1>{first_title} <small>{second_title}</small></h1>
</div>  
<?php echo error_success($this->session)?>
<div class="span11" id="mails">
	<div class="headInfo">
		<div class="input-append">
			<input type="text" name="search" placeholder="search project..." id="widgetInputMessage"/><button class="btn btn-success" type="button">Search</button>
		</div>                                           
		<div class="arrow_down"></div>
	</div>    

	<div class="block-fluid" id="inbox">
		<div class="toolbar clearfix">
			<div class="left">
				<div class="btn-group">
					<button class="btn btn-small btn-success tip" title="Refresh Project Baru"><span class="icon-refresh icon-white"></span></button>
					
				</div>
				
			</div>
			<div class="right">
				<div class="pagination pagination-mini">
					<ul>
						<li class="disabled"><a href="#">Prev</a></li>
						<li class="disabled"><a href="#">1</a></li>
						<li><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>
						<li><a href="#">Next</a></li>
					</ul>
				</div>                                  
			</div>
		</div>
		<table cellpadding="0" cellspacing="0" width="100%" class="table">
			<thead>
				<tr>
					<th width="3%">No</th>
					<th></th>
					<th width="20%">Project</th>
					<th width="65%">Keterangan</th>
					<th width="10%">Deadline</th>
					<th width="10%">Action</th>                                                                        
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>1.</td>
					<td><span class="icon-folder-close"></span></td>
					<td>Project. Bulan Purnama</td>
										
					<td><strong><a href="#" class="mails_show" data-toggle="modal" data-show="mail-1">Ketarangan Project disini.</a></strong></td>
					<td>12-04-2015</td>
					<td><div class="btn-group">
						<button class="btn btn-small btn-success tip" title="Eksekusi"><span class="icon-share icon-white"></span></button>
						
					</div></td>
				</tr>
				<tr>
					<td>2.</td>
					<td><span class="icon-folder-close"></span></td>
					<td>Project. Bulan Purnama</td>
										
					<td><strong><a href="#" class="mails_show" data-toggle="modal" data-show="mail-1">Ketarangan Project disini.</a></strong></td>
					<td>12-04-2015</td>
					<td><div class="btn-group">
						<button class="btn btn-small btn-success tip" title="Eksekusi"><span class="icon-share icon-white"></span></button>
						
					</div></td>
				</tr>
				<tr>
					<td colspan="6" class="heading">Yesterday</td>
				</tr>                                
				<tr>
					<td>3.</td>
					<td><span class="icon-folder-close"></span></td>
					<td>Project. Bulan Purnama</td>
										
					<td><strong><a href="#" class="mails_show" data-toggle="modal" data-show="mail-1">Ketarangan Project disini.</a></strong></td>
					<td>12-04-2015</td>
					<td><div class="btn-group">
						<button class="btn btn-small btn-success tip" title="Eksekusi"><span class="icon-share icon-white"></span></button>
						
					</div></td>
				</tr>                               
			</tbody>
		</table>                       
		<div class="toolbar bottom-toolbar clearfix">
			
		</div>                                                                     
	</div>

	

</div>  