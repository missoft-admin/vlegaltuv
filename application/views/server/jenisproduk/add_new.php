<div class="page-header">
	<h1>{first_title} <small>{second_title}</small></h1>
</div>  
<?php echo error_success($this->session)?>
<?php if($error != '') echo error_message($error)?>
<div class="row-fluid">
	<div class="span12">
		<div class="head">
			<div class="isw-documents"></div>
			<h1>JENIS PRODUK FORM</h1>
			<ul class="buttons">
				<li><a href="{admin_url}jenisproduk" class="isw-list" title="Produk List"></a></li>                                                        
			</ul>
			<div class="clear"></div>
		</div>
		<div class="block-fluid">                        
			<?php echo form_open('server/jenisproduk/save','id = "adminform"') ?>
			
			
			<div class="row-form">
				<div class="span2">Nama Jenis Produk:</div>
				<div class="span10"><input type="text" name="jenis_produk" placeholder="Nama Jenis Produk" value="{jenis_produk}"/></div>
				<div class="clear"></div>
			</div> 
			<div class="row-form">
				<div class="span2">Satuan:</div>
				<div class="span10">
					<select id="satuan" name="satuan">
						<option value="0">Pilih Satuan</option>
						<?php foreach($satuan_list as $row){ ?>
							<option value="<?php echo $row->satuan_id;?>" <?php if ($row->satuan_id==$satuan){echo 'selected="selected"';}?>><?php echo $row->satuan_kode;?></option>
						<?php } ?>
					</select>
				</div> 
				<div class="clear"></div>
			</div> 
			
			<div class="row-form">
				<div class="span2">Status:</div>
				<div class="span10"><input type="checkbox" name="xstatus" value="{xstatus}"  checked>Active</div>
				<div class="clear"></div>
			</div>			
			<div class="row-form">
				<div class="span12"><button class="btn" type="submit" id="btnsimpan">Simpan</button></div>
				<div class="clear"></div>
			</div> 
			
			<?php echo form_hidden('idjenisproduk', $idjenisproduk); ?>
			<?php echo form_close() ?>
		</div>
	</div>
	
</div>
