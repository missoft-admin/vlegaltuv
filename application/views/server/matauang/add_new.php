<div class="page-header">
	<h1>{first_title} <small>{second_title}</small></h1>
</div>  
<?php echo error_success($this->session)?>
<?php if($error != '') echo error_message($error)?>
<div class="row-fluid">
	<div class="span12">
		<div class="head">
			<div class="isw-documents"></div>
			<h1>MATA UANG FORM</h1>
			<ul class="buttons">
				<li><a href="{admin_url}matauang" class="isw-list" title="Mata Uang List"></a></li>                                                        
			</ul>
			<div class="clear"></div>
		</div>
		<div class="block-fluid">                        
			<?php echo form_open('server/matauang/save','id = "adminform"') ?>
			
			<div class="row-form">
				<div class="span2">Kode:</div>
				<div class="span10"><input type="text" name="kode" placeholder="Kode" value="{kode}"/></div>
				<div class="clear"></div>
			</div>
			<div class="row-form">
				<div class="span2">Nama Mata Uang:</div>
				<div class="span10"><input type="text" name="nama" placeholder="Nama Mata Uang" value="{nama}"/></div>
				<div class="clear"></div>
			</div> 
			
			<div class="row-form">
				<div class="span12"><button class="btn" type="submit" id="btnsimpan">Simpan</button></div>
				<div class="clear"></div>
			</div> 
			
			<input type="hidden" name="negara" id="negara" value="{negara}"/>
			<?php echo form_hidden('iduang', $iduang); ?>
			<?php echo form_close() ?>
		</div>
	</div>
	
</div>
