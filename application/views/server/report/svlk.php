<div class="page-header">
	<h1>{first_title} <small>{second_title}</small></h1>
</div>  
<?php echo error_success($this->session)?>
<div class="row-fluid">
	<div class="span12">                    
		<div class="head">
			<div class="isw-grid"></div><h1> {table_title}</h1>
			<ul class="buttons">
				<li>
					<a href="#" class="isw-settings"></a>
					<ul class="dd-list">
						<li><a href="{admin_url}report/add"><span class="isw-plus"></span> Add Report</a></li>
					</ul>
				</li>
			</ul>                        
			<div class="clear"></div>
		</div>
		<div class="block-fluid table-sorting">
			<div class="dataTables_wrapper">
            <div class="dataTables_filter" id="tSortable_filter" style="width:auto;">
				<form action="{admin_url}report/search" method="post" style="margin-bottom:0;">
				<label>Search: <input type="text" style="width:300px;" value="{searchText}" name="searchText" placeholder="Search Report "></label>
				</form>
			</div>
			<table cellpadding="0" cellspacing="0" width="100%" class="table">
				<thead>
					<tr>                                    
						<th width="5%">No.</th>
						<th width="50%">Title</th>
						<th width="10%">Download</th>
					</tr>
				</thead>
				<tbody>
                    <tr>                                    
						<td>1</td>
						<td>VLK Certified Client per 31 April 2015</td>							
						<td>
							<?php 
								echo tool_download('assets/client_report/List_of_VLK_Certified_Clients_31_April_2015.pdf');
							?>
						</td>                                    
					</tr>
                    <tr>                                    
						<td>2</td>
						<td>VLK Hutan Certified Client per Maret 2016</td>							
						<td>
							<?php 
								echo tool_download('assets/client_report/List_of_VLK_Hutan_Certified_Clients_March_2016.pdf.pdf');
							?>
						</td>                                    
					</tr>
                    <tr>                                    
						<td>3</td>
						<td>VLK Industri Certified Client per Maret 2016</td>							
						<td>
							<?php 
								echo tool_download('assets/client_report/List_of_VLK_Industri_Certified_Clients_May_2016.pdf');
							?>
						</td>                                    
					</tr>
                    <tr>                                    
						<td>4</td>
						<td>PHPL Certified Client per Maret 2016</td>							
						<td>
							<?php 
								echo tool_download('assets/client_report/List_of_PHPL_Certified_Clients_March_2016.pdf');
							?>
						</td>                                    
					</tr>
                    <tr>                                    
						<td>5</td>
						<td>VLK Industri Certified Client per Mei 2016</td>							
						<td>
							<?php 
								echo tool_download('assets/client_report/List_of_VLK_Industri_Certified_Clients_May_2016.pdf');
							?>
						</td>                                    
					</tr>
				</tbody>
			</table>
			
		</div>
		<div class="clear"></div>
	</div>                                
</div>