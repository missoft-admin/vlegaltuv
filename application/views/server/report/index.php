<div class="page-header">
	<h1>{first_title} <small>{second_title}</small></h1>
</div>  
<?php echo error_success($this->session)?>
<div class="row-fluid">
	<div class="span12">                    
		<div class="head">
			<div class="isw-grid"></div><h1> {table_title}</h1>
			<ul class="buttons">
				<li>
					<a href="#" class="isw-settings"></a>
					<ul class="dd-list">
						<li><a href="{admin_url}report/add"><span class="isw-plus"></span> Add Report</a></li>
					</ul>
				</li>
			</ul>                        
			<div class="clear"></div>
		</div>
		<div class="block-fluid table-sorting">
			<div class="dataTables_wrapper">
            <div class="dataTables_filter" id="tSortable_filter" style="width:auto;">
				<form action="{admin_url}report/search" method="post" style="margin-bottom:0;">
				<label>Search: <input type="text" style="width:300px;" value="{searchText}" name="searchText" placeholder="Search Report "></label>
				</form>
			</div>
			<table cellpadding="0" cellspacing="0" width="100%" class="table">
				<thead>
					<tr>                                    
						<th width="5%">No.</th>
						<th width="50%">Periode</th>
						<th width="10%">Download</th>
					</tr>
				</thead>
				<tbody>
                    <tr>                                    
						<td>1</td>
						<td>Januari 2015</td>							
						<td>
							<?php 
								echo tool_download('assets/report/Jan 2015.xlsx');
							?>
						</td>                                    
					</tr>
                    <tr>                                    
						<td>2</td>
						<td>Februari 2015</td>							
						<td>
							<?php 
								echo tool_download('assets/report/Feb 2015.xlsx');
							?>
						</td>                                    
					</tr>    
                    <tr>                                    
						<td>3</td>
						<td>Maret 2015</td>							
						<td>
							<?php 
								echo tool_download('assets/report/Maret 2015.xlsx');
							?>
						</td>                                    
					</tr>
                    <tr>                                    
						<td>4</td>
						<td>April 2015</td>							
						<td>
							<?php 
								echo tool_download('assets/report/April 2015.xlsx');
							?>
						</td>                                    
					</tr>  
                    <tr>                                    
						<td>5</td>
						<td>Mei 2015</td>							
						<td>
							<?php 
								echo tool_download('assets/report/Mei 2015.xlsx');
							?>
						</td>                                    
					</tr>    
                    <tr>                                    
						<td>6</td>
						<td>Juni 2015</td>							
						<td>
							<?php 
								echo tool_download('assets/report/Juni 2015.pdf');
							?>
						</td>                                    
					</tr> 
                    <tr>                                    
						<td>7</td>
						<td>Juli 2015</td>							
						<td>
							<?php 
								echo tool_download('assets/report/Juli 2015.pdf');
							?>
						</td>                                    
					</tr>
                    <tr>                                    
						<td>8</td>
						<td>Agustus 2015</td>							
						<td>
							<?php 
								echo tool_download('assets/report/Aug 2015.pdf');
							?>
						</td>                                    
					</tr> 
                    <tr>                                    
						<td>9</td>
						<td>September 2015</td>							
						<td>
							<?php 
								echo tool_download('assets/report/Sept 2015.pdf');
							?>
						</td>                                    
					</tr>  
                    <tr>                                    
						<td>10</td>
						<td>Oktober 2015</td>							
						<td>
							<?php 
								echo tool_download('assets/report/Okto 2015.pdf');
							?>
						</td>                                    
					</tr>  
                    <tr>                                    
						<td>11</td>
						<td>November 2015</td>							
						<td>
							<?php 
								echo tool_download('assets/report/November 2015.pdf');
							?>
						</td>                                    
					</tr> 
                    <tr>                                    
						<td>12</td>
						<td>Desember 2015</td>							
						<td>
							<?php 
								echo tool_download('assets/report/Desember 2015.pdf');
							?>
						</td>                                    
					</tr> 
                    <tr>                                    
						<td>13</td>
						<td>Januari 2016</td>							
						<td>
							<?php 
								echo tool_download('assets/report/Januari 2016.pdf');
							?>
						</td>                                    
					</tr> 
                    <tr>                                    
						<td>14</td>
						<td>Februari 2016</td>							
						<td>
							<?php 
								echo tool_download('assets/report/Februari 2016.pdf');
							?>
						</td>                                    
					</tr>  
                    <tr>                                    
						<td>15</td>
						<td>Maret 2016</td>							
						<td>
							<?php 
								echo tool_download('assets/report/Maret 2016.pdf');
							?>
						</td>                                    
					</tr> 
                    <tr>                                    
						<td>16</td>
						<td>April 2016</td>							
						<td>
							<?php 
								echo tool_download('assets/report/April 2016.pdf');
							?>
						</td>                                    
					</tr>    
					<tr>                                    
						<td>17</td>
						<td>Mei 2016</td>							
						<td>
							<?php 
								echo tool_download('assets/report/Mei 2016.pdf');
							?>
						</td>                                    
					</tr>    
					
				</tbody>
			</table>
			
		</div>
		<div class="clear"></div>
	</div>                                
</div>