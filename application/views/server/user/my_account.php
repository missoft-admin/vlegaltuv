<?php echo error_success($this->session)?>
<?php echo ($error !='')? error_message($error) : ''?>
<div class="row-fluid">
	<div class="span12">
		<div class="head">
			<div class="isw-documents"></div>
			<h1>{page_title}</h1>
			<div class="clear"></div>
		</div>
		<div class="block-fluid">                        
			<?php echo form_open_multipart('admin/user/update_account','id = "adminform"') ?>
								
			<div class="row-form">
				<div class="span2">Nama:</div>
				<div class="span10"><input type="text" name="nama" placeholder="Nama" value="{nama}"/></div>
				<div class="clear"></div>
			</div> 
			
			<div class="row-form">
				<div class="span2">Email:</div>
				<div class="span10"><input type="text" name="email" placeholder="Email" value="{email}"/></div>
				<div class="clear"></div>
			</div>
			
			<div class="row-form">
				<div class="span2">No. Telp:</div>
				<div class="span10"><input type="text" name="phone" placeholder="Phone" value="{phone}"/></div>
				<div class="clear"></div>
			</div>
			
			<div class="row-form">
				<div class="span2">Alamat:</div>
				<div class="span10"><input type="text" name="alamat" placeholder="Alamat" value="{alamat}"/></div>
				<div class="clear"></div>
			</div> 
			<div class="row-form">
				<div class="span2">Wilayah:</div>
				<div class="span10">{penempatan}</div>
				<div class="clear"></div>
			</div>
			<div class="row-form">
				<div class="span2">Jabatan:</div>
				<div class="span10">{jabatan}</div>
				<div class="clear"></div>
			</div> 	
			<div class="row-form">
				<div class="span2">Username:</div>
				<div class="span10">{username}</div>
				<div class="clear"></div>
			</div> 
			
			<div class="row-form">
				<div class="span2">Password:</div>
				<div class="span10"><input type="password" name="password" placeholder="Password" value=""/></div>
				<div class="clear"></div>
			</div> 
			
			<div class="row-form">
				<div class="span2">Avatar:</div>
				<div class="span10"><input type="file" name="avatar"/></div>
				<div class="clear"></div>
			</div>
			
	
			<?php if($avatar != ''){ ?>
			<div class="row-form">
				<div class="span2">Preview Avatar:</div>
				<div class="span10"><img src="{avatar_path}{avatar}" alt="Preview" style="width:100px;"/></div>
				<div class="clear"></div>
			</div>
			<?php } ?>
			
			<div class="row-form">
				<div class="span12"><button class="btn" type="submit" id="btnsimpan">Simpan</button></div>
				<div class="clear"></div>
			</div> 
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_hidden('old_username', $username); ?> 
			<?php echo form_hidden('avatar', $avatar); ?> 
			<?php echo form_close() ?>
		</div>
	</div>
	
</div>