<div class="page-header">
	<h1>{first_title} <small>{second_title}</small></h1>
</div>  
<?php echo ($error !='')? error_message($error) : ''?>
<div class="row-fluid">
	<div class="span12">
		<div class="head">
			<div class="isw-documents"></div>
			<h1>{page_title}</h1>
			<ul class="buttons">
				<li><a href="{site_url}user" class="isw-list" title="User List"></a></li>                                                        
			</ul>
			<div class="clear"></div>
		</div>
		<div class="block-fluid">                        
			<?php echo form_open_multipart('user/{action}','id = "adminform"') ?>
								
			<div class="row-form">
				<div class="span2">Nama:</div>
				<div class="span10"><input type="text" name="nama" placeholder="Nama" value="{nama}"/></div>
				<div class="clear"></div>
			</div> 
			
			<div class="row-form">
				<div class="span2">Email:</div>
				<div class="span10"><input type="text" name="email" placeholder="Email" value="{email}"/></div>
				<div class="clear"></div>
			</div>
			
			<div class="row-form">
				<div class="span2">No. Telp:</div>
				<div class="span10"><input type="text" name="phone" placeholder="Phone" value="{phone}"/></div>
				<div class="clear"></div>
			</div>
			
			<div class="row-form">
				<div class="span2">Alamat:</div>
				<div class="span10"><input type="text" name="alamat" placeholder="Alamat" value="{alamat}"/></div>
				<div class="clear"></div>
			</div>
			<div class="row-form">
				<div class="span2">Username:</div>
				<div class="span10"><input type="text" name="username" placeholder="Username" value="{username}"/></div>
				<div class="clear"></div>
			</div> 
			
			<div class="row-form">
				<div class="span2">Password:</div>
				<div class="span10"><input type="password" name="password" placeholder="Password" value=""/><small>Default Password : 12345</small></div>
				<div class="clear"></div>
			</div> 
			
			<div class="row-form">
				<div class="span2">Role:</div>
				<div class="span10">
					<select name="role">
						<option value="">Pilih Role user</option>
						<option <?= ($role == 'SA')? 'selected' : ''; ?> value="SA">Super Admin</option>
						<option <?= ($role == 'A')? 'selected' : ''; ?> value="A">Administrator</option>
						<option <?= ($role == 'E')? 'selected' : ''; ?> value="E">Editor</option>
					</select>
				</div>
				<div class="clear"></div>
			</div> 
			
			<div class="row-form">
				<div class="span2">Avatar:</div>
				<div class="span10"><input type="file" name="avatar"/></div>
				<div class="clear"></div>
			</div>
			
	
			<?php if($avatar != ''){ ?>
			<div class="row-form">
				<div class="span2">Preview Avatar:</div>
				<div class="span10"><img src="{avatar_path}{avatar}" alt="Preview" style="width:100px;"/></div>
				<div class="clear"></div>
			</div>
			<?php } ?>
			
			<div class="row-form">
				<div class="span2">Status:</div>
				<div class="span10">
					<select size="1" name="active" id="active">
						<option <?= ($active == '1')? 'selected="selected"' : ''?> value="1">Aktif</option>
						<option <?= ($active == '0')? 'selected="selected"' : ''?> value="0">Non Aktif</option>
					</select>
				</div>
				<div class="clear"></div>
			</div>
			
			<div class="row-form">
				<div class="span12"><button class="btn" type="submit" id="btnsimpan">Simpan</button></div>
				<div class="clear"></div>
			</div> 
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_hidden('old_username', $username); ?> 
			<?php echo form_hidden('avatar', $avatar); ?> 
			<?php echo form_close() ?>
		</div>
	</div>
	
</div>