<!DOCTYPE html>
<html lang="en">
<head>        
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

    <title>Indonesian Quality Award Foundation Administrator Administrator Panel - Lost Password</title>

    <link rel="icon" type="image/ico" href="favicon.ico"/>
    
    <link href="<?= $css_path?>stylesheets.css" rel="stylesheet" type="text/css" />  
</head>
<body>
    
    <div class="loginBox">        
        <div class="loginHead">
            <img src="<?= $images_path?>logo_iqaf.png" alt="Indonesian Quality Award Foundation Administrator Panel" title="Indonesian Quality Award Foundation Administrator Panel"/>
			<span style="font-weight:bold;font-size:16px;margin-left:15%;">RESET PASSWORD</span>
        </div>
        <form class="form-horizontal" action="<?= $site_url?>admin/user/reset" method="POST">            
            <div class="control-group">
                <label for="inputEmail">Username / Email</label>                
                <input type="text" id="email" name="email" value="<?=$email?>"/>
            </div>
            <div class="form-actions">
                <button type="submit" class="btn btn-block">Reset</button>
            </div>
			<p style="text-align:center;color:red"><?php echo $error ?></p>	
        </form>        
        
    </div>    
    
</body>
</html>
