<div class="page-header">
	<h1>{first_title} <small>{second_title}</small></h1>
</div>  
<?php echo error_success($this->session)?>
<div class="row-fluid">
	<div class="span12">                    
		<div class="head">
			<div class="isw-grid"></div>
			<h1>{page_title}</h1>      
			<ul class="buttons">
				<li>
					<a href="#" class="isw-settings"></a>
					<ul class="dd-list">
						<li><a href="{site_url}user/add"><span class="isw-plus"></span> Add User</a></li>
					</ul>
				</li>
			</ul>                        
			<div class="clear"></div>
		</div>
		<div class="block-fluid table-sorting">
			<div class="dataTables_wrapper">
			<div class="dataTables_filter" id="tSortable_filter" style="width:auto;">
				<form action="{site_url}user/search" method="post" style="margin-bottom:0;">
				<label>Search: <input type="text" style="width:300px;" value="{searchText}" name="searchText" placeholder="Search User By Nama"></label>
				</form>
			</div>
			<table cellpadding="0" cellspacing="0" width="100%" class="table">
				<thead>
					<tr>                                    
						<th width="5%">No.</th>
						<th width="15%">Avatar</th>
						<th width="20%">Nama</th>
						<th width="10%">Email</th>
						<th width="10%">Phone</th>
						<th width="10%">Role</th>
						<th width="15%">Last Login</th>
						<th width="15%">Action</th>                                    
					</tr>
				</thead>
				<tbody>
					<?php $no = $this->uri->segment(4) ?>
					<?php foreach($query as $row){ ?>
					<?php $no = $no + 1 ?>
					<tr>                                    
						<td><?= $no?></td>
						<td><?php echo ($row->avatar !='')? '<img src="{avatar_thumbs_path}'.$row->avatar.'" alt="Preview" style="width:50px;"/>':''; ?></td>
						<td><?php echo anchor('user/edit/'.$row->id,$row->nama) ?></td>
						<td><?php echo $row->email ?></td>
						<td><?php echo $row->phone ?></td>
						<td><?php echo type_user($row->role) ?></td>
						<td><?php echo ($row->last_login != null)? human_date_time($row->last_login) : "Not Available" ; ?></td>
						<td>
							<?php 
								echo tool_edit('user/edit/'.$row->id);
								echo tool_remove('user/delete/'.$row->id);
							?>
						</td>                                    
					</tr>                                
					<?php } ?>
				</tbody>
			</table>
			<?php echo $pagination ?>
			
		</div>
		<div class="clear"></div>
	</div>                                
</div>