<?php echo error_success($this->session)?>
<div class="row-fluid">
	<div class="span12">                    
		<div class="head">
			<div class="isw-grid"></div>
			<h1>User Login History</h1>                            
			<div class="clear"></div>
		</div>
		<div class="block-fluid table-sorting">
			<div class="dataTables_wrapper">
			<div class="dataTables_filter" id="tSortable_filter" style="width:auto;">
				<form action="{site_url}admin/login_history/search" method="post" style="margin-bottom:0;">
				<label>Search: <input type="text" style="width:300px;" value="{searchText}" name="searchText" placeholder="Search User By Nama"></label>
				</form>
			</div>
			<table cellpadding="0" cellspacing="0" width="100%" class="table">
				<thead>
					<tr>                                    
						<th width="5%">No.</th>
						<th width="15%">Avatar</th>
						<th width="20%">Nama</th>
						<th width="10%">Jabatan</th>
						<th width="10%">Total Login</th>
						<th width="10%">Login Tahun Ini</th>
						<th width="10%">Login Bulan Ini</th>
						<th width="15%">Last Login</th>
						                                    
					</tr>
				</thead>
				<tbody>
					<?php $no = $this->uri->segment(4) ?>
					<?php foreach($query as $row){ ?>
					<?php $no = $no + 1 ?>
					<tr>                                    
						<td><?= $no?></td>
						<td><?php echo ($row->avatar !='')? '<img src="{avatar_tpath}'.$row->avatar.'" alt="Preview" style="width:50px;"/>':''; ?></td>
						<td><?php echo anchor('admin/user/edit/'.$row->id,$row->nama) ?></td>
						<td><?php echo $row->jabatan ?></td>
						<td><?php echo number_format($row->total_login,0) ?> Kali</td>
						<td><?php echo number_format($row->tahun_ini,0) ?> Kali</td>
						<td><?php echo number_format($row->bulan_ini,0) ?> Kali</td>
						<td><?php echo ($row->last_login != null)? human_date_time($row->last_login) : "Not Available" ; ?></td>
						                                    
					</tr>                                
					<?php } ?>
				</tbody>
			</table>
			<?php echo $pagination ?>
			
		</div>
		<div class="clear"></div>
	</div>                                
</div>