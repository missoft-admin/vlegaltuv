<!DOCTYPE html>
<html lang="en">
<head>        
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <!--[if gt IE 8]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <![endif]-->
    
    <title> V-Legal Indonesia TÜV Rheinland </title>

    <link rel="icon" type="image/ico" href="{images_path}tuvicon.ico"/>
    
    <link href="{css_path}stylesheets.css" rel="stylesheet" type="text/css" />  
    <link href="{css_path}override.css" rel="stylesheet" type="text/css" />  
    <!--[if lt IE 8]>
        <link href="css/ie7.css" rel="stylesheet" type="text/css" />
    <![endif]-->            
    
    <script type='text/javascript' src='{js_path}plugins/jquery/jquery-1.10.2.min.js'></script>
    <script type='text/javascript' src='{js_path}plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='{js_path}plugins/jquery/jquery-migrate-1.2.1.min.js'></script>
    <script type='text/javascript' src='{js_path}plugins/jquery/jquery.mousewheel.min.js'></script>
    
    <script type='text/javascript' src='{js_path}plugins/cookie/jquery.cookies.2.2.0.min.js'></script>
    
    <script type='text/javascript' src='{js_path}plugins/bootstrap.min.js'></script>
    
    <script type='text/javascript' src='{js_path}plugins/charts/excanvas.min.js'></script>
    <script type='text/javascript' src='{js_path}plugins/charts/jquery.flot.js'></script>    
    <script type='text/javascript' src='{js_path}plugins/charts/jquery.flot.stack.js'></script>    
    <script type='text/javascript' src='{js_path}plugins/charts/jquery.flot.pie.js'></script>
    <script type='text/javascript' src='{js_path}plugins/charts/jquery.flot.resize.js'></script>
    
    <script type='text/javascript' src='{js_path}plugins/sparklines/jquery.sparkline.min.js'></script>
    
    <script type='text/javascript' src='{js_path}plugins/fullcalendar/fullcalendar.min.js'></script>
    
    <script type='text/javascript' src='{js_path}plugins/select2/select2.min.js'></script>
    
    <script type='text/javascript' src='{js_path}plugins/uniform/uniform.js'></script>
    
    <script type='text/javascript' src='{js_path}plugins/maskedinput/jquery.maskedinput-1.3.min.js'></script>
    
    <script type='text/javascript' src='{js_path}plugins/validation/languages/jquery.validationEngine-id.js' charset='utf-8'></script>
    <script type='text/javascript' src='{js_path}plugins/validation/jquery.validationEngine.js' charset='utf-8'></script>
    
    <script type='text/javascript' src='{js_path}plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
    <script type='text/javascript' src='{js_path}plugins/animatedprogressbar/animated_progressbar.js'></script>
    
    <script type='text/javascript' src='{js_path}plugins/qtip/jquery.qtip-1.0.0-rc3.min.js'></script>
    
    <script type='text/javascript' src='{js_path}plugins/cleditor/jquery.cleditor.js'></script>
    
    <script type='text/javascript' src='{js_path}plugins/dataTables/jquery.dataTables.min.js'></script>    
    
    <script type='text/javascript' src='{js_path}plugins/fancybox/jquery.fancybox.pack.js'></script>
    
    <script type='text/javascript' src='{js_path}plugins/pnotify/jquery.pnotify.min.js'></script>
    <script type='text/javascript' src='{js_path}plugins/ibutton/jquery.ibutton.min.js'></script>
    
    <script type='text/javascript' src='{js_path}plugins/scrollup/jquery.scrollUp.min.js'></script>
    <script type='text/javascript' src='{js_path}jquery.number.min.js'></script>
    
    <script type='text/javascript' src='{js_path}cookies.js'></script>
    <script type='text/javascript' src='{js_path}actions.js'></script>
    <script type='text/javascript' src='{js_path}charts.js'></script>
    <script type='text/javascript' src='{js_path}plugins.js'></script>
    <script type='text/javascript' src='{js_path}settings.js'></script>
</head>
<body>
    <div class="wrapper green"> 
            
        <div class="header">
            <!--<a class="logo" href="index-2.html"><img src="{images_path}logo.png" alt="Aplikasi Audit TUV Rheinland" title="Aplikasi Audit TUV Rheinland"/></a>-->
            <ul class="header_menu">
                <li class="list_icon"><a href="#">&nbsp;</a></li>
                <li class="settings_icon">
                    <a href="#" class="link_themeSettings">&nbsp;</a>
                    
                    <div id="themeSettings" class="popup">
                        <div class="head clearfix">
                            <div class="arrow"></div>
                            <span class="isw-settings"></span>
                            <span class="name">Theme settings</span>
                        </div>
                        <div class="body settings">
                            <div class="row-fluid">
                                <div class="span3"><strong>Style:</strong></div>
                                <div class="span9">
                                    <a class="styleExample tip active" title="Default style" data-style="">&nbsp;</a>                                    
                                    <a class="styleExample silver tip" title="Silver style" data-style="silver">&nbsp;</a>
                                    <a class="styleExample dark tip" title="Dark style" data-style="dark">&nbsp;</a>
                                    <a class="styleExample marble tip" title="Marble style" data-style="marble">&nbsp;</a>
                                    <a class="styleExample red tip" title="Red style" data-style="red">&nbsp;</a>                                    
                                    <a class="styleExample green tip" title="Green style" data-style="green">&nbsp;</a>
                                    <a class="styleExample lime tip" title="Lime style" data-style="lime">&nbsp;</a>
                                    <a class="styleExample purple tip" title="Purple style" data-style="purple">&nbsp;</a>                                    
                                </div>
                            </div>                            
                            <div class="row-fluid">
                                <div class="span3"><strong>Background:</strong></div>
                                <div class="span9">
                                    <a class="bgExample tip active" title="Default" data-style="">&nbsp;</a>
                                    <a class="bgExample bgCube tip" title="Cubes" data-style="cube">&nbsp;</a>
                                    <a class="bgExample bghLine tip" title="Horizontal line" data-style="hline">&nbsp;</a>
                                    <a class="bgExample bgvLine tip" title="Vertical line" data-style="vline">&nbsp;</a>
                                    <a class="bgExample bgDots tip" title="Dots" data-style="dots">&nbsp;</a>
                                    <a class="bgExample bgCrosshatch tip" title="Crosshatch" data-style="crosshatch">&nbsp;</a>
                                    <a class="bgExample bgbCrosshatch tip" title="Big crosshatch" data-style="bcrosshatch">&nbsp;</a>
                                    <a class="bgExample bgGrid tip" title="Grid" data-style="grid">&nbsp;</a>
                                </div>
                            </div>                            
                            <div class="row-fluid">
                                <div class="span3"><strong>Fixed layout:</strong></div>
                                <div class="span9">
                                    <input type="checkbox" name="settings_fixed" value="1"/>
                                </div> 
                            </div>
                            <div class="row-fluid">
                                <div class="span3"><strong>Hide menu:</strong></div>
                                <div class="span9">
                                    <input type="checkbox" name="settings_menu" value="1"/>
                                </div>                                           
                            </div>                            
                        </div>
                        <div class="footer">                            
                            <button class="btn link_themeSettings" type="button">Close</button>
                        </div>
                    </div>                    
                    
                </li>
            </ul>    
        </div>

        <div class="menu">                

            <div class="breadLine">            
                <div class="arrow"></div>
                <div class="adminControl active">
                    Hi, {user_name}
                </div>
            </div>
            <div class="admin">
                <div class="image">
                    <img src="{avatar_thumbs_path}{user_avatar}" class="img-polaroid"/>                
                </div>
                <ul class="control">                
                    <li><span class="icon-comment"></span> <a href="{admin_url}vlegal">Pengajuan V-Legal</a> <a href="{admin_url}vlegal" class="caption red">12</a></li>
                    <li><span class="icon-cog"></span> <a href="{admin_url}user/myaccount">Settings</a></li>
                    <li><span class="icon-share-alt"></span> <a href="{admin_url}user/logout">Logout</a></li>
                </ul>
                <div class="info">
                    <span>Welcome back! <br/>Your last Login: {user_last_login}</span><br/>
                    <span>Login as <strong>{user_role}</strong></span>
                </div>
            </div>

            <!--MENUU -->
			<?php $this->load->view('server/inc_menu') ?>

        </div>

        <div class="content">


            <div class="breadLine">

                <ul class="breadcrumb">
                    <?php echo admin_breadcrum($breadcrum)?>
                </ul>
			</div>
                
            <div class="workplace">
				<?php $this->load->view('server/'.$template) ?> 
			</div>   
		
    </div>
</html>
