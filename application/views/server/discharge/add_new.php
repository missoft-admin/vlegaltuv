<div class="page-header">
	<h1>{first_title} <small>{second_title}</small></h1>
</div>  
<?php echo error_success($this->session)?>
<?php if($error != '') echo error_message($error)?>
<div class="row-fluid">
	<div class="span12">
		<div class="head">
			<div class="isw-documents"></div>
			<h1>DISCHARGE FORM</h1>
			<ul class="buttons">
				<li><a href="{admin_url}discharge" class="isw-list" title="Discharge List"></a></li>                                                        
			</ul>
			<div class="clear"></div>
		</div>
		<div class="block-fluid">                        
			<?php echo form_open('server/discharge/save','id = "adminform"') ?>
			
			<div class="row-form">
				<div class="span2">Kode:</div>
				<div class="span10"><input type="text" name="kode" placeholder="Kode" value="{kode}"/></div>
				<div class="clear"></div>
			</div>
			<div class="row-form">
				<div class="span2">Nama Discharge:</div>
				<div class="span10"><input type="text" name="uraian" placeholder="Nama Discharge" value="{uraian}"/></div>
				<div class="clear"></div>
			</div> 
			
			<div class="row-form">
				<div class="span2">Status:</div>
				<div class="span10"><input type="checkbox" name="status" value="{status}"  checked>Active</div>
				<div class="clear"></div>
			</div>			
			<div class="row-form">
				<div class="span12"><button class="btn" type="submit" id="btnsimpan">Simpan</button></div>
				<div class="clear"></div>
			</div> 
			
			<input type="hidden" name="negara" id="negara" value="{negara}"/>
			<?php echo form_hidden('iddischarge', $iddischarge); ?>
			<?php echo form_close() ?>
		</div>
	</div>
	
</div>
