<div class="page-header">
	<h1>{first_title} <small>{second_title}</small></h1>
</div>  
<?php echo error_success($this->session)?>
<div class="row-fluid">
	<div class="span12">                    
		<div class="head">
			<div class="isw-grid"></div><h1> {table_title}</h1>
			<ul class="buttons">
				<li>
					<a href="#" class="isw-settings"></a>
					<ul class="dd-list">
						<li><a href="{admin_url}discharge/add"><span class="isw-plus"></span> Add Discharge</a></li>
					</ul>
				</li>
			</ul>                        
			<div class="clear"></div>
		</div>
		<div class="block-fluid table-sorting">
			<div class="dataTables_wrapper">
            <div class="dataTables_filter" id="tSortable_filter" style="width:auto;">
				<form action="{admin_url}discharge/search" method="post" style="margin-bottom:0;">
				<label>Search: <input type="text" style="width:300px;" value="{searchText}" name="searchText" placeholder="Search Discharge By Nama Discharge / Kode"></label>
				</form>
			</div>
			<table cellpadding="0" cellspacing="0" width="100%" class="table">
				<thead>
					<tr>                                    
						<th width="5%">No.</th>
						<th width="15%">Kode</th>
						<th width="60%">Nama Discharge</th>
						<th width="10%">Status</th>
                        <th width="10%">Option</th>                                                 
					</tr>
				</thead>
				<tbody>
					<?php foreach($query as $row){ ?>
					<?php $no = $no + 1 ?>
					<tr>                                    
						<td><?= $no?></td>
						<td><?php echo $row->kode ?></td>
						<td><?php echo anchor('server/discharge/update/'.$row->iddischarge,$row->uraian) ?></td>							
						<td align="center"><?=($row->status ? 'Active':'Not Active'); ?></td>
						<td>
							<?php 
								echo tool_edit('server/discharge/update/'.$row->iddischarge);
								echo tool_remove('server/discharge/delete/'.$row->iddischarge);								
							?>
						</td>                                    
					</tr>                                
					<?php } ?>
				</tbody>
			</table>
			<?php echo $pagination ?>
			
		</div>
		<div class="clear"></div>
	</div>                                
</div>