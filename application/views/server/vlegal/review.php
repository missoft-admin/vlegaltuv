<div class="page-header">
	<h1>{first_title} <small>{second_title}</small></h1>
</div>  
<?php echo error_success($this->session)?>
<?php if($error != '') echo error_message($error)?>
<div class="row-fluid">
	<div class="span12">
		<div class="head">
			<div class="isw-documents"></div>
			<h1>Informasi Data Ekspor</h1>
			<ul class="buttons">
				<li><a href="{client_url}ekspor" class="isw-list" title="List Pengajuan"></a></li>                                                        
			</ul>
			<div class="clear"></div>
		</div>
		<div class="block-fluid">                        
            
            <div class="row-form">
                <div class="span12">
                    <a href="#faccept" id="accept" role="button" title="Send to LIU" data-toggle="modal"><span class="ibb-accept-vlegal"></span></a>
                    <a href="#freject" id="reject" role="button" title="Reject V-Legal" data-toggle="modal"><span class="ibb-reject-vlegal"></span></a>
                </div>
                <div class="clear"></div>
            </div>
            <div class="row-form">
                <div class="span12" style="text-align:center"><h4>EKSPORTIR</h4></div>
                <div class="clear"></div>
            </div>
			<div class="row-form" style="padding:0px;">
                <div class="span12"> 
                    <table cellpadding="0" cellspacing="0" width="100%" class="table head_list">
                        <tr>
                            <td width="17%"><strong>NPWP</strong></td>
                            <td width="2%">:</td>
                            <td width="30%">{npwp}</td>
                            <td width="2%">&nbsp;</td>
                            <td width="17%"><strong>PROPINSI</strong></td>
                            <td width="2%">:</td>
                            <td width="30%">{nama_provinsi}</td>
                        </tr>
                        <tr>
                            <td><strong>NAMA EKSPORTIR</strong></td>
                            <td>:</td>
                            <td>{nama_client}</td>
                            <td>&nbsp;</td>
                            <td><strong>KABUPATEN / KOTA</strong></td>
                            <td>:</td>
                            <td>{nama_kota}</td>
                        </tr>
                        <tr>
                            <td><strong>ALAMAT EKSPORTIR</strong></td>
                            <td>:</td>
                            <td>{alamat_client}</td>
                            <td>&nbsp;</td>
                            <td><strong>NO. ETPIK</strong></td>
                            <td>:</td>
                            <td>{etpik}</td>
                        </tr>
                    </table>
                </div>
                <div class="clear"></div>
			</div>
            <div class="row-form">
                <div class="span12" style="text-align:center"><h4>IMPORTIR</h4></div>
                <div class="clear"></div>
            </div>
            <div class="row-form" style="padding:0px;">
                <div class="span12"> 
                    <table cellpadding="0" cellspacing="0" width="100%" class="table head_list">
                        <tr>
                            <td width="17%"><strong>NAMA IMPORTIR</strong></td>
                            <td width="2%">:</td>
                            <td width="30%">{namabuyer}</td>
                            <td width="2%">&nbsp;</td>
                            <td width="17%"><strong>PELABUHAN MUAT</strong></td>
                            <td width="2%">:</td>
                            <td width="30%">{loading_name}</td>
                        </tr>
                        <tr>
                            <td><strong>ALAMAT IMPORTIR</strong></td>
                            <td>:</td>
                            <td>{alamat_buyer}</td>
                            <td>&nbsp;</td>
                            <td><strong>PELABUHAN BONGKAR</strong></td>
                            <td>:</td>
                            <td>{discharge_name}</td>
                        </tr>
                        <tr>
                            <td><strong>NEGARA TUJUAN EKSPOR</strong></td>
                            <td>:</td>
                            <td>{idnegara} - {country_name}</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </div>
                <div class="clear"></div>
			</div>
            <div class="row-form">
                <div class="span12" style="text-align:center"><h4>DATA V-LEGAL</h4></div>
                <div class="clear"></div>
            </div>
            <div class="row-form" style="padding:0px;">
                <div class="span12"> 
                    <table cellpadding="0" cellspacing="0" width="100%" class="table head_list">
                        <tr>
                            <td width="17%"><strong>NO. INVOICE</strong></td>
                            <td width="2%">:</td>
                            <td width="30%">{invoice}</td>
                            <td width="2%">&nbsp;</td>
                            <td width="17%"><strong>TGL. INVOICE</strong></td>
                            <td width="2%">:</td>
                            <td width="30%"><?= human_date_short($tglinvoice) ?></td>
                        </tr>
                        <tr>
                            <td><strong>NEGARA TUJUAN</strong></td>
                            <td>:</td>
                            <td>{country_name}</td>
                            <td>&nbsp;</td>
                            <td><strong>KETERANGAN</strong></td>
                            <td>:</td>
                            <td>{keterangan}</td>
                        </tr>
                        <tr>
                            <td><strong>NO. URUT</strong></td>
                            <td>:</td>
                            <td>{no_urut}</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                         <tr>
                            <td><strong>TRANSPORTASI</strong></td>
                            <td>:</td>
                            <td><?= define_vessel($vessel) ?></td>
                            <td>&nbsp;</td>
                            <td><strong>LOKASI STUFFING</strong></td>
                            <td>:</td>
                            <td>{stuffing}</td>
                        </tr>
                    </table>
                </div>
                <div class="clear"></div>
			</div>
            <div class="row-form">
                <div class="span12" style="text-align:center"><h4>DETAIL</h4></div>
                <div class="clear"></div>
            </div>
            <div class="row-form" style="padding:0px;">
				<div class="span12">
                        <table cellpadding="0" cellspacing="0" width="100%" class="table" id="head_list">
                            <thead>
                                <tr>
                                    <th width="5%">No.</th>
                                    <th width="10%">HS NUMBER</th>
                                    <th width="20%">DESKRIPSI</th>
                                    <th width="10%">VOLUME</th>
                                    <th width="10%">BERAT BERSIH</th>
                                    <th width="10%">NUMBER OF UNIT</th>
                                    <th width="10%">VALUE</th>
                                    <th width="10%">CURRENCY</th>
                                    <th width="20%">SCIENTIFIC NAMES</th>
                                    <th width="20%">HARVEST COUNTRY</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                <?php $no = 0;$totvolume = 0;$totberat = 0;$totjumlah = 0;$totnilai = 0;?>
                                <?php foreach($detail_list as $row) { ?>
                                <?php $no++;?>
                                    <tr>
                                        <td ><?= $no ?></td>
                                        <td ><?= $row->hscode ?></td>
                                        <td ><?= $row->namaproduk ?></td>
                                        <td class="angka_det"><?= titik_to_koma($row->volume) ?></td>
                                        <td class="angka_det"><?= titik_to_koma($row->berat) ?></td>
                                        <td class="angka_det"><?= titik_to_koma($row->jumlah) ?></td>
                                        <td class="angka_det"><?= titik_to_koma($row->nilai) ?></td>
                                        <td ><?= $valuta ?></td>
                                        <td ><?= implode(array_column(json_decode($row->jenis_kayu,true),'name'),"<br/>"); ?></td>
                                        <td ><?= implode(array_column(json_decode($row->negara_asal,true),'name'),"<br/>"); ?></td>
                                    </tr>
                                <?php $totvolume += $row->volume;$totberat += $row->berat;$totjumlah += $row->jumlah;$totnilai += $row->nilai; ?>
                                <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="3">&nbsp;</th>
                                    <th class="angka_det"><?= titik_to_koma($totvolume) ?></th>
                                    <th class="angka_det"><?= titik_to_koma($totberat) ?></th>
                                    <th class="angka_det"><?= titik_to_koma($totjumlah) ?></th>
                                    <th class="angka_det"><?= titik_to_koma($totnilai) ?></th>
                                    <th colspan="3">&nbsp;</th>
                                </tr>
                            </tfoot>
                        </table>
                </div>
				<div class="clear"></div>
			</div> 
            
			
            
		</div>
	</div>
    <div id="faccept" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            <h3 id="myModalLabel">Send to LIU</h3>
        </div>        
        <?php echo form_open('server/vlegal/sendtoliu','id = "frmTransaksi"') ?>
        <div class="row-fluid">
            <div class="block-fluid">
                <div class="row-form clearfix">
                    <div class="span3">Nama Client:</div>
                    <div class="span9">{nama_client}</div>
                </div>            
                <div class="row-form clearfix">
                    <div class="span3">No. Invoice:</div>
                    <div class="span9">{invoice}</div>                    
                </div>  
                <div class="row-form clearfix">
                    <div class="span3">Tgl. Invoice</div>
                    <div class="span9"><?= human_date_short($tglinvoice) ?></div>                    
                </div>
                <div class="row-form clearfix">
                    <div class="span3">Pilih Pejabat:</div>
                    <div class="span9">
                        <select name="pejabat_ttd" class="validate[required]">
                            <option value="">Pejabat TTD</option>
                            <?php foreach($pejabat_list as $row){ ?>
                            <option value="<?= $row->kode_pejabat?>"><?= $row->nama_pejabat?></option>
                            <?php } ?>
                        </select>
                    </div>                    
                </div> 
                <div class="row-form clearfix">
                    <div class="span3">No. Urut Dokumen:</div>
                    <div class="span9"><input class="validate[required]" type="text" value="{no_urut}" name="no_urut"/></div>                    
                </div>                                    
                                                              
            </div>                
        </div>   
        
        <div class="modal-footer">
            <button class="btn btn-warning" type="submit" aria-hidden="true">Send to LIU</button> 
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>            
        </div>
        <?php echo form_hidden('idekspor', $idekspor); ?>
        <?php echo form_close() ?>
    </div>
    <div id="freject" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            <h3 id="myModalLabel">Reject Dokumen</h3>
        </div>        
        <?php echo form_open('server/vlegal/rejectdocument','id = "frmTransaksi2"') ?>
        <div class="row-fluid">
            <div class="block-fluid">
                <div class="row-form clearfix">
                    <div class="span3">Nama Client:</div>
                    <div class="span9">{nama_client}</div>
                </div>            
                <div class="row-form clearfix">
                    <div class="span3">No. Invoice:</div>
                    <div class="span9">{invoice}</div>                    
                </div>  
                <div class="row-form clearfix">
                    <div class="span3">Tgl. Invoice</div>
                    <div class="span9"><?= human_date_short($tglinvoice) ?></div>                    
                </div>
                <div class="row-form clearfix">
                    <div class="span3">Alasan Reject:</div>
                    <div class="span9"><textarea class="validate[required]" id="keterangan" name="keterangan" placeholder="Alasan Reject"></textarea></div>                    
                </div>                                    
                                                              
            </div>                
        </div>   
        
        <div class="modal-footer">
            <button class="btn btn-danger" type="submit" aria-hidden="true">Reject Dokumen</button> 
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>            
        </div>
        <?php echo form_hidden('idekspor', $idekspor); ?>
        <?php echo form_close() ?>
    </div>  
	
</div>
<script type="text/javascript">
$(document).ready(function(){
    $('.angka_det').number(true,3,',','.');
});
</script>