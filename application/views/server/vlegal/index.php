<div class="page-header">
	<h1>{first_title} <small>{second_title}</small></h1>
</div>  
<?php echo error_success($this->session)?>
<div class="row-fluid">
	<div class="span12">                    
		<div class="head">
			<div class="isw-grid"></div><h1> {table_title}</h1>
			<div class="clear"></div>
		</div>
		<div class="block-fluid table-sorting">
			<div class="dataTables_wrapper">
            <div class="dataTables_filter" id="tSortable_filter" style="width:auto;">
				<form action="{admin_url}vlegal/search" method="post" style="margin-bottom:0;">
				<label>Search: <input type="text" style="width:300px;" value="{searchText}" name="searchText" placeholder="Search By No. Invoice or Client Name"></label>
				</form>
			</div>
			<table cellpadding="0" cellspacing="0" width="100%" class="table">
				<thead>
					<tr>                                    
						<th width="5%">No.</th>
						<th width="10%">Kode Permintaan</th>
						<th width="7%">Tanggal Input / Edit</th>
						<th width="20%">Klien</th>
                        <th width="7%">Status Dokumen</th>
                        <th width="10%">Option</th>                                                 
					</tr>
				</thead>
				<tbody>
					<?php foreach($query as $row){ ?>
					<?php $no = $no + 1 ?>
					<tr>                                    
						<td><?= $no?></td>
						<td><?php echo $row->no_urut." / ".$row->invoice?></td>
						<td><?php echo human_date_short($row->tglinvoice)." / ".human_date_short($row->editdate) ?></td>							
						<td><?php echo $row->nama_client ?></td>
						<td><?php echo $row->status_liu ?></td>
						<td>
                            <a href="{admin_url}vlegal/review/<?= $row->idekspor ?>"><button class="btn">Review V-Legal</button></a>
						</td>                                    
					</tr>                                
					<?php } ?>
				</tbody>
			</table>
			<?php echo $pagination ?>
			
		</div>
		<div class="clear"></div>
	</div>                                
</div>