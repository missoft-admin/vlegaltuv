<div class="page-header">
	<h1>{first_title} <small>{second_title}</small></h1>
</div>  
<?php echo error_success($this->session)?>
<?php if($error != '') echo error_message($error)?>
<div class="row-fluid">
	<div class="span12">
		<div class="head">
			<div class="isw-documents"></div>
			<h1>Form Penjualan Ekspor</h1>
			<ul class="buttons">
				<li><a href="{client_url}ekspor" class="isw-list" title="List Pengajuan"></a></li>                                                        
			</ul>
			<div class="clear"></div>
		</div>
		<div class="block-fluid">                        
			<?php echo form_open('client/ekspor/save','id = "frmTransaksi"') ?>
			<div class="row-form">
				<div class="span2">Nama Pembeli:</div>
				<div class="span4">
                    <select name="idbuyer" id="idbuyer">
						<option value="{idbuyer}">{namabuyer}</option>
					</select>
                
                </div>
                <div class="span2">No. Invoice:</div>
				<div class="span4"><input type="text" name="invoice" placeholder="No. Invoice" value="{invoice}"/></div>
				<div class="clear"></div>
			</div> 
            <div class="row-form">
				<div class="span2">Alamat Pembeli:</div>
				<div class="span4"><textarea readonly name="alamat" id="alamat" placeholder="Alamat Pembeli">{alamat}</textarea></div>
                 <div class="span2">Tanggal Invoice:</div>
				<div class="span4"><input class="datepickertxt" type="text" name="tglinvoice" placeholder="Tanggal Invoice" value="{tglinvoice}"/></div>
				<div class="clear"></div>
			</div> 
			<div class="row-form">
				<div class="span2">Negara Tujuan:</div>
				<div class="span4">
                        <select name="idnegara" id="idnegara">
                            <option value="{idnegara}">{country_name}</option>
                        </select>
						<input type="hidden" name="country_name" id="country_name" value="{country_name}"/>
				</div>	
                <div class="span2">ISO Code:</div>
				<div class="span4">
						<input type="text" name="iso" readonly id="iso" value="{iso}"/>
				</div>	
				<div class="clear"></div>
			</div> 
			
            <div class="row-form lokal">
				<div class="span2">Mata Uang:</div>
				<div class="span4">
					<select name="valuta" id="valuta">
						<option value="{valuta}">{nama_valuta}</option>
					</select>
				</div>	
                <div class="span2">Alat Angkut:</div>
				<div class="span4">
					<select name="vessel" id="vessel">
						<option value="">- Pilih Alat Angkut-</option>
						<option value="1" <?= ($vessel == 1)? 'selected' : ''?>>By Sea</option>
						<option value="2" <?= ($vessel == 2)? 'selected' : ''?>>By Air</option>
						<option value="3" <?= ($vessel == 3)? 'selected' : ''?>>By Land</option>
					</select>
				</div>				
				<div class="clear"></div>
			</div>
            
            <div class="row-form import">
				<div class="span2">Port of Loading:</div>
				<div class="span4">
					<select name="loading" id="loading">
						<option value="{loading}">{loading_name}</option>
					</select>
				</div>	
                <div class="span2">Tanggal Shipment:</div>
				<div class="span4"><input class="datepickertxt" type="text" name="tglship" placeholder="Tanggal Shipment" value="{tglship}"/></div>	
				<div class="clear"></div>
			</div>
            <div class="row-form import">
				<div class="span2">Port of Discharge:</div>
				<div class="span4">
					<select name="discharge" id="discharge">
						<option value="{discharge}">{discharge_name}</option>
					</select>
				</div>	
                <div class="span2">No. Sertifikat:</div>
				<div class="span4"><input type="text" readonly name="sertifikat" value="{sertifikat}"/></div>
				<div class="clear"></div>
			</div>	
            <div class="row-form lokal">
				<div class="span2">No. ETPIK:</div>
				<div class="span4">
						<input type="text" name="etpik" readonly id="etpik" value="{etpik}"/>
				</div>	
                <div class="span2">NPWP:</div>
				<div class="span4">
						<input type="text" name="npwp" readonly id="npwp" value="{npwp}"/>
				</div>	
                
				<div class="clear"></div>
			</div>
            <div class="row-form lokal">
				<div class="span2">Lokasi Stuffing:</div>
				<div class="span4">
                    <textarea readonly name="stuffing" id="stuffing" placeholder="Lokasi Stuffing">{stuffing}</textarea>
				</div>	
                <div class="span2">Status Dokumen:</div>
				<div class="span4">
					<select name="status" id="status">
						<option value="">- Pilih Status Dokumen-</option>
						<option value="vlegal" <?= ($status == 'vlegal')? 'selected' : ''?>>Dokumen V-Legal</option>
						<option value="non-vlegal" <?= ($status == 'non-vlegal')? 'selected' : ''?>>Dokumen Non V-Legal</option>
					</select>
				</div>
                
				<div class="clear"></div>
			</div>
            <div class="row-form lokal">
				<div class="span2">Keterangan:</div>
				<div class="span4"><textarea name="keterangan">{keterangan}</textarea></div>	
				<div class="clear"></div>
			</div>
            <div class="row-form">
                <div class="span12" style="text-align:center"><h4>Data Detail Ekspor</h4></div>
                <div class="clear"></div>
            </div>
			<div class="row-form" style="padding:0px;">
                <div class="span12">
                    <div class="block-fluid"  style="position:relative;z-index:1;margin-bottom:0px;background-color:#F1F1F1">
                        <table cellpadding="0" cellspacing="0" width="100%" class="table">
                            <thead>
                                <tr>
                                    <th  width="15%">Product</th>
                                    <th colspan="9" style="vertical-align:middle">
                                        <input type="hidden" class="detail" id="rowindex"/>
                                        <input type="hidden"  class="detail" id="nomor"/>
                                        <input type="hidden"  class="detail" id="hscode"/>
                                        <select name="idproduct" id="idproduct">
                                            <option value="">Pilih Product</option>
                                        </select>
                                    </th>
                                    <th width="5%" rowspan="3" style="vertical-align:middle">
                                        <button class="btn" id="cmdAdd" type="button">Add</button>
                                        
                                    </th>                                                 
                                </tr>
                                <tr>
                                    <th  width="10%">Jenis Kayu</th>
                                    <th colspan="3"style="vertical-align:middle" >
                                        <select id="idkayu" data-placeholder="Jenis Kayu" multiple>
                                            <option value="">-Pilih Jenis Kayu-</option>
                                        </select>
                                    </th>
                                    <th colspan="3"style="vertical-align:middle" >
                                        <select id="idkayu2" data-placeholder="Jenis Kayu">
                                            <option value="">-Pilih Jenis Kayu-</option>
                                        </select>
                                    </th>
                                    <th colspan="3"style="vertical-align:middle" >
                                        <select id="idkayu3" data-placeholder="Jenis Kayu">
                                            <option value="">-Pilih Jenis Kayu-</option>
                                        </select>
                                    </th>
                                </tr>
                                <tr>    
                                    <th  width="10%">Negara Asal</th>
                                    <th colspan="3"style="vertical-align:middle" >
                                        <select id="idnegaraasal" data-placeholder="Negara Asal">
                                            <option value="">-Pilih Negara Asal-</option>
                                        </select>
                                    </th>
                                    <th colspan="3"style="vertical-align:middle" >
                                        <select id="idnegaraasal2" data-placeholder="Negara Asal">
                                            <option value="">-Pilih Negara Asal-</option>
                                        </select>
                                    </th>
                                    <th colspan="3"style="vertical-align:middle" >
                                        <select id="idnegaraasal3" data-placeholder="Negara Asal">
                                            <option value="">-Pilih Negara Asal-</option>
                                        </select>
                                    </th>
                                    
                                </tr>
                                <tr>
                                    <th  width="10%">No. FAKO / Nota</th>
                                    <th colspan="9" style="vertical-align:middle">
                                        <input type="text" class="detail" id="fako" name="fako"/>
                                    </th>
                                    
                                </tr>
                                <tr>
                                    <th  width="10%">&nbsp;</th>
                                    <th width="10%">Jumlah</th>
                                    <th width="10%">Satuan</th>
                                    <th width="10%">Volume</th>
                                    <th width="10%">Satuan</th>
                                    <th width="10%">Berat</th>		
                                    <th width="10%">Satuan</th> 
                                    <th width="10%">Nilai (FOB)</th>
                                    <th width="10%">Nilai (CNF)</th>
                                    <th width="10%">Nilai (CIF)</th>
                                    <th width="5%" rowspan="2" style="vertical-align:middle">
                                        <button class="btn btn-danger btn-small" id="cmdClear" type="button">Clear</button>
                                    </th>
                                </tr>
                                <tr>
                                     
                                    <th>&nbsp;</th>
                                    <th><input style="width:100%" type="text" class="detail angka" id="jumlah"></th>                                        
                                    <th>
                                        <select style="width:100%" class="detail" id="sat_jumlah">
                                            <option value="">-</option>
                                            <?php foreach($satuan_jumlah as $row){?>
                                                <option value="<?= $row->satuan_id ?>"><?= $row->satuan_kode ?> </option>
                                            <?php }?>
                                        </select>
                                    </th>
                                    <th><input style="width:100%"  type="text" class="detail angka" id="volume"></th>                                        
                                    <th>
                                        <select style="width:100%" class="detail" id="sat_volume">
                                            <option value="">-</option>
                                            <?php foreach($satuan_volume as $row){?>
                                                <option value="<?= $row->satuan_id ?>"><?= $row->satuan_kode ?> </option>
                                            <?php }?>
                                        </select>
                                    </th>
                                    <th><input style="width:100%" type="text" class="detail angka" id="berat"></th>                                        
                                    <th style="border-right: 1px solid #DDD;">
                                        <select style="width:100%" class="detail" id="sat_berat">
                                            <option value="">-</option>
                                            <?php foreach($satuan_berat as $row){?>
                                                <option value="<?= $row->satuan_id ?>"><?= $row->satuan_kode ?> </option>
                                            <?php }?>
                                        </select>
                                    </th>
                                    <th><input style="width:100%" type="text" class="detail angka"  id="fob"></th>   
                                    <th><input style="width:100%" type="text" class="detail angka" id="cnf"></th>                                        
                                    <th><input style="width:100%" type="text" class="detail angka" id="cif"></th>                                        
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="clear"></div>
			</div>
            <div class="row-form" style="padding:0px;">
				<div class="span12">
                    <div class="block-fluid"  style="position:relative;padding-right:17px;margin-bottom:0px;background-color:#F1F1F1">
                        <table cellpadding="0" cellspacing="0" width="100%" class="table">
                            <thead>
                                <tr>
                                    <th rowspan="2" width="5%">No.</th>
                                    <th rowspan="2" width="10%">FAKO</th>
                                    <th rowspan="2" width="20%">Nama Produk Jadi</th>
                                    <th rowspan="2" width="10%">HS CODE</th>
                                    <th colspan="3">Jumlah</th>
                                    <th rowspan="2" width="10%">Jenis Kayu</th>
                                    <th rowspan="2" width="10%">Negara Asal</th>
                                    <th rowspan="2" width="10%">Nilai</th>
                                    <th rowspan="2" width="5%" style="border-right: 1px solid #DDD;">Option</th>
                                </tr>
                                <tr>                                    
                                    <th width="5%">m3</th>
                                    <th width="5%">kg</th>
                                    <th width="5%">unit</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="block-fluid" style="height:300px;margin-bottom:0px;overflow-y:scroll;position:relative;">
                        <table cellpadding="0" cellspacing="0" width="100%" style="margin-top:-58px" class="table" id="detail_list">
                            <thead>
                                <tr>
                                    <th rowspan="2" width="5%">No.</th>
                                    <th rowspan="2" width="10%">FAKO</th>
                                    <th rowspan="2" width="20%">Nama Produk Jadi</th>
                                    <th rowspan="2" width="10%">HS CODE</th>
                                    <th colspan="3">Jumlah</th>
                                    <th rowspan="2" width="10%">Jenis Kayu</th>
                                    <th rowspan="2" width="10%">Negara Asal</th>
                                    <th rowspan="2" width="10%">Nilai</th>
                                    <th rowspan="2" width="5%" style="border-right: 1px solid #DDD;">Option</th>
                                </tr>
                                <tr>                                    
                                    <th width="5%">m3</th>
                                    <th width="5%">kg</th>
                                    <th width="5%">unit</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                <?php $no = 0;?>
                                <?php foreach($detail_list as $row) { ?>
                                <?php $no++;?>
                                    <tr>
                                        <td width="5%"><?= $no ?></td>
                                        <td width="10%"><?= $row->fako ?></td>
                                        <td style="display:none"><?= $row->idproduk ?></td>
                                        <td width="10%"><?= $row->namaproduk ?>)</td>
                                        <td width="10%"><?= $row->hscode ?></td>
                                        <td style="display:none"><?= $row->volume ?></td>
                                        <td width="5%" class="angka_det"><?= titik_to_koma($row->volume) ?></td>
                                        <td style="display:none"><?= $row->satuan_volume ?></td>
                                        <td width="5%" class="angka_det"><?= titik_to_koma($row->berat) ?></td>
                                        <td style="display:none"><?= $row->berat ?></td>
                                        <td style="display:none"><?= $row->satuan_berat ?></td>
                                        <td width="5%" class="angka_det"><?= titik_to_koma($row->jumlah) ?></td>
                                        <td style="display:none"><?= $row->jumlah ?></td>
                                        <td style="display:none"><?= $row->satuan_jumlah ?></td>
                                        <td style="display:none"><?= $row->idkayu ?></td>
                                        <td style="display:none"><?= $row->namakayu1 ?></td>
                                        <td style="display:none"><?= $row->idkayu2 ?></td>
                                        <td style="display:none"><?= $row->namakayu2 ?></td>
                                        <td style="display:none"><?= $row->idkayu3 ?></td>
                                        <td style="display:none"><?= $row->namakayu3 ?></td>
                                        <td width="10%"><?= $row->namakayu1."<br/>".$row->namakayu2."<br/>".$row->namakayu1 ?></td>
                                        <td style="display:none"><?= $row->idnegaraasal ?></td>
                                        <td style="display:none"><?= $row->asalnegara1 ?></td>
                                        <td style="display:none"><?= $row->idnegaraasal2 ?></td>
                                        <td style="display:none"><?= $row->asalnegara2 ?></td>
                                        <td style="display:none"><?= $row->idnegaraasal3 ?></td>
                                        <td style="display:none"><?= $row->asalnegara3 ?></td>
                                        <td width="10%"><?=  $row->asalnegara1."<br/>".$row->asalnegara2."<br/>".$row->asalnegara3 ?></td>
                                        <td width="5%" class="angka_det"><?= titik_to_koma($row->nilai) ?></td>
                                        <td style="display:none"><?= $row->nilai ?></td>
                                        <td style="display:none"><?= $row->nilai_cnf ?></td>
                                        <td style="display:none"><?= $row->nilai_cif ?></td>
                                        <td width="5%">
                                            <a href="#" class="edit"><i class="icon-pencil"></i></a>
                                            &nbsp;&nbsp;
                                            <a href="#" class="hapus"><i class="icon-remove"></i></a> 
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
				<div class="clear"></div>
			</div> 
            <div class="row-form">
				<div class="span12"><button class="btn" type="button" id="btnsimpan">Simpan</button></div>
				<div class="clear"></div>
			</div>
			<input type="hidden" id="namabuyer" name="namabuyer" value="{namabuyer}"/>
			<input type="hidden" id="nama_valuta" name="nama_valuta" value="{nama_valuta}"/>
			<input type="hidden" id="loading_name" name="loading_name" value="{loading_name}"/>
			<input type="hidden" id="discharge_name" name="discharge_name" value="{discharge_name}"/>
            <input type="hidden" id="detail_value" name="detail_value"/>
			<?php echo form_hidden('idekspor', $idekspor); ?>
			<?php echo form_close() ?>
		</div>
	</div>
	
</div>
<link href="{css_path}chosen.css" rel="stylesheet" type="text/css" />
<script src="{js_path}chosen.jquery.js" type="text/javascript"></script>
<script src="{js_path}ajax-chosen.js" type="text/javascript"></script>
<script type="text/javascript">

$(document).ready(function(){
    $('.angka').number(true,3,',','.');
    $('.angka_det').number(true,3,',','.');
    
    $("#idkayu2,#idkayu3").ajaxChosen({
        method: 'GET',
        url: '{client_url}ekspor/find_jenis_kayu',
        dataType: 'json',
        minTermLength: 3,
        afterTypeDelay: 300,
    },function (data) {
        var terms = {};
        
        $.each(data.jenis_kayu, function (i,kayu) {
            terms[kayu.idkayu] = kayu.nama;
        });
        
        return terms;
    });
    
    $("#idkayu").ajaxChosen({
        method: 'GET',
        url: '{client_url}ekspor/find_jenis_kayu',
        dataType: 'json',
        minTermLength: 3,
        afterTypeDelay: 300,
    },function (data) {
        var terms = {};
        
        $.each(data.jenis_kayu, function (i,kayu) {
            terms[kayu.idkayu] = kayu.nama;
        });
        
        return terms;
    });	 
    
    $("#idproduct").ajaxChosen({
        method: 'GET',
        url: '{client_url}ekspor/find_product',
        dataType: 'json',
        minTermLength: 3,
        afterTypeDelay: 300
    },function (data) {
        var terms = {};
        
        $.each(data.list_product, function (i,product) {
            terms[product.idproduk] = product.produk;
        });
        
        return terms;
    });	
    
    $("#idproduct").change(function(){
        $.ajax({
          url: '{client_url}ekspor/find_hscode/'+$(this).val(),
          dataType: 'json',
          success: function(data) {
            $("#hscode").val(data);
          }
        });
    });
    
    
    $("#cmdAdd").click(function(){
        var valid = validate_detail();
        if(!valid) return false;
                
        //var duplicate = false;
        
        if($("#nomor").val() != ''){
            var no = $("#nomor").val();
            var content = "";
        }else{
            var no = $('#detail_list tr').length - 1;
            var content = "<tr>";
            /*
            $('#detail_list tbody tr').filter(function (){
                var $cells = $(this).children('td');
                if($cells.eq(1).text() === $("#idkayu").val()){
                    alert("Jenis Kayu " + $("#idkayu option:selected").text() + " sudah ada didalam daftar di baris ke " + $cells.eq(0).text());
                    duplicate = true;
                }
            });	*/
        }
        
        //if(duplicate) return false;
        // console.log($("#idkayu2 option:selected").text());
        // console.log($("#idkayu3 option:selected").text());
        // console.log($("#idnegaraasal2 option:selected").text());
        // console.log($("#idnegaraasal3 option:selected").text());
        
        content += "<td width='5%'>" + no + "</td>";  //0
        content += "<td width='10%'>" + $("#fako").val(); + "</td>"; //1
        content += "<td style='display:none'>" + $("#idproduct").val(); + "</td>"; //2
        content += "<td width='10%'>" + $("#idproduct option:selected").text(); + "</td>"; //3
        content += "<td width='10%'>" + $("#hscode").val(); + "</td>"; //4
        content += "<td style='display:none'>" + $("#volume").val(); + "</td>"; //5
        content += "<td width='5%' class='angka'>" + $("#volume").val(); + "</td>"; //6
        content += "<td style='display:none'>" + $("#sat_volume").val(); + "</td>"; //7
        content += "<td width='5%' class='angka'>" + $("#berat").val(); + "</td>"; //8
        content += "<td style='display:none'>" + $("#berat").val(); + "</td>"; //9
        content += "<td style='display:none'>" + $("#sat_berat").val(); + "</td>"; //10
        content += "<td width='5%' class='angka'>" + $("#jumlah").val(); + "</td>";//11 
        content += "<td style='display:none'>" + $("#jumlah").val(); + "</td>"; //12
        content += "<td style='display:none'>" + $("#sat_jumlah").val(); + "</td>"; //13
        content += "<td style='display:none'>" + $("#idkayu").val(); + "</td>"; //14
        content += "<td style='display:none'>" + $("#idkayu option:selected").text(); + "</td>"; //15
        content += "<td style='display:none'>" + $("#idkayu2").val(); + "</td>"; //16
        content += "<td style='display:none'>" + $("#idkayu2 option:selected").text(); + "</td>"; //17
        content += "<td style='display:none'>" + $("#idkayu3").val(); + "</td>"; //18
        content += "<td style='display:none'>" + $("#idkayu3 option:selected").text(); + "</td>"; //19
        content += "<td width='10%'>" + $("#idkayu option:selected").text() + "<br/>" + $("#idkayu2 option:selected").text() + "<br/>" + $("#idkayu3 option:selected").text() + "</td>"; //20
        content += "<td style='display:none'>" + $("#idnegaraasal").val(); + "</td>"; //21
        content += "<td style='display:none'>" + $("#idnegaraasal option:selected").text(); + "</td>"; //22
        content += "<td style='display:none'>" + $("#idnegaraasal2").val(); + "</td>"; //23
        content += "<td style='display:none'>" + $("#idnegaraasal2 option:selected").text(); + "</td>"; //24
        content += "<td style='display:none'>" + $("#idnegaraasal3").val(); + "</td>"; //25
        content += "<td style='display:none'>" + $("#idnegaraasal3 option:selected").text(); + "</td>"; //26
        content += "<td width='10%'>" + $("#idnegaraasal option:selected").text() + "<br/>" + $("#idnegaraasal2 option:selected").text() + "<br/>" + $("#idnegaraasal3 option:selected").text() + "</td>"; //27
        content += "<td width='5%' class='angka'>" + $("#fob").val(); + "</td>"; //28
        content += "<td style='display:none'>" + $("#fob").val(); + "</td>"; //29
        content += "<td style='display:none'>" + $("#cnf").val(); + "</td>"; //30
        content += "<td style='display:none'>" + $("#cif").val(); + "</td>"; //31
        content += "<td width='5%'><a href='#' class='edit'><i class='icon-pencil'></i></a>&nbsp;&nbsp;<a href='#' class='hapus'><i class='icon-remove'></i></a> </td>";
        
        
        if($("#rowindex").val() != ''){
            $('#detail_list tbody tr:eq(' + $("#rowindex").val() + ')').html(content);
        }else{
            content += "</tr>";
            $('#detail_list tbody').append(content);
        }

        $("#cmdClear").trigger('click');
        $('.angka_det').number(true,3,',','.');
    });
    
    function validate_detail()
	{
		if($("#idsortimen").val() == ""){
			alert("Sortimen Belum Dipilih");
			$("#idsortimen").trigger("liszt:activate");
			return false;
		}
		
		if($("#dok_jumlah").val() == "" && $("#dok_volume").val() == "" && $("#dok_berat").val() == ""){
			alert("Jumlah Penerimaan (Document) Belum Diisi");
			$("#dok_jumlah").focus();
			return false;
		}
        
        if($("#fisik_jumlah").val() == "" && $("#fisik_volume").val() == "" && $("#fisik_berat").val() == ""){
			alert("Jumlah Penerimaan (Fisik) Belum Diisi");
			$("#fisik_jumlah").focus();
			return false;
		}
		
		
		return true;
	}
    
    
    $(document).on("click",".edit",function(){
			
        $("#rowindex").val($(this).closest('tr')[0].sectionRowIndex);
        $("#nomor").val($(this).closest('tr').find("td:eq(0)").html());
        $("#fako").val($(this).closest('tr').find("td:eq(1)").html());
        $("#hscode").val($(this).closest('tr').find("td:eq(4)").html());
        $("#volume").val($(this).closest('tr').find("td:eq(6)").html());
        $("#sat_volume").val($(this).closest('tr').find("td:eq(7)").html());
        $("#berat").val($(this).closest('tr').find("td:eq(9)").html());
        $("#sat_berat").val($(this).closest('tr').find("td:eq(10)").html());
        $("#jumlah").val($(this).closest('tr').find("td:eq(12)").html());
        $("#sat_jumlah").val($(this).closest('tr').find("td:eq(13)").html());
        
        $("#fob").val($(this).closest('tr').find("td:eq(29)").html());
        $("#cnf").val($(this).closest('tr').find("td:eq(30)").html());
        $("#cif").val($(this).closest('tr').find("td:eq(31)").html());
        
        var idproduct = $(this).closest('tr').find("td:eq(2)").html();
        var nama_product = $(this).closest('tr').find("td:eq(3)").html();
        
        var idkayu = $(this).closest('tr').find("td:eq(14)").html();
        var nama_jenis = $(this).closest('tr').find("td:eq(15)").html();
        
        var idkayu2 = $(this).closest('tr').find("td:eq(16)").html();
        var nama_jenis2 = $(this).closest('tr').find("td:eq(17)").html();
        
        var idkayu3 = $(this).closest('tr').find("td:eq(18)").html();
        var nama_jenis3 = $(this).closest('tr').find("td:eq(19)").html();
        
        
        var negara_asal = $(this).closest('tr').find("td:eq(21)").html();
        var nama_negara_asal = $(this).closest('tr').find("td:eq(22)").html();
        
        var negara_asal2 = $(this).closest('tr').find("td:eq(23)").html();
        var nama_negara_asal2 = $(this).closest('tr').find("td:eq(24)").html();
        
        var negara_asal3 = $(this).closest('tr').find("td:eq(25)").html();
        var nama_negara_asal3 = $(this).closest('tr').find("td:eq(26)").html();
        
        $('#idproduct').find('option').remove().end()
        .append('<option value="">- Pilih Produk -</option><option value="'+ idproduct +'">' + nama_product + '</option>')
        .val(idproduct)
        .trigger("liszt:updated");
        
        $('#idkayu').find('option').remove().end()
        .append('<option value="">- Pilih Jenis Kayu -</option><option value="'+ idkayu +'">' + nama_jenis + '</option>')
        .val(idkayu)
        .trigger("liszt:updated");
        
        $('#idkayu2').find('option').remove().end()
        .append('<option value="">- Pilih Jenis Kayu -</option><option value="'+ idkayu2 +'">' + nama_jenis2 + '</option>')
        .val(idkayu2)
        .trigger("liszt:updated");
        
        $('#idkayu3').find('option').remove().end()
        .append('<option value="">- Pilih Jenis Kayu -</option><option value="'+ idkayu3 +'">' + nama_jenis3 + '</option>')
        .val(idkayu3)
        .trigger("liszt:updated");
        
        $('#idnegaraasal').find('option').remove().end()
        .append('<option value="">- Pilih Negara -</option><option value="'+ negara_asal +'">' + nama_negara_asal + '</option>')
        .val(negara_asal)
        .trigger("liszt:updated");
        
        $('#idnegaraasal2').find('option').remove().end()
        .append('<option value="">- Pilih Negara -</option><option value="'+ negara_asal2 +'">' + nama_negara_asal2 + '</option>')
        .val(negara_asal2)
        .trigger("liszt:updated");
        
        $('#idnegaraasal3').find('option').remove().end()
        .append('<option value="">- Pilih Negara -</option><option value="'+ negara_asal3 +'">' + nama_negara_asal3 + '</option>')
        .val(negara_asal3)
        .trigger("liszt:updated");
    
        $("#jumlah").focus();
        return false;
    });
    $(document).on("click",".hapus",function(){
        if(confirm("Hapus Data ?") == true){
            $(this).closest('td').parent().remove();
            $("#idkayu").trigger("liszt:activate");
            $("#idkayu2").trigger("liszt:activate");
            $("#idkayu3").trigger("liszt:activate");
            $("#idnegaraasal").trigger("liszt:activate");
            $("#idnegaraasal2").trigger("liszt:activate");
            $("#idnegaraasal3").trigger("liszt:activate");
            $("#idproduct").trigger("liszt:activate");
        }
        return false;
    });
    $("#cmdClear").click(function(){
        $(".detail").val('');
        $('#idkayu').find('option').remove().end().append('<option value="">- Pilih Jenis Kayu -</option>').val('').trigger("liszt:updated");
        $('#idkayu2').find('option').remove().end().append('<option value="">- Pilih Jenis Kayu -</option>').val('').trigger("liszt:updated");
        $('#idkayu3').find('option').remove().end().append('<option value="">- Pilih Jenis Kayu -</option>').val('').trigger("liszt:updated");
        $('#idnegaraasal').find('option').remove().end().append('<option value="">- Pilih Negara Asal -</option>').val('').trigger("liszt:updated");
        $('#idnegaraasal2').find('option').remove().end().append('<option value="">- Pilih Negara Asal -</option>').val('').trigger("liszt:updated");
        $('#idnegaraasal3').find('option').remove().end().append('<option value="">- Pilih Negara Asal -</option>').val('').trigger("liszt:updated");
        $('#idproduct').find('option').remove().end().append('<option value="">- Pilih Jenis Sortimen -</option>').val('').trigger("liszt:updated");
        $("#idproduct").trigger("liszt:activate");
    });
    
    $(document).on("click","#btnsimpan",function(){
        var tbl = $('table#detail_list tbody tr').get().map(function(row) {
          return $(row).find('td').get().map(function(cell) {
            return $(cell).html();
          });
        });
        $("#detail_value").val(JSON.stringify(tbl));
        $("#frmTransaksi").submit();
    });
    
    
    $("input").bind("keypress", function(e) {
        if (e.keyCode == 13){
            return false;
        }
	});
    
    $("#idnegara").ajaxChosen({
        method: 'GET',
        url: '{client_url}ekspor/find_negara',
        dataType: 'json',
        minTermLength: 3,
        afterTypeDelay: 300
    },function (data) {
        var terms = {};
        
        $.each(data.negara, function (i,ngr) {
            terms[ngr.idnegara] = ngr.negara;
        });
        
        return terms;
    });	

    $("#idnegaraasal,#idnegaraasal2,#idnegaraasal3").ajaxChosen({
        method: 'GET',
        url: '{client_url}ekspor/find_negara',
        dataType: 'json',
        minTermLength: 3,
        afterTypeDelay: 300,
    },function (data) {
        var terms = {};
        
        $.each(data.negara, function (i,ngr) {
            terms[ngr.idnegara] = ngr.negara;
        });
        
        return terms;
    });	

    $("#idnegara").change(function(){
        $("#country_name").val($("#idnegara option:selected").text());
        $("#iso").val($("#idnegara").val());
    });
    
    $("#idbuyer").ajaxChosen({
        method: 'GET',
        url: '{client_url}ekspor/find_buyer',
        dataType: 'json',
        minTermLength: 3,
        afterTypeDelay: 300
    },function (data) {
        var terms = {};
        
        $.each(data.buyer_list, function (i,buyer) {
            terms[buyer.idbuyer] = buyer.buyer;
        });
        
        return terms;
    });	
    
    $("#idbuyer").change(function(){
        $("#namabuyer").val($("#idbuyer option:selected").text());
        $.ajax({
          url: '{client_url}ekspor/buyer_detail/'+$(this).val(),
          dataType: 'json',
          success: function(data) {
            console.log(data);
            $("#namabuyer").val(data.buyer);
            $("#alamat").val(data.alamat);
            //$("#idnegara").val(data.idnegara);
            //$("#iso").val(data.idnegara);
            //$("#country_name").val(data.country_name);
          }
        });
    });
    
    $("#valuta").ajaxChosen({
        method: 'GET',
        url: '{client_url}ekspor/find_valuta',
        dataType: 'json',
        minTermLength: 3,
        afterTypeDelay: 300
    },function (data) {
        var terms = {};
        
        $.each(data.valuta_list, function (i,valuta) {
            terms[valuta.kode] = valuta.kode+' - '+valuta.nama;
        });
        
        return terms;
    });	

    $("#valuta").change(function(){
        $("#nama_valuta").val($("#valuta option:selected").text())
    });
    
    
    $("#loading").ajaxChosen({
        method: 'GET',
        url: '{client_url}ekspor/find_loading',
        dataType: 'json',
        minTermLength: 3,
        afterTypeDelay: 300
    },function (data) {
        var terms = {};
        
        $.each(data.loading_list, function (i,loading) {
            terms[loading.idloading] = loading.kode+' ['+loading.uraian+']';
        });
        
        return terms;
    });	

    $("#loading").change(function(){
        $("#loading_name").val($("#loading option:selected").text())
    });
    
    
    $("#discharge").ajaxChosen({
        method: 'GET',
        url: '{client_url}ekspor/find_discharge',
        dataType: 'json',
        minTermLength: 1,
        afterTypeDelay: 300,
        beforeSend: function(xhr, opts) {
            opts.url += '&idnegara='+$("#iso").val();
        },
    },function (data) {
        var terms = {};
        
        $.each(data.discharge_list, function (i,discharge) {
            terms[discharge.iddischarge] = discharge.kode+' ['+discharge.uraian+']';
        });
        
        return terms;
    });	

    $("#discharge").change(function(){
        $("#discharge_name").val($("#discharge option:selected").text())
    });
    
    });
</script>