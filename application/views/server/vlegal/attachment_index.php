<div class="page-header">
	<h1>{first_title} <small>{second_title}</small></h1>
</div>  
<?php echo error_success($this->session)?>
<div class="row-fluid">
	<div class="span12">                    
		<div class="head">
			<div class="isw-grid"></div><h1> {table_title}</h1>
            
			<ul class="buttons">
				<li>
					<a href="{admin_url}vlegal/approved" class="isw-left_circle" title="Back"></a>
					
				</li>
			</ul>                        
			<div class="clear"></div>
		</div>
		<div class="block-fluid table-sorting">
			<div class="dataTables_wrapper">
            <!--
            <div class="dataTables_length" style="width:auto;">
                <button class="btn popupbutton" type="button" id="popup">Advanced Filter</button>
                <div class="dialog popdialog" id="popdialog" style="display: none;" title="Advanced Filter">
				<form id="validationsmall" action="{site_url}admin/transaksi/go_hasil_filter" method="post">
				<table border="0" class="table-form">
					<tr>
						<td>Client</td>
						<td>:</td>
						<td>
							<select name="client_id" id="client_id">
							<?php if ($client_id != ''){ ?>
                                <option value="{client_id}"><?php echo $client_nama; ?></option>
                            <?php }else{ ?>
                                <option value="">Pilih Client</option>
                            <?php } ?>	
							</select>
						</td>
					</tr>
                    <tr>
						<td>Jenis Dokumen</td>
						<td>:</td>
						<td>
							<select size="1" name="jenis_dokumen" id="jenis_dokumen">
							<option <?= ($jenis_dokumen == '')? 'selected="selected"' : ''?> value="0">-- All --</option>
                            <option <?= ($jenis_dokumen == 'lmk')? 'selected="selected"' : ''?> value="lmk">LMK</option>
                            <option <?= ($jenis_dokumen == 'vlegal-request')? 'selected="selected"' : ''?> value="vlegal-request">Permohonan</option>
                            <option <?= ($jenis_dokumen == 'vlegal-invoice')? 'selected="selected"' : ''?> value="vlegal-invoice">Invoice</option>
                            <option <?= ($jenis_dokumen == 'vlegal-packing')? 'selected="selected"' : ''?> value="vlegal-packing">Packing List</option>
                            <option <?= ($jenis_dokumen == 'vlegal-peb')? 'selected="selected"' : ''?> value="vlegal-peb">PEB</option>
                            <option <?= ($jenis_dokumen == 'vlegal-bl-pascaekspor')? 'selected="selected"' : ''?> value="vlegal-bl-pascaekspor">Bill of Lading</option>
                            <option <?= ($jenis_dokumen == 'lap_produksi')? 'selected="selected"' : ''?> value="lap_produksi">Lap. Produksi</option>
							</select>
						</td>
					</tr>
					<tr>
						<td><button type="submit" class="btn">Filter</button></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>	
				</table>
				</form>
				</div>
			</div>
            -->
            <!--
            <div class="dataTables_filter" id="tSortable_filter" style="width:auto;"> 
                <form action="{admin_url}vlegal/search_attachment" method="post" style="margin-bottom:0;">
				<label>Search: <input type="text" style="width:300px;" value="{searchText}" name="searchText" placeholder="Search By No. Invoice or Client Name"></label>
				</form>
			</div>
            -->
			<table cellpadding="0" cellspacing="0" width="100%" class="table">
				<thead>
					<tr>                                    
						<th width="5%">No.</th>
						<th width="10%">Jenis Dokumen</th>
						<th width="10%">No. Dokumen</th>
						<th width="10%">Keterangan</th>
						<th width="10%">Upload Date</th>
						<th width="10%">Download Attachment</th>
						<th width="10%">Status</th>
                        <!--
                        <th width="10%">Option</th>                                                 
                        -->
					</tr>
				</thead>
				<tbody>
					<?php foreach($query as $row){ ?>
					<?php $no = $no + 1 ?>
					<tr>                                    
						<td><?= $no?></td>
						<td><?php echo define_jenis_dokumen($row->attach_doc)?></td>
						<td><?php echo $row->no_doc ?></td>							
						<td><?php echo $row->attach_keterangan ?></td>							
						<td><?php echo human_date_time($row->attach_date) ?></td>							
						<td><?php echo tool_download('assets/lampiran/'.$row->client_id."/".$row->attach_file) ?></td>							
						<td><?php echo ($row->attach_status == 1)? "Active" : "Inactive" ?></td>
						<!--
                        <td>
                            <div class="btn-group">                                        
                                <button data-toggle="dropdown" class="btn dropdown-toggle">Action <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="{admin_url}vlegal/edit_attachment/<?= $row->attach_id ?>">Edit</a></li>
                                    <li><a href="{admin_url}vlegal/delete_attachment/<?= $row->attach_id ?>" onClick="return confirm('Delete Attachment?')">Delete</a></li>
                                </ul>
                            </div>
						</td>                                    
                        -->
					</tr>                                
					<?php } ?>
				</tbody>
			</table>
			{pagination}
		</div>
		<div class="clear"></div>
	</div>                                
</div>
<link href="{css_path}chosen.css" rel="stylesheet" type="text/css" />
<script src="{js_path}chosen.jquery.js" type="text/javascript"></script>
<script src="{js_path}ajax-chosen.js" type="text/javascript"></script>
<script type="text/javascript">

$(document).ready(function(){
	$("#client_id").ajaxChosen({
        method: 'GET',
        url: '{admin_url}buyer/find_client',
        dataType: 'json',
        minTermLength: 3,
        afterTypeDelay: 300
    },function (data) {
        var terms = {};
        
        $.each(data.client, function (i,sup) {
            terms[sup.id] = sup.nama;
        });
        
        return terms;
    });	 
});

</script>