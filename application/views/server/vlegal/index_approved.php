<div class="page-header">
	<h1>{first_title} <small>{second_title}</small></h1>
</div>  
<?php echo error_success($this->session)?>
<div class="row-fluid">
	<div class="span12">                    
		<div class="head">
			<div class="isw-grid"></div><h1> {table_title}</h1>
			<div class="clear"></div>
		</div>
		<div class="block-fluid table-sorting">
			<div class="dataTables_wrapper">
            <div class="dataTables_filter" id="tSortable_filter" style="width:auto;">
				<form action="{admin_url}vlegal/search_approved" method="post" style="margin-bottom:0;">
				<label>Search: <input type="text" style="width:300px;" value="{searchText}" name="searchText" placeholder="Search By No. Invoice or Client Name"></label>
				</form>
			</div>
			<table cellpadding="0" cellspacing="0" width="100%" class="table">
				<thead>
					<tr>                                    
						<th width="5%">No.</th>
						<th width="15%">No. Dokumen</th>
						<th width="10%">No. Invoice</th>
						<th width="7%">Tanggal</th>
						<th width="10%">Barcode</th>
						<th width="20%">Klien</th>
                        <th width="7%">Reviewer</th>
                        <th width="10%">Option</th>                                                 
					</tr>
				</thead>
				<tbody>
					<?php foreach($query as $row){ ?>
					<?php $no = $no + 1 ?>
					<tr>                                    
						<td><?= $no?></td>
						<td><?php echo $row->no_dokumen?></td>
						<td><?php echo $row->invoice?></td>
						<td><?php echo human_date_time($row->submitdate) ?></td>							
						<td style="display:none"><?php echo human_date_short($row->tglinvoice) ?></td>							
						<td><?php echo $row->barcode ?></td>
						<td><?php echo $row->nama_client ?></td>
						<td><?php echo $row->reviewer ?></td>
						<td>
                            <?php echo tool_download_vlegal('server/vlegal/downloadVLegal/'.$row->vid) ?>
                            <?php echo tool_resend_vlegal('server/vlegal/resendVLegalEmail/'.$row->vid) ?>
                            <?php echo tool_cancel_vlegal('#freject',$row->idekspor) ?>
                            <?php echo tool_attachment('server/vlegal/attachment/'.$row->idekspor) ?>
                            
						</td>                                    
					</tr>                                
					<?php } ?>
				</tbody>
			</table>
			<?php echo $pagination ?>
			
		</div>
		<div class="clear"></div>
	</div>                                
</div>
<div id="freject" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
    <h3 id="myModalLabel">Reject Dokumen</h3>
</div>        
<?php echo form_open('server/vlegal/cancelDocument','id = "frmTransaksi2"') ?>
<div class="row-fluid">
    <div class="block-fluid">
        <div class="row-form clearfix">
            <div class="span3">Nama Client:</div>
            <div class="span9" id="nama_client">&nbsp;</div>
        </div>            
        <div class="row-form clearfix">
            <div class="span3">No. Invoice:</div>
            <div class="span9" id="invoice">&nbsp;</div>                    
        </div>  
        <div class="row-form clearfix">
            <div class="span3">Tgl. Invoice</div>
            <div class="span9" id="tglinvoice">&nbsp;</div>                    
        </div>
        <div class="row-form clearfix">
            <div class="span3">Alasan Cancel:</div>
            <div class="span9"><textarea class="validate[required]" id="alasan" name="alasan" placeholder="Alasan Cancel"></textarea></div>                    
        </div>                                    
                                                      
    </div>                
</div>   

<div class="modal-footer">
    <button class="btn btn-danger" type="submit" aria-hidden="true">Cancel Dokumen</button> 
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>            
</div>
<input type="hidden" name="idekspor" id="idekspor"/>
<?php echo form_close() ?>
</div>  