<div class="page-header">
	<h1>{first_title} <small>{second_title}</small></h1>
</div>  
<?php echo error_success($this->session)?>
<?php if($error != '') echo error_message($error)?>
<div class="row-fluid">
	<div class="span12">
		<div class="head">
			<div class="isw-documents"></div>
			<h1>SUPPLIER FORM</h1>
			<ul class="buttons">
				<li><a href="{admin_url}supplier" class="isw-list" title="Supplier List"></a></li>                                                        
			</ul>
			<div class="clear"></div>
		</div>
		<div class="block-fluid">                        
			<?php echo form_open('server/supplier/save','id = "adminform"') ?>
			<div class="row-form">
				<div class="span2">Client:</div>
				<div class="span4">
					<select name="client_id" id="client_id">
						<?php if ($client_id != ''){ ?>
							<option value="{client_id}"><?php echo $client_nama; ?></option>
						<?php }else{ ?>
							<option value="0">Pilih Client</option>
						<?php } ?>
						
					</select>
				</div>	
                
				<div class="clear"></div>
			</div> 
			<div class="row-form">
				<div class="span2">Negara:</div>
				<div class="span10">
					<select name="idnegara">
						<option value="0">Pilih Negara</option>
						<?php foreach ($negara_list as $row){ ?>
							<option <?=($row->idnegara==$idnegara ? 'selected':'') ?> value=<?=$row->idnegara ?>><?=$row->negara ?></option>
						<?php } ?>
						
					</select>
				</div>				
				<div class="clear"></div>
			</div> 
			<div class="row-form">
				<div class="span2">Nama Supplier:</div>
				<div class="span10"><input type="text" name="supplier" placeholder="Nama Supplier" value="{supplier}"/></div>
				<div class="clear"></div>
			</div> 
			<div class="row-form">
				<div class="span2">Alamat:</div>
				<div class="span10"><input type="text" name="alamat" placeholder="Alamat Supplier" value="{alamat}"/></div>
				<div class="clear"></div>
			</div>
			<div class="row-form">
				<div class="span2">Status:</div>
				<div class="span10"><input type="checkbox" name="status" value="{status}"  checked>Active</div>
				<div class="clear"></div>
			</div>			
			<div class="row-form">
				<div class="span12"><button class="btn" type="submit" id="btnsimpan">Simpan</button></div>
				<div class="clear"></div>
			</div> 
			
			<input type="hidden" name="negara" id="negara" value="{negara}"/>
			<?php echo form_hidden('idsupplier', $idsupplier); ?>
			<?php echo form_close() ?>
		</div>
	</div>
	
</div>
<link href="{css_path}chosen.css" rel="stylesheet" type="text/css" />
<script src="{js_path}chosen.jquery.js" type="text/javascript"></script>
<script src="{js_path}ajax-chosen.js" type="text/javascript"></script>
<script type="text/javascript">

$(document).ready(function(){
	$("#client_id").ajaxChosen({
        method: 'GET',
        url: '{admin_url}buyer/find_client',
        dataType: 'json',
        minTermLength: 3,
        afterTypeDelay: 300
    },function (data) {
        var terms = {};
        
        $.each(data.client, function (i,sup) {
            terms[sup.id] = sup.nama;
        });
        
        return terms;
    });	 
});

</script>