<?
function basic_info() {
	$CI =& get_instance();
	$data = array(
		'site_url' 						=> site_url(),	
		'base_url' 						=> base_url(),
		'css_path' 						=> base_url().'assets/css/',
		'js_path' 						=> base_url().'assets/js/',
		'images_path' 					=> base_url().'assets/img/',

		
	);
	return $data;
}

function admin_info() {
	$CI =& get_instance();
	$data = array(
		'site_url' 						=> site_url(),
		'admin_url'						=> site_url()."server/",			
		'client_url'					=> site_url()."client/",			
		'base_url' 						=> base_url(),
		'current_url' 					=> current_url(),
		'css_path' 						=> base_url().'assets/css/',
		'js_path' 						=> base_url().'assets/js/',
		'tinymce_path' 					=> base_url().'assets/js/tiny_mce/',
		'images_path' 					=> base_url().'assets/img/',
		'avatar_path' 					=> 'http://localhost/audittuv/assets/avatar/',
		'avatar_thumbs_path' 			=> 'http://localhost/audittuv/assets/avatar/thumbs/',
		
		'current_date'					=> date("Y-m-d"),
		'current_date_time'				=> date("Y-m-d H:i:s"),
		
		'client_id' 					=> $CI->session->userdata('client_id'),
		'client_name' 					=> $CI->session->userdata('client_name'),
		'client_alamat' 				=> $CI->session->userdata('client_alamat'),
		'def_stuffing' 					=> $CI->session->userdata('def_stuffing'),
		'avatar' 					    => ($CI->session->userdata('avatar') == '')? 'default.jpg' : $CI->session->userdata('avatar'),
		'active' 						=> $CI->session->userdata('active'),
		'client_last_login' 			=> ($CI->session->userdata('last_login') != null)? human_date_admin($CI->session->userdata('last_login')) : "First Login",
		'user_role' 					=> type_user($CI->session->userdata('role')),
        
        'user_id' 						=> $CI->session->userdata('id'),
		'user_email' 					=> $CI->session->userdata('username'),
		'user_name' 					=> $CI->session->userdata('nama'),
		'user_avatar' 					=> ($CI->session->userdata('avatar') == '')? 'default.jpg' : $CI->session->userdata('avatar'),
		'user_last_login' 				=> ($CI->session->userdata('last_login') != null)? human_date_admin($CI->session->userdata('last_login')) : "First Login",
		'user_role' 					=> type_user($CI->session->userdata('role')),
	);
	return $data;
}


function server_info() {
	$CI =& get_instance();
	$data = array(
		'site_url' 						=> site_url(),
		'admin_url'						=> site_url()."server/",			
		'client_url'					=> site_url()."client/",			
		'base_url' 						=> base_url(),
		'current_url' 					=> current_url(),
		'css_path' 						=> base_url().'assets/css/',
		'js_path' 						=> base_url().'assets/js/',
		'tinymce_path' 					=> base_url().'assets/js/tiny_mce/',
		'images_path' 					=> base_url().'assets/img/',
		
		'current_date'					=> date("Y-m-d"),
		
		'article_original01' 			=> base_url().'/assets/img/dinamic/image_master_article/01/',
		'article_thumbs01' 				=> base_url().'/assets/img/dinamic/image_master_article/01/thumbs/',
		'article_original02' 			=> base_url().'/assets/img/dinamic/image_master_article/02/',
		'article_thumbs02' 				=> base_url().'/assets/img/dinamic/image_master_article/02/thumbs/',
		'article_original03' 			=> base_url().'/assets/img/dinamic/image_master_article/03/',
		'article_thumbs03' 				=> base_url().'/assets/img/dinamic/image_master_article/03/thumbs/',
		'article_default' 				=> base_url().'/assets/img/dinamic/image_master_article/default.jpg',
		
		'bahan_baku_original' 			=> base_url().'assets/img/dinamic/image_bahan_baku/',
		'bahan_baku_thumbs' 			=> base_url().'assets/img/dinamic/image_bahan_baku/thumbs/',
		
		'barang_inventaris_original' 	=> base_url().'assets/img/dinamic/image_barang_inventaris/',
		'barang_inventaris_thumbs' 		=> base_url().'assets/img/dinamic/image_barang_inventaris/thumbs/',
		
		'user_avatar_original' 			=> base_url().'assets/img/dinamic/image_user_avatar/',
		'user_avatar_thumbs' 			=> base_url().'assets/img/dinamic/image_user_avatar/thumbs/',
		
		'karyawan_original' 			=> base_url().'assets/img/dinamic/image_karyawan/',
		'karyawan_thumbs' 				=> base_url().'assets/img/dinamic/image_karyawan/thumbs/',
		
		'supplier_ktp_original' 		=> base_url().'assets/img/dinamic/image_supplier_ktp/',
		'supplier_ktp_thumbs' 			=> base_url().'assets/img/dinamic/image_supplier_ktp/thumbs/',
		
		'user_id' 						=> $CI->session->userdata('id'),
		'user_email' 					=> $CI->session->userdata('username'),
		'user_name' 					=> $CI->session->userdata('nama'),
		'user_avatar' 					=> $CI->session->userdata('avatar'),
		'user_address' 					=> $CI->session->userdata('alamat'),
		'user_ktp' 						=> $CI->session->userdata('noktp'),
		'user_last_login' 				=> ($CI->session->userdata('last_login') != null)? human_date_admin($CI->session->userdata('last_login')) : "First Login",
		'user_role' 					=> type_user($CI->session->userdata('role')),
		
		'po_manager1'					=> $CI->config->item('po_manager1'),
		'po_manager2'					=> $CI->config->item('po_manager2'),
		'global_faktur_header'			=> $CI->config->item('global_faktur_header'),
		'biaya_pendaftaran'				=> number_format($CI->config->item('biaya_pendaftaran')),
		
	);
	return $data;
}



?>