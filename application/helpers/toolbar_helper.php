<?php

function tool_email($link) {
	$tool = anchor($link,'<span class="ibb-mail"></span>',array('title'=>'Resend Email'));
	return $tool;
}


function tool_edit($link) {
	$tool = anchor($link,'<span class="ibb-edit"></span>',array('title'=>'Edit'));
	return $tool;
}

function tool_audit($link) {
	$tool = anchor($link,'<span class="ibb-list"></span>',array('title' => "Input Audit Checklist"));
	return $tool;
}

function tool_qq($link) {
	$tool = anchor($link,'<span class="ibb-text_document"></span>',array('title' => "Input Q &amp; Q Check Detail"));
	return $tool;
}

function tool_submit_checklist($link) {
	$tool = anchor($link,'<span class="ibb-ok"></span>',array('title' => "Submit Checklist",'onClick' => 'return confirm(\'Submit Checklist ?\')'));
	return $tool;
}

function tool_approve_checklist($link) {
	$tool = anchor($link,'<span class="ibb-ok"></span>',array('title' => "Approve Checklist",'onClick' => 'return confirm(\'Approve Checklist ?\')'));
	return $tool;
}

function tool_reject_checklist($link) {
	$tool = anchor($link,'<span class="ibb-cancel"></span>',array('title' => "Reject Checklist",'onClick' => 'return confirm(\'Reject Checklist ?\')'));
	return $tool;
}

function tool_remove($link) {
	$tool =  anchor($link, '<span class="ibb-delete"></span>',array('title' => 'Remove','onClick' => 'return confirm(\'Delete ?\')'));
	return $tool;
}
function tool_active($link) {
	$tool =  anchor($link, '<span class="ibb-ok"></span>',array('title' => 'Activate','onClick' => 'return confirm(\'Activate ?\')'));
	return $tool;
}
function tool_publish($link) {
	$tool =  anchor($link, '<span class="ibb-up"></span>',array('title' => 'Publish','onClick' => 'return confirm(\'Publish ?\')'));
	return $tool;
}

function tool_unpublish($link) {
	$tool =  anchor($link, '<span class="ibb-down"></span>',array('title' => 'Unpublish','onClick' => 'return confirm(\'Unpublish ?\')'));
	return $tool;
}
function tool_unactive($link) {
	$tool =  anchor($link, '<span class="ibb-cancel"></span>',array('title' => 'Inactivate','onClick' => 'return confirm(\'Inactivate ?\')'));
	return $tool;
}

function tool_download($link) {
	$tool =  anchor($link, '<span class="ibb-download"></span>', array('title' => 'Download File','target' => '_blank'));
	return $tool;
}

function tool_view($link) {
	$tool =  anchor($link, '<span class="ibb-folder"></span>', array('title' => 'View'));
	return $tool;
}

function tool_download_pdf($link) {
	$tool =  anchor($link, '<span class="ibb-download"></span>', array('title' => 'Download Checklist as PDF','target' => '_blank'));
	return $tool;
}

function tool_posisi($link) {
	$tool = anchor($link,'<span class="ibb-text_document"></span>',array('title' => "Cek Posisi Audit SPBU"));
	return $tool;
}
function tool_reject_vlegal($link) {
	$tool = anchor($link,'<span class="ibb-reject-vlegal"></span>',array('title' => "Reject V-Legal"));
	return $tool;
}
function tool_accept_vlegal($link) {
	$tool = anchor($link,'<span class="ibb-accept-vlegal"></span>',array('title' => "Send to LIU"));
	return $tool;
}

function tool_download_vlegal($link) {
	$tool = anchor($link,'<span class="ibb-download-vlegal"></span>',array('title' => "Download Dokumen V-Legal"));
	return $tool;
}

function tool_resend_vlegal($link) {
	$tool = anchor($link,'<span class="ibb-resend-vlegal"></span>',array('title' => "Resend V-Legal to Client",'onClick' => 'return confirm(\'Resend Dokumen V-Legal to Client ?\')'));
	return $tool;
}

function tool_cancel_vlegal($link,$id) {
	//$tool = anchor($link,'<span class="ibb-cancel-vlegal"></span>',array('title' => "Cancel Dokumen V-Legal",'onClick' => 'return confirm(\'Cancel Dokumen V-Legal ?\')'));
    $tool = "<a href=\"$link\" data-toggle=\"modal\" data-idekspor=\"$id\" title=\"Cancel Dokumen V-Legal\" class=\"canceldoc\"><span class=\"ibb-cancel-vlegal\"></span></a>";
	return $tool;
}

function tool_finalize($link) {
	$tool = anchor($link,'<span class="ibb-ok"></span>',array('title' => "Finalize Transaksi",'onClick' => 'return confirm(\'Finalize Transaksi ?\')'));
	return $tool;
}

function tool_finalized($link,$text) {
	$tool = anchor($link,'<button class="btn success">Final</button>',array('title' => $text));
	return $tool;
}

function tool_attachment($link) {
	$tool = anchor($link,'<span class="ibb-documents"></span>',array('title' => "Lampiran"));
	return $tool;
}

function tool_approve_peb($link) {
	$tool = anchor($link,'<span class="ibb-ok"></span>',array('title' => "Approve PEB",'onClick' => 'return confirm(\'Approve PEB ?\')'));
	return $tool;
}

function tool_cancel_peb($link) {
	$tool = anchor($link,'<span class="ibb-cancel"></span>',array('title' => "Cancel PEB",'onClick' => 'return confirm(\'Cancel PEB ?\')'));
	return $tool;
}

?>