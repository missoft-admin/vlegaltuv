<?php 
function watermarkText($SourceFile, $WaterMarkText, $DestinationFile, $fontFile) {
    list($width, $height) = getimagesize($SourceFile);
    $image_p = imagecreatetruecolor($width, $height);
    $image = imagecreatefromjpeg($SourceFile);
    imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width, $height);
    //$black = imagecolorallocate($image_p, 0, 0, 0);
	$fontColor = imagecolorallocate($image_p, 255, 255, 255);
    $font = $fontFile;
	echo $fontFile;
    $font_size = 30;
    //imagettftext($image_p, $font_size, 0, 10, 35, $fontColor, $font, $WaterMarkText);
	imagettftext($image_p, $font_size, 0, $height - 10, $width - 50, $fontColor, $font, $WaterMarkText);
    if ($DestinationFile <> '') {
        imagejpeg($image_p, $DestinationFile, 100);
    } else {
        header('Content-Type: image/jpeg');
        imagejpeg($image_p, null, 100);
    }
    ;
    imagedestroy($image);
    imagedestroy($image_p);
}

function watermarkImage($SourceFile, $WaterMarkImage, $DestinationFile) {
    header('Content-Type: image/jpeg');
    
    $watermark = imagecreatefrompng($WaterMarkImage);
    $watermark_width = imagesx($watermark);
    $watermark_height = imagesy($watermark);
    $image = imagecreatetruecolor($watermark_width, $watermark_height);
    $image = imagecreatefromjpeg($SourceFile);
    $size = getimagesize($SourceFile);
    $dest_x = $size[0] - $watermark_width - 5;
    $dest_y = $size[1] - $watermark_height - 5;
    imagecopymerge($image, $watermark, $dest_x, $dest_y, 0, 0, $watermark_width, $watermark_height, 100);
    imagejpeg($image, $DestinationFile, 100);
	//imagejpeg($image);
    imagedestroy($image);
    imagedestroy($watermark);
}

function randomString($charNum = 7) {
    $codelenght = $charNum;
    while ($newcode_length < $codelenght) {
        $x = 1;
        $y = 3;
        $part = rand($x, $y);
        if ($part == 1) {
            $a = 48;
            $b = 57;
        } // Numbers
        if ($part == 2) {
            $a = 65;
            $b = 90;
        } // UpperCase
        if ($part == 3) {
            $a = 97;
            $b = 122;
        } // LowerCase
        $code_part = chr(rand($a, $b));
        $newcode_length = $newcode_length + 1;
        $newcode = $newcode.$code_part;
    }
    return $newcode;
}

// Generate Guid
function NewGuid() {
    $s = strtolower(md5(uniqid(rand(), true)));
    $guidText = substr($s, 0, 8).'-'.substr($s, 8, 4).'-'.substr($s, 12, 4).'-'.substr($s, 16, 4).'-'.substr($s, 20);
    return $guidText;
}
// End Generate Guid

function getIndonesianDay($int="1"){
	switch($int){
		case "7":
			$strReturn = "Minggu";
		break;
		case "6":
			$strReturn = "Sabtu";
		break;
		case "5":
			$strReturn = "Jum'at";
		break;
		case "4":
			$strReturn = "Kamis";
		break;
		case "3":
			$strReturn = "Rabu";
		break;
		case "2":
			$strReturn = "Selasa";
		break;
		case "1":
		default:
			$strReturn = "Senin";
		break;
	}
	return $strReturn;
}

function getIndonesianMonth($int="1"){
	switch($int){
		case "12":
			$strReturn = "Desember";
		break;
		case "11":
			$strReturn = "November";
		break;
		case "10":
			$strReturn = "Oktober";
		break;
		case "9":
			$strReturn = "September";
		break;
		case "8":
			$strReturn = "Agustus";
		break;
		case "7":
			$strReturn = "Juli";
		break;
		case "6":
			$strReturn = "Juni";
		break;
		case "5":
			$strReturn = "Mei";
		break;
		case "4":
			$strReturn = "April";
		break;
		case "3":
			$strReturn = "Maret";
		break;
		case "2":
			$strReturn = "Februari";
		break;
		case "1":
		default:
			$strReturn = "Januari";
		break;
	}
	return $strReturn;
}

function getEnglishMonth($int="1"){
	switch($int){
		case "12":
			$strReturn = "December";
		break;
		case "11":
			$strReturn = "November";
		break;
		case "10":
			$strReturn = "October";
		break;
		case "9":
			$strReturn = "September";
		break;
		case "8":
			$strReturn = "August";
		break;
		case "7":
			$strReturn = "July";
		break;
		case "6":
			$strReturn = "June";
		break;
		case "5":
			$strReturn = "May";
		break;
		case "4":
			$strReturn = "April";
		break;
		case "3":
			$strReturn = "March";
		break;
		case "2":
			$strReturn = "February";
		break;
		case "1":
		default:
			$strReturn = "January";
		break;
	}
	return $strReturn;
}

function getEnglishShortMonth($int="1"){
	switch($int){
		case "12":
			$strReturn = "Dec";
		break;
		case "11":
			$strReturn = "Nov";
		break;
		case "10":
			$strReturn = "Oct";
		break;
		case "9":
			$strReturn = "Sep";
		break;
		case "8":
			$strReturn = "Aug";
		break;
		case "7":
			$strReturn = "July";
		break;
		case "6":
			$strReturn = "June";
		break;
		case "5":
			$strReturn = "May";
		break;
		case "4":
			$strReturn = "Apr";
		break;
		case "3":
			$strReturn = "Mar";
		break;
		case "2":
			$strReturn = "Feb";
		break;
		case "1":
		default:
			$strReturn = "Jan";
		break;
	}
	return $strReturn;
}

function ReformatDate($date) // yyyy-mm-dd
{
	$d = explode('-', $date);
	return $d[2] . ' ' . getEnglishShortMonth($d[1]) . ' ' . $d[0];
}

function ReformatDate2($date) // yyyy-mm-dd
{
	$d = explode('-', $date);
	return $d[2] . '-' . getEnglishShortMonth($d[1]) . '-' . substr($d[0],-2);
}

function ReformatDateTime($date) // yyyy-mm-dd H:i:s
{
	$d = explode(' ', $date);
	$dd = explode('-', $d[0]);
	$ss = explode(':', $d[1]);
	return $dd[2] . ' ' . getEnglishShortMonth($dd[1]) . ' ' . $dd[0] .' '. $ss[0] . ':' .$ss[1];
}

function ReformatDateTime2($date) // yyyy-mm-dd H:i:s
{
	$d = explode(' ', $date);
	$dd = explode('-', $d[0]);
	$ss = explode(':', $d[1]);
	return $dd[2] . ' ' . getEnglishShortMonth($dd[1]) . ' ' . $dd[0];
}

function findFileExtension($filename) {
    $filename = strtolower($filename);
    #$exts = split("[/\\.]", $filename); 
	$exts = preg_split("[\.]", $filename);
    $n = count($exts) - 1;
    $exts = $exts[$n];
    return $exts;
}

function string_limit_words($string, $word_limit) {
	$words = explode(' ', $string);
	return implode(' ', array_slice($words, 0, $word_limit));
}

function slug($string, $delimiter = '-') {
	$new_string = strip_tags($string);
	$new_string = strtolower($new_string);
	$new_string = preg_replace("/&(.)(uml);/", "$1e", $new_string);
	$new_string = preg_replace("/&(.)(acute|cedil|circ|ring|tilde|uml);/", "$1", $new_string);
	$new_string = preg_replace("/([^a-z0-9]+)/", $delimiter, $new_string);
	$new_string = trim($new_string, $delimiter);
	return $new_string;
}

	
    function categoriesTree($data,$parent,$echo = '1'){
	if(isset($data[$parent])){
	      //$str = '<ul id="categoryTree">';
	      foreach($data[$parent] as $value){
		$child = categoriesTree($data,$value['term_id']);
		$str .= '<li>';
		$str .= $value['term_title'];
		if($child != ''){
		      $str .= '<ul>';
		      $str .= $child;
		      $str .= '</ul>';
		}
		$str .= '</li>';
	      }
	      //$str .= '</ul>';
	      return $str;
	}else{
	    return '';
	}
      }
      
      function categoriesCheckBox($data,$parent,$echo = '1'){
	if(isset($data[$parent])){
	      //$str = '<ul id="categoryTree">';
	      foreach($data[$parent] as $value){
		$child = categoriesCheckBox($data,$value['term_id']);
		$str .= '<li>';
		$str .= '<input type="checkbox" name="categories[]" value="' . $value['term_id'] . '" />' . $value['term_title'];
		if($child != ''){
		      $str .= '<ul>';
		      $str .= $child;
		      $str .= '</ul>';
		}
		$str .= '</li>';
	      }
	      //$str .= '</ul>';
	      return $str;
	}else{
	    return '';
	}
      }
      
      function categoriesTreeWithLink($data,$parent,$category,$echo = '1'){
	if(isset($data[$parent])){
	      //$str = '<ul id="categoryTree">';
	      foreach($data[$parent] as $value){
		$child = categoriesTreeWithLink($data,$value['term_id'],$category);
		$str .= '<li>';
		$str .= $value['term_title'] . '&nbsp;&nbsp;';
		$str .= '<a href=' . base_url() .'category/edit/'  . $value['term_id'] . ' title="Edit"><img src="' . base_url() . 'images/pencil.png" alt="Edit" /></a>';
		$str .= '<a onclick="return confirm(\'Are you sure want to remove this item?\')" href="' . base_url() . 'category/delete/' . $value['term_id'] . '"><img src="' . base_url() . 'images/cross.png" title="Delete" /></a>';
		$str .= '<a href="' . base_url() . 'category/add/' . $category .'/' . $value['term_id'] . '"><img src="' . base_url() . 'images/add_small.png" title="Add Child" /></a>';
		if($child != ''){
		      $str .= '<ul>';
		      $str .= $child;
		      $str .= '</ul>';
		}
		$str .= '</li>';
	      }
	      //$str .= '</ul>';
	      return $str;
	}else{
	    return '';
	}
      }
    
    function unzip($file){
	
	$dirName = dirname($file);
	//$zip=zip_open(realpath(".")."/".$file);
	$zip=zip_open($file);
	if(!$zip) {return("Unable to proccess file '{$file}'");}
	$e='';
	while($zip_entry=zip_read($zip)) {
	   $zdir=dirname(zip_entry_name($zip_entry));
	   $zname=zip_entry_name($zip_entry);
    
	   if(!zip_entry_open($zip,$zip_entry,"r")) {$e.="Unable to proccess file '{$zname}'";continue;}
	   if(!is_dir($zdir)) mkdir($zdir,0777);
    
	   //print "{$zdir} | {$zname} \n";
	   $zip_fs=zip_entry_filesize($zip_entry);
	   if(empty($zip_fs)) continue;
	   $zz=zip_entry_read($zip_entry,$zip_fs);
	   $z=fopen($dirName . '/' . $zname,"w");
	   fwrite($z,$zz);
	   fclose($z);
	   zip_entry_close($zip_entry);
	}
	zip_close($zip);
	return($e);
}

function parseDateTime($date){
	/*
	param : yyyy-mm-dd hh:ii:ss
	*/
	
		$int = preg_match("/(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/",$date,$match);
		if (!$int) return false;
		$data['year']	= $match[1];
		$data['month']	= $match[2];
		$data['day']	= $match[3];
		$data['hour']	= $match[4];
		$data['minute']	= $match[5];
		$data['second']	= $match[6];
		$data['day_of_week'] = date("N",mktime(0,0,0,intval($data['month']),intval($data['day']),intval($data['year'])));
		return $data;
}	
	function searchArray($myarray, $id,$sortimen) {
		foreach ($myarray as $item) {
			if ($item['idkayu'] == $id && $item['idsortimen']==$sortimen)
				return true;
		}
		return false;
	}

?>