<?php

class Home extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
	}
    
    function index()
    {
        redirect('client/home');
    }
	
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */