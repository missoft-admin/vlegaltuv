<?php

class Supplier extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		permission_basic_client($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('supplier_model');
		$this->client_id = $this->session->userdata('client_id');
	}
	
	function index()
	{
		$data = array();
		$data['template'] = 'supplier/index';	
		$data['query'] = $this->supplier_model->supplier_list($this->uri->segment(4),5,$this->client_id);		
		$data['first_title'] = 'Master';		
		$data['second_title'] = 'Supplier';
		$data['searchText'] = '';
		$data['total_list']=$this->supplier_model->count($this->client_id);
		$this->pagination->initialize(paging_admin($data['total_list'],'client/supplier/index',4,5));
		$data['pagination'] =  $this->pagination->create_links();
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Supplier','client/supplier'),
								   array('List','')
						     );
		$data['table_title'] = 'DATA SUPPLIER  " [TOTAL '.number_format($data['total_list']).' SUPPLIER] " ACTIVE';					 
		$data['no'] = $this->uri->segment(4);
		$data = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
	}
    
    function search($search = '',$limit = '')
	{
		if($this->input->post('searchText')==''){
			if($this->uri->segment(4) != ''){
				$data = array();
				$data['template'] = 'supplier/index';	
				$data['first_title'] = 'Search';		
                $data['second_title'] = 'Supplier';
				$data['query'] = $this->supplier_model->supplier_search(addslashes(rawurldecode($search)),$limit,5,$this->client_id);	
				$data['total_list']=$this->supplier_model->count_search(addslashes(rawurldecode($search)),$this->client_id);
				$this->pagination->initialize(paging_admin($data['total_list'],
															"client/supplier/search/$search/",5,5));		
				$data['pagination'] =  $this->pagination->create_links();
				
                $data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
                                       array('Master Supplier','client/supplier'),
                                       array('Search','')
                                 );
				$data['searchText'] = rawurldecode($search);
				$data['table_title'] = 'DATA SUPPLIER " [TOTAL '.$data['total_list'].' SUPPLIER] " HASIL PENCARIAN';
                $data['no'] = $limit;
				$data = array_merge($data,admin_info());
				$this->parser->parse('client/index',$data);
			}else{
				redirect('client/supplier');
			}
		}else{
			redirect('client/supplier/search/'.rawurlencode($this->input->post('searchText')));
		}
	}
		
	function add()
	{
		$data = array(
				'idsupplier' => '',
				'idnegara' => 'ID',
				'client_id' => $this->client_id,
				'client_nama' => $this->master_model->get_nama_client($this->client_id),
				'supplier' => '',
				'alamat' => '',
				'status' => '1',
				);
		
		$data['negara_list'] = $this->supplier_model->negara_list();		
		$data['template'] = 'supplier/add_new';		
		$data['first_title'] = 'Add';		
		$data['second_title'] = 'Supplier';
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Supplier','client/supplier'),
								   array('Add New','')
						     );
		$data['error'] = '';
		//print_r($data['negara_list']);exit();
		$data = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
	}
	
	function update($id)
	{
		if($id != ''){
			$row = $this->supplier_model->detail($id);
			if(isset($row->idsupplier)){
				$data = array(
						'idsupplier' => $row->idsupplier,
						'client_id' => $row->client_id,						
						'client_nama' => $row->client_nama,				
						'idnegara' => $row->idnegara,						
						'negara' => $row->negara,						
						'supplier' => $row->supplier,						
						'alamat' => $row->alamat,						
						'status' => $row->status,
						);	
						//print_r($data);exit();
				$data['negara_list'] = $this->supplier_model->negara_list();	
				$data['template'] = 'supplier/add_new';	
				$data['first_title'] = 'Edit';		
                $data['second_title'] = 'Supplier';
				$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Supplier','client/supplier'),
								   array('Edit','')
						     );
				$data['error'] = '';	
				$data = array_merge($data,admin_info());				
				$this->parser->parse('client/index',$data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','Data Tidak Ditemukan.');
				redirect('client/supplier','location');
			}		
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','Data Tidak Ditemukan.');
			redirect('client/supplier');
		}
	}
	
	function save()
	{
		$this->form_validation->set_rules('idnegara', 'Negara', 'trim|required|min_length[1]');
		$this->form_validation->set_rules('supplier', 'Nama Supplier', 'trim|required|min_length[1]');
		$this->form_validation->set_rules('alamat', 'Alamat Supplier', 'trim|required|min_length[1]');
		
		if ($this->form_validation->run() == TRUE){	
			
			
			if($this->input->post('idsupplier') == '' ) {
				if($this->supplier_model->insert()){
					//print_r('Beruasuil');exit();
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','Data Tersimpan.');
					redirect('client/supplier','location');
				}else{
					$this->failed_save($this->input->post('idsupplier'),true);
				}
			} else {
				if($this->supplier_model->update()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','Data Tersimpan.');
					redirect('client/supplier','location');
				}else{
					$this->failed_save($this->input->post('idsupplier'),true);
				}
			}
		}else{
			
			$this->failed_save($this->input->post('idsupplier'));
		}	
	}
	
	function failed_save($id,$dbact=false)
	{
		$data = $this->input->post();
		
		$data['template'] = 'supplier/add_new';	
				
		$data['error'] = validation_errors();
		if($dbact) $data['error'] .= $this->supplier_model->error_message;
		
		if($id==''){
			$data['first_title'] = 'Add';		
            $data['second_title'] = 'Supplier';	
			$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Supplier','client/supplier'),
								   array('Add New','')
						     );
		}else{
			$data['first_title'] = 'Edit';		
            $data['second_title'] = 'Supplier';	
			$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Supplier','client/supplier'),
								   array('Edit','')
						     );
		}
							 
		$data = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
		
	}
	
	function delete(){
		$this->supplier_model->delete($this->uri->segment(4));
			
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','Data Berhasil Dihapus.');
		redirect('client/supplier/','location');
	}
		
	function carinegara()
	{
		$arr['neg_list'] = $this->supplier_model->neg_findajax($this->input->get('term'));
		$this->output->set_output(json_encode($arr));
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
?>