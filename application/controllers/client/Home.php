<?php

class Home extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		permission_basic_client($this->session);
	}
	
	function index()
	{
		//print_r('Masuk SINI');exit();
		$data = array();
		$data['first_title'] = 'Dashboard';		
		$data['second_title'] = 'V-Legal Indonesia';		
		$data['template'] = 'home';		
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'client/home'),array("Dashboard",''));
	
		$data = array_merge($data,admin_info());
		//	print_r($data );exit();
		$this->parser->parse('client/index',$data);
		
	}	
	
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */