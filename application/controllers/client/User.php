<?php

class User extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
	}
		
	/* Login  and  Logout */
	
	function login()
    {
		permission_client_logged_in($this->session);
		
		$data =  array(
						'username' => '',
						'remember' => FALSE,
						'error' => '',
				);
		$data = array_merge($data,admin_info());
        $this->parser->parse('client/user/login',$data);
	}
	
	function do_login()
    {
		permission_client_logged_in($this->session);
		
		if($this->user_model->client_login()){			
			redirect('client/home','location');
		}else{
			$data =  array(
						'username' => $this->input->post('username'),
						'error' => 'Username atau Password Salah!',
				);
				
			$data = array_merge($data,admin_info());
			$this->parser->parse('client/user/login',$data);
		}
	}
	
	function logout()
	{
		permission_basic_client($this->session);
		$this->user_model->client_logout();
		redirect('client/user/login','location');
	}
	
	/* END Login  and  Logout */
	
}

/* End of file user.php */
/* Location: ./application/controllers/user.php */