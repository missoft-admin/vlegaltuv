<?php

class Produk extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		permission_basic_client($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('produk_model');
        $this->client_id = $this->session->userdata('client_id');
	}
	
	function index()
	{
		$data = array();
		$data['template'] = 'produk/index';	
		$data['query'] = $this->produk_model->produk_list($this->uri->segment(4),5,$this->client_id);		
		$data['first_title'] = 'Master';		
		$data['second_title'] = 'Produk';
		$data['searchText'] = '';
		$data['total_list']=$this->produk_model->count($this->client_id);
		$this->pagination->initialize(paging_admin($data['total_list'],'client/produk/index',4,5));
		$data['pagination'] =  $this->pagination->create_links();
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Produk','client/produk'),
								   array('List','')
						     );
		$data['table_title'] = 'DATA PRODUK  " [TOTAL '.number_format($data['total_list']).' PRODUK] " ACTIVE';					 
		$data['no'] = $this->uri->segment(4);
		$data = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
	}
    
    function search($search = '',$limit = '')
	{
		if($this->input->post('searchText')==''){
			if($this->uri->segment(4) != ''){
				$data = array();
				$data['template'] = 'produk/index';	
				$data['first_title'] = 'Search';		
                $data['second_title'] = 'Produk';
				$data['query'] = $this->produk_model->produk_search(addslashes(rawurldecode($search)),$limit,5,$this->client_id);	
				$data['total_list']=$this->produk_model->count_search(addslashes(rawurldecode($search)),$this->client_id);
				$this->pagination->initialize(paging_admin($data['total_list'],
															"client/produk/search/$search/",5,5));		
				$data['pagination'] =  $this->pagination->create_links();
				
                $data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
                                       array('Master Produk','client/produk'),
                                       array('Search','')
                                 );
				$data['searchText'] = rawurldecode($search);
				$data['table_title'] = 'DATA PRODUK " [TOTAL '.number_format($data['total_list']).' PRODUK] " HASIL PENCARIAN';
                $data['no'] = $limit;
				$data = array_merge($data,admin_info());
				$this->parser->parse('client/index',$data);
			}else{
				redirect('client/produk');
			}
		}else{
			redirect('client/produk/search/'.rawurlencode($this->input->post('searchText')));
		}
	}
		
	function add()
	{
		$data = array(
				'idproduk'  => '',
				'produk'    => '',
				'kodehs'    => '',
				'status'    => '1',
				);
		$data['template'] = 'produk/add_new';		
		$data['first_title'] = 'Add';		
		$data['second_title'] = 'Produk';
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Produk','client/produk'),
								   array('Add New','')
						     );
		$data['error'] = '';
		//print_r($data['negara_list']);exit();
		$data = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
	}
	
	function update($id)
	{
		if($id != ''){
			$row = $this->produk_model->detail($id,$this->client_id);
			if(isset($row->idproduk)){
				$data = array(
						'idproduk'  => $row->idproduk,
						'produk'    => $row->produk,						
						'kodehs'    => $row->kodehs,						
						'status'    => $row->status,
						);	
				$data['template'] = 'produk/add_new';	
				$data['first_title'] = 'Edit';		
                $data['second_title'] = 'Produk';
				$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Produk','client/produk'),
								   array('Edit','')
						     );
				$data['error'] = '';	
				$data = array_merge($data,admin_info());				
				$this->parser->parse('client/index',$data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','Data Tidak Ditemukan.');
				redirect('client/produk','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','Data Tidak Ditemukan.');
			redirect('client/produk');
		}	
	}
	
	function save()
	{
		$this->form_validation->set_rules('produk', 'Nama Produk', 'trim|required|min_length[1]');
		$this->form_validation->set_rules('kodehs', 'Kode HS', 'trim|required|min_length[1]');
		
		if ($this->form_validation->run() == TRUE){
			if($this->input->post('idproduk') == '' ) {
				if($this->produk_model->insert($this->client_id)){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','Data Tersimpan.');
					redirect('client/produk','location');
				}else{
					$this->failed_save($this->input->post('idproduk'),true);
				}
			} else {
				if($this->produk_model->update()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','Data Tersimpan.');
					redirect('client/produk','location');
				}else{
					$this->failed_save($this->input->post('idproduk'),true);
				}
			}
		}else{
			$this->failed_save($this->input->post('idproduk'));
		}	
	}
	
	function failed_save($id,$dbact=false)
	{
		$data = $this->input->post();
		
		$data['template'] = 'produk/add_new';	
				
		$data['error'] = validation_errors();
		if($dbact) $data['error'] .= $this->produk_model->error_message;
		
		if($id==''){
			$data['first_title'] = 'Add';		
            $data['second_title'] = 'Produk';	
			$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Produk','client/produk'),
								   array('Add New','')
						     );
		}else{
			$data['first_title'] = 'Edit';		
            $data['second_title'] = 'Produk';	
			$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Produk','client/produk'),
								   array('Edit','')
						     );
		}
							 
		$data = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
		
	}
	
	function delete(){
		$this->produk_model->delete($this->uri->segment(4),$this->client_id);
			
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','Data Berhasil Dihapus.');
		redirect('client/produk/','location');
	}
    
    function check_hscode(){
        $hscode = preg_replace("/[^A-Za-z0-9]/", "",$this->input->post('kodehs'));
        if($this->produk_model->check_hscode($hscode)){
            $this->output->set_output("ikeh");
        }else{
            $this->output->set_output("no");
        }
    }
		
	function carinegara()
	{
		$arr['neg_list'] = $this->produk_model->neg_findajax($this->input->get('term'));
		$this->output->set_output(json_encode($arr));
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
?>