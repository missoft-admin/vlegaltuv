<?php

class Bahan_baku extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		permission_basic_client($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('bahan_baku_model');
        $this->client_id = $this->session->userdata('client_id');
	}
	
	function index()
	{
		$data = array();
		$data['template'] = 'bahan_baku/index';	
		$data['query'] = $this->bahan_baku_model->bahan_baku_list($this->uri->segment(4),5,$this->client_id);		
		$data['first_title'] = 'Stok';		
		$data['second_title'] = 'Bahan Baku';
		$data['searchText'] = '';
		$data['total_list']=$this->bahan_baku_model->count($this->client_id);
		$this->pagination->initialize(paging_admin($data['total_list'],'client/bahan_baku/index',4,5));
		$data['pagination'] =  $this->pagination->create_links();
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Stok Bahan Baku','client/bahan_baku'),
								   array('List','')
						     );
		$data['table_title'] = 'DATA STOK BAHAN BAKU  " [TOTAL '.number_format($data['total_list']).' DATA] " ACTIVE';					 
		$data['no'] = $this->uri->segment(4);
		$data = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
	}
    
    function search($search = '',$limit = '')
	{
		if($this->input->post('searchText')==''){
			if($this->uri->segment(4) != ''){
				$data = array();
				$data['template'] = 'bahan_baku/index';	
				$data['first_title'] = 'Search';		
                $data['second_title'] = 'Bahan Baku';
				$data['query'] = $this->bahan_baku_model->bahan_baku_search(rawurldecode($search),$limit,5,$this->client_id);	
				$data['total_list']=$this->bahan_baku_model->count_search(rawurldecode($search),$this->client_id);
				$this->pagination->initialize(paging_admin($data['total_list'],
															"client/bahan_baku/search/$search/",5,5));		
				$data['pagination'] =  $this->pagination->create_links();
				
                $data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
                                       array('Stok Bahan Baku','client/bahan_baku'),
                                       array('Search','')
                                 );
				$data['searchText'] = rawurldecode($search);
				$data['table_title'] = 'DATA STOK BAHAN BAKU " [TOTAL '.$data['total_list'].' DATA] " HASIL PENCARIAN';
                $data['no'] = $limit;
				$data = array_merge($data,admin_info());
				$this->parser->parse('client/index',$data);
			}else{
				redirect('client/bahan_baku');
			}
		}else{
			redirect('client/bahan_baku/search/'.rawurlencode($this->input->post('searchText')));
		}
	}
	
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
?>