<?php

class Produksi extends CI_Controller {
    var $client_id;
	function __construct()
	{
		parent::__construct();
		permission_basic_client($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('produksi_model');
        $this->client_id = $this->session->userdata('client_id');
	}
	
	function index()
	{
		$data = array();
		$data['template']       = 'produksi/index';	
		$data['query']          = $this->produksi_model->list_index($this->uri->segment(4),5,'',$this->client_id);		
		$data['first_title']    = 'Realisasi';		
		$data['second_title']   = 'Produksi';
		$data['searchText']     = '';
		$data['total_list']     = $this->produksi_model->count('',$this->client_id);
		$this->pagination->initialize(paging_admin($data['total_list'],'client/produksi/index',4,5),'',$this->client_id);
		$data['pagination']     = $this->pagination->create_links();
		$data['breadcrum']      = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
                                        array('Realisasi Produksi','produksi/index'),
                                        array('List','')
                                );
		$data['table_title']    = 'DATA REALISASI PRODUKSI " [TOTAL '.number_format($data['total_list']).' DATA] " ACTIVE';					 
		$data['no']             = $this->uri->segment(4);
		$data                   = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
	}
    
    function search($search = '',$limit = '')
	{
		if($this->input->post('searchText')==''){
			if($this->uri->segment(4) != ''){
				$data = array();
				$data['template']       = 'produksi/index';	
				$data['first_title']    = 'Search';		
                $data['second_title']   = 'Realisasi Produksi';
				$data['query']          = $this->produksi_model->list_index($limit,5,addslashes(rawurldecode($search)),$this->client_id);	
				$data['total_list']     = $this->produksi_model->count(addslashes(rawurldecode($search)),$this->client_id);
				$this->pagination->initialize(paging_admin($data['total_list'],'client/produksi/index',5,5),'',$this->client_id);		
				$data['pagination']     = $this->pagination->create_links();
				
               $data['breadcrum']       = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
                                                array('Realisasi Produksi','produksi/index'),
                                                array('Search','')
                                        );
				$data['searchText']     = rawurldecode($search);
				$data['table_title']    = 'DATA REALISASI PRODUKSI " [TOTAL '.$data['total_list'].' DATA] " HASIL PENCARIAN';
                $data['no']             = $limit;
				$data                   = array_merge($data,admin_info());
				$this->parser->parse('client/index',$data);
			}else{
				redirect('client/produksi');
			}
		}else{
			redirect('client/produksi/search/'.rawurlencode($this->input->post('searchText')));
		}
	}
		
	function add()
	{
		$data = array(
				'idproduksi'            => '',
				'production_date'       => '',
				'jumlah_produksi'       => '',
				'volume_produksi'       => '',
				'berat_produksi'        => '',
				'rendemen_volume'       => '',
				'rendemen_berat'        => '',
				'idproduk'              => '',
				'produk'                => '',
				'kodehs'                => '',
                'final'                 => 0,
                'nama_produk'           => 0,
                'kapasitas_id'          => '',
                'kapasitas_produksi'    => 0,
                'saldo_kapasitas'       => 0,
				);
		
        
        $data['detail_list']            = array();		
        $data['detail_value_html']      = '';	
        $data['detail_list_sup']        = array();		
		$data['template']               = 'produksi/add_new';		
		$data['first_title']            = 'Add';		
		$data['second_title']           = 'Realisasi Produksi';
        $data['breadcrum']              = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
                                                array('Realisasi Produksi','produksi/index'),
                                                array('Add New','')
                                          );
		$data['error'] = '';
		$data = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
	}
    
    function edit($idproduksi)
    {
        $data = $this->produksi_model->detail($idproduksi,$this->client_id);
        $data['detail_list']            = $this->produksi_model->detail_list($idproduksi);		
        $data['detail_value_html']      = '';	
        $data['detail_list_sup']        = $this->produksi_model->detail_list_supplier($idproduksi);		
		$data['template']               = 'produksi/add_new';		
		$data['first_title']            = 'Edit';		
		$data['second_title']           = 'Realisasi Produksi';
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Realisasi Produksi','produksi/index'),
								   array('Edit','')
						     );
		$data['error'] = '';
		$data = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
    }
	
	function save()
	{
        $this->form_validation->set_rules('production_date', 'Tanggal Produksi', 'trim|required');
		$this->form_validation->set_rules('idproduk', 'Produk', 'trim|required');
		$this->form_validation->set_rules('jumlah_produksi', 'Jumlah Produksi', 'trim|required');
		//$this->form_validation->set_rules('volume_produksi', 'Volume Produksi', 'trim|required');
		//$this->form_validation->set_rules('berat_produksi', 'Berat Produksi', 'trim|required');
		$this->form_validation->set_rules('detail_value', 'Data Detail Produksi', 'trim|required|callback_json_check');
        //$this->form_validation->set_rules('detail_value_sup', 'Data Asal Kayu', 'trim|required|callback_json_check');
        $this->form_validation->set_message('json_check', '%s belum disii');
		//print_r($_POST);exit();
		if ($this->form_validation->run() == TRUE){
			if($this->input->post('idproduksi') == '' ) {
				if($this->produksi_model->save()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','Data Tersimpan.');
					redirect('client/produksi','location');
				}else{
					$this->failed_save($this->input->post('idproduksi'),true);
				}
			} else {
				if($this->produksi_model->update()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','Data Tersimpan.');
					redirect('client/produksi','location');
				}else{
					$this->failed_save($this->input->post('idproduksi'),true);
				}
			}
		}else{
			$this->failed_save($this->input->post('idproduksi'));
		}	
	}
    
    function json_check($str){
        $arr = json_decode($str);
        if(count($arr) > 0){
            return true;
        }else{
            return false;
        }
    }
	
	function failed_save($id,$dbact=false)
	{
        $data = $this->input->post();
        $data['detail_list']        = array();	
        $data['detail_list_sup']    = json_decode($data['detail_list_sup']);	
        
		$data['template']           = 'produksi/add_new';	
				
		$data['error']              = validation_errors();
		if($dbact) $data['error'] .= $this->produksi_model->error_message;
		
		if($id==''){
			$data['first_title'] = 'Add';		
            $data['second_title'] = 'Realisasi Produksi';
            $data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
                                       array('Realisasi Produksi','produksi/index'),
                                       array('Add','')
                                 );
		}else{
			$data['first_title'] = 'Edit';		
            $data['second_title'] = 'Realisasi Produksi';
            $data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
                                       array('Realisasi Produksi','produksi/index'),
                                       array('Edit','')
                                 );
		}
							 
		$data = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
		
	}
	
	function delete(){
		$this->produksi_model->delete($this->uri->segment(4),$this->client_id);
			
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','Data Berhasil Dihapus.');
		redirect('client/produksi/','location');
	}
    
    function finalize(){
		$this->produksi_model->finalize($this->uri->segment(4),$this->client_id);
			
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','Data Berhasil Disimpan.');
		redirect('client/produksi/','location');
	}
    
		
	function find_supplier()
	{
		$results = $this->master_model->get_list_supplier($this->input->get('term'),$this->client_id,true);
		$this->output->set_output(json_encode($results));
	}
    function find_produk()
	{
		$results = $this->master_model->get_list_produk($this->input->get('term'),$this->client_id,true);
		$this->output->set_output(json_encode($results));
	}
    function find_produk_hscode($idproduk)
	{
		$results = $this->master_model->get_produk_hscode($idproduk);
		$this->output->set_output(json_encode($results));
	}
    
    function find_sortimen()
	{
		$results = $this->master_model->get_list_sortimen($this->input->get('term'),$this->client_id,true);
		$this->output->set_output(json_encode($results));
	}
    
    function find_provinsi()
	{
		$results = $this->master_model->get_list_provinsi($this->input->get('term'),true);
		$this->output->set_output(json_encode($results));
	}
    
    function find_negara()
	{
		$results = $this->master_model->get_list_negara($this->input->get('term'),true);
		$this->output->set_output(json_encode($results));
	}
    
    function find_jenis_kayu()
	{
		$results = $this->master_model->get_list_jenis_kayu($this->input->get('term'),true);
		$this->output->set_output(json_encode($results));
	}

    function find_jenis_kayu_bahan_baku($idsortimen)
	{
		$results = $this->master_model->get_list_jenis_kayu_bahan_baku($idsortimen,$this->input->get('term'),true);
		$this->output->set_output(json_encode($results));
	}
    
    function find_stok_bahan_baku($idsortimen,$idkayu)
	{
		$results = $this->master_model->get_stok_bahan_baku($idsortimen,$idkayu,true);
		$this->output->set_output(json_encode($results));
	}
    
    function find_kota($provinsi_id)
	{
		$arr['kota'] = $this->master_model->get_list_kota('',$provinsi_id,true);
		$this->output->set_output(json_encode($arr));
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
?>