<?php

class Penerimaan extends CI_Controller {
    var $client_id;
	function __construct()
	{
		parent::__construct();
		permission_basic_client($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('penerimaan_model');
        $this->client_id = $this->session->userdata('client_id');
	}
	
	function index()
	{
		$data = array();
		$data['template']       = 'penerimaan/index';	
		$data['query']          = $this->penerimaan_model->list_index($this->uri->segment(4),5,'','',$this->client_id);		
		$data['first_title']    = 'Penerimaan';		
		$data['second_title']   = 'Bahan Baku';
		$data['searchText']     = '';
		$data['total_list']     = $this->penerimaan_model->count('','',$this->client_id);
		$this->pagination->initialize(paging_admin($data['total_list'],'client/penerimaan/index',4,5),'',$this->client_id);
		$data['pagination']     = $this->pagination->create_links();
		$data['breadcrum']      = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
                                        array('Penerimaan Bahan Baku','penerimaan/index'),
                                        array('List','')
                                );
		$data['table_title']    = 'DATA PENERIMAAN BAHAN BAKU  " [TOTAL '.number_format($data['total_list']).' DATA] " ACTIVE';					 
		$data['no']             = $this->uri->segment(4);
		$data                   = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
	}
    
    function search($search = '',$limit = '')
	{
		if($this->input->post('searchText')==''){
			if($this->uri->segment(4) != ''){
				$data = array();
				$data['template']       = 'penerimaan/index';	
				$data['first_title']    = 'Search';		
                $data['second_title']   = 'Penerimaan Bahan Baku';
				$data['query']          = $this->penerimaan_model->list_index($limit,5,addslashes(rawurldecode($search)),'',$this->client_id);	
				$data['total_list']     = $this->penerimaan_model->count(addslashes(rawurldecode($search)),'',$this->client_id);
				$this->pagination->initialize(paging_admin($data['total_list'],'client/penerimaan/index',5,5),'',$this->client_id);		
				$data['pagination']     = $this->pagination->create_links();
				
               $data['breadcrum']       = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
                                                array('Penerimaan Bahan Baku','penerimaan/index'),
                                                array('Search','')
                                        );
				$data['searchText']     = rawurldecode($search);
				$data['table_title']    = 'DATA PENERIMAAN BAHAN BAKU " [TOTAL '.$data['total_list'].' DATA] " HASIL PENCARIAN';
                $data['no']             = $limit;
				$data                   = array_merge($data,admin_info());
				$this->parser->parse('client/index',$data);
			}else{
				redirect('client/penerimaan');
			}
		}else{
			redirect('client/penerimaan/search/'.rawurlencode($this->input->post('searchText')));
		}
	}
		
	function add()
	{
		$data = array(
				'idpenerimaan'              => '',
				'periode_awal'              => '',
				'periode_akhir'             => '',
				'idsupplier'                => '',
                'nama_supplier'             => 'Pilih Supplier',
				'idhutan'                   => '',
				'idsertifikasi'             => '',
				'no_sertifikat'             => '',
				'tgl_berlaku_sertifikat'    => '',
				'iddokumen'                 => '',
				'tgl_berlaku_dokumen'       => '',
				'idnegara'                  => '',
				'nama_negara'               => 'Pilih Asal Kayu',
				'type_penerimaan'           => 'LOKAL',
				'nama_kota'                 => 'Pilih Kota',
                'idprovinsi'                => '',
				'nama_provinsi'             => 'Pilih Provinsi',
                'noinvoice'                 => '',
                'id_deklarasi_import'       => '',
                'no_deklarasi_import'       => '',
                'tgl_berlaku_deklarasi'     => '',
                'kuota_import'              => '',
                'final'                     => 0,
				);
		$data['hutan_list']             = $this->master_model->get_list_hutan();		
		$data['jenisdokumen_list']      = $this->master_model->get_list_jenis_dokumen();		
        $data['sertifikasi_list']       = $this->master_model->get_list_sertifikasi();		
        $data['satuan_jumlah']          = $this->master_model->get_list_satuan('jumlah');		
        $data['satuan_volume']          = $this->master_model->get_list_satuan('volume');		
        $data['satuan_berat']           = $this->master_model->get_list_satuan('berat');		
        $data['detail_list']            = array();		
        $data['detail_value_html']      = '';		
		$data['template']               = 'penerimaan/add_new';		
		$data['first_title']            = 'Add';		
		$data['second_title']           = 'Penerimaan';
        $data['breadcrum']              = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
                                                array('Penerimaan Bahan Baku','penerimaan/index'),
                                                array('Add New','')
                                          );
		$data['error'] = '';
		$data = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
	}
    
    function edit($nopenerimaan)
    {
        $data = $this->penerimaan_model->detail($nopenerimaan,'',$this->client_id);
        $data['hutan_list']             = $this->master_model->get_list_hutan();		
		$data['jenisdokumen_list']      = $this->master_model->get_list_jenis_dokumen();		
        $data['sertifikasi_list']       = $this->master_model->get_list_sertifikasi();		
        $data['satuan_jumlah']          = $this->master_model->get_list_satuan('jumlah');		
        $data['satuan_volume']          = $this->master_model->get_list_satuan('volume');		
        $data['satuan_berat']           = $this->master_model->get_list_satuan('berat');		
        $data['detail_list']            = $this->penerimaan_model->detail_list($nopenerimaan);		
        $data['detail_value_html']      = '';		
		$data['template']               = 'penerimaan/add_new';		
		$data['first_title']            = 'Edit';		
		$data['second_title']           = 'Penerimaan';
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Penerimaan Bahan Baku','penerimaan/index'),
								   array('Edit','')
						     );
		$data['error'] = '';
		$data = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
    }
	
	function save()
	{
        $this->form_validation->set_rules('periode_awal', 'Tanggal', 'trim|required');
		$this->form_validation->set_rules('idsupplier', 'Supplier', 'trim|required');
		$this->form_validation->set_rules('noinvoice', 'No. Invoice', 'trim|required');
		$this->form_validation->set_rules('detail_value', 'Data Detail Penerimaan', 'trim|required|callback_json_check');
        $this->form_validation->set_message('json_check', '%s belum disii');
		
		if ($this->form_validation->run() == TRUE){
			if($this->input->post('idpenerimaan') == '' ) {
				if($this->penerimaan_model->save()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','Data Tersimpan.');
					redirect('client/penerimaan','location');
				}else{
					$this->failed_save($this->input->post('idpenerimaan'),true);
				}
			} else {
				if($this->penerimaan_model->update()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','Data Tersimpan.');
					redirect('client/penerimaan','location');
				}else{
					$this->failed_save($this->input->post('idpenerimaan'),true);
				}
			}
		}else{
			$this->failed_save($this->input->post('idpenerimaan'));
		}	
	}
    
    function json_check($str){
        $arr = json_decode($str);
        if(count($arr) > 0){
            return true;
        }else{
            return false;
        }
    }
	
	function failed_save($id,$dbact=false)
	{
		$data = $this->input->post();
		$data['hutan_list']         = $this->master_model->get_list_hutan();		
		$data['jenisdokumen_list']  = $this->master_model->get_list_jenis_dokumen();		
        $data['sertifikasi_list']   = $this->master_model->get_list_sertifikasi();		
        $data['satuan_jumlah']      = $this->master_model->get_list_satuan('jumlah');		
        $data['satuan_volume']      = $this->master_model->get_list_satuan('volume');		
        $data['satuan_berat']       = $this->master_model->get_list_satuan('berat');		
        $data['detail_list']        = array();	
		$data['template']           = 'penerimaan/add_new';	
				
		$data['error']              = validation_errors();
		if($dbact) $data['error'] .= $this->penerimaan_model->error_message;
		
		if($id==''){
			$data['first_title'] = 'Add';		
            $data['second_title'] = 'Penerimaan';
            $data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
                                       array('Penerimaan Bahan Baku','penerimaan/index'),
                                       array('Add','')
                                 );
		}else{
			$data['first_title'] = 'Edit';		
            $data['second_title'] = 'Penerimaan';
            $data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
                                       array('Penerimaan Bahan Baku','penerimaan/index'),
                                       array('Edit','')
                                 );
		}
							 
		$data = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
		
	}
	
	function delete(){
		$this->penerimaan_model->delete($this->uri->segment(4),$this->client_id);
			
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','Data Berhasil Dihapus.');
		redirect('client/penerimaan/','location');
	}
    
    function finalize(){
		$this->penerimaan_model->finalize($this->uri->segment(4),$this->client_id);
			
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','Data Berhasil Disimpan.');
		redirect('client/penerimaan/','location');
	}
    
		
	function find_supplier()
	{
		$results = $this->master_model->get_list_supplier($this->input->get('term'),$this->client_id,true);
		$this->output->set_output(json_encode($results));
	}
    
    function find_sortimen()
	{
		$results = $this->master_model->get_list_sortimen($this->input->get('term'),$this->client_id,true);
		$this->output->set_output(json_encode($results));
	}
    
    function find_provinsi()
	{
		$results = $this->master_model->get_list_provinsi($this->input->get('term'),true);
		$this->output->set_output(json_encode($results));
	}
    
    function find_negara()
	{
		$results = $this->master_model->get_list_negara($this->input->get('term'),true);
		$this->output->set_output(json_encode($results));
	}
    
    function find_jenis_kayu()
	{
		$results = $this->master_model->get_list_jenis_kayu($this->input->get('term'),true);
		$this->output->set_output(json_encode($results));
	}
    
    function find_deklarasi_import()
	{
		$results = $this->master_model->get_deklarasi_import($this->input->get('term'),$this->client_id,true);
		$this->output->set_output(json_encode($results));
	}

    function find_deklarasi_import_detail($deklarasi_id = '')
	{
		$results = $this->master_model->get_deklarasi_import_detail($deklarasi_id,true);
		$this->output->set_output(json_encode($results));
	}
    
    function find_kota($provinsi_id)
	{
		$arr['kota'] = $this->master_model->get_list_kota('',$provinsi_id,true);
		$this->output->set_output(json_encode($arr));
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
?>