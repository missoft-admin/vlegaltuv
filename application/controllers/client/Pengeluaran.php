<?php

class Pengeluaran extends CI_Controller {
    var $client_id;
    
	function __construct()
	{
		parent::__construct();
		permission_basic_client($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('pengeluaran_model');
        $this->client_id = $this->session->userdata('client_id');
	}
	
	function index()
	{
		$data = array();
		$data['template'] = 'pengeluaran/index';	
		$data['query'] = $this->pengeluaran_model->list_index($this->uri->segment(4),5,'',$this->client_id);		
		$data['first_title'] = 'Pengeluaran';		
		$data['second_title'] = 'Bahan Baku';
		$data['searchText'] = '';
		$data['total_list']=    $this->pengeluaran_model->count('',$this->client_id);
		$this->pagination->initialize(paging_admin($data['total_list'],'client/pengeluaran/index',4,5));
		$data['pagination'] =  $this->pagination->create_links();
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'client/home'),
								   array('Pengeluaran Bahan Baku','client/pengeluaran/index'),
								   array('List','')
						     );
		$data['table_title'] = 'DATA PENGELUARAN BAHAN BAKU  " [TOTAL '.number_format($data['total_list']).' DATA] " ACTIVE';					 
		$data['no'] = $this->uri->segment(4);
		$data = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
	}
    
    function search($search = '',$limit = '')
	{
		if($this->input->post('searchText')==''){
			if($this->uri->segment(4) != ''){
				$data = array();
				$data['template'] = 'pengeluaran/index';	
                $data['first_title'] = 'Search';		
                $data['second_title'] = 'Pengeluaran Bahan Baku';
				$data['query'] = $this->pengeluaran_model->list_index($limit,5,'',$this->client_id,rawurldecode($search));	
				$data['total_list']=$this->pengeluaran_model->count('',$this->client_id,rawurldecode($search));
				$this->pagination->initialize(paging_admin($data['total_list'],"client/pengeluaran/search/$search",5,5));	
				$data['pagination'] =  $this->pagination->create_links();
				
                $data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'client/home'),
								   array('Pengeluaran Bahan Baku','client/pengeluaran/index'),
								   array('Search','')
						     );
				$data['searchText'] = rawurldecode($search);
				$data['table_title'] = 'DATA PENGELUARAN BAHAN BAKU " [TOTAL '.$data['total_list'].' DATA] " HASIL PENCARIAN';
                $data['no'] = $limit;
				$data = array_merge($data,admin_info());
				$this->parser->parse('client/index',$data);
			}else{
				redirect('client/pengeluaran');
			}
		}else{
			redirect('client/pengeluaran/search/'.rawurlencode($this->input->post('searchText')));
		}
	}
		
	function add()
	{
		$data = array(
				'idpengeluaran'     => '',
				'periode_awal'      => '',
				'periode_akhir'     => '',
				'jenis_pengeluaran' => '',
				'type_pengeluaran'  => 'LOKAL',
				'final'             => 0,
				);
		$data['jenis_pengeluaran_list'] = $this->master_model->get_list_pengeluaran();		
        $data['satuan_jumlah']          = $this->master_model->get_list_satuan('jumlah');		
        $data['satuan_volume']          = $this->master_model->get_list_satuan('volume');		
        $data['satuan_berat']           = $this->master_model->get_list_satuan('berat');		
        $data['detail_list']            = array();		
        $data['detail_value_html']      = '';		
		$data['template']               = 'pengeluaran/add_new';		
		$data['first_title']            = 'Add';		
		$data['second_title']           = 'Pengeluaran';
        $data['breadcrum']              = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
                                                array('Pengeluaran Bahan Baku','pengeluaran/index'),
                                                array('Add New','')
                                          );
		$data['error'] = '';
		//print_r($data['negara_list']);exit();
		$data = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
	}
    
    function edit($idpengeluaran)
	{
		$data = $this->pengeluaran_model->detail($idpengeluaran,'',$this->client_id);
		$data['jenis_pengeluaran_list'] = $this->master_model->get_list_pengeluaran();		
        $data['satuan_jumlah'] = $this->master_model->get_list_satuan('jumlah');		
        $data['satuan_volume'] = $this->master_model->get_list_satuan('volume');		
        $data['satuan_berat'] = $this->master_model->get_list_satuan('berat');		
        $data['detail_list'] = $this->pengeluaran_model->detail_list($idpengeluaran);		
        $data['detail_value_html'] = '';		
		$data['template'] = 'pengeluaran/add_new';		
		$data['first_title'] = 'Add';		
		$data['second_title'] = 'Pengeluaran';
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Pengeluaran Bahan Baku','pengeluaran/index'),
								   array('Add New','')
						     );
		$data['error'] = '';
		$data = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
	}
	
	function save()
	{
        $this->form_validation->set_rules('periode_awal', 'Tanggal', 'trim|required');
		$this->form_validation->set_rules('jenis_pengeluaran', 'Jenis Pengeluaran', 'trim|required');
		$this->form_validation->set_rules('detail_value', 'Data Detail Pengeluaran', 'trim|required|callback_json_check');
        $this->form_validation->set_message('json_check', '%s belum disii');
		if ($this->form_validation->run() == TRUE){
			if($this->input->post('idpengeluaran') == '' ) {
				if($this->pengeluaran_model->save($this->client_id)){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','Data Tersimpan.');
					redirect('client/pengeluaran','location');
				}else{
					$this->failed_save($this->input->post('idpengeluaran'),true);
				}
			} else {
				if($this->pengeluaran_model->update($this->client_id)){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','Data Tersimpan.');
					redirect('client/pengeluaran','location');
				}else{
					$this->failed_save($this->input->post('idpengeluaran'),true);
				}
			}
		}else{
			$this->failed_save($this->input->post('idpengeluaran'));
		}	
	}
    
    function json_check($str){
        $arr = json_decode($str);
        if(count($arr) > 0){
            return true;
        }else{
            return false;
        }
    }
	
	function failed_save($id,$dbact=false)
	{
		$data = $this->input->post();
		$data['jenis_pengeluaran_list'] = $this->master_model->get_list_pengeluaran();		
        $data['satuan_jumlah']          = $this->master_model->get_list_satuan('jumlah');		
        $data['satuan_volume']          = $this->master_model->get_list_satuan('volume');		
        $data['satuan_berat']           = $this->master_model->get_list_satuan('berat');		
        $data['detail_list']            = array();	
		$data['template']               = 'pengeluaran/add_new';	
				
		$data['error'] = validation_errors();
		if($dbact) $data['error'] .= $this->pengeluaran_model->error_message;
		
		if($id==''){
			$data['first_title'] = 'Add';		
            $data['second_title'] = 'Pengeluaran';
            $data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
                                       array('Pengeluaran Bahan Baku','pengeluaran/index'),
                                       array('Add','')
                                 );
		}else{
			$data['first_title'] = 'Edit';		
            $data['second_title'] = 'Pengeluaran';
            $data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
                                       array('Pengeluaran Bahan Baku','pengeluaran/index'),
                                       array('Edit','')
                                 );
		}
							 
		$data = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
		
	}
	
	function delete(){
		$this->pengeluaran_model->delete($this->uri->segment(4),$this->client_id);
			
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','Data Berhasil Dihapus.');
		redirect('client/pengeluaran/','location');
	}
    
    function finalize(){
		$this->pengeluaran_model->finalize($this->uri->segment(4),$this->client_id);
			
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','Data Berhasil Disimpan.');
		redirect('client/pengeluaran/','location');
	}
    
		
	function find_supplier()
	{
		$arr['supplier'] = $this->master_model->get_list_supplier($this->input->get('term'),$this->client_id,true);
		$this->output->set_output(json_encode($arr));
	}
    
    function find_sortimen()
	{
		$results  = $this->master_model->get_list_sortimen($this->input->get('term'),$this->client_id,true);
		$this->output->set_output(json_encode($results));
	}
    
    function find_provinsi()
	{
		$arr['provinsi'] = $this->master_model->get_list_provinsi($this->input->get('term'),true);
		$this->output->set_output(json_encode($arr));
	}
    
    function find_negara()
	{
		$arr['negara'] = $this->master_model->get_list_negara($this->input->get('term'),true);
		$this->output->set_output(json_encode($arr));
	}
    
    function find_jenis_kayu()
	{
		$results = $this->master_model->get_list_jenis_kayu($this->input->get('term'),true);
		$this->output->set_output(json_encode($results));
	}
    
    function find_kota($provinsi_id)
	{
		$arr['kota'] = $this->master_model->get_list_kota('',$provinsi_id,true);
		$this->output->set_output(json_encode($arr));
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
?>