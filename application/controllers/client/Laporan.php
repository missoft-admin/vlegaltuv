<?php

class Laporan extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		permission_basic_client($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('laporan_model');
        $this->client_id = $this->session->userdata('client_id');
	}
	
	function lmko()
	{
		$data = array();
		$data['template'] = 'laporan/index_lmko';	
		$data['query'] = $this->laporan_model->lmko_list($this->uri->segment(4),5,$this->client_id);		
		$data['query_fg'] = $this->laporan_model->lmko_list_fg($this->uri->segment(4),5,$this->client_id);		
		$data['first_title'] = 'Laporan Mutasi';		
		$data['second_title'] = 'Kayu Olahan';
		$data['searchText'] = '';
		$data['total_list']=$this->laporan_model->count_lmko($this->client_id);
		$this->pagination->initialize(paging_admin($data['total_list'],'client/laporan/lmko',4,5));
		$data['pagination'] =  $this->pagination->create_links();
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Laporan Mutasi Kayu Olahan','client/laporan'),
								   array('List','')
						     );
		$data['table_title'] = 'DATA LMKO  " [TOTAL '.number_format($data['total_list']).' DATA] " ACTIVE';					 
		$data['no'] = $this->uri->segment(4);
		$data = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
	}
    
    function search_lmko($search = '',$limit = '')
	{
		if($this->input->post('searchText')==''){
			if($this->uri->segment(4) != ''){
				$data = array();
				$data['template'] = 'laporan/index_lmko';	
				$data['first_title'] = 'Laporan Mutasi';		
                $data['second_title'] = 'Kayu Olahan';
				$data['query'] = $this->laporan_model->lmko_search(rawurldecode($search),$limit,5,$this->client_id);	
				$data['total_list']=$this->laporan_model->count_lmko_search(rawurldecode($search),$this->client_id);
				$this->pagination->initialize(paging_admin($data['total_list'],
															"client/laporan/search_lmko/$search/",5,5));		
				$data['pagination'] =  $this->pagination->create_links();
				
                $data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
                                       array('Laporan Mutasi Kayu Olahan','client/laporan'),
                                       array('Search','')
                                 );
				$data['searchText'] = rawurldecode($search);
				$data['table_title'] = 'DATA LMKO " [TOTAL '.$data['total_list'].' DATA] " HASIL PENCARIAN';
                $data['no'] = $limit;
				$data = array_merge($data,admin_info());
				$this->parser->parse('client/index',$data);
			}else{
				redirect('client/laporan');
			}
		}else{
			redirect('client/laporan/search_lmko/'.rawurlencode($this->input->post('searchText')));
		}
	}
	
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
?>