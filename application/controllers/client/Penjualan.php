<?php

class Penjualan extends CI_Controller {
    var $client_id;
	function __construct()
	{
		parent::__construct();
		permission_basic_client($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('penjualan_model');
        $this->client_id = $this->session->userdata('client_id');
	}
	
	function index()
	{
        $data = array();
		$data['template'] = 'penjualan/index';	
		$data['query'] = $this->penjualan_model->list_index($this->uri->segment(4),20,$this->client_id,'');		
		$data['first_title'] = 'Penjualan';		
		$data['second_title'] = 'Lokal';
		$data['searchText'] = '';
		$data['total_list']= $this->penjualan_model->count($this->client_id,'');
		$this->pagination->initialize(paging_admin($data['total_list'],'client/penjualan/index',4,20));
		$data['pagination'] =  $this->pagination->create_links();
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'client/home'),
								   array('Penjualan Lokal','client/penjualan/index'),
								   array('List','')
						     );
		$data['table_title'] = 'DATA PENJUALAN LOKAL  " [TOTAL '.number_format($data['total_list']).' DATA] "';					 
		$data['no'] = $this->uri->segment(4);
		$data = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
	}
    
    function search($search = '',$limit = '')
	{
		if($this->input->post('searchText')==''){
			if($this->uri->segment(4) != ''){
				$data = array();
				$data['template'] = 'penjualan/index';	
				$data['first_title'] = 'Search';		
                $data['second_title'] = 'Penjualan Lokal';
				$data['query'] =  $this->penjualan_model->list_index($limit,20,$this->client_id,rawurldecode($search));
				$data['total_list']=$this->penjualan_model->count($this->client_id,rawurldecode($search));
				$this->pagination->initialize(paging_admin($data['total_list'],
															"client/penjualan/search/$search/",5,20));		
				$data['pagination'] =  $this->pagination->create_links();
				
               $data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'client/home'),
								   array('Penjualan Lokal','client/penjualan/index'),
								   array('Search','')
						     );
				$data['searchText'] = rawurldecode($search);
				$data['table_title'] = 'DATA PENJUALAN LOKAL " [TOTAL '.$data['total_list'].' DATA] " HASIL PENCARIAN';
                $data['no'] = $limit;
				$data = array_merge($data,admin_info());
				$this->parser->parse('client/index',$data);
			}else{
				redirect('client/buyer');
			}
		}else{
			redirect('client/penjualan/search/'.rawurlencode($this->input->post('searchText')));
		}
	}
    
	function add()
	{
		$client = $this->master_model->get_client_vlegal($this->client_id);	
        $data = array(
                'idjual'        => '',
                'idbuyer'       => '',
                'namabuyer'     => '',
                'alamat'        => '',
                'periode_awal'  => '',
                'idprovinsi'    => '',
                'nama_provinsi' => '',
                'idkota'        => '',
                'nama_kota'     => '',
                'nilai'         => '',
                'invoice'       => '',
                'nonota'        => '',
                'no_po'         => '',
                'tgl_po'        => '',
                'tglterbit'     => '',
                'final'         => 0,
                );
			
		$data['template'] = 'penjualan/add_new';		
		$data['first_title'] = 'Add Penjualan';		
		$data['second_title'] = 'Lokal';
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Penjualan Lokal','penjualan/index'),
								   array('Add New','')
						     );
		$data['detail_list'] = array();
		$data['detail_value_html'] = '';
        $data['satuan_jumlah'] = $this->master_model->get_list_satuan('jumlah');		
        $data['satuan_volume'] = $this->master_model->get_list_satuan('volume');		
        $data['satuan_berat'] = $this->master_model->get_list_satuan('berat');		
		$data['error'] = '';
		$data = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
	}
    
    function edit($idjual)
    {
        $data = $this->penjualan_model->detail($idjual);
		$data['template'] = 'penjualan/add_new';		
		$data['first_title'] = 'Edit Penjualan';		
		$data['second_title'] = 'Lokal';
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Penjualan Lokal','penjualan/index'),
								   array('Edit','')
						     );
		$data['detail_value_html'] = '';
        $data['detail_list'] = $this->penjualan_model->detail_list($idjual);
        $data['satuan_jumlah'] = $this->master_model->get_list_satuan('jumlah');		
        $data['satuan_volume'] = $this->master_model->get_list_satuan('volume');		
        $data['satuan_berat'] = $this->master_model->get_list_satuan('berat');		
        $data['error'] = '';
		$data = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
    }
	
	
	function save()
	{
        $this->form_validation->set_rules('idbuyer', 'Pembeli', 'trim|required');
        $this->form_validation->set_rules('invoice', 'No. Invoice', 'trim|required');
        $this->form_validation->set_rules('nonota', 'No. Nota', 'trim|required');
        $this->form_validation->set_rules('periode_awal', 'Tanggal Invoice', 'trim|required');
        $this->form_validation->set_rules('tglterbit', 'Tanggal Terbit', 'trim|required');
        $this->form_validation->set_rules('detail_value', 'Data Detail Penjualan', 'trim|required|callback_json_check');
        $this->form_validation->set_message('json_check', '%s belum disii');
		
		if ($this->form_validation->run() == TRUE){
			if($this->input->post('idjual') == '' ) {
				if($this->penjualan_model->save($this->client_id)){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','Data Tersimpan.');
					redirect('client/penjualan','location');
				}else{
					$this->failed_save($this->input->post('idjual'),true);
				}
			} else {
				if($this->penjualan_model->update()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','Data Tersimpan.');
					redirect('client/penjualan','location');
				}else{
					$this->failed_save($this->input->post('idjual'),true);
				}
			}
		}else{
			$this->failed_save($this->input->post('idekspor'));
		}	
	}
    
    function json_check($str){
        $arr = json_decode($str);
        if(count($arr) > 0){
            return true;
        }else{
            return false;
        }
    }
	
	function failed_save($id,$dbact=false)
	{
		$data = $this->input->post();
		$data['template'] = 'penjualan/add_new';	
				
		$data['error'] = validation_errors();
        $data['detail_list'] = array();
        $data['satuan_jumlah'] = $this->master_model->get_list_satuan('jumlah');		
        $data['satuan_volume'] = $this->master_model->get_list_satuan('volume');		
        $data['satuan_berat'] = $this->master_model->get_list_satuan('berat');		
		if($dbact) $data['error'] .= $this->penjualan_model->error_message;
		
		if($id==''){
			$data['first_title'] = 'Add';		
            $data['second_title'] = 'Penjualan Lokal';	
			$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Penjualan Lokal','penjualan/index'),
								   array('Add New','')
                                );
		}else{
			$data['first_title'] = 'Edit';		
            $data['second_title'] = 'Penjualan V-Lega';	
			$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Penjualan Lokal','penjualan/index'),
								   array('Edit','')
                                );
		}
							 
		$data = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
		
	}
	
	function delete(){
		$this->penjualan_model->delete($this->uri->segment(4),$this->client_id);
			
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','Data Berhasil Dihapus.');
		redirect('client/penjualan/','location');
	}
    
    function finalize(){
		$this->penjualan_model->finalize($this->uri->segment(4),$this->client_id);
			
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','Data Berhasil Disimpan.');
		redirect('client/penjualan/','location');
	}
    
    
    function detail($idekspor)
	{
        $data = $this->penjualan_model->detail($idekspor);
		$data['template'] = 'penjualan/detail';	
		//$data['query'] = $this->penjualan_model->list_detail($idekspor,$this->client_id);		
		$data['first_title'] = 'Detail Penjualan';		
		$data['second_title'] = 'Lokal';
		$data['searchText'] = '';
		$data['pagination'] =  $this->pagination->create_links();
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Penjualan Lokal','penjualan/index'),
								   array('List','')
						     );
		$data['satuan_jumlah'] = $this->master_model->get_list_satuan('jumlah');		
        $data['satuan_volume'] = $this->master_model->get_list_satuan('volume');		
        $data['satuan_berat'] = $this->master_model->get_list_satuan('berat');		
        $data['detail_list'] = $this->penjualan_model->detail_ekspor($idekspor);
        $data['table_title'] = 'DETAIL EKSPOR FORM';					 
		$data['no'] = 0;
        $data['error'] = '';
		$data = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
	}
    
    function attachment($idekspor)
	{
        $data = array();
		$data['template'] = 'penjualan/attachment_index';	
		$data['query'] = $this->penjualan_model->list_attachment($idekspor,$this->client_id);		
		$data['noinvoice'] = $this->penjualan_model->get_no_invoice($idekspor);		
		$data['idekspor'] = $idekspor;		
		$data['first_title'] = 'Attachment';		
		$data['second_title'] = 'Lokal';
		$data['searchText'] = '';
		$data['pagination'] =  $this->pagination->create_links();
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Penjualan Lokal','penjualan/index'),
								   array('Attachment',"penjualan/attachment/$idekspor"),
								   array('List','')
						     );
        $data['table_title'] = 'V-Legal Attachment List Ekspor #'.$data['noinvoice'];					 
		$data['no'] = 0;
        $data['error'] = '';
		$data = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
	}
    
    function add_attachment($idekspor)
	{
        $data = array(
                'attach_id'         => '',
                'idekspor'          => $idekspor,
                'client_id'         => $this->client_id,
                'attach_doc'        => '',
                'attach_file'       => '',
                'attach_keterangan' => '',
                'attach_status'     => '1',
                'attach_bln'        => '',
                'attach_thn'        => '',
                'no_doc'            => '',
                'attach_file'       => '',
                'attach_invoice'    => $this->penjualan_model->get_no_invoice($idekspor),
                );
			
		$data['template'] = 'penjualan/attachment_add_new';		
		$data['first_title'] = 'Add';		
		$data['second_title'] = 'Attachment';
        $data['table_title'] = 'V-Legal Attachment Ekspor #'.$data['attach_invoice'];
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Penjualan Lokal','penjualan/index'),
                                   array('Attachment',"penjualan/attachment/$idekspor"),
								   array('Add New','')
						     );
		$data['detail_list'] = array();
		$data['error'] = '';
		$data = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
	}
    
    function edit_attachment($attach_id)
	{
        $data = $this->penjualan_model->get_detail_attachment($attach_id);
		$data['template'] = 'penjualan/attachment_add_new';		
		$data['first_title'] = 'Add';		
		$data['second_title'] = 'Attachment';
        $data['table_title'] = 'V-Legal Attachment Ekspor #'.$data['attach_invoice'];
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Penjualan Lokal','penjualan/index'),
                                   array('Attachment',"penjualan/attachment/".$data['idekspor']),
								   array('Edit','')
						     );
		$data['detail_list'] = array();
		$data['error'] = '';
		$data = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
	}
    
    function save_attachment()
    {
        $this->form_validation->set_rules('no_doc', 'No. Dokumen', 'trim|required');
		
		if ($this->form_validation->run() == TRUE){
			if($this->input->post('attach_id') == '' ) {
				if($this->penjualan_model->save_attachment($this->client_id)){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','Data Tersimpan.');
					redirect('client/penjualan/attachment/'.$this->input->post('idekspor'),'location');
				}else{
					$this->failed_save_attachment($this->input->post('attach_id'),true);
				}
			} else {
				if($this->penjualan_model->update_attachment($this->client_id)){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','Data Tersimpan.');
					redirect('client/penjualan/attachment'.$this->input->post('idekspor'),'location');
				}else{
					$this->failed_save_attachment($this->input->post('attach_id'),true);
				}
			}
		}else{
			$this->failed_save($this->input->post('attach_id'));
		}	
	}
	
	function failed_save_attachment($id,$dbact=false)
	{
		$data = $this->input->post();
		$data['template'] = 'penjualan/attachment_add_new';	
				
		$data['error'] = validation_errors();
		if($dbact) $data['error'] .= $this->penjualan_model->error_message;
		
		if($id==''){
			$data['first_title'] = 'Add';		
            $data['second_title'] = 'Attachment';
            $data['table_title'] = 'V-Legal Attachment Ekspor #'.$data['attach_invoice'];
            $data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
                                       array('Penjualan Lokal','penjualan/index'),
                                       array('Attachment',"penjualan/attachment/$idekspor"),
                                       array('Add New',''));
		}else{
			$data['first_title'] = 'Edit';		
            $data['second_title'] = 'Attachment';
            $data['table_title'] = 'V-Legal Attachment Ekspor #'.$data['attach_invoice'];
            $data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
                                       array('Penjualan Lokal','penjualan/index'),
                                       array('Attachment',"penjualan/attachment/$idekspor"),
                                       array('Edit',''));
		}
							 
		$data = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
		
	}
    
    function delete_attachment($attach_id)
    {
        $idekspor = $this->penjualan_model->delete_attachment($attach_id);
        if($idekspor != ''){
            $this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','Attachment berhasil dihapus.');
            redirect("client/penjualan/attachment/$idekspor",'location');
        }else{
            $this->session->set_flashdata('error',true);
            $this->session->set_flashdata('message_flash','Error.');
            redirect('client/penjualan','location');
        }
    
    }
    
    function submit_vlegal($idekspor)
    {
         $this->penjualan_model->updateStatusDokumen($idekspor,"Final");
         redirect('client/penjualan','location');
    }
    
    
		
	function find_buyer()
	{
		$arr = $this->master_model->get_list_buyer($this->input->get('term'),$this->client_id,true);
		$this->output->set_output(json_encode($arr));
	}
    
    function buyer_detail($idbuyer)
	{
		$arr = $this->master_model->get_buyer_detail($idbuyer,true);
		$this->output->set_output(json_encode($arr));
	}
    
    function find_valuta()
	{
		$arr['valuta_list'] = $this->master_model->get_list_valuta($this->input->get('term'),true);
		$this->output->set_output(json_encode($arr));
	}

    function find_loading()
	{
		$arr['loading_list'] = $this->master_model->get_list_loading($this->input->get('term'),true);
		$this->output->set_output(json_encode($arr));
	}
    
    function find_discharge()
	{
		$arr['discharge_list'] = $this->master_model->get_list_discharge($this->input->get('term'),$this->input->get('idnegara'),true);
		$this->output->set_output(json_encode($arr));
	}
    
    function find_provinsi()
	{
		$arr['provinsi'] = $this->master_model->get_list_provinsi($this->input->get('term'),true);
		$this->output->set_output(json_encode($arr));
	}
    
    function find_negara()
	{
		$arr['negara'] = $this->master_model->get_list_negara($this->input->get('term'),true);
		$this->output->set_output(json_encode($arr));
	}
    
    function find_jenis_kayu()
	{
		$arr = $this->master_model->get_list_jenis_kayu($this->input->get('term'),true);
		$this->output->set_output(json_encode($arr));
	}
    
    function find_product()
	{
		$arr = $this->master_model->get_list_product($this->input->get('term'),'',true);
		$this->output->set_output(json_encode($arr));
	}
    
    function find_kota($provinsi_id)
	{
		$arr['kota'] = $this->master_model->get_list_kota('',$provinsi_id,true);
		$this->output->set_output(json_encode($arr));
	}
    function find_hscode($product_id)
	{
		$hscode = $this->master_model->get_hscode($product_id);
		$this->output->set_output(json_encode($hscode));
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
?>