<?php

class Sortimen extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		permission_basic_client($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('sortimen_model');
        $this->client_id = $this->session->userdata('client_id');
	}
	
	function index()
	{
		$data = array();
		$data['template'] = 'sortimen/index';	
		$data['query'] = $this->sortimen_model->sortimen_list($this->uri->segment(4),5,$this->client_id);		
		$data['first_title'] = 'Master';		
		$data['second_title'] = 'Sortimen';
		$data['searchText'] = '';
		$data['total_list']=$this->sortimen_model->count($this->client_id);
		$this->pagination->initialize(paging_admin($data['total_list'],'client/sortimen/index',4,5));
		$data['pagination'] =  $this->pagination->create_links();
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Sortimen','client/sortimen'),
								   array('List','')
						     );
		$data['table_title'] = 'DATA SORTIMEN  " [TOTAL '.number_format($data['total_list']).' SORTIMEN] " ACTIVE';					 
		$data['no'] = $this->uri->segment(4);
		$data = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
	}
    
    function search($search = '',$limit = '')
	{
		if($this->input->post('searchText')==''){
			if($this->uri->segment(4) != ''){
				$data = array();
				$data['template'] = 'sortimen/index';	
				$data['first_title'] = 'Search';		
                $data['second_title'] = 'Sortimen';
				$data['query'] = $this->sortimen_model->sortimen_search(rawurldecode($search),$limit,5,$this->client_id);	
				$data['total_list']=$this->sortimen_model->count_search(rawurldecode($search),$this->client_id);
				$this->pagination->initialize(paging_admin($data['total_list'],
															"client/sortimen/search/$search/",5,5));		
				$data['pagination'] =  $this->pagination->create_links();
				
                $data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
                                       array('Master Sortimen','client/sortimen'),
                                       array('Search','')
                                 );
				$data['searchText'] = rawurldecode($search);
				$data['table_title'] = 'DATA SORTIMEN " [TOTAL '.$data['total_list'].' SORTIMEN] " HASIL PENCARIAN';
                $data['no'] = $limit;
				$data = array_merge($data,admin_info());
				$this->parser->parse('client/index',$data);
			}else{
				redirect('client/sortimen');
			}
		}else{
			redirect('client/sortimen/search/'.rawurlencode($this->input->post('searchText')));
		}
	}
		
	function add()
	{
		$data = array(
				'idsortimen'    => '',
				'sortimen'      => '',
				'status'        => '1',
				);
		$data['template'] = 'sortimen/add_new';		
		$data['first_title'] = 'Add';		
		$data['second_title'] = 'Sortimen';
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Sortimen','client/sortimen'),
								   array('Add New','')
						     );
		$data['error'] = '';
		//print_r($data['negara_list']);exit();
		$data = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
	}
	
	function update($id)
	{
		if($id != ''){
			$row = $this->sortimen_model->detail($id,$this->client_id);
			if(isset($row->idsortimen)){
				$data = array(
						'idsortimen'    => $row->idsortimen,
						'sortimen'      => $row->sortimen,						
						'status'        => $row->status,
						);	
				$data['template'] = 'sortimen/add_new';	
				$data['first_title'] = 'Edit';		
                $data['second_title'] = 'Sortimen';
				$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Sortimen','client/sortimen'),
								   array('Edit','')
						     );
				$data['error'] = '';	
				$data = array_merge($data,admin_info());				
				$this->parser->parse('client/index',$data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','Data Tidak Ditemukan.');
				redirect('client/sortimen','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','Data Tidak Ditemukan.');
			redirect('client/sortimen');
		}	
	}
	
	function save()
	{
		$this->form_validation->set_rules('sortimen', 'Nama Sortimen', 'trim|required|min_length[1]');
		
		if ($this->form_validation->run() == TRUE){
			if($this->input->post('idsortimen') == '' ) {
				if($this->sortimen_model->insert($this->client_id)){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','Data Tersimpan.');
					redirect('client/sortimen','location');
				}else{
					$this->failed_save($this->input->post('idsortimen'),true);
				}
			} else {
				if($this->sortimen_model->update()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','Data Tersimpan.');
					redirect('client/sortimen','location');
				}else{
					$this->failed_save($this->input->post('idsortimen'),true);
				}
			}
		}else{
			$this->failed_save($this->input->post('idsortimen'));
		}	
	}
	
	function failed_save($id,$dbact=false)
	{
		$data = $this->input->post();
		
		$data['template'] = 'sortimen/add_new';	
				
		$data['error'] = validation_errors();
		if($dbact) $data['error'] .= $this->sortimen_model->error_message;
		
		if($id==''){
			$data['first_title'] = 'Add';		
            $data['second_title'] = 'Sortimen';	
			$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Sortimen','client/sortimen'),
								   array('Add New','')
						     );
		}else{
			$data['first_title'] = 'Edit';		
            $data['second_title'] = 'Sortimen';	
			$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Sortimen','client/sortimen'),
								   array('Edit','')
						     );
		}
							 
		$data = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
		
	}
	
	function delete(){
		$this->sortimen_model->delete($this->uri->segment(4),$this->client_id);
			
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','Data Berhasil Dihapus.');
		redirect('client/sortimen/','location');
	}
		
	function carinegara()
	{
		$arr['neg_list'] = $this->sortimen_model->neg_findajax($this->input->get('term'));
		$this->output->set_output(json_encode($arr));
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
?>