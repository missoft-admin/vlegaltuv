<?php

class Ekspor extends CI_Controller {
    var $client_id;
	function __construct()
	{
		parent::__construct();
		permission_basic_client($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('ekspor_model');
        $this->client_id = $this->session->userdata('client_id');
	}
	
	function index()
	{
        $data = array();
		$data['template'] = 'ekspor/index';	
		$data['query'] = $this->ekspor_model->list_index($this->uri->segment(4),20,$this->client_id);		
		$data['first_title'] = 'Penjualan ';		
		$data['second_title'] = 'Ekspor';
		$data['searchText'] = '';
		$data['total_list']= $this->ekspor_model->count($this->client_id);
		$this->pagination->initialize(paging_admin($data['total_list'],'client/ekspor/index',4,20));
		$data['pagination'] =  $this->pagination->create_links();
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'client/home'),
								   array('Penjualan Ekspor','client/ekspor/index'),
								   array('List','')
						     );
		$data['table_title'] = 'DATA EKSPOR  " [TOTAL '.number_format($data['total_list']).' DATA] "';					 
		$data['no'] = $this->uri->segment(4);
		$data = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
	}
    
    function search($search = '',$limit = '')
	{
		if($this->input->post('searchText')==''){
			if($this->uri->segment(4) != ''){
				$data = array();
				$data['template'] = 'ekspor/index';	
				$data['first_title'] = 'Search';		
                $data['second_title'] = 'Penjualan Ekspor';
				$data['query'] =  $this->ekspor_model->list_index($limit,20,$this->client_id,'','',rawurldecode($search));
				$data['total_list']=$this->ekspor_model->count($this->client_id,'','',rawurldecode($search));
				$this->pagination->initialize(paging_admin($data['total_list'],
															"client/ekspor/search/$search/",5,20));		
				$data['pagination'] =  $this->pagination->create_links();
				
               $data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'client/home'),
								   array('Penjualan Ekspor','client/ekspor/index'),
								   array('Search','')
						     );
				$data['searchText'] = rawurldecode($search);
				$data['table_title'] = 'DATA EKSPOR " [TOTAL '.$data['total_list'].' DATA] " HASIL PENCARIAN';
                $data['no'] = $limit;
				$data = array_merge($data,admin_info());
				$this->parser->parse('client/index',$data);
			}else{
				redirect('client/ekspor');
			}
		}else{
			redirect('client/ekspor/search/'.rawurlencode($this->input->post('searchText')));
		}
	}
    
    function draft()
	{
        $data = array();
		$data['template'] = 'ekspor/index_draft';	
		$data['query'] = $this->ekspor_model->list_index($this->uri->segment(4),20,$this->client_id,'','Editing');		
		$data['first_title'] = 'Pengajuan V-Legal ';		
		$data['second_title'] = 'Draft';
		$data['searchText'] = '';
		$data['total_list']= $this->ekspor_model->count($this->client_id,'','Editing');
		$this->pagination->initialize(paging_admin($data['total_list'],'client/ekspor/draft',4,20));
		$data['pagination'] =  $this->pagination->create_links();
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'client/home'),
								   array('Pengajuan V-Legal Draft','client/ekspor/draft'),
								   array('List','')
						     );
		$data['table_title'] = 'DATA PENGAJUAN DRAFT " [TOTAL '.number_format($data['total_list']).' DATA] "';					 
		$data['no'] = $this->uri->segment(4);
		$data = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
	}
    
    function search_draft($search = '',$limit = '')
	{
		if($this->input->post('searchText')==''){
			if($this->uri->segment(4) != ''){
				$data = array();
				$data['template'] = 'ekspor/index_draft';	
				$data['first_title'] = 'Search';		
                $data['second_title'] = 'Pengajuan V-Legal Draft';
				$data['query'] =  $this->ekspor_model->list_index($limit,20,$this->client_id,'','Editing',rawurldecode($search));
				$data['total_list']=$this->ekspor_model->count($this->client_id,'','Editing',rawurldecode($search));
				$this->pagination->initialize(paging_admin($data['total_list'],
															"client/ekspor/search_draft/$search/",5,20));		
				$data['pagination'] =  $this->pagination->create_links();
				
               $data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'client/home'),
								   array('Pengajuan V-Legal Draft','client/ekspor/draft'),
								   array('Search','')
						     );
				$data['searchText'] = rawurldecode($search);
				$data['table_title'] = 'DATA PENGAJUAN DRAFT " [TOTAL '.$data['total_list'].' DATA] " HASIL PENCARIAN';
                $data['no'] = $limit;
				$data = array_merge($data,admin_info());
				$this->parser->parse('client/index',$data);
			}else{
				redirect('client/ekspor/draft');
			}
		}else{
			redirect('client/ekspor/search_draft/'.rawurlencode($this->input->post('searchText')));
		}
	}
    
    function on_progress()
	{
        $data = array();
		$data['template'] = 'ekspor/index_progress';	
		$data['query'] = $this->ekspor_model->list_index($this->uri->segment(4),20,$this->client_id,'','Final');		
		$data['first_title'] = 'Pengajuan V-Legal';		
		$data['second_title'] = 'On Progress';
		$data['searchText'] = '';
		$data['total_list']= $this->ekspor_model->count($this->client_id,'','Final');
		$this->pagination->initialize(paging_admin($data['total_list'],'client/ekspor/on_progress',4,20));
		$data['pagination'] =  $this->pagination->create_links();
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Pengajuan V-Legal On Progress','client/ekspor/on_progress'),
								   array('List','')
						     );
		$data['table_title'] = 'DATA PENGAJUAN ON PROGRESS  " [TOTAL '.number_format($data['total_list']).' DATA] "';					 
		$data['no'] = $this->uri->segment(4);
		$data = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
	}
    
    function d($search = '',$limit = '')
	{
		if($this->input->post('searchText')==''){
			if($this->uri->segment(4) != ''){
				$data = array();
				$data['template'] = 'ekspor/index_progress';	
				$data['first_title'] = 'Search';		
                $data['second_title'] = 'Pengajuan V-Legal On Progres';
				$data['query'] =  $this->ekspor_model->list_index($limit,20,$this->client_id,'','Final',rawurldecode($search));
				$data['total_list']=$this->ekspor_model->count($this->client_id,'','Final',rawurldecode($search));
				$this->pagination->initialize(paging_admin($data['total_list'],
															"client/ekspor/search_progress/$search/",5,20));		
				$data['pagination'] =  $this->pagination->create_links();
				
               $data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'client/home'),
								   array('Pengajuan V-Legal On Progress','client/ekspor/on_progress'),
								   array('Search','')
						     );
				$data['searchText'] = rawurldecode($search);
				$data['table_title'] = 'DATA PENGAJUAN ON PROGRESS " [TOTAL '.$data['total_list'].' DATA] " HASIL PENCARIAN';
                $data['no'] = $limit;
				$data = array_merge($data,admin_info());
				$this->parser->parse('client/index',$data);
			}else{
				redirect('client/ekspor/on_progress');
			}
		}else{
			redirect('client/ekspor/search_progress/'.rawurlencode($this->input->post('searchText')));
		}
	}
    
    
    function rejected()
	{
        $data = array();
		$data['template'] = 'ekspor/index_reject';	
		$data['query'] = $this->ekspor_model->list_index($this->uri->segment(4),20,$this->client_id,'reject','Final');		
		$data['first_title'] = 'Pengajuan V-Legal';		
		$data['second_title'] = 'Rejected';
		$data['searchText'] = '';
		$data['total_list']= $this->ekspor_model->count($this->client_id,'reject','Final');
		$this->pagination->initialize(paging_admin($data['total_list'],'client/ekspor/rejected',4,20));
		$data['pagination'] =  $this->pagination->create_links();
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Pengajuan V-Legal Reject','client/ekspor/on_progress'),
								   array('List','')
						     );
		$data['table_title'] = 'DATA PENGAJUAN REJECT  " [TOTAL '.number_format($data['total_list']).' DATA] "';					 
		$data['no'] = $this->uri->segment(4);
		$data = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
	}
    
    function search_rejected($search = '',$limit = '')
	{
		if($this->input->post('searchText')==''){
			if($this->uri->segment(4) != ''){
				$data = array();
				$data['template'] = 'ekspor/index_reject';	
				$data['first_title'] = 'Search';		
                $data['second_title'] = 'Pengajuan V-Legal Rejected';
				$data['query'] =  $this->ekspor_model->list_index($limit,20,$this->client_id,'reject','Final',rawurldecode($search));
				$data['total_list']=$this->ekspor_model->count($this->client_id,'reject','Final',rawurldecode($search));
				$this->pagination->initialize(paging_admin($data['total_list'],
															"client/ekspor/search_rejected/$search/",5,20));		
				$data['pagination'] =  $this->pagination->create_links();
				
               $data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'client/home'),
								   array('Pengajuan V-Legal Rejected','client/ekspor/rejected'),
								   array('Search','')
						     );
				$data['searchText'] = rawurldecode($search);
				$data['table_title'] = 'DATA PENGAJUAN REJECT " [TOTAL '.$data['total_list'].' DATA] " HASIL PENCARIAN';
                $data['no'] = $limit;
				$data = array_merge($data,admin_info());
				$this->parser->parse('client/index',$data);
			}else{
				redirect('client/ekspor/rejected');
			}
		}else{
			redirect('client/ekspor/search_rejected/'.rawurlencode($this->input->post('searchText')));
		}
	}
    
    
    function approved()
	{
        $data = array();
		$data['template'] = 'ekspor/index_approved';	
		$data['query'] = $this->ekspor_model->list_index($this->uri->segment(4),20,$this->client_id,'send','Final');		
		$data['first_title'] = 'Pengajuan V-Legal';		
		$data['second_title'] = 'Approved';
		$data['searchText'] = '';
		$data['total_list']= $this->ekspor_model->count($this->client_id,'send','Final');
		$this->pagination->initialize(paging_admin($data['total_list'],'client/ekspor/approved',4,20));
		$data['pagination'] =  $this->pagination->create_links();
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Pengajuan V-Legal Approved','client/ekspor/approved'),
								   array('List','')
						     );
		$data['table_title'] = 'DATA PENGAJUAN APPROVED  " [TOTAL '.number_format($data['total_list']).' DATA] "';					 
		$data['no'] = $this->uri->segment(4);
		$data = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
	}
    
    function search_approved($search = '',$limit = '')
	{
		if($this->input->post('searchText')==''){
			if($this->uri->segment(4) != ''){
				$data = array();
				$data['template'] = 'ekspor/index_approved';	
				$data['first_title'] = 'Search';		
                $data['second_title'] = 'Pengajuan V-Legal Rejected';
				$data['query'] =  $this->ekspor_model->list_index($limit,20,$this->client_id,'send','Final',rawurldecode($search));
				$data['total_list']=$this->ekspor_model->count($this->client_id,'send','Final',rawurldecode($search));
				$this->pagination->initialize(paging_admin($data['total_list'],
															"client/ekspor/search_approved/$search/",5,20));		
				$data['pagination'] =  $this->pagination->create_links();
				
               $data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'client/home'),
								   array('Pengajuan V-Legal Approved','client/ekspor/approved'),
								   array('Search','')
						     );
				$data['searchText'] = rawurldecode($search);
				$data['table_title'] = 'DATA PENGAJUAN APPROVED " [TOTAL '.$data['total_list'].' DATA] " HASIL PENCARIAN';
                $data['no'] = $limit;
				$data = array_merge($data,admin_info());
				$this->parser->parse('client/index',$data);
			}else{
				redirect('client/ekspor/approved');
			}
		}else{
			redirect('client/ekspor/search_approved/'.rawurlencode($this->input->post('searchText')));
		}
	}
    
		
	function add($ref_url = '')
	{
		$client = $this->master_model->get_client_vlegal($this->client_id);	
        $data = array(
                'idekspor'      => '',
                'idbuyer'       => '',
                'namabuyer'       => '',
                'alamat'        => '',
                'idnegara'      => '',
                'iso'           => '',
                'loading'       => '',
                'discharge'     => '',
                'invoice'       => '',
                'tglinvoice'    => '',
                'no_po'         => '',
                'tgl_po'        => '',
                'peb'           => '',
                'tglship'       => '',
                'vessel'        => '',
                'bl'            => '',
                'etpik'         => $client->etpik,
                'sertifikat'    => $client->client_sertifikat,
                'npwp'          => $client->npwp,
                'keterangan'    => '',
                'valuta'        => '',
                'status'        => '',
                'status_dokumen' => '',
                'stuffing'          => $client->stuffing_data,
                'country_name'      => '',
                'nama_valuta'       => '',
                'loading_name'      => '',
                'discharge_name'    => '',
                'paper_number'      => '[]',
                'final'             => 0,
                );
			
		$data['template'] = 'ekspor/add_new';		
		$data['first_title'] = 'Add Pengajuan';		
		$data['second_title'] = 'V-Legal';
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Pengajuan V-Legal','ekspor/index'),
								   array('Add New','')
						     );
		$data['detail_list'] = array();
        $data['satuan_jumlah'] = $this->master_model->get_list_satuan('jumlah');		
        $data['satuan_volume'] = $this->master_model->get_list_satuan('volume');		
        $data['satuan_berat'] = $this->master_model->get_list_satuan('berat');		
		$data['error'] = '';
        $data['ref_url'] = $ref_url;
		$data = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
	}
    
    function edit($idekspor,$ref_url = '')
    {
        $data                   = $this->ekspor_model->detail($idekspor);
		$data['template']       = 'ekspor/add_new';		
		$data['final']          = 0;		
		$data['first_title']    = 'Edit Pengajuan';		
		$data['second_title']   = 'V-Legal';
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Pengajuan V-Legal','ekspor/index'),
								   array('Edit','')
						     );
		$data['detail_list'] = $this->ekspor_model->detail_ekspor($idekspor);
        $data['satuan_jumlah'] = $this->master_model->get_list_satuan('jumlah');		
        $data['satuan_volume'] = $this->master_model->get_list_satuan('volume');		
        $data['satuan_berat'] = $this->master_model->get_list_satuan('berat');		
        $data['error'] = '';
        $data['ref_url'] = $ref_url;
		$data = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
    }
	
	
	function save()
	{
        $this->form_validation->set_rules('idnegara', 'Buyer', 'trim|required');
        $this->form_validation->set_rules('invoice', 'No. Invoice', 'trim|required');
        $this->form_validation->set_rules('tglinvoice', 'Tanggal Invoice', 'trim|required');
        $this->form_validation->set_rules('valuta', 'Mata Uang', 'trim|required');
        $this->form_validation->set_rules('loading', 'Port of Loading', 'trim|required');
        $this->form_validation->set_rules('discharge', 'Port of Discharge', 'trim|required');
        $this->form_validation->set_rules('tglship', 'Tanggal Shipment', 'trim|required');
        $this->form_validation->set_rules('vessel', 'Alat Angkut', 'trim|required');
        $this->form_validation->set_rules('status', 'Status Dokumen', 'trim|required');
        //$this->form_validation->set_rules('keterangan', 'Keterangan', 'trim|required');
		
		if ($this->form_validation->run() == TRUE){
			if($this->input->post('idekspor') == '' ) {
				if($this->ekspor_model->save($this->client_id)){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','Data Tersimpan.');
					redirect('client/ekspor/'.$this->input->post('ref_url'),'location');
				}else{
					$this->failed_save($this->input->post('idekspor'),true);
				}
			} else {
				if($this->ekspor_model->update()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','Data Tersimpan.');
					redirect('client/ekspor/'.$this->input->post('ref_url'),'location');
				}else{
					$this->failed_save($this->input->post('idekspor'),true);
				}
			}
		}else{
			$this->failed_save($this->input->post('idekspor'));
		}	
	}
	
	function failed_save($id,$dbact=false)
	{
		$data = $this->input->post();
		$data['template'] = 'ekspor/add_new';	
				
		$data['error']  = validation_errors();
		if($dbact) $data['error'] .= $this->ekspor_model->error_message;
		$data['final']  = 0;
		if($id==''){
			$data['first_title'] = 'Add';		
            $data['second_title'] = 'Pengajuan V-Legal';	
			$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Pengajuan V-Legal','ekspor/index'),
								   array('Add New','')
                                );
		}else{
			$data['first_title'] = 'Edit';		
            $data['second_title'] = 'Pengajuan V-Lega';	
			$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Pengajuan V-Legal','ekspor/index'),
								   array('Edit','')
                                );
		}
							 
		$data = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
		
	}
	
	function delete($idekspor,$ref_url = ''){
		$this->ekspor_model->delete($idekspor);
			
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','Data Berhasil Dihapus.');
		redirect('client/ekspor/'.$ref_url);
	}
    
    function cancel($idekspor,$vid,$ref_url = ''){
		$this->ekspor_model->cancel($idekspor,$vid);
			
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','V-Legal Berhasil Dicancel.');
		redirect('client/ekspor/'.$ref_url);
	}
    
    
    function detail($idekspor,$ref_url='')
	{
        $data = $this->ekspor_model->detail($idekspor);
		$data['template'] = 'ekspor/detail';	
		//$data['query'] = $this->ekspor_model->list_detail($idekspor,$this->client_id);		
		$data['first_title'] = 'Detail Pengajuan';		
		$data['second_title'] = 'V-Legal';
		$data['searchText'] = '';
		$data['pagination'] =  $this->pagination->create_links();
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Pengajuan V-Legal','ekspor/index'),
								   array('List','')
						     );
		$data['satuan_jumlah'] = $this->master_model->get_list_satuan('jumlah');		
        $data['satuan_volume'] = $this->master_model->get_list_satuan('volume');		
        $data['satuan_berat'] = $this->master_model->get_list_satuan('berat');		
        $data['detail_list'] = $this->ekspor_model->detail_ekspor($idekspor);
        $data['table_title'] = 'DETAIL EKSPOR FORM';					 
		$data['ref_url'] = $ref_url;
		$data['no'] = 0;
        $data['error'] = '';
		$data = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
	}
    
    function attachment($idekspor,$ref_url='')
	{
        $data = array();
		$data['template'] = 'ekspor/attachment_index';	
		$data['query'] = $this->ekspor_model->list_attachment($idekspor,$this->client_id);		
		$data['noinvoice'] = $this->ekspor_model->get_no_invoice($idekspor);		
		$data['idekspor'] = $idekspor;		
		$data['first_title'] = 'Attachment';		
		$data['second_title'] = 'V-Legal';
		$data['searchText'] = '';
		$data['pagination'] =  $this->pagination->create_links();
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Pengajuan V-Legal','ekspor/index'),
								   array('Attachment',"ekspor/attachment/$idekspor"),
								   array('List','')
						     );
        $data['table_title'] = 'V-Legal Attachment List Ekspor #'.$data['noinvoice'];					 
		$data['no'] = 0;
        $data['error'] = '';
        $data['ref_url'] = $ref_url;
		$data = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
	}
    
    function add_attachment($idekspor,$ref_url='')
	{
        $data = array(
                'attach_id'         => '',
                'idekspor'          => $idekspor,
                'client_id'         => $this->client_id,
                'attach_doc'        => '',
                'attach_file'       => '',
                'attach_keterangan' => '',
                'attach_status'     => '1',
                'attach_bln'        => '',
                'attach_thn'        => '',
                'no_doc'            => '',
                'attach_file'       => '',
                'attach_invoice'    => $this->ekspor_model->get_no_invoice($idekspor),
                );
			
		$data['template'] = 'ekspor/attachment_add_new';		
		$data['first_title'] = 'Add';		
		$data['second_title'] = 'Attachment';
        $data['table_title'] = 'V-Legal Attachment Ekspor #'.$data['attach_invoice'];
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Pengajuan V-Legal','ekspor/index'),
                                   array('Attachment',"ekspor/attachment/$idekspor"),
								   array('Add New','')
						     );
		$data['detail_list'] = array();
		$data['error'] = '';
        $data['ref_url'] = $ref_url;
		$data = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
	}
    
    function edit_attachment($attach_id,$ref_url='')
	{
        $data = $this->ekspor_model->get_detail_attachment($attach_id);
		$data['template'] = 'ekspor/attachment_add_new';		
		$data['first_title'] = 'Add';		
		$data['second_title'] = 'Attachment';
        $data['table_title'] = 'V-Legal Attachment Ekspor #'.$data['attach_invoice'];
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Pengajuan V-Legal','ekspor/index'),
                                   array('Attachment',"ekspor/attachment/".$data['idekspor']),
								   array('Edit','')
						     );
		$data['detail_list'] = array();
		$data['error'] = '';
        $data['ref_url'] = $ref_url;
		$data = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
	}
    
    function save_attachment()
    {
        $this->form_validation->set_rules('no_doc', 'No. Dokumen', 'trim|required');
		
		if ($this->form_validation->run() == TRUE){
			if($this->input->post('attach_id') == '' ) {
				if($this->ekspor_model->save_attachment($this->client_id)){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','Data Tersimpan.');
					redirect('client/ekspor/attachment/'.$this->input->post('idekspor').'/'.$this->input->post('ref_url'),'location');
				}else{
					$this->failed_save_attachment($this->input->post('attach_id'),true);
				}
			} else {
				if($this->ekspor_model->update_attachment($this->client_id)){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','Data Tersimpan.');
					redirect('client/ekspor/attachment'.$this->input->post('idekspor').'/'.$this->input->post('ref_url'),'location');
				}else{
					$this->failed_save_attachment($this->input->post('attach_id'),true);
				}
			}
		}else{
			$this->failed_save($this->input->post('attach_id'));
		}	
	}
	
	function failed_save_attachment($id,$dbact=false)
	{
		$data = $this->input->post();
		$data['template'] = 'ekspor/attachment_add_new';	
				
		$data['error'] = validation_errors();
		if($dbact) $data['error'] .= $this->ekspor_model->error_message;
		
		if($id==''){
			$data['first_title'] = 'Add';		
            $data['second_title'] = 'Attachment';
            $data['table_title'] = 'V-Legal Attachment Ekspor #'.$data['attach_invoice'];
            $data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
                                       array('Pengajuan V-Legal','ekspor/index'),
                                       array('Attachment',"ekspor/attachment/$idekspor"),
                                       array('Add New',''));
		}else{
			$data['first_title'] = 'Edit';		
            $data['second_title'] = 'Attachment';
            $data['table_title'] = 'V-Legal Attachment Ekspor #'.$data['attach_invoice'];
            $data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
                                       array('Pengajuan V-Legal','ekspor/index'),
                                       array('Attachment',"ekspor/attachment/$idekspor"),
                                       array('Edit',''));
		}
							 
		$data = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
		
	}
    
    function delete_attachment($attach_id,$ref_url = '')
    {
        $idekspor = $this->ekspor_model->delete_attachment($attach_id);
        if($idekspor != ''){
            $this->session->set_flashdata('confirm',true);
			$this->session->set_flashdata('message_flash','Attachment berhasil dihapus.');
            redirect("client/ekspor/attachment/$idekspor/$ref_url",'location');
        }else{
            $this->session->set_flashdata('error',true);
            $this->session->set_flashdata('message_flash','Error.');
            redirect('client/ekspor','location');
        }
    
    }
    
    function submit_vlegal($idekspor,$ref_url = '')
    {
         $this->ekspor_model->updateStatusDokumen($idekspor,"Final");
         redirect("client/ekspor/$ref_url",'location');
    }
    
    
		
	function find_buyer()
	{
		$arr = $this->master_model->get_list_buyer($this->input->get('term'),$this->client_id,true);
		$this->output->set_output(json_encode($arr));
	}
    
    function buyer_detail($idbuyer)
	{
		$arr = $this->master_model->get_buyer_detail($idbuyer,true);
		$this->output->set_output(json_encode($arr));
	}
    
    function find_valuta()
	{
		$arr = $this->master_model->get_list_valuta($this->input->get('term'),true);
		$this->output->set_output(json_encode($arr));
	}

    function find_loading()
	{
		$arr = $this->master_model->get_list_loading($this->input->get('term'),true);
		$this->output->set_output(json_encode($arr));
	}
    
    function find_discharge()
	{
		$arr = $this->master_model->get_list_discharge($this->input->get('term'),$this->input->get('idnegara'),true);
		$this->output->set_output(json_encode($arr));
	}
    
    function find_provinsi()
	{
		$arr = $this->master_model->get_list_provinsi($this->input->get('term'),true);
		$this->output->set_output(json_encode($arr));
	}
    
    function find_negara()
	{
		$arr = $this->master_model->get_list_negara($this->input->get('term'),true);
		$this->output->set_output(json_encode($arr));
	}
    
    function find_negara_token()
	{
		$arr = $this->master_model->get_list_negara_token($this->input->get('q'),true);
		$this->output->set_output(json_encode($arr));
	}
    
    function find_vlegal_paper()
	{
		$arr = $this->master_model->get_list_paper($this->client_id,$this->input->get('q'),true);
		$this->output->set_output(json_encode($arr));
	}
    
    function find_jenis_kayu()
	{
		$arr = $this->master_model->get_list_jenis_kayu_token($this->input->get('q'),true);
		$this->output->set_output(json_encode($arr));
	}
    
    function find_product()
	{
		$arr = $this->master_model->get_list_product($this->input->get('term'),'',true);
		$this->output->set_output(json_encode($arr));
	}
    
    function find_kota($provinsi_id)
	{
		$arr = $this->master_model->get_list_kota('',$provinsi_id,true);
		$this->output->set_output(json_encode($arr));
	}
    function find_hscode($product_id)
	{
		$hscode = $this->master_model->get_hscode($product_id);
		$this->output->set_output(json_encode($hscode));
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
?>