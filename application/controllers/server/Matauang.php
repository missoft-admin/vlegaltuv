<?php

class Matauang extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		permission_basic_server($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('matauang_model');
	}
	
	function index()
	{
		$data = array();
		$data['template'] = 'matauang/index';	
		$data['query'] = $this->matauang_model->matauang_list($this->uri->segment(4),5);		
		$data['first_title'] = 'Master';		
		$data['second_title'] = 'Mata Uang';
		$data['searchText'] = '';
		$data['total_list']=$this->matauang_model->count();
		$this->pagination->initialize(paging_admin($data['total_list'],'server/matauang/index',4,5));
		$data['pagination'] =  $this->pagination->create_links();
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Mata Uang','server/matauang'),
								   array('List','')
						     );
		$data['table_title'] = 'DATA MATA UANG  " [TOTAL '.number_format($data['total_list']).' MATA UANG] " ACTIVE';					 
		$data['no'] = $this->uri->segment(4);
		$data = array_merge($data,admin_info());
		$this->parser->parse('server/index',$data);
	}
    
    function search($search = '',$limit = '')
	{
		
		if($this->input->post('searchText')==''){
			if($this->uri->segment(4) != ''){
				$data = array();
				$data['template'] = 'matauang/index';	
				$data['first_title'] = 'Search';		
                $data['second_title'] = 'Mata Uang';
				$data['query'] = $this->matauang_model->matauang_search(rawurldecode($search),$limit,5);	
				$data['total_list']=$this->matauang_model->count_search(rawurldecode($search));
				$this->pagination->initialize(paging_admin($data['total_list'],
															"server/matauang/search/$search/",5,5));		
				$data['pagination'] =  $this->pagination->create_links();
				
                $data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
                                       array('Master Mata Uang','server/matauang'),
                                       array('Search','')
                                 );
				$data['searchText'] = rawurldecode($search);
				$data['table_title'] = 'DATA MATA UANG " [TOTAL '.number_format($data['total_list']).' MATA UANG] " HASIL PENCARIAN';
                $data['no'] = $limit;
				$data = array_merge($data,admin_info());
				
				$this->parser->parse('server/index',$data);
			}else{
				
				redirect('server/matauang');
			}
		}else{
			//print_r('Tas');exit();
			redirect('server/matauang/search/'.rawurlencode($this->input->post('searchText')));
		}
	}
		
	function add()
	{
		$data = array(
				'iduang' => '',
				'nama' => '',
				'kode' => '',
				);
		$data['template'] = 'matauang/add_new';		
		$data['first_title'] = 'Add';		
		$data['second_title'] = 'Mata Uang';
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Mata Uang','server/matauang'),
								   array('Add New','')
						     );
		$data['error'] = '';
		//print_r($data['negara_list']);exit();
		$data = array_merge($data,admin_info());
		$this->parser->parse('server/index',$data);
	}
	
	function update($id)
	{
		if($id != ''){
			$row = $this->matauang_model->detail($id);
			if(isset($row->iduang)){
				$data = array(
						'iduang' => $row->iduang,
						'kode' => $row->kode,
						'nama' => $row->nama,						
						);	
				$data['template'] = 'matauang/add_new';	
				$data['first_title'] = 'Edit';		
                $data['second_title'] = 'Mata Uang';
				$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Mata Uang','server/matauang'),
								   array('Edit','')
						     );
				$data['error'] = '';	
				$data = array_merge($data,admin_info());				
				$this->parser->parse('server/index',$data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','Data Tidak Ditemukan.');
				redirect('server/matauang','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','Data Tidak Ditemukan.');
			redirect('server/matauang');
		}	
	}
	
	function save()
	{
		$this->form_validation->set_rules('nama', 'Nama Mata Uang', 'trim|required|min_length[1]');
		$this->form_validation->set_rules('kode', 'Kode', 'trim|required|min_length[1]');
		
		if ($this->form_validation->run() == TRUE){
			if($this->input->post('iduang') == '' ) {
				if($this->matauang_model->insert()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','Data Tersimpan.');
					redirect('server/matauang','location');
				}else{
					$this->failed_save($this->input->post('iduang'),true);
				}
			} else {
				if($this->matauang_model->update()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','Data Tersimpan.');
					redirect('server/matauang','location');
				}else{
					$this->failed_save($this->input->post('iduang'),true);
				}
			}
		}else{
			$this->failed_save($this->input->post('iduang'));
		}	
	}
	
	function failed_save($id,$dbact=false)
	{
		$data = $this->input->post();
		
		$data['template'] = 'matauang/add_new';	
				
		$data['error'] = validation_errors();
		if($dbact) $data['error'] .= $this->matauang_model->error_message;
		
		if($id==''){
			$data['first_title'] = 'Add';		
            $data['second_title'] = 'Mata Uang';	
			$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Mata Uang','server/matauang'),
								   array('Add New','')
						     );
		}else{
			$data['first_title'] = 'Edit';		
            $data['second_title'] = 'Mata Uang';	
			$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Mata Uang','server/matauang'),
								   array('Edit','')
						     );
		}
							 
		$data = array_merge($data,admin_info());
		$this->parser->parse('server/index',$data);
		
	}
	
	function delete(){
		$this->matauang_model->delete($this->uri->segment(4));
			
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','Data Berhasil Dihapus.');
		redirect('server/matauang/','location');
	}
		
	function carinegara()
	{
		$arr['neg_list'] = $this->matauang_model->neg_findajax($this->input->get('term'));
		$this->output->set_output(json_encode($arr));
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
?>