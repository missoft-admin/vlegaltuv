<?php

class Vlegal extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		permission_basic_server($this->session);
		$this->load->library('form_validation');
        $this->load->library("Silk");
        $this->load->library("email");
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('ekspor_model');
		$this->load->model('client_model');
		
      
	}
	
	function index()
	{
        $data = array();
		$data['template'] = 'vlegal/index';	
		$data['query'] = $this->ekspor_model->list_index_pengajuan($this->uri->segment(4),20);		
		$data['first_title'] = 'Pengajuan';		
		$data['second_title'] = 'V-Legal';
		$data['searchText'] = '';
		$data['total_list']= $this->ekspor_model->count_pengajuan();
		$this->pagination->initialize(paging_admin($data['total_list'],'server/vlegal/index',4,20));
		$data['pagination'] =  $this->pagination->create_links();
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'server/home'),
								   array('Pengajuan V-Legal','server/ekspor/index'),
								   array('List','')
						     );
		$data['table_title'] = 'DATA PENGAJUAN  " [TOTAL '.number_format($data['total_list']).' DATA] "';					 
		$data['no'] = $this->uri->segment(4);
		$data = array_merge($data,admin_info());
		$this->parser->parse('server/index',$data);
	}
    
    function search($search = '',$limit = '')
	{
		if($this->input->post('searchText')==''){
			if($this->uri->segment(4) != ''){
				$data = array();
				$data['template'] = 'vlegal/index';	
				$data['first_title'] = 'Search';		
                $data['second_title'] = 'Pengajuan V-Legal';
				$data['query'] =  $this->ekspor_model->list_index_pengajuan($limit,20,rawurldecode($search));
				$data['total_list']=$this->ekspor_model->count_pengajuan(rawurldecode($search));
				$this->pagination->initialize(paging_admin($data['total_list'],
															"server/vlegal/search/$search/",5,20));		
				$data['pagination'] =  $this->pagination->create_links();
				
               $data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'server/home'),
								   array('Pengajuan V-Legal','server/vlegal/index'),
								   array('Search','')
						     );
				$data['searchText'] = rawurldecode($search);
				$data['table_title'] = 'DATA PENGAJUAN " [TOTAL '.$data['total_list'].' DATA] " HASIL PENCARIAN';
                $data['no'] = $limit;
				$data = array_merge($data,admin_info());
				$this->parser->parse('server/index',$data);
			}else{
				redirect('server/vlegal');
			}
		}else{
			redirect('server/vlegal/search/'.rawurlencode($this->input->post('searchText')));
		}
	}
    
    function rejected()
	{
        $data = array();
		$data['template'] = 'vlegal/index_reject';	
		$data['query'] = $this->ekspor_model->list_index_processed($this->uri->segment(4),20,'rejected');		
		$data['first_title'] = 'Dokumen V-Legal';		
		$data['second_title'] = 'Reject';
		$data['searchText'] = '';
		$data['total_list']= $this->ekspor_model->count_processed('rejected');
		$this->pagination->initialize(paging_admin($data['total_list'],'server/vlegal/rejected',4,20));
		$data['pagination'] =  $this->pagination->create_links();
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'server/home'),
								   array('Pengajuan V-Legal Reject','server/vlegal/rejected'),
								   array('List','')
						     );
		$data['table_title'] = 'DATA PENGAJUAN REJECT  " [TOTAL '.number_format($data['total_list']).' DATA] "';					 
		$data['no'] = $this->uri->segment(4);
		$data = array_merge($data,admin_info());
		$this->parser->parse('server/index',$data);
	}
    
    function search_reject($search = '',$limit = '')
	{
		if($this->input->post('searchText')==''){
			if($this->uri->segment(4) != ''){
				$data = array();
				$data['template'] = 'vlegal/index_reject';	
				$data['first_title'] = 'Search';		
                $data['second_title'] = 'Dokumen V-Legal Reject';
				$data['query'] =  $this->ekspor_model->list_index_processed($limit,20,'rejected',rawurldecode($search));
				$data['total_list']=$this->ekspor_model->count_processed('rejected',rawurldecode($search));
				$this->pagination->initialize(paging_admin($data['total_list'],
															"server/vlegal/search_reject/$search/",5,20));		
				$data['pagination'] =  $this->pagination->create_links();
				
               $data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'server/home'),
								   array('Pengajuan V-Legal','server/vlegal/index'),
								   array('Search','')
						     );
				$data['searchText'] = rawurldecode($search);
				$data['table_title'] = 'DATA PENGAJUAN REJECT " [TOTAL '.$data['total_list'].' DATA] " HASIL PENCARIAN';
                $data['no'] = $limit;
				$data = array_merge($data,admin_info());
				$this->parser->parse('server/index',$data);
			}else{
				redirect('server/vlegal/rejected');
			}
		}else{
			redirect('server/vlegal/search_reject/'.rawurlencode($this->input->post('searchText')));
		}
	}
    
    
    function approved()
	{
        $data = array();
		$data['template'] = 'vlegal/index_approved';	
		$data['query'] = $this->ekspor_model->list_index_processed($this->uri->segment(4),20,'send');		
		$data['first_title'] = 'Dokumen V-Legal';		
		$data['second_title'] = 'Approved';
		$data['searchText'] = '';
		$data['total_list']= $this->ekspor_model->count_processed('send');
		$this->pagination->initialize(paging_admin($data['total_list'],'server/vlegal/approved',4,20));
		$data['pagination'] =  $this->pagination->create_links();
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'server/home'),
								   array('Dokumen V-Legal Approved','server/vlegal/approved'),
								   array('List','')
						     );
		$data['table_title'] = 'DATA PENGAJUAN APPROVED  " [TOTAL '.number_format($data['total_list']).' DATA] "';					 
		$data['no'] = $this->uri->segment(4);
		$data = array_merge($data,admin_info());
		$this->parser->parse('server/index',$data);
	}
    
    function search_approved($search = '',$limit = '')
	{
		if($this->input->post('searchText')==''){
			if($this->uri->segment(4) != ''){
				$data = array();
				$data['template'] = 'vlegal/index_approved';	
				$data['first_title'] = 'Search';		
                $data['second_title'] = 'Dokumen V-Legal Approved';
				$data['query'] =  $this->ekspor_model->list_index_processed($limit,20,'send',rawurldecode($search));
				$data['total_list']=$this->ekspor_model->count_processed('send',rawurldecode($search));
				$this->pagination->initialize(paging_admin($data['total_list'],
															"server/vlegal/search_approved/$search/",5,20));		
				$data['pagination'] =  $this->pagination->create_links();
				
               $data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'server/home'),
								   array('Pengajuan V-Legal','server/vlegal/index'),
								   array('Search','')
						     );
				$data['searchText'] = rawurldecode($search);
				$data['table_title'] = 'DATA PENGAJUAN APPROVED " [TOTAL '.$data['total_list'].' DATA] " HASIL PENCARIAN';
                $data['no'] = $limit;
				$data = array_merge($data,admin_info());
				$this->parser->parse('server/index',$data);
			}else{
				redirect('server/vlegal/approved');
			}
		}else{
			redirect('server/vlegal/search_approved/'.rawurlencode($this->input->post('searchText')));
		}
	}
    
		
	
	function delete(){
		$this->buyer_model->delete($this->uri->segment(4));
			
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','Data Berhasil Dihapus.');
		redirect('server/buyer/','location');
	}
    
    
    function review($idekspor)
	{
        $data = $this->ekspor_model->review($idekspor);
		$data['template'] = 'vlegal/review';	
		//$data['query'] = $this->ekspor_model->list_detail($idekspor,$this->client_id);		
		$data['first_title'] = 'Review';		
		$data['second_title'] = 'V-Legal';
		$data['searchText'] = '';
		$data['pagination'] =  $this->pagination->create_links();
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Pengajuan V-Legal','server/vlegal/index'),
								   array('Review','')
						     );
        $data['detail_list'] = $this->ekspor_model->detail_ekspor($idekspor);
        $data['pejabat_list'] = $this->master_model->get_list_pejabat_ttd();
        $data['table_title'] = 'Infromasi Pengajuan V-Legal';
		$data['no'] = 0;
        $data['error'] = '';
		$data = array_merge($data,admin_info());
		$this->parser->parse('server/index',$data);
	}
    
    function downloadVlegal($id){
        $this->load->helper('download_helper');
		$data = $this->ekspor_model->getVlegalById($id);
		$dir = $this->config->item('vlegal_dir');
		$path = $dir . $data['clientid'].'/'.$data['no_dokumen'] .'.pdf';
        
        if (file_exists($path)) {
            $data = file_get_contents($path); // Read the file's contents
            $name = basename($path);
            force_download($name, $data);
        }
	}
    
    function resendVLegalEmail($id)
    {
        if($this->silk->sendEmail($id)){
            $this->session->set_flashdata('confirm',true);
            $this->session->set_flashdata('message_flash','Email Berhasil Dikirim.');   
        }else{
            $this->session->set_flashdata('error',true);
            $this->session->set_flashdata('message_flash','Email Gagal Terkirim.');   
        }
    
    }
    
    function attachment($idekspor)
	{
        $data = array();
		$data['template'] = 'vlegal/attachment_index';	
		$data['query'] = $this->ekspor_model->list_attachment($idekspor);		
		$data['first_title']    = 'Attachment';		
		$data['second_title']   = 'V-Legal';
		$data['searchText']     = '';
		$data['client_id']      = '';
		$data['client_name']    = '';
		$data['jenis_dokumen']  = '';
        $data['total_list']     = $this->ekspor_model->count_all_attachment();
		$this->pagination->initialize(paging_admin($data['total_list'],'server/vlegal/attachment',4,20));
		$data['pagination'] =  $this->pagination->create_links();
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Pengajuan V-Legal','vlegal/index'),
								   array('Attachment',"vlegal/attachment/"),
								   array('List','')
						     );
        $data['table_title'] = 'V-Legal Attachment List';					 
		$data['no'] = 0;
        $data['error'] = '';
		$data = array_merge($data,admin_info());
		$this->parser->parse('server/index',$data);
	}
    
    
    
       
    function sendtoliu()
    {
        $idekspor   = $this->input->post('idekspor');
        $pejabat 	= $this->input->post('pejabat_ttd');
		$no_urut	= $this->input->post('no_urut');
        
        if($this->silk->submit($idekspor,$pejabat,$no_urut)){
            if($this->silk->status_vlegal == true){
                $this->session->set_flashdata('confirm',true);
                $this->session->set_flashdata('message_flash','Dokumen berhasil terkirim ke LIU');
                redirect('server/vlegal','location');
            }else{
                $this->session->set_flashdata('error',true);
                $this->session->set_flashdata('message_flash','Dokumen gagal terkirim ke LIU'); 
                redirect('server/vlegal/rejected','location');
            }   
        }else{
            $this->session->set_flashdata('error',true);
            $this->session->set_flashdata('message_flash',$this->silk->error_message);   
            redirect('server/vlegal','location');    
        }   
    }
    
    function rejectdocument()
    {
        $idekspor       = $this->input->post('idekspor');
        $keterangan 	= $this->input->post('keterangan');
        
        $this->silk->rejectDocument($idekspor,$keterangan);
        redirect('server/vlegal/rejected','location');
    }
    
    function cancelDocument(){
		$idekspor   = $this->input->post('idekspor');
		$alasan     = $this->input->post('alasan');
        
        $this->silk->cancelDocument($idekspor,$alasan);
		
		$data = $this->mvlegal->getVlegalItemById($id);
		redirect('server/vlegal/approved');
	}
    
    function test_send_email(){
        	$this->email->set_newline("\r\n");
            $this->email->clear(TRUE);
			$this->email->from($this->config->item('email_vlegal'),$this->config->item('email_vlegal_name'));
			$this->email->to('arefun.safety@gmail.com');			
						
			$message = $this->master_model->get_email_content('vlegalsend');
			$data_email = array ('invoice'      => "1234455",	 
                                 'no_dokumen'   => 'TEST',
								);
			$this->email->subject("Test Document V-Legal - ". $data_email['no_dokumen'] . " - No. Invoice: " . $data_email['invoice']);
			$message = $this->parser->parse_string($message,$data_email,true);
			$this->email->message($message);
		
			if($this->email->send()){
                echo "Sukses";
            }else{
                $this->email->print_debugger();
            }
    }
    
    
    
    
		
	function find_buyer()
	{
		$arr['buyer_list'] = $this->master_model->get_list_buyer($this->input->get('term'),$this->client_id,true);
		$this->output->set_output(json_encode($arr));
	}
    
    function buyer_detail($idbuyer)
	{
		$arr = $this->master_model->get_buyer_detail($idbuyer,true);
		$this->output->set_output(json_encode($arr));
	}
    
    function find_valuta()
	{
		$arr['valuta_list'] = $this->master_model->get_list_valuta($this->input->get('term'),true);
		$this->output->set_output(json_encode($arr));
	}

    function find_loading()
	{
		$arr['loading_list'] = $this->master_model->get_list_loading($this->input->get('term'),true);
		$this->output->set_output(json_encode($arr));
	}
    
    function find_discharge()
	{
		$arr['discharge_list'] = $this->master_model->get_list_discharge($this->input->get('term'),$this->input->get('idnegara'),true);
		$this->output->set_output(json_encode($arr));
	}
    
    function find_provinsi()
	{
		$arr['provinsi'] = $this->master_model->get_list_provinsi($this->input->get('term'),true);
		$this->output->set_output(json_encode($arr));
	}
    
    function find_negara()
	{
		$arr['negara'] = $this->master_model->get_list_negara($this->input->get('term'),true);
		$this->output->set_output(json_encode($arr));
	}
    
    function find_jenis_kayu()
	{
		$arr['jenis_kayu'] = $this->master_model->get_list_jenis_kayu($this->input->get('term'),true);
		$this->output->set_output(json_encode($arr));
	}
    
    function find_product()
	{
		$arr['list_product'] = $this->master_model->get_list_product($this->input->get('term'),'',true);
		$this->output->set_output(json_encode($arr));
	}
    
    function find_kota($provinsi_id)
	{
		$arr['kota'] = $this->master_model->get_list_kota('',$provinsi_id,true);
		$this->output->set_output(json_encode($arr));
	}
    function find_hscode($product_id)
	{
		$hscode = $this->master_model->get_hscode($product_id);
		$this->output->set_output(json_encode($hscode));
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
?>