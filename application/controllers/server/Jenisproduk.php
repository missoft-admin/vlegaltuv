<?php

class Jenisproduk extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		permission_basic_server($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('jenisproduk_model');
	}
	
	function index()
	{
		//print_r('Sini');exit();
		$data = array();
		$data['template'] = 'jenisproduk/index';	
		$data['query'] = $this->jenisproduk_model->jenisproduk_list($this->uri->segment(4),5);		
		$data['first_title'] = 'Master';		
		$data['second_title'] = 'Jenis Produk';
		$data['searchText'] = '';
		$data['total_list']=$this->jenisproduk_model->count();
		$this->pagination->initialize(paging_admin($data['total_list'],'server/jenisproduk/index',4,5));
		$data['pagination'] =  $this->pagination->create_links();
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Jenisproduk','server/jenisproduk'),
								   array('List','')
						     );
		$data['table_title'] = 'DATA KAYU  " [TOTAL '.number_format($data['total_list']).' KAYU] " ACTIVE';					 
		$data['no'] = $this->uri->segment(4);
		$data = array_merge($data,admin_info());
		$this->parser->parse('server/index',$data);
	}
    
    function search($search = '',$limit = '')
	{
		if($this->input->post('searchText')==''){
			if($this->uri->segment(4) != ''){
				$data = array();
				$data['template'] = 'jenisproduk/index';	
				$data['first_title'] = 'Search';		
                $data['second_title'] = 'Jenisproduk';
				$data['query'] = $this->jenisproduk_model->jenisproduk_search(rawurldecode($search),$limit,5);	
				$data['total_list']=$this->jenisproduk_model->count_search(rawurldecode($search));
				$this->pagination->initialize(paging_admin($data['total_list'],
															"server/jenisproduk/search/$search/",5,5));		
				$data['pagination'] =  $this->pagination->create_links();
				
                $data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
                                       array('Master Jenisproduk','server/jenisproduk'),
                                       array('Search','')
                                 );
				$data['searchText'] = rawurldecode($search);
				$data['table_title'] = 'DATA KAYU " [TOTAL '.$data['total_list'].' KAYU] " HASIL PENCARIAN';
                $data['no'] = $limit;
				$data = array_merge($data,admin_info());
				$this->parser->parse('server/index',$data);
			}else{
				redirect('server/jenisproduk');
			}
		}else{
			redirect('server/jenisproduk/search/'.rawurlencode($this->input->post('searchText')));
		}
	}
	function add()
		
	{
		$data = array(
				'idjenisproduk' => '',
				'jenis_produk' => '',
				'satuan' => '',
				'xstatus' => '1',
				);
		$data['template'] = 'jenisproduk/add_new';		
		$data['satuan_list'] = $this->jenisproduk_model->satuan_list();		
		$data['first_title'] = 'Add';		
		$data['second_title'] = 'Jenis Produk';
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Jenisproduk','server/jenisproduk'),
								   array('Add New','')
						     );
		$data['error'] = '';
		$data = array_merge($data,admin_info());
		$this->parser->parse('server/index',$data);
	}
	
	function update($id)
	{
		if($id != ''){
			$row = $this->jenisproduk_model->detail($id);
			if(isset($row->idjenisproduk)){
				$data = array(
						'idjenisproduk' => $row->idjenisproduk,
						'jenis_produk' => $row->jenis_produk,						
						'satuan' => $row->satuan,						
						'xstatus' => $row->xstatus,
						);	
						//print_r($data);exit();
				$data['template'] = 'jenisproduk/add_new';	
				$data['satuan_list'] = $this->jenisproduk_model->satuan_list();		
				$data['first_title'] = 'Edit';		
                $data['second_title'] = 'Jenis Produk';
				$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Jenisproduk','server/jenisproduk'),
								   array('Edit','')
						     );
				$data['error'] = '';	
				$data = array_merge($data,admin_info());				
				$this->parser->parse('server/index',$data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','Data Tidak Ditemukan.');
				redirect('server/jenisproduk','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','Data Tidak Ditemukan.');
			redirect('jenisproduk');
		}	
	}
	
	function save()
	{
		$this->form_validation->set_rules('jenis_produk', 'Nama Jenisproduk', 'trim|required|min_length[1]');
		
		if ($this->form_validation->run() == TRUE){
			if($this->input->post('idjenisproduk') == '' ) {
				if($this->jenisproduk_model->insert()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','Data Tersimpan.');
					redirect('server/jenisproduk','location');
				}else{
					$this->failed_save($this->input->post('idjenisproduk'),true);
				}
			} else {
				if($this->jenisproduk_model->update()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','Data Tersimpan.');
					redirect('server/jenisproduk','location');
				}else{
					$this->failed_save($this->input->post('idjenisproduk'),true);
				}
			}
		}else{
			$this->failed_save($this->input->post('idjenisproduk'));
		}	
	}
	
	function failed_save($id,$dbact=false)
	{
		$data = $this->input->post();
		
		$data['template'] = 'jenisproduk/add_new';	
				
		$data['error'] = validation_errors();
		if($dbact) $data['error'] .= $this->jenisproduk_model->error_message;
		
		if($id==''){
			$data['first_title'] = 'Add';		
            $data['second_title'] = 'Jenisproduk';	
			$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Jenisproduk','server/jenisproduk'),
								   array('Add New','')
						     );
		}else{
			$data['first_title'] = 'Edit';		
            $data['second_title'] = 'Jenisproduk';	
			$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Jenisproduk','server/jenisproduk'),
								   array('Edit','')
						     );
		}
							 
		$data = array_merge($data,admin_info());
		$this->parser->parse('server/index',$data);
		
	}
	
	function delete(){
		$this->jenisproduk_model->delete($this->uri->segment(4));
			
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','Data Berhasil Dihapus.');
		redirect('server/jenisproduk/','location');
	}
		
	
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
?>