<?php

class Peb extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		permission_basic_server($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('peb_model');
		$this->client_id = $this->session->userdata('client_id');
	}
	
	function index()
	{
		$data = array();
		$data['template'] = 'peb/index';	
		$data['query'] = $this->peb_model->peb_approval_list(0,$this->uri->segment(4),30);
		$data['first_title'] = 'Verifikasi';		
		$data['second_title'] = 'PEB';
		$data['searchText'] = '';
		$data['total_list']=$this->peb_model->count_approval(0);
		$this->pagination->initialize(paging_admin($data['total_list'],'server/peb/index',4,30));
		$data['pagination'] =  $this->pagination->create_links();
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Verifikasi PEB','server/peb'),
								   array('List','')
						     );
		$data['table_title'] = 'VERIFIKASI PEB  " [TOTAL '.number_format($data['total_list']).' DATA] "';					 
		$data['no'] = $this->uri->segment(4);
		$data = array_merge($data,admin_info());
		$this->parser->parse('server/index',$data);
	}
    
    function search($search = '',$limit = '')
	{
		if($this->input->post('searchText')==''){
			if($this->uri->segment(4) != ''){
				$data = array();
				$data['template'] = 'peb/index';	
				$data['first_title'] = 'Search';		
                $data['second_title'] = 'PEB';
				$data['query'] = $this->peb_model->peb_approval_search(0,addslashes(rawurldecode($search)),$limit,5);	
				$data['total_list']=$this->peb_model->count_approval_search(0,addslashes(rawurldecode($search)));
				$this->pagination->initialize(paging_admin($data['total_list'],
															"server/peb/search/$search/",5,5));		
				$data['pagination'] =  $this->pagination->create_links();
				
                $data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
                                       array('Verifikasi PEB','server/peb'),
                                       array('Search','')
                                 );
				$data['searchText'] = rawurldecode($search);
				$data['table_title'] = 'VERIFIKASI PEB " [TOTAL '.$data['total_list'].' DATA] " HASIL PENCARIAN';
                $data['no'] = $limit;
				$data = array_merge($data,admin_info());
				$this->parser->parse('server/index',$data);
			}else{
				redirect('server/peb');
			}
		}else{
			redirect('server/peb/search/'.rawurlencode($this->input->post('searchText')));
		}
	}
    
    function approved()
	{
		$data = array();
		$data['template'] = 'peb/index_approved';	
		$data['query'] = $this->peb_model->peb_approval_list(1,$this->uri->segment(4),30);
		$data['first_title'] = 'Verified';		
		$data['second_title'] = 'PEB';
		$data['searchText'] = '';
		$data['total_list']=$this->peb_model->count_approval(1);
		$this->pagination->initialize(paging_admin($data['total_list'],'server/peb/index',4,30));
		$data['pagination'] =  $this->pagination->create_links();
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Verified PEB','server/peb'),
								   array('List','')
						     );
		$data['table_title'] = 'VERIFIED PEB  " [TOTAL '.number_format($data['total_list']).' DATA] "';					 
		$data['no'] = $this->uri->segment(4);
		$data = array_merge($data,admin_info());
		$this->parser->parse('server/index',$data);
	}
    
    function approved_search($search = '',$limit = '')
	{
		if($this->input->post('searchText')==''){
			if($this->uri->segment(4) != ''){
				$data = array();
				$data['template'] = 'peb/index_approved';	
				$data['first_title'] = 'Search';		
                $data['second_title'] = ' Verified PEB';
				$data['query'] = $this->peb_model->peb_approval_search(1,addslashes(rawurldecode($search)),$limit,5);	
				$data['total_list']=$this->peb_model->count_approval_search(1,addslashes(rawurldecode($search)));
				$this->pagination->initialize(paging_admin($data['total_list'],
															"server/peb/search/$search/",5,5));		
				$data['pagination'] =  $this->pagination->create_links();
				
                $data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
                                       array('Verified PEB','server/peb'),
                                       array('Search','')
                                 );
				$data['searchText'] = rawurldecode($search);
				$data['table_title'] = 'VERIFIED PEB " [TOTAL '.$data['total_list'].' DATA] " HASIL PENCARIAN';
                $data['no'] = $limit;
				$data = array_merge($data,admin_info());
				$this->parser->parse('server/index',$data);
			}else{
				redirect('server/peb');
			}
		}else{
			redirect('server/peb/approved_search/'.rawurlencode($this->input->post('searchText')));
		}
	}
		
	function add()
	{
		$data = array(
				'id'            => '',
				'vid'           => '',
				'no_vlegal'     => '',
				'tgl_vlegal'    => '',
				'nomor_peb'     => '',
				'tgl_peb'       => '',
				'stuffing'      => '',
				'volume'        => '',
				'netto'         => '',
				'jumlah'        => '',
				'nilai'         => '',
				'nilai_usd'     => '',
				);
			
		$data['template'] = 'peb/add_new';		
		$data['first_title'] = 'Add';		
		$data['second_title'] = 'PEB';
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Rekapitulasi PEB','client/peb'),
								   array('Add New','')
						     );
		$data['error'] = '';
		//print_r($data['negara_list']);exit();
		$data = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
	}
	
	function update($id)
	{
		if($id != ''){
			$row = $this->peb_model->detail($id);
			if(isset($row->id)){
				$data = array(
						'id'            => $row->id,
						'vid'           => $row->vid,						
						'no_vlegal'     => $row->no_vlegal,						
						'tgl_vlegal'    => $row->tgl_vlegal,						
						'nomor_peb'     => $row->nomor_peb,						
						'tgl_peb'       => $row->tgl_peb,						
						'stuffing'      => $row->stuffing,						
						'volume'        => format_angka_id($row->volume,3),						
						'netto'         => format_angka_id($row->netto,3),
						'jumlah'        => format_angka_id($row->jumlah,3),
						'nilai'         => format_angka_id($row->nilai,3),
						'nilai_usd'     => format_angka_id($row->nilai_usd,3),
						);	
				$data['template'] = 'peb/add_new';	
				$data['first_title'] = 'Edit';		
                $data['second_title'] = 'PEB';
				$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Rekapitulasi PEB','client/peb'),
								   array('Edit','')
						     );
				$data['error'] = '';	
				$data = array_merge($data,admin_info());				
				$this->parser->parse('client/index',$data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','Data Tidak Ditemukan.');
				redirect('client/peb','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','Data Tidak Ditemukan.');
			redirect('client/peb');
		}	
	}
	
	function save()
	{
		$this->form_validation->set_rules('vid', 'No. V-Legal', 'trim|required');
		$this->form_validation->set_rules('tgl_vlegal', 'Tanggal Terbit V-Legal', 'trim|required');
		$this->form_validation->set_rules('nomor_peb', 'No. PEB', 'trim|required');
		$this->form_validation->set_rules('tgl_peb', 'Tanggal PEB', 'trim|required');
		$this->form_validation->set_rules('stuffing', 'Stuffing', 'trim|required');
		$this->form_validation->set_rules('volume', 'Volume', 'trim|required');
		$this->form_validation->set_rules('netto', 'Netto', 'trim|required');
		$this->form_validation->set_rules('jumlah', 'Jumlah', 'trim|required');
		$this->form_validation->set_rules('nilai', 'Nilai', 'trim|required');
		$this->form_validation->set_rules('nilai_usd', 'Nilai USD', 'trim|required');
		
		if ($this->form_validation->run() == TRUE){
			if($this->input->post('id') == '' ) {
				if($this->peb_model->insert($this->client_id)){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','Data Tersimpan.');
					redirect('client/peb','location');
				}else{
					$this->failed_save($this->input->post('id'),true);
				}
			} else {
				if($this->peb_model->update()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','Data Tersimpan.');
					redirect('client/peb','location');
				}else{
					$this->failed_save($this->input->post('id'),true);
				}
			}
		}else{
			$this->failed_save($this->input->post('id'));
		}	
	}
	
	function failed_save($id,$dbact=false)
	{
		$data = $this->input->post();
		$data['template'] = 'peb/add_new';	
				
		$data['error'] = validation_errors();
		if($dbact) $data['error'] .= $this->peb_model->error_message;
		
		if($id==''){
			$data['first_title'] = 'Add';		
            $data['second_title'] = 'PEB';	
			$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Rekapitulasi PEB','client/peb'),
								   array('Add New','')
						     );
		}else{
			$data['first_title'] = 'Edit';		
            $data['second_title'] = 'PEB';	
			$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Rekapitulasi PEB','client/peb'),
								   array('Edit','')
						     );
		}
							 
		$data = array_merge($data,admin_info());
		$this->parser->parse('client/index',$data);
		
	}
	
	function delete(){
		$this->peb_model->delete($this->uri->segment(4));
			
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','Data Berhasil Dihapus.');
		redirect('client/peb/','location');
	}
    
    function approval($id){
		$this->peb_model->approve($id);
			
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','PEB Berhasil di Approve.');
		redirect('server/peb/','location');
	}

    function cancel($id){
		$this->peb_model->cancel($id);
			
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','PEB Berhasil di Cancel.');
		redirect('server/peb/approved','location');
	}
		
	function carinegara()
	{
		$arr['neg_list'] = $this->peb_model->neg_findajax($this->input->get('term'));
		$this->output->set_output(json_encode($arr));
	}
	function find_client()
	{
		$arr['client'] = $this->master_model->get_list_client($this->input->get('term'),'',true);
		$this->output->set_output(json_encode($arr));
	}
    
    function find_vlegal()
	{
		$results = $this->peb_model->get_list_vlegal($this->input->get('term'),$this->client_id);
		$this->output->set_output(json_encode($results));
	}
    
    function vlegal_detail($vid)
	{
		$results = $this->peb_model->get_vlegal_detail($vid);
		$this->output->set_output(json_encode($results));
	}
    
    function find_stuffing()
	{
		$results = $this->peb_model->get_list_stuffing($this->input->get('term'),$this->client_id);
		$this->output->set_output(json_encode($results));
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
?>