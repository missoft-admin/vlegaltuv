<?php

class User extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
	}
		
	function index()
	{		
		permission_basic_server($this->session);
		//permission_super_admin($this->session);
		$data = array();
        $data['first_title'] = 'User';		
		$data['second_title'] = 'Account';		
		$data['template'] = 'user/index';		
		$data['query'] = $this->user_model->all_user($this->uri->segment(4),5);
		$data['page_title'] = 'User Account List';
		$data['searchText'] = '';
		
		$this->pagination->initialize(paging_admin($this->user_model->count(),'user/index'));
		$data['pagination'] =  $this->pagination->create_links();
		$data['breadcrum'] = array(array("Aplikasi Audit TUV Rheinland",'home'),
								   array('User Account','user'),
								   array('List','')
						     );
		
		$data = array_merge($data,admin_info());
		$this->parser->parse('index',$data);
	}
	
	function search()
	{
		permission_basic_server($this->session);
		//permission_super_admin($this->session);
		if($this->input->post('searchText')==''){
			if($this->uri->segment(3) != ''){
				$data = array();
                $data['first_title'] = 'User';		
                $data['second_title'] = 'Account';		    
				$data['template'] = 'user/index';
				$data['page_title'] = 'User Account Search Results';
				$data['query'] = $this->user_model->user_find(rawurldecode($this->uri->segment(3)),$this->uri->segment(4),5);
				$this->pagination->initialize(paging_admin($this->user_model->count_search(rawurldecode($this->uri->segment(3))),
															'user/search/'.$this->uri->segment(3).'/',4));		
				$data['pagination'] =  $this->pagination->create_links();
				$data['breadcrum'] = array(array("Aplikasi Audit TUV Rheinland",'home'),
								   array('User Account','user'),
								   array('Search','')
						     );
				$data['searchText'] = rawurldecode($this->uri->segment(3));
				$data = array_merge($data,admin_info());
				$this->parser->parse('index',$data);
			}else{
				redirect('user');
			}
		}else{
			redirect('user/search/'.rawurlencode($this->input->post('searchText')));
		}
	}
	
	
	
	/* Login  and  Logout */
	
	function login(){
		permission_server_logged_in($this->session);
		
		$data =  array(
						'username' => '',
						'remember' => FALSE,
						'error' => '',
				);
		$data = array_merge($data,admin_info());
        $this->parser->parse('server/user/login',$data);
	}
	
	function do_login(){
		permission_server_logged_in($this->session);
		
		if($this->user_model->server_login()){			
			redirect('server/home','location');
		}else{
			$data =  array(
						'username' => $this->input->post('username'),
						'error' => 'Username atau Password Salah!',
				);
				
			$data = array_merge($data,admin_info());
			$this->parser->parse('server/user/login',$data);
		}
	}
	
	function logout()
	{
		permission_basic_server($this->session);
		$this->user_model->server_logout();
		redirect('server/user/login','location');
	}
	
	/* END Login  and  Logout */
	
	/* Add User */
	function add()
	{
		permission_basic_admin($this->session);
		permission_super_admin($this->session);
		
		$data = (array)$this->user_model ;
		$data['id'] = '';
		$data['nama'] = '';
		$data['alamat'] = '';
		$data['phone'] = '';
		$data['email'] = '';
		$data['role'] = '';
		$data['username'] = '';
		$data['avatar'] = '';
		$data['active'] = '';
		$data['action'] = 'save';
        $data['first_title'] = 'Add';		
		$data['second_title'] = 'User Account';		
		$data['template'] = 'user/add_new';
		$data['page_title'] = 'Add User';
		$data['breadcrum'] = array(array("Aplikasi Audit TUV Rheinland",'home'),
								   array('User Account','user'),
								   array('Add New','')
						     );
		$data['error'] = '';
		
		$data = array_merge($data,admin_info());
		$this->parser->parse('index',$data);
	}

	function save()
	{
		permission_basic_admin($this->session);
		permission_super_admin($this->session);
		
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('username', 'Username', 'trim|required|alpha_dot|callback_user_check');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('phone', 'No. Telp', 'trim|required');
		$this->form_validation->set_rules('role', 'Role User', 'required');
		$this->form_validation->set_message('user_check','Username sudah digunakan.');
	
		if ($this->form_validation->run() == FALSE)
		{
			$this->save_failed();
		}
		else
		{
			if($this->user_model->insert(true)){
				$this->session->set_flashdata('confirm',TRUE);
				$this->session->set_flashdata('message_flash','Penyimpanan Berhasil.');
				redirect('user/index');
			}else{	
				$this->save_failed(true);
			}
		}
	}
	
	function user_check()
	{
		return $this->user_model->user_check(true);
	}

	private function save_failed($upload=false){
		permission_basic_admin($this->session);
		permission_super_admin($this->session);
		
		$data = $this->input->post() ;
		$data['id'] = '';
		$data['template'] = 'user/add_new';
		$data['action'] = 'save';
		$data['page_title'] = 'Add User Account';
        $data['first_title'] = 'Add';		
		$data['second_title'] = 'User Account';		
		$data['breadcrum'] = array(array("Aplikasi Audit TUV Rheinland",'home'),
								   array('User Account','user'),
								   array('Add New','')
						     );
		$data['error'] = validation_errors() ;
		if($upload) $data['error'] = $data['error']."<br./>".$this->user_model->error_message;
		$data = array_merge($data,admin_info());
		$this->parser->parse('index',$data);
	  }
	
	/* END Add User */
  
	/* Edit User */
  
  
	function edit()
	{	
		permission_basic_admin($this->session);
		permission_super_admin($this->session);
		
		$data = (array) $this->user_model->profile($this->uri->segment(3));
        $data['first_title'] = 'Edit';		
		$data['second_title'] = 'User Account';		
		$data['template'] = 'user/add_new';
		$data['page_title'] = 'Edit User Account';
		$data['action'] = 'update';
		$data['breadcrum'] = array(array("Aplikasi Audit TUV Rheinland",'home'),
								   array('User','user'),
								   array('Add New','')
						     );
		$data['error'] = '';
		
		$data = array_merge($data,admin_info());
		$this->parser->parse('index',$data);
	}
  
	function update()
	{
		permission_basic_admin($this->session);
		permission_super_admin($this->session);
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('username', 'Username', 'trim|required|alpha_dot|callback_user_update_check');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('phone', 'No. Telp', 'trim|required');
		$this->form_validation->set_rules('role', 'Role User', 'required');
		$this->form_validation->set_message('user_update_check','Username sudah digunakan.');		
	
		if ($this->form_validation->run() == FALSE){
			$this->update_fail();
		}else{
			if($this->user_model->update()){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','Penyimpanan Berhasil.');
				redirect('user/index');
			}else{
				$this->update_fail(true);
			}
		}
	}
	
	function user_update_check()
	{
		return $this->user_model->user_update_check();
	}
	
	function update_fail($upload=false)
	{
		permission_basic_admin($this->session);
		permission_super_admin($this->session);
		
		$data = $this->input->post();
		$data['template'] = 'user/add_new';
		$data['page_title'] = 'Edit User';
        $data['first_title'] = 'Edit';		
		$data['second_title'] = 'User Account';	
		$data['action'] = 'update';
		$data['breadcrum'] = array(array("Aplikasi Audit TUV Rheinland",'home'),
								   array('User','user'),
								   array('Add New','')
						     );
		
		$data['error'] = validation_errors() ;
		if($upload) $data['error'] = $data['error']."<br./>".$this->user_model->error_message;
		
		$data = array_merge($data,admin_info());
		$this->parser->parse('index',$data);
	}
	
  
	/* END   Edit User */
	
	
	/* My Account */
	
	function myaccount()
	{	
		permission_basic_admin($this->session);
		
		$data = (array) $this->user_model->profile($this->session->userdata('userid'));
		$data['template'] = 'user/my_account';
		$data['page_title'] = 'My Account';
		$data['breadcrum'] = array(array("Aplikasi Audit TUV Rheinland",'home'),
								   array('My Account','')
						     );
		$data['error'] = '';
		$data = array_merge($data,admin_info());
		$this->parser->parse('index',$data);
	}
	
	function update_account()
	{
		permission_basic_admin($this->session);
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required|xss_clean');
		$this->form_validation->set_rules('phone', 'No. Telp', 'trim|required|xss_clean');
		$this->form_validation->set_message('user_update_check','Username sudah digunakan.');
	
		if ($this->form_validation->run() == FALSE){
			$this->update_account_fail();
		}else{
			if($this->user_model->update(true)){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','Penyimpanan Berhasil.');
				redirect('user/myaccount');
			}else{
				$this->update_account_fail(true);
			}
		}
	}
	
	function update_account_fail($upload=false)
	{
		permission_basic_admin($this->session);
		
		$data = $this->input->post();
		$data['template'] = 'user/my_account';
		$data['page_title'] = 'My Account';
		$data['breadcrum'] = array(array("Aplikasi Audit TUV Rheinland",'home'),
								   array('My Account','')
						     );
		
		$data['error'] = validation_errors() ;
		if($upload) $data['error'] = $data['error']."<br./>".$this->user_model->error_message;
		$data = array_merge($data,admin_info());
		$this->parser->parse('index',$data);
	}
  
	/* END My Account */
	
	function delete()
	{
		permission_basic_admin($this->session);
		permission_super_admin($this->session);
		
		$this->user_model->delete($this->uri->segment(4));
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','User berhasil dihapus.');
		
		redirect('user');
	}
	
	/* Reset Password */
	
	function lost_password(){
		permission_admin_logged_in($this->session);
		$data =  array(
						'email' => '',
						'error' => '',
				);
		$data = array_merge($data,admin_info());		
		$this->load->view('user/forgot',$data);
	}
	
	function reset()
	{
		permission_admin_logged_in($this->session);
		$this->form_validation->set_rules('email', 'Username', 'trim|required|valid_email|callback_user_reset_check');
		if ($this->form_validation->run() == FALSE){
			$data =  array(
						'email' => $this->input->post('email'),
						'error' => 'User Tidak Terdaftar!',
				);
			$data = array_merge($data,admin_info());		
			$this->load->view('user/forgot',$data);
		}else{
			$verification = $this->user_model->request_password();
			$this->load->library('email');
			$this->email->set_newline("\r\n");

			$this->email->from('no-reply@indonesianqualityaward.org', 'IQA Foundation');
			$this->email->to($this->input->post('email'));
			$message = "Dear, IQA Foundation User<br/><br/>
						Berikut adalah kode verifikasi untuk melakukan reset password,<br/><br/>
						Kode verifikasi : ".$verification."<br/><br/><br/>
						Terima kasih,<br/> 
						IQA Foundation";
			
			$this->email->subject("Permintaan Reset Password");
			$this->email->message($message);

			if (!$this->email->send())
			{
				$data =  array(
						'email' => $this->input->post('email'),
						'error' => 'Proses Reset Password Gagal!, silahkan ulangi.',
				);
				$data = array_merge($data,admin_info());		
				$this->load->view('user/forgot',$data);
			}
			else
			{
				$data =  array(
						'email' => '',
						'verification' => '',
						'error' => '',
				);
				$data = array_merge($data,admin_info());		
				$this->load->view('user/verify',$data);
			}
		}
	}
	
	function verify_password()
	{
		permission_admin_logged_in($this->session);
		$this->form_validation->set_rules('email', 'Username', 'trim|required|valid_email|callback_user_reset_check');
		$this->form_validation->set_rules('verification', 'Verification Code', 'trim|required|callback_user_verify_reset');
		if ($this->form_validation->run() == FALSE){
			$data =  array(
						'email' => $this->input->post('email'),	
						'verification' => $this->input->post('verification'),
						'error' => 'Verifikasi Gagal!',
				);
			$data = array_merge($data,admin_info());		
			$this->load->view('user/verify',$data);
		}else{
			$this->user_model->reset_password();
			$this->load->library('email');
			$this->email->set_newline("\r\n");

			$this->email->from('no-reply@indonesianqualityaward.org', 'IQA Foundation');
			$this->email->to($this->input->post('email'));
			$message = "Dear, IQA Foundation User<br/><br/>
						Berikut adalah informasi password baru anda,<br/><br/>
						Email : ".$this->input->post('email')."<br/><br/><br/>
						Password : ".$this->user_model->generate_password."<br/><br/><br/>
						Terima kasih,<br/> 
						IQA Foundation";
			
			$this->email->subject("Informasi Perubahan Password");
			$this->email->message($message);

			if (!$this->email->send())
			{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','Password Berhasil Dirubah, Email Perubahan Password Gagal Dikirim. Segera Ubah Password Anda');
			}
			else
			{
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash',$this->user_model->success_message);
			}
			redirect('home','location');
		}
	}
	
	function user_reset_check()
	{
		return $this->user_model->user_check(false);
	}
	function user_verify_reset()
	{
		return $this->user_model->validate_reset_user();
	}
}

/* End of file user.php */
/* Location: ./application/controllers/user.php */