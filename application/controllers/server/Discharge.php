<?php

class Discharge extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		permission_basic_server($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('discharge_model');
	}
	
	function index()
	{
		$data = array();
		$data['template'] = 'discharge/index';	
		$data['query'] = $this->discharge_model->discharge_list($this->uri->segment(4),5);		
		$data['first_title'] = 'Master';		
		$data['second_title'] = 'Discharge';
		$data['searchText'] = '';
		$data['total_list']=$this->discharge_model->count();
		$this->pagination->initialize(paging_admin($data['total_list'],'server/discharge/index',4,5));
		$data['pagination'] =  $this->pagination->create_links();
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Discharge','server/discharge'),
								   array('List','')
						     );
		$data['table_title'] = 'DATA DISCHARGE  " [TOTAL '.number_format($data['total_list']).' DISCHARGE] " ACTIVE';					 
		$data['no'] = $this->uri->segment(4);
		$data = array_merge($data,admin_info());
		$this->parser->parse('server/index',$data);
	}
    
    function search($search = '',$limit = '')
	{
		
		if($this->input->post('searchText')==''){
			if($this->uri->segment(4) != ''){
				$data = array();
				$data['template'] = 'discharge/index';	
				$data['first_title'] = 'Search';		
                $data['second_title'] = 'Discharge';
				$data['query'] = $this->discharge_model->discharge_search(rawurldecode($search),$limit,5);	
				$data['total_list']=$this->discharge_model->count_search(rawurldecode($search));
				$this->pagination->initialize(paging_admin($data['total_list'],
															"server/discharge/search/$search/",5,5));		
				$data['pagination'] =  $this->pagination->create_links();
				
                $data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
                                       array('Master Discharge','server/discharge'),
                                       array('Search','')
                                 );
				$data['searchText'] = rawurldecode($search);
				$data['table_title'] = 'DATA DISCHARGE " [TOTAL '.number_format($data['total_list']).' DISCHARGE] " HASIL PENCARIAN';
                $data['no'] = $limit;
				$data = array_merge($data,admin_info());
				
				$this->parser->parse('server/index',$data);
			}else{
				
				redirect('server/discharge');
			}
		}else{
			//print_r('Tas');exit();
			redirect('server/discharge/search/'.rawurlencode($this->input->post('searchText')));
		}
	}
		
	function add()
	{
		$data = array(
				'iddischarge' => '',
				'uraian' => '',
				'kode' => '',
				'status' => '1',
				);
		$data['template'] = 'discharge/add_new';		
		$data['first_title'] = 'Add';		
		$data['second_title'] = 'Discharge';
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Discharge','server/discharge'),
								   array('Add New','')
						     );
		$data['error'] = '';
		//print_r($data['negara_list']);exit();
		$data = array_merge($data,admin_info());
		$this->parser->parse('server/index',$data);
	}
	
	function update($id)
	{
		if($id != ''){
			$row = $this->discharge_model->detail($id);
			if(isset($row->iddischarge)){
				$data = array(
						'iddischarge' => $row->iddischarge,
						'uraian' => $row->uraian,						
						'kode' => $row->kode,						
						'status' => $row->status,
						);	
				$data['template'] = 'discharge/add_new';	
				$data['first_title'] = 'Edit';		
                $data['second_title'] = 'Discharge';
				$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Discharge','server/discharge'),
								   array('Edit','')
						     );
				$data['error'] = '';	
				$data = array_merge($data,admin_info());				
				$this->parser->parse('server/index',$data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','Data Tidak Ditemukan.');
				redirect('server/discharge','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','Data Tidak Ditemukan.');
			redirect('server/discharge');
		}	
	}
	
	function save()
	{
		$this->form_validation->set_rules('uraian', 'Nama Discharge', 'trim|required|min_length[1]');
		$this->form_validation->set_rules('kode', 'Kode', 'trim|required|min_length[1]');
		
		if ($this->form_validation->run() == TRUE){
			if($this->input->post('iddischarge') == '' ) {
				if($this->discharge_model->insert()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','Data Tersimpan.');
					redirect('server/discharge','location');
				}else{
					$this->failed_save($this->input->post('iddischarge'),true);
				}
			} else {
				if($this->discharge_model->update()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','Data Tersimpan.');
					redirect('server/discharge','location');
				}else{
					$this->failed_save($this->input->post('iddischarge'),true);
				}
			}
		}else{
			$this->failed_save($this->input->post('iddischarge'));
		}	
	}
	
	function failed_save($id,$dbact=false)
	{
		$data = $this->input->post();
		
		$data['template'] = 'discharge/add_new';	
				
		$data['error'] = validation_errors();
		if($dbact) $data['error'] .= $this->discharge_model->error_message;
		
		if($id==''){
			$data['first_title'] = 'Add';		
            $data['second_title'] = 'Discharge';	
			$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Discharge','server/discharge'),
								   array('Add New','')
						     );
		}else{
			$data['first_title'] = 'Edit';		
            $data['second_title'] = 'Discharge';	
			$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Discharge','server/discharge'),
								   array('Edit','')
						     );
		}
							 
		$data = array_merge($data,admin_info());
		$this->parser->parse('server/index',$data);
		
	}
	
	function delete(){
		$this->discharge_model->delete($this->uri->segment(4));
			
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','Data Berhasil Dihapus.');
		redirect('server/discharge/','location');
	}
		
	function carinegara()
	{
		$arr['neg_list'] = $this->discharge_model->neg_findajax($this->input->get('term'));
		$this->output->set_output(json_encode($arr));
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
?>