<?php

class Text extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		permission_basic_server($this->session);
	}
	
	function index()
	{
		$data = array();
		$data['first_title'] = 'Dashboard';		
		$data['second_title'] = 'Audit TUV';		
		$data['tinymce_path'] = 'http://'.$_SERVER['SERVER_NAME']."/tsadmin14/assets/js/tiny_mce/tiny_mce.js";
		$data['template'] = 'text';		
		$data['breadcrum'] = array(array("Aplikasi Audit TUV Rheinland",'home'),array("Dashboard",''));
	
		$data = array_merge($data,basic_info());
		//	print_r($data );exit();
		$this->parser->parse('index',$data);
		
	}	
	
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */