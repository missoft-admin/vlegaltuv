<?php

class Home extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		permission_basic_server($this->session);
	}
	
	function index()
	{
		$data = array();
		$data['first_title'] = 'Dashboard';		
		$data['second_title'] = 'V-Legal Indonesia';		
		$data['template'] = 'home';		
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'server/home'),array("Dashboard",''));
	
		$data = array_merge($data,admin_info());
		$this->parser->parse('server/index',$data);
		
	}	
	
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */