<?php

class Hutan extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		permission_basic_server($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('hutan_model');
	}
	
	function index()
	{
		$data = array();
		$data['template'] = 'hutan/index';	
		$data['query'] = $this->hutan_model->hutan_list($this->uri->segment(4),5);		
		$data['first_title'] = 'Master';		
		$data['second_title'] = 'Hutan';
		$data['searchText'] = '';
		$data['total_list']=$this->hutan_model->count();
		$this->pagination->initialize(paging_admin($data['total_list'],'server/hutan/index',4));
		$data['pagination'] =  $this->pagination->create_links();
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Hutan','server/hutan'),
								   array('List','')
						     );
		$data['table_title'] = 'DATA HUTAN  " [TOTAL '.number_format($data['total_list']).' HUTAN] " ACTIVE';					 
		$data['no'] = $this->uri->segment(4);
		$data = array_merge($data,admin_info());
		$this->parser->parse('server/index',$data);
	}
    
    function search($search = '',$limit = '')
	{
		if($this->input->post('searchText')==''){
			if($this->uri->segment(4) != ''){
				$data = array();
				$data['template'] = 'hutan/index';	
				$data['first_title'] = 'Search';		
                $data['second_title'] = 'Hutan';
				$data['query'] = $this->hutan_model->hutan_search(rawurldecode($search),$limit,5);	
				$data['total_list']=$this->hutan_model->count_search(rawurldecode($search));
				$this->pagination->initialize(paging_admin($data['total_list'],
															"server/hutan/search/$search/",5,5));		
				$data['pagination'] =  $this->pagination->create_links();
				
                $data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
                                       array('Master Hutan','server/hutan'),
                                       array('Search','')
                                 );
				$data['searchText'] = rawurldecode($search);
				$data['table_title'] = 'DATA HUTAN " [TOTAL '.$data['total_list'].' HUTAN] " HASIL PENCARIAN';
                $data['no'] = $limit;
				$data = array_merge($data,admin_info());
				$this->parser->parse('server/index',$data);
			}else{
				redirect('server/hutan');
			}
		}else{
			redirect('server/hutan/search/'.rawurlencode($this->input->post('searchText')));
		}
	}
		
	function add()
	{
		$data = array(
				'idhutan' => '',
				'hutan' => '',
				'status' => '1',
				);
		$data['template'] = 'hutan/add_new';		
		$data['first_title'] = 'Add';		
		$data['second_title'] = 'Hutan';
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Hutan','server/hutan'),
								   array('Add New','')
						     );
		$data['error'] = '';
		//print_r($data['negara_list']);exit();
		$data = array_merge($data,admin_info());
		$this->parser->parse('server/index',$data);
	}
	
	function update($id)
	{
		if($id != ''){
			$row = $this->hutan_model->detail($id);
			if(isset($row->idhutan)){
				$data = array(
						'idhutan' => $row->idhutan,
						'hutan' => $row->hutan,						
						'status' => $row->status,
						);	
				$data['template'] = 'hutan/add_new';	
				$data['first_title'] = 'Edit';		
                $data['second_title'] = 'Hutan';
				$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Hutan','server/hutan'),
								   array('Edit','')
						     );
				$data['error'] = '';	
				$data = array_merge($data,admin_info());				
				$this->parser->parse('server/index',$data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','Data Tidak Ditemukan.');
				redirect('server/hutan','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','Data Tidak Ditemukan.');
			redirect('server/hutan');
		}	
	}
	
	function save()
	{
		$this->form_validation->set_rules('hutan', 'Nama Hutan', 'trim|required|min_length[1]');
		
		if ($this->form_validation->run() == TRUE){
			if($this->input->post('idhutan') == '' ) {
				if($this->hutan_model->insert()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','Data Tersimpan.');
					redirect('server/hutan','location');
				}else{
					$this->failed_save($this->input->post('idhutan'),true);
				}
			} else {
				if($this->hutan_model->update()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','Data Tersimpan.');
					redirect('server/hutan','location');
				}else{
					$this->failed_save($this->input->post('idhutan'),true);
				}
			}
		}else{
			$this->failed_save($this->input->post('idhutan'));
		}	
	}
	
	function failed_save($id,$dbact=false)
	{
		$data = $this->input->post();
		
		$data['template'] = 'hutan/add_new';	
				
		$data['error'] = validation_errors();
		if($dbact) $data['error'] .= $this->hutan_model->error_message;
		
		if($id==''){
			$data['first_title'] = 'Add';		
            $data['second_title'] = 'Hutan';	
			$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Hutan','server/hutan'),
								   array('Add New','')
						     );
		}else{
			$data['first_title'] = 'Edit';		
            $data['second_title'] = 'Hutan';	
			$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Hutan','server/hutan'),
								   array('Edit','')
						     );
		}
							 
		$data = array_merge($data,admin_info());
		$this->parser->parse('server/index',$data);
		
	}
	
	function delete(){
		$this->hutan_model->delete($this->uri->segment(4));
			
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','Data Berhasil Dihapus.');
		redirect('server/hutan/','location');
	}
		
	function carinegara()
	{
		$arr['neg_list'] = $this->hutan_model->neg_findajax($this->input->get('term'));
		$this->output->set_output(json_encode($arr));
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
?>