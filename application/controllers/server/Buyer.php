<?php

class Buyer extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		permission_basic_server($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('buyer_model');
	}
	
	function index()
	{
		$data = array();
		$data['template'] = 'buyer/index';	
		$data['query'] = $this->buyer_model->buyer_list($this->uri->segment(4),5);		
		$data['first_title'] = 'Master';		
		$data['second_title'] = 'Buyer';
		$data['searchText'] = '';
		$data['total_list']=$this->buyer_model->count();
		$this->pagination->initialize(paging_admin($data['total_list'],'server/buyer/index',4,5));
		$data['pagination'] =  $this->pagination->create_links();
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Buyer','server/buyer'),
								   array('List','')
						     );
		$data['table_title'] = 'DATA BUYER  " [TOTAL '.number_format($data['total_list']).' BUYER] " ACTIVE';					 
		$data['no'] = $this->uri->segment(4);
		$data = array_merge($data,admin_info());
		$this->parser->parse('server/index',$data);
	}
    
    function search($search = '',$limit = '')
	{
		if($this->input->post('searchText')==''){
			if($this->uri->segment(4) != ''){
				$data = array();
				$data['template'] = 'buyer/index';	
				$data['first_title'] = 'Search';		
                $data['second_title'] = 'Buyer';
				$data['query'] = $this->buyer_model->buyer_search(rawurldecode($search),$limit,5);	
				$data['total_list']=$this->buyer_model->count_search(rawurldecode($search));
				$this->pagination->initialize(paging_admin($data['total_list'],
															"server/buyer/search/$search/",5,5));		
				$data['pagination'] =  $this->pagination->create_links();
				
                $data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
                                       array('Master Buyer','server/buyer'),
                                       array('Search','')
                                 );
				$data['searchText'] = rawurldecode($search);
				$data['table_title'] = 'DATA BUYER " [TOTAL '.$data['total_list'].' BUYER] " HASIL PENCARIAN';
                $data['no'] = $limit;
				$data = array_merge($data,admin_info());
				$this->parser->parse('server/index',$data);
			}else{
				redirect('server/buyer');
			}
		}else{
			redirect('server/buyer/search/'.rawurlencode($this->input->post('searchText')));
		}
	}
		
	function add()
	{
		$data = array(
				'idbuyer' => '',
				'client_id' => '',
				'client_nama' => '',
				'idnegara' => 'ID',
				'buyer' => '',
				'alamat' => '',
				'ket' => 'L',
				'status' => '1',
				);
		$data['negara_list'] = $this->buyer_model->negara_list();		
		$data['template'] = 'buyer/add_new';		
		$data['first_title'] = 'Add';		
		$data['second_title'] = 'Buyer';
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Buyer','server/buyer'),
								   array('Add New','')
						     );
		$data['error'] = '';
		//print_r($data['negara_list']);exit();
		$data = array_merge($data,admin_info());
		$this->parser->parse('server/index',$data);
	}
	
	function update($id)
	{
		if($id != ''){
			$row = $this->buyer_model->detail($id);
			if(isset($row->idbuyer)){
				$data = array(
						'idbuyer' => $row->idbuyer,
						'client_id' => $row->client_id,						
						'client_nama' => $row->client_nama,						
						'idnegara' => $row->idnegara,						
						'negara' => $row->negara,						
						'buyer' => $row->buyer,						
						'alamat' => $row->alamat,						
						'ket' => $row->ket,
						'status' => $row->status,
						);	
						//print_r($data);exit();
				$data['negara_list'] = $this->buyer_model->negara_list();	
				$data['template'] = 'buyer/add_new';	
				$data['first_title'] = 'Edit';		
                $data['second_title'] = 'Buyer';
				$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Buyer','server/buyer'),
								   array('Edit','')
						     );
				$data['error'] = '';	
				$data = array_merge($data,admin_info());				
				$this->parser->parse('server/index',$data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','Data Tidak Ditemukan.');
				redirect('server/buyer','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','Data Tidak Ditemukan.');
			redirect('server/buyer');
		}	
	}
	
	function save()
	{
		$this->form_validation->set_rules('idnegara', 'Negara', 'trim|required|min_length[1]');
		$this->form_validation->set_rules('buyer', 'Nama Buyer', 'trim|required|min_length[1]');
		$this->form_validation->set_rules('alamat', 'Alamat Buyer', 'trim|required|min_length[1]');
		
		if ($this->form_validation->run() == TRUE){
			if($this->input->post('idbuyer') == '' ) {
				if($this->buyer_model->insert()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','Data Tersimpan.');
					redirect('server/buyer','location');
				}else{
					$this->failed_save($this->input->post('idbuyer'),true);
				}
			} else {
				if($this->buyer_model->update()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','Data Tersimpan.');
					redirect('server/buyer','location');
				}else{
					$this->failed_save($this->input->post('idbuyer'),true);
				}
			}
		}else{
			$this->failed_save($this->input->post('idbuyer'));
		}	
	}
	
	function failed_save($id,$dbact=false)
	{
		$data = $this->input->post();
		$data['negara_list'] = $this->buyer_model->negara_list();		
		$data['template'] = 'buyer/add_new';	
				
		$data['error'] = validation_errors();
		if($dbact) $data['error'] .= $this->buyer_model->error_message;
		
		if($id==''){
			$data['first_title'] = 'Add';		
            $data['second_title'] = 'Buyer';	
			$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Buyer','server/buyer'),
								   array('Add New','')
						     );
		}else{
			$data['first_title'] = 'Edit';		
            $data['second_title'] = 'Buyer';	
			$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Buyer','server/buyer'),
								   array('Edit','')
						     );
		}
							 
		$data = array_merge($data,admin_info());
		$this->parser->parse('server/index',$data);
		
	}
	
	function delete(){
		$this->buyer_model->delete($this->uri->segment(4));
			
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','Data Berhasil Dihapus.');
		redirect('server/buyer/','location');
	}
		
	function carinegara()
	{
		$arr['neg_list'] = $this->buyer_model->neg_findajax($this->input->get('term'));
		$this->output->set_output(json_encode($arr));
	}
	function find_client()
	{
		$arr['client'] = $this->master_model->get_list_client($this->input->get('term'),'',true);
		$this->output->set_output(json_encode($arr));
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
?>