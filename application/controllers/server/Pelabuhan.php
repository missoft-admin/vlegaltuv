<?php

class Pelabuhan extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		permission_basic_server($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('pelabuhan_model');
	}
	
	function index()
	{
		$data = array();
		$data['template'] = 'pelabuhan/index';	
		$data['query'] = $this->pelabuhan_model->pelabuhan_list($this->uri->segment(4),5);		
		$data['first_title'] = 'Master';		
		$data['second_title'] = 'Pelabuhan';
		$data['searchText'] = '';
		$data['total_list']=$this->pelabuhan_model->count();
		$this->pagination->initialize(paging_admin($data['total_list'],'server/pelabuhan/index',4,5));
		$data['pagination'] =  $this->pagination->create_links();
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Pelabuhan','server/pelabuhan'),
								   array('List','')
						     );
		$data['table_title'] = 'DATA PELABUHAN  " [TOTAL '.number_format($data['total_list']).' PELABUHAN] " ACTIVE';					 
		$data['no'] = $this->uri->segment(4);
		$data = array_merge($data,admin_info());
		$this->parser->parse('server/index',$data);
	}
    
    function search($search = '',$limit = '')
	{
		
		if($this->input->post('searchText')==''){
			if($this->uri->segment(4) != ''){
				$data = array();
				$data['template'] = 'pelabuhan/index';	
				$data['first_title'] = 'Search';		
                $data['second_title'] = 'Pelabuhan';
				$data['query'] = $this->pelabuhan_model->pelabuhan_search(rawurldecode($search),$limit,5);	
				$data['total_list']=$this->pelabuhan_model->count_search(rawurldecode($search));
				$this->pagination->initialize(paging_admin($data['total_list'],
															"server/pelabuhan/search/$search/",5,5));		
				$data['pagination'] =  $this->pagination->create_links();
				
                $data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
                                       array('Master Pelabuhan','server/pelabuhan'),
                                       array('Search','')
                                 );
				$data['searchText'] = rawurldecode($search);
				$data['table_title'] = 'DATA PELABUHAN " [TOTAL '.$data['total_list'].' PELABUHAN] " HASIL PENCARIAN';
                $data['no'] = $limit;
				$data = array_merge($data,admin_info());
				
				$this->parser->parse('server/index',$data);
			}else{
				
				redirect('server/pelabuhan');
			}
		}else{
			//print_r('Tas');exit();
			redirect('server/pelabuhan/search/'.rawurlencode($this->input->post('searchText')));
		}
	}
		
	function add()
	{
		$data = array(
				'idloading' => '',
				'uraian' => '',
				'kode' => '',
				'status' => '1',
				);
		$data['template'] = 'pelabuhan/add_new';		
		$data['first_title'] = 'Add';		
		$data['second_title'] = 'Pelabuhan';
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Pelabuhan','server/pelabuhan'),
								   array('Add New','')
						     );
		$data['error'] = '';
		//print_r($data['negara_list']);exit();
		$data = array_merge($data,admin_info());
		$this->parser->parse('server/index',$data);
	}
	
	function update($id)
	{
		if($id != ''){
			$row = $this->pelabuhan_model->detail($id);
			if(isset($row->idloading)){
				$data = array(
						'idloading' => $row->idloading,
						'uraian' => $row->uraian,						
						'kode' => $row->kode,						
						'status' => $row->status,
						);	
				$data['template'] = 'pelabuhan/add_new';	
				$data['first_title'] = 'Edit';		
                $data['second_title'] = 'Pelabuhan';
				$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Pelabuhan','server/pelabuhan'),
								   array('Edit','')
						     );
				$data['error'] = '';	
				$data = array_merge($data,admin_info());				
				$this->parser->parse('server/index',$data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','Data Tidak Ditemukan.');
				redirect('server/pelabuhan','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','Data Tidak Ditemukan.');
			redirect('server/pelabuhan');
		}	
	}
	
	function save()
	{
		$this->form_validation->set_rules('uraian', 'Nama Pelabuhan', 'trim|required|min_length[1]');
		$this->form_validation->set_rules('kode', 'Kode', 'trim|required|min_length[1]');
		
		if ($this->form_validation->run() == TRUE){
			if($this->input->post('idloading') == '' ) {
				if($this->pelabuhan_model->insert()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','Data Tersimpan.');
					redirect('server/pelabuhan','location');
				}else{
					$this->failed_save($this->input->post('idloading'),true);
				}
			} else {
				if($this->pelabuhan_model->update()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','Data Tersimpan.');
					redirect('server/pelabuhan','location');
				}else{
					$this->failed_save($this->input->post('idloading'),true);
				}
			}
		}else{
			$this->failed_save($this->input->post('idloading'));
		}	
	}
	
	function failed_save($id,$dbact=false)
	{
		$data = $this->input->post();
		
		$data['template'] = 'pelabuhan/add_new';	
				
		$data['error'] = validation_errors();
		if($dbact) $data['error'] .= $this->pelabuhan_model->error_message;
		
		if($id==''){
			$data['first_title'] = 'Add';		
            $data['second_title'] = 'Pelabuhan';	
			$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Pelabuhan','server/pelabuhan'),
								   array('Add New','')
						     );
		}else{
			$data['first_title'] = 'Edit';		
            $data['second_title'] = 'Pelabuhan';	
			$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Pelabuhan','server/pelabuhan'),
								   array('Edit','')
						     );
		}
							 
		$data = array_merge($data,admin_info());
		$this->parser->parse('server/index',$data);
		
	}
	
	function delete(){
		$this->pelabuhan_model->delete($this->uri->segment(4));
			
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','Data Berhasil Dihapus.');
		redirect('server/pelabuhan/','location');
	}
		
	function carinegara()
	{
		$arr['neg_list'] = $this->pelabuhan_model->neg_findajax($this->input->get('term'));
		$this->output->set_output(json_encode($arr));
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
?>