<?php

class Negara extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		permission_basic_server($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('negara_model');
	}
	
	function index()
	{
		$data = array();
		$data['template'] = 'negara/index';	
		$data['query'] = $this->negara_model->negara_list($this->uri->segment(3),5);		
		$data['first_title'] = 'Master';		
		$data['second_title'] = 'Negara';
		$data['total_list']=$this->negara_model->count();
		$data['table_title'] = 'Daftar Negara  "[Total '.$data['total_list'].' Negara]"';
		$data['searchText'] = '';
		
		$this->pagination->initialize(paging_admin($data['total_list'],'server/negara/index',4,5));
		$data['pagination'] =  $this->pagination->create_links();
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Negara','negara'),
								   array('List','')
						     );
		$data['no'] = $this->uri->segment(3);
		$data = array_merge($data,admin_info());
		$this->parser->parse('server/index',$data);
	}
    function notif()
	{
		$data = array();
		$data['template'] = 'notif';	
		
		$data['first_title'] = 'Notification';		
		$data['second_title'] = 'Project';
		
		
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Negara','negara'),
								   array('List','')
						     );
		
		$data = array_merge($data,admin_info());
		$this->parser->parse('server/index',$data);
	}
    function search($search = '',$limit = '')
	{
		if($this->input->post('searchText')==''){
			if($this->uri->segment(3) != ''){
				$data = array();
				$data['template'] = 'negara/index';	
				$data['first_title'] = 'Search';		
                $data['second_title'] = 'Negara';
				$data['query'] = $this->negara_model->negara_search(rawurldecode($search),$limit,5);
				$data['total_list']=$this->negara_model->count_search(rawurldecode($search));		
				$this->pagination->initialize(paging_admin($data['total_list'],
															"server/negara/search/$search/",5,5));		
				$data['pagination'] =  $this->pagination->create_links();
				
                $data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
                                       array('Master Negara','negara'),
                                       array('Search','')
                                 );
				$data['table_title'] = 'Daftar Negara  "[Total '.$data['total_list'].' Negara]"';
				$data['searchText'] = rawurldecode($search);
                $data['no'] = $limit;
				$data = array_merge($data,admin_info());
				$this->parser->parse('server/index',$data);
			}else{
				redirect('server/negara');
			}
		}else{
			redirect('server/negara/search/'.rawurlencode($this->input->post('searchText')));
		}
	}
		
	function add()
	{
		$data = array(
				'idnegara' => '',
				'negara' => '',
				'st_insert' => 1,
				
				);
		$data['template'] = 'negara/add_new';		
		$data['first_title'] = 'Add';		
		$data['second_title'] = 'Negara';
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Negara','negara'),
								   array('Add New','')
						     );
		$data['error'] = '';
		$data = array_merge($data,admin_info());
		$this->parser->parse('server/index',$data);
	}
	
	function update($id)
	{
		if($id != ''){
			$row = $this->negara_model->detail($id);
			if(isset($row->idnegara)){
				$data = array(
						'idnegara' => $row->idnegara,
						'negara' => $row->negara,						
						'st_insert' => 0,
						);		
				$data['template'] = 'negara/add_new';	
				$data['first_title'] = 'Edit';		
                $data['second_title'] = 'Negara';
				$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Negara','negara'),
								   array('Edit','')
						     );
				$data['error'] = '';	
				$data = array_merge($data,admin_info());				
				$this->parser->parse('server/index',$data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','Data Tidak Ditemukan.');
				redirect('server/negara','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','Data Tidak Ditemukan.');
			redirect('server/negara');
		}	
	}
	
	function save(){
		//print_r($this->input->post('st_insert'));exit();
		$this->form_validation->set_rules('idnegara', 'ID Negara', 'trim|required|min_length[1]');
		$this->form_validation->set_rules('negara', 'Negara', 'trim|required|min_length[1]');
		if($this->input->post('st_insert') == 1) {
			$this->form_validation->set_rules('idnegara', 'idnegara', 'callback_cek_idnegara');
		}
		if ($this->form_validation->run() == TRUE){
			if($this->input->post('st_insert') == 1) {
				
					if($this->negara_model->insert()){
						$this->session->set_flashdata('confirm',true);
						$this->session->set_flashdata('message_flash','Data Tersimpan.');
						redirect('server/negara','location');
					}else{
						$this->failed_save($this->input->post('idnegara'),true);
					}
				
			} else {
				if($this->negara_model->update()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','Data Tersimpan.');
					redirect('server/negara','location');
				}else{
					$this->failed_save($this->input->post('idnegara'),true);
				}
			}
		}else{
			$this->failed_save($this->input->post('idnegara'));
		}	
	}
	public function cek_idnegara($str)
	{
		
		if ($this->negara_model->cek_idnegara($this->input->post('idnegara'))>0){
			$this->form_validation->set_message('cek_idnegara', 'ID Negara "'.$str.'" Sudah Ada');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	function failed_save($id,$dbact=false)
	{
		$data = $this->input->post();
		
		$data['template'] = 'negara/add_new';	
				
		$data['error'] = validation_errors();
		if($dbact) $data['error'] .= $this->negara_model->error_message;
		
		if($id==''){
			$data['first_title'] = 'Add';		
            $data['second_title'] = 'Negara';	
			$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Negara','negara'),
								   array('Add New','')
						     );
		}else{
			$data['first_title'] = 'Edit';		
            $data['second_title'] = 'Negara';	
			$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Negara','negara'),
								   array('Edit','')
						     );
		}
							 
		$data = array_merge($data,admin_info());
		$this->parser->parse('server/index',$data);
		
	}
	
	function delete(){
		$this->negara_model->delete($this->uri->segment(4));
			
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','Data Berhasil Dihapus.');
		redirect('server/negara/','location');
	}
		
	function cariprov()
	{
		$arr['prov_list'] = $this->negara_model->prov_findajax($this->input->get('term'));
		$this->output->set_output(json_encode($arr));
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
?>