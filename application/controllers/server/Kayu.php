<?php

class Kayu extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		permission_basic_server($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('kayu_model');
	}
	
	function index()
	{
		//print_r('Sini');exit();
		$data = array();
		$data['template'] = 'kayu/index';	
		$data['query'] = $this->kayu_model->kayu_list($this->uri->segment(4),5);		
		$data['first_title'] = 'Master';		
		$data['second_title'] = 'Jenis Kayu';
		$data['searchText'] = '';
		$data['total_list']=$this->kayu_model->count();
		$this->pagination->initialize(paging_admin($data['total_list'],'server/kayu/index',4,5));
		$data['pagination'] =  $this->pagination->create_links();
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Kayu','server/kayu'),
								   array('List','')
						     );
		$data['table_title'] = 'DATA KAYU  " [TOTAL '.number_format($data['total_list']).' KAYU] " ACTIVE';					 
		$data['no'] = $this->uri->segment(4);
		$data = array_merge($data,admin_info());
		$this->parser->parse('server/index',$data);
	}
    
    function search($search = '',$limit = '')
	{
		if($this->input->post('searchText')==''){
			if($this->uri->segment(4) != ''){
				$data = array();
				$data['template'] = 'kayu/index';	
				$data['first_title'] = 'Search';		
                $data['second_title'] = 'Kayu';
				$data['query'] = $this->kayu_model->kayu_search(rawurldecode($search),$limit,5);	
				$data['total_list']=$this->kayu_model->count_search(rawurldecode($search));
				$this->pagination->initialize(paging_admin($data['total_list'],
															"server/kayu/search/$search/",5,5));		
				$data['pagination'] =  $this->pagination->create_links();
				
                $data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
                                       array('Master Kayu','server/kayu'),
                                       array('Search','')
                                 );
				$data['searchText'] = rawurldecode($search);
				$data['table_title'] = 'DATA KAYU " [TOTAL '.$data['total_list'].' KAYU] " HASIL PENCARIAN';
                $data['no'] = $limit;
				$data = array_merge($data,admin_info());
				$this->parser->parse('server/index',$data);
			}else{
				redirect('kayu');
			}
		}else{
			redirect('server/kayu/search/'.rawurlencode($this->input->post('searchText')));
		}
	}
	function add()
		
	{
		$data = array(
				'idkayu' => '',
				'nama' => '',
				'noser' => '',
				'status' => '1',
				);
		$data['template'] = 'kayu/add_new';		
		$data['first_title'] = 'Add';		
		$data['second_title'] = 'Kayu';
		$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Kayu','server/kayu'),
								   array('Add New','')
						     );
		$data['error'] = '';
		$data = array_merge($data,admin_info());
		$this->parser->parse('server/index',$data);
	}
	
	function update($id)
	{
		if($id != ''){
			$row = $this->kayu_model->detail($id);
			if(isset($row->idkayu)){
				$data = array(
						'idkayu' => $row->idkayu,
						'nama' => $row->nama,						
						'status' => $row->status,
						);	
						//print_r($data);exit();
				$data['template'] = 'kayu/add_new';	
				$data['first_title'] = 'Edit';		
                $data['second_title'] = 'Jenis Kayu';
				$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Kayu','server/kayu'),
								   array('Edit','')
						     );
				$data['error'] = '';	
				$data = array_merge($data,admin_info());				
				$this->parser->parse('server/index',$data);
			}else{
				$this->session->set_flashdata('error',true);
				$this->session->set_flashdata('message_flash','Data Tidak Ditemukan.');
				redirect('server/kayu','location');
			}
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','Data Tidak Ditemukan.');
			redirect('kayu');
		}	
	}
	
	function save()
	{
		$this->form_validation->set_rules('nama', 'Nama Kayu', 'trim|required|min_length[1]');
		
		if ($this->form_validation->run() == TRUE){
			if($this->input->post('idkayu') == '' ) {
				if($this->kayu_model->insert()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','Data Tersimpan.');
					redirect('server/kayu','location');
				}else{
					$this->failed_save($this->input->post('idkayu'),true);
				}
			} else {
				if($this->kayu_model->update()){
					$this->session->set_flashdata('confirm',true);
					$this->session->set_flashdata('message_flash','Data Tersimpan.');
					redirect('server/kayu','location');
				}else{
					$this->failed_save($this->input->post('idkayu'),true);
				}
			}
		}else{
			$this->failed_save($this->input->post('idkayu'));
		}	
	}
	
	function failed_save($id,$dbact=false)
	{
		$data = $this->input->post();
		
		$data['template'] = 'kayu/add_new';	
				
		$data['error'] = validation_errors();
		if($dbact) $data['error'] .= $this->kayu_model->error_message;
		
		if($id==''){
			$data['first_title'] = 'Add';		
            $data['second_title'] = 'Kayu';	
			$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Kayu','server/kayu'),
								   array('Add New','')
						     );
		}else{
			$data['first_title'] = 'Edit';		
            $data['second_title'] = 'Kayu';	
			$data['breadcrum'] = array(array("Aplikasi V-Legal TUV Rheinland",'home'),
								   array('Master Kayu','server/kayu'),
								   array('Edit','')
						     );
		}
							 
		$data = array_merge($data,admin_info());
		$this->parser->parse('server/index',$data);
		
	}
	
	function delete(){
		$this->kayu_model->delete($this->uri->segment(4));
			
		$this->session->set_flashdata('confirm',true);
		$this->session->set_flashdata('message_flash','Data Berhasil Dihapus.');
		redirect('server/kayu/','location');
	}
		
	
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
?>