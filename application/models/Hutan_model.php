<?php
class Hutan_model extends CI_Model {

    var $hutan	= '';    

    function __construct()
    {
        parent::__construct();
    }
	
	function count()
	{
		$this->db->from('mhutan');
		$this->db->where('status','1');
		$query = $this->db->count_all_results();
        return $query;
	}
    
    function count_search($textsearch)
	{
		$this->db->from('mhutan');
        $this->db->or_like('hutan',$textsearch);
        $this->db->or_like('idhutan',$textsearch);
		$query = $this->db->count_all_results();
        return $query;
	}
	function cek_idhutan($idhutan){
		$this->db->where('idhutan',$idhutan);
		$this->db->from('mhutan');
		$row=$this->db->count_all_results();
		//print_r($row);exit();
		return $row;
	}
	function insert()
    {	
		$this->idhutan = $_POST['idhutan']; 
		$this->hutan = $_POST['hutan']; 
		if (isset($_POST['status'])) {
		  $this->status = 1;
		} else {
		  $this->status = 0;
		}
		//$this->status = $_POST['status']; 
		//print_r($this);exit();
		if($this->db->insert('mhutan', $this)){
			return true;
		}else{	
			$this->error_message = "Penyimpanan Gagal";
			return false;
		}
    }

    function update()
    {
		$this->idhutan = $_POST['idhutan']; 
		$this->hutan = $_POST['hutan']; 
		if (isset($_POST['status'])) {
		  $this->status = 1;
		} else {
		  $this->status = 0;
		}
		$this->idhutan = $_POST['idhutan']; 
		
		if($this->db->update('mhutan', $this, array('idhutan' => $_POST['idhutan']))){
			return true;
		}else{
			$this->error_message = "Penyimpanan Gagal";
			return false;
		}
    }
	
	function delete($id)
    {
		$data = array(
               'status' =>0
            );

		$this->db->where('idhutan', $id);
		$this->db->update('mhutan', $data); 
		/* $this->status = '0'; 
		//$this->hutan = $_POST['hutan']; 
		//print_r($id);exit();
		$this->db->update('mhutan', $this, array('idhutan' => $id)); */
    }
	
	function hutan_list($limit,$offset)
    {
		$this->db->select('mhutan.*');
		$this->db->from('mhutan');
		$this->db->where('status','1');
		$this->db->order_by('idhutan','ASC');
		($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
		$query = $this->db->get(); 	
		//print_r($query->result());exit();
		return $query->result();
    }
    
    
    function hutan_search($textsearch,$limit,$offset)
    {
		$this->db->select('mhutan.*');
		$this->db->from('mhutan');
        $this->db->or_like('hutan',$textsearch);
        $this->db->or_like('idhutan',$textsearch);
		$this->db->order_by('idhutan','ASC');
		($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
		$query = $this->db->get(); 	
		return $query->result();
    }
	
	function detail($id)
    {
		//print_r($id);exit();
		$this->db->select('mhutan.*');    
		$this->db->from('mhutan');
		$this->db->where('idhutan',$id);
		$query = $this->db->get();
		//print_r($query->row());exit();
        return $query->row();
    }
	
	
}

?>