<?php
class Supplier_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
	
	function count($client_id='')
	{
		if ($client_id !=''){
			$this->db->where('msupplier.client_id',$client_id);
		}
		$this->db->from('msupplier');
		$this->db->where('msupplier.deleted',0);
		$query = $this->db->count_all_results();
        return $query;
	}
    
    function count_search($textsearch,$client_id)
	{
		if ($client_id !=''){
			$this->db->where('msupplier.client_id',$client_id);
		}
		$this->db->from('msupplier');
		$where = "(alamat LIKE '%$textsearch%' OR supplier LIKE '%$textsearch%' OR idnegara LIKE '%$textsearch%')";
		$this->db->where($where);
        $this->db->where('msupplier.deleted',0);
		$query = $this->db->count_all_results();
        return $query;
	}
	function cek_idsupplier($idsupplier){
		$this->db->where('idsupplier',$idsupplier);
		$this->db->from('msupplier');
		$row=$this->db->count_all_results();
		//print_r($row);exit();
		return $row;
	}
	function insert()
    {	
		$this->client_id    = $_POST['client_id']; 
		$this->idnegara     = $_POST['idnegara']; 
		$this->supplier     = $_POST['supplier']; 
		$this->alamat       = $_POST['alamat']; 
		if (isset($_POST['status'])) {
		  $this->status = 1;
		} else {
		  $this->status = 0;
		}
		//print_r($this);exit();
		if($this->db->insert('msupplier', $this)){
			return true;
		}else{	
			$this->error_message = "Penyimpanan Gagal";
			return false;
		}
    }

    function update()
    {
		$this->client_id    = $_POST['client_id']; 
		$this->idnegara     = $_POST['idnegara']; 
		$this->supplier     = $_POST['supplier']; 
		$this->alamat       = $_POST['alamat']; 
		if (isset($_POST['status'])) {
		  $this->status = 1;
		} else {
		  $this->status = 0;
		}
		$this->idsupplier = $_POST['idsupplier']; 
		
		if($this->db->update('msupplier', $this, array('idsupplier' => $_POST['idsupplier']))){
			return true;
		}else{
			$this->error_message = "Penyimpanan Gagal";
			return false;
		}
    }
	
	function delete($id)
    {
		$data = array(
               'deleted' =>1
            );

		$this->db->where('idsupplier', $id);
		$this->db->update('msupplier', $data); 
    }
	
	function supplier_list($limit,$offset,$client_id='')
    {
		if ($client_id !=''){
			$this->db->where('msupplier.client_id',$client_id);
		}
		$this->db->select('msupplier.*,clients.nama,mnegara.negara');
		$this->db->from('msupplier');
		$this->db->join('clients','clients.id=msupplier.client_id','LEFT');
		$this->db->join('mnegara','mnegara.idnegara=msupplier.idnegara','LEFT');
		$this->db->where('msupplier.deleted',0);
		$this->db->order_by('idnegara','ASC');
		($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
		$query = $this->db->get(); 	
		//print_r($query->result());exit();
		return $query->result();
    }
    function negara_list()
    {
		$this->db->select('idnegara,negara');
		$this->db->from('mnegara');
	//	$this->db->where('status','1');
		$this->db->order_by('negara','ASC');		
		$query = $this->db->get(); 	
		return $query->result();
    }
    
    function supplier_search($textsearch,$limit,$offset,$client_id)
    {
		if ($client_id !=''){
			$this->db->where('msupplier.client_id',$client_id);
		}
		$this->db->select('msupplier.*,clients.nama,mnegara.negara');
		$this->db->from('msupplier');
		$this->db->join('clients','clients.id=msupplier.client_id','LEFT');
        $this->db->join('mnegara','mnegara.idnegara=msupplier.idnegara','LEFT');
		$where = "(msupplier.alamat LIKE '%$textsearch%' OR supplier LIKE '%$textsearch%' OR msupplier.idnegara LIKE '%$textsearch%')";
		$this->db->where($where);
        $this->db->where('msupplier.deleted',0);
		$this->db->order_by('idnegara','ASC');
		($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
		$query = $this->db->get(); 	
		//print_r($this->db->last_query());exit();
		return $query->result();
    }
	
	function detail($id)
    {
		//print_r($id);exit();
		$this->db->select('msupplier.*,mnegara.negara,clients.nama as client_nama');    
		$this->db->from('msupplier');
		$this->db->join('mnegara','mnegara.idnegara=msupplier.idnegara');
		$this->db->join('clients','clients.id=msupplier.client_id');
		$this->db->where('idsupplier',$id);
		$query = $this->db->get();
		//print_r($query->row());exit();
        return $query->row();
    }
	
	function neg_findajax($qsearch)
    {
		$this->db->like('negara',$qsearch);
        $this->db->or_like('idnegara',$qsearch);
		$this->db->order_by('idnegara','DESC');
		$query = $this->db->get('mnegara');	
		return $query->result_array();
		
    }
	
}

?>