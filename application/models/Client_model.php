<?php
class Client_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
	
	function count()
	{
		$this->db->from('clients');
		$query = $this->db->count_all_results();
        return $query;
	}
    
    function count_search($textsearch)
	{
		$this->db->from('clients');
        $this->db->like('nama',$textsearch);
        $this->db->or_like('nick',$textsearch);
		$query = $this->db->count_all_results();
        return $query;
	}
    function get_list_jenis_produk($qsearch='',$arr = false)
	{
        if($qsearch != ''){
            $this->db->like('produk',$qsearch);
        }
        
        $this->db->order_by('produk');
		$query = $this->db->get('mproduk');
        
        if($arr){
            return $query->result_array();
        }else{
            return $query->result();
        }
	}
    function count_iui($client_id)
	{
		$this->db->from('client_iui');
		$this->db->where('client_id',$client_id);
		$query = $this->db->count_all_results();
        return $query;
	}
    function count_stuffing($client_id)
	{
		$this->db->from('client_stuffing');
		$this->db->where('client_id',$client_id);
		$query = $this->db->count_all_results();
        return $query;
	}
    
    function count_iui_search($client_id,$textsearch)
	{
		$this->db->select('client_iui.*,clients.nama,jenis.deskripsi as jenis_des,
		primer.deskripsi as primer_des,instansi.deskripsi as instansi_des');
		$this->db->from('client_iui');
		$this->db->join('clients','clients.id = client_iui.client_id');
		$this->db->join('simplefield as jenis','jenis.id = client_iui.iui_jenis_ijin');
		$this->db->join('simplefield as primer','primer.id = client_iui.primer_jenis_ijin');
		$this->db->join('simplefield as instansi','instansi.id = client_iui.instansi_ijin');
      
       $this->db->where('client_iui.client_id',$client_id);
        $this->db->like("(jenis.deskripsi like '$textsearch' OR primer.deskripsi like '$textsearch' 
						OR instansi.deskripsi like '$textsearch' 
						OR client_iui.jenisproduk_ijin like '$textsearch')");
		$query = $this->db->count_all_results();
        return $query;
	}
	function count_stuffing_search($client_id,$textsearch)
	{
		$this->db->select('client_stuffing.*');
		$this->db->from('client_stuffing');
        $this->db->where('client_stuffing.client_id',$client_id);
        $this->db->where("(stuff_data like '%".$textsearch."%')");      
		$query = $this->db->count_all_results();
        return $query;
	}
	
	function insert()
    {	
		$this->nick       			= $_POST['nick']; 
		$this->nama 				= $_POST['nama']; 
		$this->alamat        		= $_POST['alamat']; 
		$this->provinsi_id        	= $_POST['provinsi_id']; 
		$this->kota_id        		= $_POST['kota_id']; 
		/* $this->alamat_pabrik1       = $_POST['alamat_pabrik1']; 
		$this->alamat_pabrik2       = $_POST['alamat_pabrik2']; 
		$this->alamat_pabrik3       = $_POST['alamat_pabrik3']; 
		$this->alamat_pabrik4       = $_POST['alamat_pabrik4']; 
		$this->alamat_pabrik5      = $_POST['alamat_pabrik5']; */  
		$this->telp        			= $_POST['telp']; 
		$this->fax        			= $_POST['fax']; 
		$this->email        		= $_POST['email']; 
		$this->website        		= $_POST['website']; 
		$this->namacp        		= $_POST['namacp']; 
		$this->jabatancp        	= $_POST['jabatancp']; 
		$this->telpcp        		= $_POST['telpcp']; 
		$this->emailcp        		= $_POST['emailcp']; 
		$this->namadv        		= $_POST['namadv']; 
		$this->jabatandv        	= $_POST['jabatandv']; 
		$this->telpdv        		= $_POST['telpdv']; 
		$this->emaildv       	 	= $_POST['emaildv']; 
		$this->aktapendirian        = $_POST['aktapendirian']; 
		$this->aktaperubahan        = $_POST['aktaperubahan']; 
		$this->siup        			= $_POST['siup']; 
		$this->siuptgl        		= $_POST['siuptgl']; 
		$this->siuptglkadaluarsa    = $_POST['siuptglkadaluarsa']; 
		$this->tdp        			= $_POST['tdp']; 
		$this->tdptgl        		= $_POST['tdptgl']; 
		$this->tdptglkadaluarsa     = $_POST['tdptglkadaluarsa']; 
		$this->npwp        			= $_POST['npwp']; 
		$this->npwpkantor        	= $_POST['npwpkantor']; 
		$this->npwpsppkp        	= $_POST['npwpsppkp']; 
		$this->npwpskt        		= $_POST['npwpskt']; 
		$this->etpik        		= $_POST['etpik']; 
		$this->etpiktgl        		= $_POST['etpiktgl']; 
		$this->etpikproduk        	= $_POST['etpikproduk']; 
		$this->etpikjenis        	= $_POST['etpikjenis']; 
		
		$this->hono        			= $_POST['hono']; 
		$this->hotanggal        	= $_POST['hotanggal']; 
		$this->holembaga        	= $_POST['holembaga']; 
		
		$this->lingkunganjenisizin  = $_POST['lingkunganjenisizin']; 
		$this->lingkunganno        	= $_POST['lingkunganno']; 
		$this->lingkungantanggal    = $_POST['lingkungantanggal']; 
		$this->lingkunganlembaga    = $_POST['lingkunganlembaga']; 
		
		$this->status        		= 1; 
		$this->username        		= $_POST['username']; 
		$this->passkey        		= $_POST['passkey']; 
		$this->client_sertifikat    = $_POST['client_sertifikat']; 
		$this->def_stuffing    = $_POST['def_stuffing']; 
		       		
		if($this->db->insert('clients', $this)){
            $client_id = $this->db->insert_id();
            $this->insert_pengaju($client_id);
            $this->insert_pabrik($client_id);
            $this->insert_stuffing($client_id);
            $this->insert_akta($client_id);
            $this->insert_npwp($client_id);
			return true;
		}else{	
			$this->error_message = "Penyimpanan Gagal";
			return false;
		}
    }
    

    function update()
    {
		$this->nick       			= $_POST['nick']; 
		$this->nama 				= $_POST['nama']; 
		$this->alamat        		= $_POST['alamat']; 
		$this->provinsi_id        	= $_POST['provinsi_id']; 
		$this->kota_id        		= $_POST['kota_id']; 
		/* $this->alamat_pabrik1       = $_POST['alamat_pabrik1']; 
		$this->alamat_pabrik2       = $_POST['alamat_pabrik2']; 
		$this->alamat_pabrik3       = $_POST['alamat_pabrik3']; 
		$this->alamat_pabrik4       = $_POST['alamat_pabrik4']; 
		$this->alamat_pabrik5       = $_POST['alamat_pabrik5'];  */
		$this->telp        			= $_POST['telp']; 
		$this->fax        			= $_POST['fax']; 
		$this->email        		= $_POST['email']; 
		$this->website        		= $_POST['website']; 
		$this->namacp        		= $_POST['namacp']; 
		$this->jabatancp        	= $_POST['jabatancp']; 
		$this->telpcp        		= $_POST['telpcp']; 
		$this->emailcp        		= $_POST['emailcp']; 
		$this->namadv        		= $_POST['namadv']; 
		$this->jabatandv        	= $_POST['jabatandv']; 
		$this->telpdv        		= $_POST['telpdv']; 
		$this->emaildv       	 	= $_POST['emaildv']; 
		$this->aktapendirian        = $_POST['aktapendirian']; 
		$this->aktaperubahan        = $_POST['aktaperubahan']; 
		$this->siup        			= $_POST['siup']; 
		$this->siuptgl        		= format_ymd($_POST['siuptgl']); 
		$this->siuptglkadaluarsa    = format_ymd($_POST['siuptglkadaluarsa']); 
		$this->tdp        			= $_POST['tdp']; 
		$this->tdptgl        		= format_ymd($_POST['tdptgl']); 
		$this->tdptglkadaluarsa     = format_ymd($_POST['tdptglkadaluarsa']); 
		$this->npwp        			= $_POST['npwp']; 
		$this->npwpkantor        	= $_POST['npwpkantor']; 
		$this->npwpsppkp        	= $_POST['npwpsppkp']; 
		$this->npwpskt        		= $_POST['npwpskt']; 
		$this->etpik        		= $_POST['etpik']; 
		$this->etpiktgl        		= format_ymd($_POST['etpiktgl']); 
		$this->etpikproduk        	= $_POST['etpikproduk']; 
		$this->etpikjenis        	= $_POST['etpikjenis']; 
		
		$this->hono        			= $_POST['hono']; 
		$this->hotanggal        	= format_ymd($_POST['hotanggal']); 
		$this->holembaga        	= $_POST['holembaga']; 
		
		$this->lingkunganjenisizin  = $_POST['lingkunganjenisizin']; 
		$this->lingkunganno        	= $_POST['lingkunganno']; 
		$this->lingkungantanggal    = format_ymd($_POST['lingkungantanggal']); 
		$this->lingkunganlembaga    = $_POST['lingkunganlembaga']; 
		
		$this->status        		= 1; 
		//$this->username        		= $_POST['username']; 
		//$this->passkey        		= $_POST['passkey']; 
		$this->client_sertifikat    = $_POST['client_sertifikat']; 
		$this->def_stuffing    		= $_POST['def_stuffing']; 
		
		if($this->db->update('clients', $this, array('id' => $_POST['id']))){
            $this->insert_pengaju( $_POST['id']);
            $this->insert_pabrik( $_POST['id']);
            $this->insert_stuffing( $_POST['id']);
            $this->insert_akta( $_POST['id']);
            $this->insert_npwp( $_POST['id']);
			return true;
		}else{
			$this->error_message = "Penyimpanan Gagal";
			return false;
		}
    }
    
    function insert_pengaju($client_id){
        $namadv     = $_POST['namadv'];
        $jabatandv  = $_POST['jabatandv'];
        $telpdv     = $_POST['telpdv'];
        $emaildv    = $_POST['emaildv'];
        $data_dv    = array();
        $counter = 1;
        
        foreach($namadv as $index => $value ){
            $data_dv[] = array('client_id' => $client_id,
                               'index_pengaju' => $counter,
                               'namadv'    => $value,
                               'jabatandv' => $jabatandv[$index],
                               'telpdv'    => $telpdv[$index],
                               'emaildv'   => $emaildv[$index]);
            $counter++;
        }
        $this->db->where('client_id',$client_id);
        $this->db->delete('client_pengaju');
        $this->db->insert_batch('client_pengaju',$data_dv);
    }
    function insert_npwp($client_id){
        $no_npwp     = $_POST['no_npwp'];
        $kantor  = $_POST['kantor'];
        $skt     = $_POST['skt'];
        $skppkp    = $_POST['skppkp'];
        $data_npwp    = array();
        $counter = 1;
        
        foreach($no_npwp as $index => $value ){
            $data_npwp[] = array('client_id' => $client_id,
                               'index_npwp' => $counter,
                               'no_npwp'    => $value,
                               'kantor' => $kantor[$index],
                               'skt'    => $skt[$index],
                               'skppkp'   => $skppkp[$index]);
            $counter++;
        }
        $this->db->where('client_id',$client_id);
        $this->db->delete('client_npwp');
        $this->db->insert_batch('client_npwp',$data_npwp);
    }
    function insert_pabrik($client_id){
        $pabrik_data     = $_POST['pabrik_data'];
        
        $data_pabrik    = array();
        $counter = 1;
        
        foreach($pabrik_data as $index => $value ){
            $data_pabrik[] = array('client_id' => $client_id,
                               'index_pabrik' => $counter,
                               'pabrik_data' => $pabrik_data[$index]);
            $counter++;
        }
        $this->db->where('client_id',$client_id);
        $this->db->delete('client_pabrik');
        $this->db->insert_batch('client_pabrik',$data_pabrik);
    }
    function insert_stuffing($client_id){
        $stuffing_data     = $_POST['stuffing_data'];
        
        $data_stuffing    = array();
        $counter = 1;
        
        foreach($stuffing_data as $index => $value ){
            $data_stuffing[] = array('client_id' => $client_id,
                               'index_stuffing' => $counter,
                               'stuffing_data' => $stuffing_data[$index]);
            $counter++;
        }
        $this->db->where('client_id',$client_id);
        $this->db->delete('client_stuffing');
        $this->db->insert_batch('client_stuffing',$data_stuffing);
    }
    function insert_akta($client_id){
        $akta_data     = $_POST['akta_data'];
        
        $data_akta    = array();
        $counter = 1;
        
        foreach($akta_data as $index => $value ){
            $data_akta[] = array('client_id' => $client_id,
                               'index_akta' => $counter,
                               'akta_data' => $akta_data[$index]);
            $counter++;
        }
        $this->db->where('client_id',$client_id);
        $this->db->delete('client_akta');
        $this->db->insert_batch('client_akta',$data_akta);
    }
    
    function get_pengaju($client_id){
        $this->db->where('client_id',$client_id);
        $this->db->order_by('index_pengaju');
        $query = $this->db->get('client_pengaju');
        return $query->result();
    }
	function get_pabrik($client_id){
        $this->db->where('client_id',$client_id);
        $this->db->order_by('index_pabrik');
        $query = $this->db->get('client_pabrik');
		//print_r($query->result());exit();
        return $query->result();
    }
	function get_stuffing($client_id){
        $this->db->where('client_id',$client_id);
        $this->db->order_by('index_stuffing');
        $query = $this->db->get('client_stuffing');
		//print_r($query->result());exit();
        return $query->result();
    }
	function get_akta($client_id){
        $this->db->where('client_id',$client_id);
        $this->db->order_by('index_akta');
        $query = $this->db->get('client_akta');
		//print_r($query->result());exit();
        return $query->result();
    }
	function get_npwp($client_id){
        $this->db->where('client_id',$client_id);
        $this->db->order_by('index_npwp');
        $query = $this->db->get('client_npwp');
		//print_r($query->result());exit();
        return $query->result();
    }
	function get_akta_last($client_id){
		
        $query = $this->db->query("select *From client_akta where client_id=".$client_id." order by index_akta DESC limit 1 ");
		//print_r($query);exit();
		if ($query->num_rows() > 0)
		{	
		   $row = $query->row_array(); 
		   
		   return $row['akta_data'];

		}else{
			return "";
		}
    }
	
	function delete($id)
    {
		
		$this->status= 0; 	
		//print_r($this);exit();
		if($this->db->update('clients', $this, array('id' => $id))){
			return true;
		}else{
			$this->error_message = "Penghapusan Gagal";
			return false;
		}
		
    }
	function iui_delete($id)
    {		
		//print_r($id);exit();
		$this->db->where('id',$id);
		if ($this->db->delete('client_iui')){
			return true;
		}else{
			$this->error_message = "Penghapusan Gagal";
			return false;
		}
		
    }
	function stuffing_delete($id)
    {		
		//print_r($id);exit();
		$this->db->where('stuff_id',$id);
		if ($this->db->delete('client_stuffing')){
			return true;
		}else{
			$this->error_message = "Penghapusan Gagal";
			return false;
		}
		
    }
	
	function client_list($limit,$offset)
    {
		$this->db->select('clients.*,kota.nama_kota,provinsi.nama_provinsi');
		$this->db->from('clients');
		$this->db->join('kota', 'clients.kota_id = kota.id','left');	
		$this->db->join('provinsi', 'clients.provinsi_id = provinsi.id','left');	
		$this->db->order_by('clients.nama','ASC');
		($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
		$query = $this->db->get(); 	
		return $query->result();
    }
    function provinsi_list()
    {
		$this->db->select('provinsi.*');
		$this->db->from('provinsi');
		$this->db->order_by('nama_provinsi','ASC');
		$query = $this->db->get(); 	
		return $query->result();
    }
    function jenis_izin_list()
    {
		$this->db->select('simplefield.*');
		$this->db->from('simplefield');
		$this->db->where('jenis',4);
		$this->db->where('st_aktif',1);
		$this->db->order_by('id','ASC');
		$query = $this->db->get(); 	
		//print_r($query->result());exit();
		return $query->result();
    }
    function kota_list($provinsi){
		$this->db->where('provinsi_id',$provinsi);		
		$this->db->order_by('nama_kota','ASC');
		$query = $this->db->get('kota');
		$row_list = $query->result();
		$data['kota_list'] = '';
		
		foreach($row_list as $row){					
			$data['kota_list'] .= '<option value="'.$row->provinsi_id.'">'.$row->nama_kota.'</option>';	
		}
		//print_r($data);exit();
		return $data;
	}
	function kota_by_provinsi($provinsi){
		$this->db->where('provinsi_id',$provinsi);		
		$this->db->order_by('nama_kota','ASC');
		$query = $this->db->get('kota');
		$row_list = $query->result();
		return $row_list;
	}
    function client_search($textsearch,$limit,$offset)
    {
		$this->db->select('clients.*,kota.nama_kota,provinsi.nama_provinsi');
		$this->db->from('clients');
		$this->db->join('kota', 'clients.kota_id = kota.id','left');	
		$this->db->join('provinsi', 'kota.provinsi_id = provinsi.id','left');		
        $this->db->like('nama',$textsearch);
        $this->db->or_like('nick',$textsearch);
        $this->db->order_by('clients.nama','ASC');
		($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
		$query = $this->db->get(); 	
		return $query->result();
    }
	
	function detail($id)
    {
		$this->db->select('clients.*,provinsi.nama_provinsi,kota.nama_kota');
		$this->db->from('clients');
		$this->db->join('provinsi','provinsi.id=clients.provinsi_id','LEFT');
		$this->db->join('kota','kota.id=clients.kota_id','LEFT');
		$this->db->where('clients.id',$id);
		$query = $this->db->get();
        return $query->row();
    }
    function nama_client($id)
    {
		$this->db->select('clients.nama');
		$this->db->from('clients');
		$this->db->where('id',$id);
		$query = $this->db->get();
	//	print_r($query->row('nama'));exit();
        return $query->row('nama');
    }
    
    function list_iui($client_id,$limit,$offset)
    {	
		$this->db->select('client_iui.*,clients.nama,jenis.deskripsi as jenis_des,
		primer.deskripsi as primer_des,instansi.deskripsi as instansi_des');
		$this->db->from('client_iui');
		$this->db->join('clients','clients.id = client_iui.client_id');
		$this->db->join('simplefield as jenis','jenis.id = client_iui.iui_jenis_ijin');
		$this->db->join('simplefield as primer','primer.id = client_iui.primer_jenis_ijin');
		$this->db->join('simplefield as instansi','instansi.id = client_iui.instansi_ijin');
       /*  $this->db->select('client_iui.*,audit_trigger.code as trigger_code,external_ref.external_name');
        
        $this->db->join('client_iui as audit_trigger','client_iui.trigger = audit_trigger.id','left');
        $this->db->join('external_ref','client_iui.external_id = external_ref.id','left'); */
        $this->db->where('client_iui.client_id',$client_id);
        ($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
        $query = $this->db->get();
        return $query->result();
    }
    function list_stuffing($client_id,$limit,$offset)
    {	
		$this->db->select('client_stuffing.*');
		$this->db->from('client_stuffing');
       
        $this->db->where('client_stuffing.client_id',$client_id);
        ($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
        $query = $this->db->get();
		//print_r($this->db->last_query());exit();
        return $query->result();
    }
    
    function iui_search($client_id,$textsearch,$limit,$offset)
    {
		$this->db->select('client_iui.*,clients.nama,jenis.deskripsi as jenis_des,
		primer.deskripsi as primer_des,instansi.deskripsi as instansi_des');
		$this->db->from('client_iui');
		$this->db->join('clients','clients.id = client_iui.client_id');
		$this->db->join('simplefield as jenis','jenis.id = client_iui.iui_jenis_ijin');
		$this->db->join('simplefield as primer','primer.id = client_iui.primer_jenis_ijin');
		$this->db->join('simplefield as instansi','instansi.id = client_iui.instansi_ijin');      
        $this->db->where('client_iui.client_id',$client_id);
        $this->db->like("(jenis.deskripsi like '$textsearch' OR primer.deskripsi like '$textsearch' 
						OR instansi.deskripsi like '$textsearch' 
						OR client_iui.jenisproduk_ijin like '$textsearch')");
      
        ($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
        $query = $this->db->get();
		//print_r($this->db->last_query());exit();
        return $query->result();
    }
    function stuffing_search($client_id,$textsearch,$limit,$offset)
    {
		$this->db->select('client_stuffing.*');
		$this->db->from('client_stuffing');
        $this->db->where('client_stuffing.client_id',$client_id);
        $this->db->where("(stuff_data like '%".$textsearch."%')");      
        ($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
        $query = $this->db->get();
		//print_r($this->db->last_query());exit();
        return $query->result();
    }
    
    function detail_iui($id)
    {
        $this->db->select('client_iui.*');
        $this->db->from('client_iui');
        $this->db->where('client_iui.id',$id);
        $query = $this->db->get();
        return $query->row_array();
    }
   
    function detail_stuffing($id)
    {
        $this->db->select('client_stuffing.*');
        $this->db->from('client_stuffing');
        $this->db->where('stuff_id',$id);
        $query = $this->db->get();
		//$print_r($this->db->last_query());exit();
        return $query->row_array();
    }
   
    function simple_field($jenis){
		$this->db->select('simplefield.*');
		$this->db->from('simplefield');
		$this->db->where('jenis',$jenis);
		$this->db->order_by('id','ASC');
		$query=$this->db->get();
		return $query->result();
	}
    
    function insert_iui($client_id)
    {	
		//print_r($this->input->post());exit();
		$this->client_id             = $client_id; 
		$this->iui_jenis_ijin        = $_POST['iui_jenis_ijin']; 
		$this->primer_jenis_ijin     = $_POST['primer_jenis_ijin']; 
		$this->nomor_ijin          	 = $_POST['nomor_ijin']; 
		$this->instansi_ijin         = $_POST['instansi_ijin']; 
		$this->tglterbit_ijin        = $_POST['tglterbit_ijin'].('Y-m-d'); 
		$this->jenisproduk_ijin      = $_POST['jenisproduk_ijin']; 
		
		if($this->db->insert('client_iui', $this)){
			return true;
		}else{	
			$this->error_message = "Penyimpanan Gagal";
			return false;
		}
    }
	/* function insert_stuffing($client_id)
    {	
		$this->client_id             = $client_id; 
		$this->stuff_data        = $_POST['stuff_data']; 
		if($this->db->insert('client_stuffing', $this)){
			return true;
		}else{	
			$this->error_message = "Penyimpanan Gagal";
			return false;
		}
    }
 */
    function update_iui($client_id)
    {
		
        $this->client_id             = $client_id; 
		$this->iui_jenis_ijin        = $_POST['iui_jenis_ijin']; 
		$this->primer_jenis_ijin     = $_POST['primer_jenis_ijin']; 
		$this->nomor_ijin          	 = $_POST['nomor_ijin']; 
		$this->instansi_ijin         = $_POST['instansi_ijin']; 
		$this->tglterbit_ijin        = $_POST['tglterbit_ijin'].('Y-m-d'); 
		$this->jenisproduk_ijin      = $_POST['jenisproduk_ijin']; 
		
		if($this->db->update('client_iui', $this, array('id' => $_POST['id']))){
			return true;
		}else{
			$this->error_message = "Penyimpanan Gagal";
			return false;
		}
    }
	function update_stuffing($stuff_id)
    {
		
		$this->stuff_data        	= $_POST['stuff_data']; 
		
		if($this->db->update('client_stuffing', $this, array('stuff_id' => $_POST['stuff_id']))){
			return true;
		}else{
			$this->error_message = "Penyimpanan Gagal";
			return false;
		}
    }
	
	function prov_findajax($qsearch)
    {
		$this->db->where('nama_provinsi LIKE',$qsearch.'%');
		$this->db->order_by('id','DESC');
		$query = $this->db->get('provinsi');	
		return $query->result_array();
		
    }
	
}

?>