<?php
class Kayu_model extends CI_Model {

    var $nama	= '';    

    function __construct()
    {
        parent::__construct();
    }
	
	function count()
	{
		$this->db->from('mjeniskayu');
		$this->db->where('status','1');
		$query = $this->db->count_all_results();
        return $query;
	}
    
    function count_search($textsearch)
	{
		$this->db->from('mjeniskayu');
        $this->db->or_like('nama',$textsearch);
		$query = $this->db->count_all_results();
        return $query;
	}
	function cek_idkayu($idkayu){
		$this->db->where('idkayu',$idkayu);
		$this->db->from('mjeniskayu');
		$row=$this->db->count_all_results();
		//print_r($row);exit();
		return $row;
	}
	function insert()
    {	
		$this->nama = $_POST['nama']; 
		if (isset($_POST['status'])) {
		  $this->status = 1;
		} else {
		  $this->status = 0;
		}
		//$this->status = $_POST['status']; 
		//print_r($this);exit();
		if($this->db->insert('mjeniskayu', $this)){
			return true;
		}else{	
			$this->error_message = "Penyimpanan Gagal";
			return false;
		}
    }

    function update()
    {
		$this->nama = $_POST['nama']; 
		if (isset($_POST['status'])) {
		  $this->status = 1;
		} else {
		  $this->status = 0;
		}
		$this->idkayu = $_POST['idkayu']; 
		
		if($this->db->update('mjeniskayu', $this, array('idkayu' => $_POST['idkayu']))){
			return true;
		}else{
			$this->error_message = "Penyimpanan Gagal";
			return false;
		}
    }
	
	function delete($id)
    {
		$data = array(
               'status' =>0
            );

		$this->db->where('idkayu', $id);
		$this->db->update('mjeniskayu', $data); 
		/* $this->status = '0'; 
		//$this->kayu = $_POST['nama']; 
		//print_r($id);exit();
		$this->db->update('mjeniskayu', $this, array('idkayu' => $id)); */
    }
	
	function kayu_list($limit,$offset)
    {
		$this->db->select('mjeniskayu.*');
		$this->db->from('mjeniskayu');
		$this->db->where('status','1');
		$this->db->order_by('nama','ASC');
		($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
		$query = $this->db->get(); 	
		//print_r($query->result());exit();
		return $query->result();
    }
    
    function kayu_search($textsearch,$limit,$offset)
    {
		$this->db->select('mjeniskayu.*');
		$this->db->from('mjeniskayu');
        $this->db->or_like('nama',$textsearch);
		$this->db->order_by('nama','ASC');
		($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
		$query = $this->db->get(); 	
		return $query->result();
    }
	
	function detail($id)
    {
		//print_r($id);exit();
		$this->db->select('mjeniskayu.*');    
		$this->db->from('mjeniskayu');
		$this->db->where('idkayu',$id);
		$query = $this->db->get();
		//print_r($query->row());exit();
        return $query->row();
    }
	
		
}

?>