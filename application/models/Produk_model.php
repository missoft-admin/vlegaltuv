<?php
class Produk_model extends CI_Model {

    var $produk	= '';    

    function __construct()
    {
        parent::__construct();
    }
	
	function count($client_id = '')
	{
		$this->db->from('mproduk');
		$this->db->where('deleted','0');
        if($client_id != ''){
            $this->db->where('client_id',$client_id);
        }
		$query = $this->db->count_all_results();
        return $query;
	}
    
    function count_search($textsearch,$client_id = '')
	{
		$this->db->from('mproduk');
        $this->db->where('deleted','0');
        $this->db->where("(kodehs LIKE '%$textsearch%' OR produk LIKE '%$textsearch%')");
        if($client_id != ''){
            $this->db->where('client_id',$client_id);
        }
		$query = $this->db->count_all_results();
        return $query;
	}
	function cek_idproduk($idproduk){
		$this->db->where('idproduk',$idproduk);
		$this->db->from('mproduk');
		$row=$this->db->count_all_results();
		//print_r($row);exit();
		return $row;
	}
	function insert($client_id = '')
    {	
		$this->idproduk     = $_POST['idproduk']; 
		$this->client_id    = ($client_id != '')? $client_id :  $_POST['idproduk']; 
		$this->produk       = $_POST['produk']; 
		$this->kodehs       = $_POST['kodehs']; 
		$this->status       = (isset($_POST['status']))? 1 : 0 ;

		if($this->db->insert('mproduk', $this)){
			return true;
		}else{	
			$this->error_message = "Penyimpanan Gagal";
			return false;
		}
    }

    function update()
    {
		$this->idproduk = $_POST['idproduk']; 
		$this->produk   = $_POST['produk']; 
		$this->kodehs   = $_POST['kodehs']; 
        $this->status   = (isset($_POST['status']))? 1 : 0 ;
		
		if($this->db->update('mproduk', $this, array('idproduk' => $_POST['idproduk']))){
			return true;
		}else{
			$this->error_message = "Penyimpanan Gagal";
			return false;
		}
    }
	
	function delete($id,$client_id)
    {
		$data = array(
               'deleted' =>1
            );

		$this->db->where('idproduk', $id);
		$this->db->where('client_id', $client_id);
		$this->db->update('mproduk', $data);
		
    }
	
	function produk_list($limit,$offset,$client_id = '')
    {
		$this->db->select('mproduk.*');
		$this->db->from('mproduk');
		$this->db->where('deleted','0');
        if($client_id != ''){
            $this->db->where('client_id',$client_id);
        }
		$this->db->order_by('idproduk','ASC');
		($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
		$query = $this->db->get(); 	
		return $query->result();
    }
    
    
    function produk_search($textsearch,$limit,$offset,$client_id = '')
    {
		$this->db->select('mproduk.*');
		$this->db->from('mproduk');
        $this->db->where("(kodehs LIKE '%$textsearch%' OR produk LIKE '%$textsearch%')");
        $this->db->where('deleted','0');
        if($client_id != ''){
            $this->db->where('client_id',$client_id);
        }
		$this->db->order_by('idproduk','ASC');
		($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
		$query = $this->db->get(); 	
		return $query->result();
    }
	
	function detail($id,$client_id='')
    {
		$this->db->select('mproduk.*');    
		$this->db->from('mproduk');
		$this->db->where('idproduk',$id);
        $this->db->where('deleted','0');
        if($client_id != ''){
            $this->db->where('client_id',$client_id);
        }
		$query = $this->db->get();
        // print_r($client_id);exit();
        return $query->row();
    }
    
    function check_hscode($hscode){
		$this->db->where("hscode",$hscode);
		$count = $this->db->count_all_results('hscode');
        if($count > 0){
            return true;
        }else{
            return false;
        }
	}
	
	function neg_findajax($qsearch)
    {
		$this->db->like('negara',$qsearch);
        $this->db->or_like('idproduk',$qsearch);
		$this->db->order_by('idproduk','DESC');
		$query = $this->db->get('mnegara');	
		return $query->result_array();
		
    }
	
}

?>