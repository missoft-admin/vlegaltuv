<?php
class Negara_model extends CI_Model {

    var $negara	= '';    

    function __construct()
    {
        parent::__construct();
    }
	
	function count()
	{
		$this->db->from('mnegara');
		$this->db->where('status','1');
		$query = $this->db->count_all_results();
        return $query;
	}
    
    function count_search($textsearch)
	{
		$this->db->from('mnegara');
        $this->db->like('negara',$textsearch);
        $this->db->or_like('idnegara',$textsearch);
		$query = $this->db->count_all_results();
        return $query;
	}
	function cek_idnegara($idnegara){
		$this->db->where('idnegara',$idnegara);
		$this->db->from('mnegara');
		$row=$this->db->count_all_results();
		//print_r($row);exit();
		return $row;
	}
	function insert()
    {	
		$this->negara = $_POST['negara']; 
		$this->idnegara = $_POST['idnegara']; 
		       		
		if($this->db->insert('mnegara', $this)){
			return true;
		}else{	
			$this->error_message = "Penyimpanan Gagal";
			return false;
		}
    }

    function update()
    {
		$this->negara = $_POST['negara']; 
		$this->idnegara = $_POST['idnegara']; 
		
		if($this->db->update('mnegara', $this, array('idnegara' => $_POST['idnegara']))){
			return true;
		}else{
			$this->error_message = "Penyimpanan Gagal";
			return false;
		}
    }
	
	function delete($id)
    {
		$data = array(
               'status' =>0
            );

		$this->db->where('idnegara', $id);
		$this->db->update('mnegara', $data); 
		/* $this->xstatus = '0'; 
		//$this->negara = $_POST['negara']; 
		//print_r($id);exit();
		$this->db->update('mnegara', $this, array('idnegara' => $id)); */
    }
	
	function negara_list($limit,$offset)
    {
		$this->db->select('mnegara.*');
		$this->db->from('mnegara');
		$this->db->where('status','1');
		$this->db->order_by('negara','ASC');
		($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
		$query = $this->db->get(); 	
		return $query->result();
    }
    
    function negara_search($textsearch,$limit,$offset)
    {
		$this->db->select('mnegara.*');
		$this->db->from('mnegara');
        $this->db->like('negara',$textsearch);
        $this->db->or_like('idnegara',$textsearch);
        $this->db->where('status','1');
		$this->db->order_by('negara','ASC');
		($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
		$query = $this->db->get(); 	
		return $query->result();
    }
	
	function detail($id)
    {
		//print_r($id);exit();
		$this->db->select('mnegara.*');    
		$this->db->from('mnegara');
		$this->db->where('idnegara',$id);
		$query = $this->db->get();
		//print_r($query->row());exit();
        return $query->row();
    }
	
	function prov_findajax($qsearch)
    {
		$this->db->where('nama_provinsi LIKE',$qsearch.'%');
		$this->db->order_by('id','DESC');
		$query = $this->db->get('provinsi');	
		return $query->result_array();
		
    }
	
}

?>