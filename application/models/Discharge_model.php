<?php
class Discharge_model extends CI_Model {

    var $uraian	= '';    

    function __construct()
    {
        parent::__construct();
    }
	
	function count()
	{
		$this->db->from('mdischarge');
		$this->db->where('status','1');
		$query = $this->db->count_all_results();
        return $query;
	}
    
    function count_search($textsearch)
	{
		$this->db->from('mdischarge');
        $this->db->like('kode',$textsearch);
        $this->db->or_like('uraian',$textsearch);
        $this->db->or_like('iddischarge',$textsearch);
		$query = $this->db->count_all_results();
        return $query;
	}
	function cek_iddischarge($iddischarge){
		$this->db->where('iddischarge',$iddischarge);
		$this->db->from('mdischarge');
		$row=$this->db->count_all_results();
		//print_r($row);exit();
		return $row;
	}
	function insert()
    {	
		$this->iddischarge = $_POST['iddischarge']; 
		$this->uraian = $_POST['uraian']; 
		$this->kode = $_POST['kode']; 
		if (isset($_POST['status'])) {
		  $this->status = 1;
		} else {
		  $this->status = 0;
		}
		//$this->status = $_POST['status']; 
		//print_r($this);exit();
		if($this->db->insert('mdischarge', $this)){
			return true;
		}else{	
			$this->error_message = "Penyimpanan Gagal";
			return false;
		}
    }

    function update()
    {
		$this->iddischarge = $_POST['iddischarge']; 
		$this->uraian = $_POST['uraian']; 
		$this->kode = $_POST['kode']; 
		if (isset($_POST['status'])) {
		  $this->status = 1;
		} else {
		  $this->status = 0;
		}
		$this->iddischarge = $_POST['iddischarge']; 
		
		if($this->db->update('mdischarge', $this, array('iddischarge' => $_POST['iddischarge']))){
			return true;
		}else{
			$this->error_message = "Penyimpanan Gagal";
			return false;
		}
    }
	
	function delete($id)
    {
		$data = array(
               'status' =>0
            );

		$this->db->where('iddischarge', $id);
		$this->db->update('mdischarge', $data); 
		/* $this->status = '0'; 
		//$this->discharge = $_POST['uraian']; 
		//print_r($id);exit();
		$this->db->update('mdischarge', $this, array('iddischarge' => $id)); */
    }
	
	function discharge_list($limit,$offset)
    {
		$this->db->select('mdischarge.*');
		$this->db->from('mdischarge');
		$this->db->where('status','1');
		$this->db->order_by('iddischarge','ASC');
		($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
		$query = $this->db->get(); 	
		//print_r($query->result());exit();
		return $query->result();
    }
    
    
    function discharge_search($textsearch,$limit,$offset)
    {
		$this->db->select('mdischarge.*');
		$this->db->from('mdischarge');
        $this->db->like('kode',$textsearch);
        $this->db->or_like('uraian',$textsearch);
        $this->db->or_like('iddischarge',$textsearch);
		$this->db->order_by('iddischarge','ASC');
		($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
		$query = $this->db->get(); 	
		return $query->result();
    }
	
	function detail($id)
    {
		//print_r($id);exit();
		$this->db->select('mdischarge.*');    
		$this->db->from('mdischarge');
		$this->db->where('iddischarge',$id);
		$query = $this->db->get();
		//print_r($query->row());exit();
        return $query->row();
    }
	
	function neg_findajax($qsearch)
    {
		$this->db->like('negara',$qsearch);
        $this->db->or_like('iddischarge',$qsearch);
		$this->db->order_by('iddischarge','DESC');
		$query = $this->db->get('mnegara');	
		return $query->result_array();
		
    }
	
}

?>