<?php
class Sortimen_model extends CI_Model {


    function __construct()
    {
        parent::__construct();
    }
	function count($client_id = '')
	{
		$this->db->from('msortimen');
		$this->db->where('deleted','0');
        if($client_id != ''){
            $this->db->where('client_id',$client_id);
        }
		$query = $this->db->count_all_results();
        return $query;
	}
    function count_search($textsearch,$client_id = '')
	{
		$this->db->from('msortimen');
        if($client_id != ''){
            $this->db->where('client_id',$client_id);
        }
        $this->db->like('sortimen',$textsearch);
		$query = $this->db->count_all_results();
        return $query;
	}
	function cek_idsortimen($idsortimen){
		$this->db->where('idsortimen',$idsortimen);
		$this->db->from('msortimen');
		$row=$this->db->count_all_results();
		return $row;
	}
	function insert($client_id = '')
    {	
        $this->idsortimen   = $_POST['idsortimen']; 
		$this->client_id    = ($client_id != '')? $client_id :  $_POST['idproduk']; 
		$this->sortimen     = $_POST['sortimen']; 
		$this->status   = (isset($_POST['status']))? 1 : 0 ;
		
		if($this->db->insert('msortimen', $this)){
			return true;
		}else{	
			$this->error_message = "Penyimpanan Gagal";
			return false;
		}
    }
    function update()
    {
		$this->idsortimen   = $_POST['idsortimen']; 
		$this->sortimen     = $_POST['sortimen']; 
        $this->status       = (isset($_POST['status']))? 1 : 0 ;
		
		if($this->db->update('msortimen', $this, array('idsortimen' => $_POST['idsortimen']))){
			return true;
		}else{
			$this->error_message = "Penyimpanan Gagal";
			return false;
		}
    }
	
	function delete($id,$client_id = '')
    {
		$data = array('deleted' => 1);
		$this->db->where('idsortimen', $id);
		if($client_id != ''){
            $this->db->where('client_id',$client_id);
        }
		$this->db->update('msortimen', $data); 
    }
	
	function sortimen_list($limit,$offset,$client_id = '')
    {
		$this->db->select('msortimen.*');
		$this->db->from('msortimen');
		$this->db->where('deleted','0');
        if($client_id != ''){
            $this->db->where('client_id',$client_id);
        }
		$this->db->order_by('idsortimen','ASC');
		($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
		$query = $this->db->get(); 	
		return $query->result();
    }
    
    
    function sortimen_search($textsearch,$limit,$offset,$client_id = '')
    {
		$this->db->select('msortimen.*');
		$this->db->from('msortimen');
        if($client_id != ''){
            $this->db->where('client_id',$client_id);
        }
        $this->db->like('sortimen',$textsearch);
		$this->db->order_by('idsortimen','ASC');
		($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
		$query = $this->db->get(); 	
		return $query->result();
    }
	
	function detail($id,$client_id = '')
    {
		$this->db->select('msortimen.*');    
		$this->db->from('msortimen');
		$this->db->where('idsortimen',$id);
         if($client_id != ''){
            $this->db->where('client_id',$client_id);
        }
		$query = $this->db->get();
        return $query->row();
    }
	
	
}

?>