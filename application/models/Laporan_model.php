<?php
class Laporan_model extends CI_Model {


    function __construct()
    {
        parent::__construct();
    }
	function count_lmko($client_id = '')
	{
		$this->db->select('mjeniskayu.*,SUM(mutasi_bahan_baku.stok_awal_volume) persediaan_awal,SUM(mutasi_bahan_baku.penerimaan_volume) perolehan,
                           SUM(mutasi_bahan_baku.produksi_volume) produksi,SUM(mutasi_bahan_baku.penjualan_volume) penjualan,');
		$this->db->from('mutasi_bahan_baku');
		$this->db->join('mjeniskayu','mjeniskayu.idkayu = mutasi_bahan_baku.idkayu');
        
        if($client_id != ''){
            $this->db->where('mutasi_bahan_baku.client_id',$client_id);
        }
		$this->db->group_by('mjeniskayu.idkayu');
		$query = $this->db->count_all_results();
        return $query;
	}
    function count_lmko_search($textsearch,$client_id = '')
	{
		$this->db->select('mjeniskayu.*,SUM(mutasi_bahan_baku.stok_awal_volume) persediaan_awal,SUM(mutasi_bahan_baku.penerimaan_volume) perolehan,
                           SUM(mutasi_bahan_baku.produksi_volume) produksi,SUM(mutasi_bahan_baku.penjualan_volume) penjualan,');
		$this->db->from('mutasi_bahan_baku');
		$this->db->join('mjeniskayu','mjeniskayu.idkayu = mutasi_bahan_baku.idkayu');
        
        if($client_id != ''){
            $this->db->where('mutasi_bahan_baku.client_id',$client_id);
        }
		$this->db->group_by('mjeniskayu.idkayu');
        $this->db->where("(mjeniskayu.nama LIKE '%$textsearch%')");
		$query = $this->db->count_all_results();
        return $query;
	}	
	function lmko_list($limit,$offset,$client_id = '')
    {
		$this->db->select('mjeniskayu.*,SUM(mutasi_bahan_baku.stok_awal_volume) persediaan_awal,SUM(mutasi_bahan_baku.penerimaan_volume) perolehan,
                           SUM(mutasi_bahan_baku.produksi_volume) produksi,SUM(mutasi_bahan_baku.penjualan_volume) penjualan,');
		$this->db->from('mutasi_bahan_baku');
		$this->db->join('mjeniskayu','mjeniskayu.idkayu = mutasi_bahan_baku.idkayu');
        
        if($client_id != ''){
            $this->db->where('mutasi_bahan_baku.client_id',$client_id);
        }
		$this->db->group_by('mjeniskayu.idkayu');
		$this->db->order_by('mjeniskayu.nama','ASC');
		($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
		$query = $this->db->get(); 	
		return $query->result();
    }
    
    function lmko_list_fg($limit,$offset,$client_id = '')
    {
		$this->db->select("mproduk.*,SUM(mutasi_product.stok_awal_volume) persediaan_awal,SUM(mutasi_product.penerimaan_volume) perolehan,
                           '-' produksi,SUM(mutasi_product.penjualan_volume) penjualan");
		$this->db->from('mutasi_product');
		$this->db->join('mproduk','mproduk.idproduk = mutasi_product.idproduk');
        
        if($client_id != ''){
            $this->db->where('mutasi_product.client_id',$client_id);
        }
		$this->db->group_by('mutasi_product.idproduk');
		$this->db->order_by('mproduk.produk','ASC');
		($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
		$query = $this->db->get(); 	
		return $query->result();
    }
    
    
    function lmko_search($textsearch,$limit,$offset,$client_id = '')
    {
		$this->db->select('mjeniskayu.*,SUM(mutasi_bahan_baku.stok_awal_volume) persediaan_awal,SUM(mutasi_bahan_baku.penerimaan_volume) perolehan,
                           SUM(mutasi_bahan_baku.produksi_volume) produksi,SUM(mutasi_bahan_baku.penjualan_volume) penjualan,');
		$this->db->from('mutasi_bahan_baku');
		$this->db->join('mjeniskayu','mjeniskayu.idkayu = mutasi_bahan_baku.idkayu');
        
        if($client_id != ''){
            $this->db->where('mutasi_bahan_baku.client_id',$client_id);
        }
		$this->db->group_by('mjeniskayu.idkayu');
        $this->db->where("(mjeniskayu.nama LIKE '%$textsearch%')");
		$this->db->order_by('mjeniskayu.nama','ASC');
		($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
		$query = $this->db->get(); 	
		return $query->result();
    }
	
	
}

?>