<?php
class Pengeluaran_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
	
	function count($type = '',$client_id = '',$searchText = '')
	{
		$this->db->from('view_pengeluaran');
        if($client_id != ''){
            $this->db->where('client_id',$client_id);
        }
        if($type != ''){
            $this->db->where('type_penerimaan',$type);
        }
        if($searchText != ''){
            $this->db->like('type_pengeluaran',$searchText);
            $this->db->or_like('nama_jenis_pengeluaran',$searchText);
            $this->db->or_like('nama_sortimen',$searchText);
            $this->db->or_like('nama_kayu',$searchText);
        }
		$query = $this->db->count_all_results();
        return $query;
	}
    
    
    function list_index($limit,$offset,$type = '',$client_id = '',$searchText = '')
    {
        if($client_id != ''){
            $this->db->where('client_id',$client_id);
        }
        if($type != ''){
            $this->db->where('type_pengeluaran',$type);
        }
        if($searchText != ''){
            $this->db->where("(type_pengeluaran LIKE '%$searchText%' OR nama_jenis_pengeluaran LIKE '%$searchText%' OR nama_sortimen LIKE '%$searchText%' OR nama_kayu LIKE '%$searchText%')",null,false);
        }
        $this->db->where('deleted',0);
        ($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
        $this->db->order_by('periode_awal','DESC');
        $query = $this->db->get('view_pengeluaran');
        return $query->result();
    }
    
    
	function cek_idbuyer($idbuyer){
		$this->db->where('idbuyer',$idbuyer);
		$this->db->from('mbuyer');
		$row=$this->db->count_all_results();
		//print_r($row);exit();
		return $row;
	}
	
    function save($client_id)
	{
		$this->periode_awal			=	to_mysql_date($_POST['periode_awal']);
		//$this->periode_akhir		=	to_mysql_date($_POST['periode_akhir']);
		$this->client_id	        =	$client_id;
		$this->jenis_pengeluaran	=	$_POST['jenis_pengeluaran'];
        $this->type_pengeluaran	    =	$_POST['type_pengeluaran'];
        $this->iduser		        =	$this->session->userdata('userid');
		
								
		$this->db->insert('pengeluaran_head',$this);
		$idpengeluaran = $this->db->insert_id();
		$detail = json_decode($_POST['detail_value']);
		foreach($detail as $row){
			$data = array();
			$data['idpengeluaran'] 	= $idpengeluaran;
			$data['urutan']         = $row[0];
			$data['idsortimen']     = $row[1];
			$data['idkayu']         = $row[3];
			$data['jumlah'] 		= to_mysql_number($row[5]);
			$data['satuan_jumlah'] 	= $row[7];
			$data['volume'] 		= to_mysql_number($row[9],".");
			$data['satuan_volume']  = $row[11];
			$data['berat'] 	        = to_mysql_number($row[13],".");
			$data['satuan_berat'] 	= $row[15];
			$this->db->insert('pengeluaran_detail',$data);
		}
		
		return $idpengeluaran;
	}

    function update($client_id)
	{
		$this->periode_awal			=	to_mysql_date($_POST['periode_awal']);
		$this->jenis_pengeluaran	=	$_POST['jenis_pengeluaran'];
        $this->type_pengeluaran	    =	$_POST['type_pengeluaran'];
        $this->iduser		        =	$this->session->userdata('userid');
        $idpengeluaran              =   $_POST['idpengeluaran'];
		
								
		$this->db->where('idpengeluaran',$idpengeluaran);
		$this->db->update('pengeluaran_head',$this);
        
        $this->db->where('idpengeluaran',$idpengeluaran);
        $this->db->delete('pengeluaran_detail');
		
		$detail = json_decode($_POST['detail_value']);
		foreach($detail as $row){
			$data = array();
			$data['idpengeluaran'] 	= $idpengeluaran;
			$data['urutan']         = $row[0];
			$data['idsortimen']     = $row[1];
			$data['idkayu']         = $row[3];
			$data['jumlah'] 		= $row[5];
			$data['satuan_jumlah'] 	= $row[7];
			$data['volume'] 		= $row[9];
			$data['satuan_volume']  = $row[11];
			$data['berat'] 	        = $row[13];
			$data['satuan_berat'] 	= $row[15];
			$this->db->insert('pengeluaran_detail',$data);
		}
		
		return true;
	}
	
	function delete($idpengeluaran,$client_id = '')
    {
		$data = array('deleted' => 1);

		$this->db->where('idpengeluaran', $idpengeluaran);
		if($client_id != ''){
            $this->db->where('client_id',$client_id);
        }
		$this->db->update('pengeluaran_head', $data); 
    }
    
    function finalize($idpengeluaran,$client_id = '')
    {
		$data = array('final' => 1, 'finalize_date' => date('Y-m-d H:i:s'), 'finalize_by' => $this->session->userdata('userid'));

		$this->db->where('idpengeluaran', $idpengeluaran);
		if($client_id != ''){
            $this->db->where('client_id',$client_id);
        }
		$this->db->update('pengeluaran_head', $data); 
    }
	
	function buyer_list($limit,$offset)
    {
		$this->db->select('mbuyer.*');
		$this->db->from('mbuyer');
		$this->db->where('xstatus','1');
		$this->db->order_by('idnegara','ASC');
		($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
		$query = $this->db->get(); 	
		//print_r($query->result());exit();
		return $query->result();
    }
    function negara_list()
    {
		$this->db->select('idnegara,negara');
		$this->db->from('mnegara');
	//	$this->db->where('xstatus','1');
		$this->db->order_by('negara','ASC');		
		$query = $this->db->get(); 	
		return $query->result();
    }
    
    function buyer_search($textsearch,$limit,$offset)
    {
		$this->db->select('mbuyer.*');
		$this->db->from('mbuyer');
        $this->db->like('alamat',$textsearch);
        $this->db->or_like('buyer',$textsearch);
        $this->db->or_like('idnegara',$textsearch);
		$this->db->order_by('idnegara','ASC');
		($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
		$query = $this->db->get(); 	
		return $query->result();
    }
	
	function detail($idpengeluaran,$type = '',$client_id = '')
    {
        if($client_id != ''){
            $this->db->where('client_id',$client_id);
        }
        if($type != ''){
            $this->db->where('type_pengeluaran',$type);
        }
        $this->db->where('idpengeluaran',$idpengeluaran);
        $query = $this->db->get('view_pengeluaran');
        return $query->row_array();
    }
    
    
    function detail_list($idpengeluaran)
    {
        $this->db->select('pengeluaran_detail.*,msortimen.sortimen,mjeniskayu.nama as nama_kayu,
                           sat_jumlah.satuan_kode sat_jumlah_kode,sat_volume.satuan_kode sat_volume_kode,sat_berat.satuan_kode sat_berat_kode ');
        $this->db->from('pengeluaran_detail');
        $this->db->join('msortimen','msortimen.idsortimen = pengeluaran_detail.idsortimen');
        $this->db->join('mjeniskayu','mjeniskayu.idkayu = pengeluaran_detail.idkayu','left');
        $this->db->join('msatuan sat_jumlah',"pengeluaran_detail.satuan_jumlah  = sat_jumlah.satuan_id AND sat_jumlah.satuan_jenis = 'jumlah'",'left');
        $this->db->join('msatuan sat_volume',"pengeluaran_detail.satuan_volume  = sat_volume.satuan_id AND sat_volume.satuan_jenis = 'volume'",'left');
        $this->db->join('msatuan sat_berat',"pengeluaran_detail.satuan_berat  = sat_berat.satuan_id AND sat_berat.satuan_jenis = 'berat'",'left');
        $this->db->where('idpengeluaran',$idpengeluaran);
        $this->db->order_by('urutan','ASC');
        $query = $this->db->get();
        return $query->result();
    }
	
}

?>