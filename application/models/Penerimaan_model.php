<?php
class Penerimaan_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
	
	function count($searchText = '',$type = '',$client_id = '')
	{
		$this->db->from('view_penerimaan');
        if($client_id != ''){
            $this->db->where('client_id',$client_id);
        }
        if($type != ''){
            $this->db->where('type_penerimaan',$type);
        }
        if($searchText != ''){
            $this->db->where("(nama_supplier LIKE '%$searchText%' OR nama_kayu LIKE '%$searchText%' OR nama_sortimen LIKE '%$searchText%')",null,false);
        }
        $this->db->where('deleted',0);
		$query = $this->db->count_all_results();
        return $query;
	}
        
    function list_index($limit,$offset,$searchText = '',$type = '',$client_id = '')
    {
        if($client_id != ''){
            $this->db->where('client_id',$client_id);
        }
        if($type != ''){
            $this->db->where('type_penerimaan',$type);
        }
        if($searchText != ''){
            $this->db->where("(nama_supplier LIKE '%$searchText%' OR nama_kayu LIKE '%$searchText%' OR nama_sortimen LIKE '%$searchText%')",null,false);
        }
        $this->db->where('deleted',0);
        ($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
        $this->db->order_by('periode_awal','DESC');
        $query = $this->db->get('view_penerimaan');
        return $query->result();
    }
    
    
	function cek_idbuyer($idbuyer){
		$this->db->where('idbuyer',$idbuyer);
		$this->db->from('mbuyer');
		$row=$this->db->count_all_results();
		return $row;
	}
	
    function save()
	{
        $this->periode_awal			    =	to_mysql_date($_POST['periode_awal']);
		$this->client_id			    =	$this->session->userdata('client_id');
		$this->noinvoice			    =	$_POST['noinvoice'];
		$this->idsupplier			    =	$_POST['idsupplier'];
		
        $this->type_penerimaan	        =	$_POST['type_penerimaan'];
        
        
        if(strtoupper($this->type_penerimaan) == 'LOKAL'){
            $this->idnegara	            =	$_POST['idkota'];
            $this->idhutan			        =	$_POST['idhutan'];
            $this->idsertifikasi		    =	$_POST['idsertifikasi'];
            $this->no_sertifikat		    =	$_POST['no_sertifikat'];
            $this->tgl_berlaku_sertifikat	=	to_mysql_date($_POST['tgl_berlaku_sertifikat']);
            $this->iddokumen	            =	$_POST['iddokumen'];
            $this->tgl_berlaku_dokumen	    =	to_mysql_date($_POST['tgl_berlaku_dokumen']);    
        }else{
            $this->idnegara	            =	$_POST['idnegara'];
            $this->id_deklarasi_import	    =	$_POST['id_deklarasi_import'];
            $this->no_deklarasi_import	    =	$_POST['no_deklarasi_import'];
            $this->tgl_berlaku_deklarasi	=	to_mysql_date($_POST['tgl_berlaku_deklarasi']);
            $this->kuota_import	            =	$_POST['kuota_import'];
        }
        $this->iduser		            =	$this->session->userdata('userid');
		
								
		$this->db->insert('penerimaan_head',$this);
		$id = $this->db->insert_id();
		$detail = json_decode($_POST['detail_value']);
		foreach($detail as $row){
			$data = array();
			$data['idpenerimaan'] 	            = $id;
			$data['urutan']                     = $row[0];
			$data['idsortimen']                 = $row[1];
			$data['idkayu']                     = $row[3];
			$data['datadokumen_jumlah'] 		= $row[5];
			$data['datadokumen_satuan_jumlah'] 	= $row[7];
			$data['datadokumen_volume'] 		= $row[9];
			$data['datadokumen_satuan_volume']  = $row[11];
			$data['datadokumen_berat'] 	        = $row[13];
			$data['datadokumen_satuan_berat'] 	= $row[15];
			$data['datafisik_jumlah']           = $row[17];
			$data['datafisik_satuan_jumlah'] 	= $row[19];
			$data['datafisik_volume'] 	        = $row[21];
			$data['datafisik_satuan_volume'] 	= $row[23];
			$data['datafisik_berat'] 	        = $row[25];
			$data['datafisik_satuan_berat'] 	= $row[27];
			$this->db->insert('penerimaan_detail',$data);
		}
		
		return $id;
	}

    function update()
    {
        $id                             =   $_POST['idpenerimaan'];
        $this->periode_awal			    =	to_mysql_date($_POST['periode_awal']);
		$this->noinvoice			    =	$_POST['noinvoice'];
		$this->idsupplier			    =	$_POST['idsupplier'];
        $this->type_penerimaan	        =	$_POST['type_penerimaan'];
        
        if(strtoupper($this->type_penerimaan) == 'LOKAL'){
            $this->idnegara	            =	$_POST['idkota'];
            $this->idhutan			        =	$_POST['idhutan'];
            $this->idsertifikasi		    =	$_POST['idsertifikasi'];
            $this->no_sertifikat		    =	$_POST['no_sertifikat'];
            $this->tgl_berlaku_sertifikat	=	to_mysql_date($_POST['tgl_berlaku_sertifikat']);
            $this->iddokumen	            =	$_POST['iddokumen'];
            $this->tgl_berlaku_dokumen	    =	to_mysql_date($_POST['tgl_berlaku_dokumen']);
        }else{
            $this->idnegara	            =	$_POST['idnegara'];
            $this->id_deklarasi_import	    =	$_POST['id_deklarasi_import'];
            $this->no_deklarasi_import	    =	$_POST['no_deklarasi_import'];
            $this->tgl_berlaku_deklarasi	=	to_mysql_date($_POST['tgl_berlaku_deklarasi']);
            $this->kuota_import	            =	$_POST['kuota_import'];
        }
        $this->update_by		        =	$this->session->userdata('userid');
		
        $this->db->where('idpenerimaan',$id);
        $this->db->update('penerimaan_head',$this);


        $this->db->where('idpenerimaan',$id);
        $this->db->delete('penerimaan_detail');
		
		$detail = json_decode($_POST['detail_value']);
		foreach($detail as $row){
			$data = array();
			$data['idpenerimaan'] 	            = $id;
			$data['urutan']                     = $row[0];
			$data['idsortimen']                 = $row[1];
			$data['idkayu']                     = $row[3];
			$data['datadokumen_jumlah'] 		= $row[5];
			$data['datadokumen_satuan_jumlah'] 	= $row[7];
			$data['datadokumen_volume'] 		= $row[9];
			$data['datadokumen_satuan_volume']  = $row[11];
			$data['datadokumen_berat'] 	        = $row[13];
			$data['datadokumen_satuan_berat'] 	= $row[15];
			$data['datafisik_jumlah']           = $row[17];
			$data['datafisik_satuan_jumlah'] 	= $row[19];
			$data['datafisik_volume'] 	        = $row[21];
			$data['datafisik_satuan_volume'] 	= $row[23];
			$data['datafisik_berat'] 	        = $row[25];
			$data['datafisik_satuan_berat'] 	= $row[27];
			$this->db->insert('penerimaan_detail',$data);
		}
		
		return $id;
    }
    
    function head($nopenerimaan)
    {
        $this->db->select('*');
        $this->db->where('nopenerimaan',$nopenerimaan);
    
    }
	
	function delete($idpenerimaan,$client_id = '')
    {
		$data = array('deleted' => 1);

		$this->db->where('idpenerimaan', $idpenerimaan);
		if($client_id != ''){
            $this->db->where('client_id',$client_id);
        }
		$this->db->update('penerimaan_head', $data); 
    }
    
    function finalize($idpenerimaan,$client_id = '')
    {
		$data = array('final' => 1, 'finalize_date' => date('Y-m-d H:i:s'), 'finalize_by' => $this->session->userdata('userid'));

		$this->db->where('idpenerimaan', $idpenerimaan);
		if($client_id != ''){
            $this->db->where('client_id',$client_id);
        }
		$this->db->update('penerimaan_head', $data); 
    }
	
	function buyer_list($limit,$offset)
    {
		$this->db->select('mbuyer.*');
		$this->db->from('mbuyer');
		$this->db->where('xstatus','1');
		$this->db->order_by('idnegara','ASC');
		($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
		$query = $this->db->get(); 	
		//print_r($query->result());exit();
		return $query->result();
    }
    function negara_list()
    {
		$this->db->select('idnegara,negara');
		$this->db->from('mnegara');
	//	$this->db->where('xstatus','1');
		$this->db->order_by('negara','ASC');		
		$query = $this->db->get(); 	
		return $query->result();
    }
    
    function buyer_search($textsearch,$limit,$offset)
    {
		$this->db->select('mbuyer.*');
		$this->db->from('mbuyer');
        $this->db->like('alamat',$textsearch);
        $this->db->or_like('buyer',$textsearch);
        $this->db->or_like('idnegara',$textsearch);
		$this->db->order_by('idnegara','ASC');
		($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
		$query = $this->db->get(); 	
		return $query->result();
    }
	
	function detail($idpenerimaan,$type = '',$client_id = '')
    {
        if($client_id != ''){
            $this->db->where('client_id',$client_id);
        }
        if($type != ''){
            $this->db->where('type_penerimaan',$type);
        }
        $this->db->where('idpenerimaan',$idpenerimaan);
        $query = $this->db->get('view_penerimaan');
        return $query->row_array();
    }
    
    function detail_list($idpenerimaan)
    {
        $this->db->select('penerimaan_detail.*,msortimen.sortimen,mjeniskayu.nama as nama_kayu,
                           dok_satuan_jml.satuan_kode datadok_satuan_jml,dok_satuan_vol.satuan_kode datadok_satuan_vol,dok_satuan_brt.satuan_kode datadok_satuan_brt,
                           fis_satuan_jml.satuan_kode datafis_satuan_jml,fis_satuan_vol.satuan_kode datafis_satuan_vol,fis_satuan_brt.satuan_kode datafis_satuan_brt');
        $this->db->from('penerimaan_detail');
        $this->db->join('msortimen','msortimen.idsortimen = penerimaan_detail.idsortimen');
        $this->db->join('mjeniskayu','mjeniskayu.idkayu = penerimaan_detail.idkayu','left');
        $this->db->join('msatuan dok_satuan_jml','penerimaan_detail.datadokumen_satuan_jumlah = dok_satuan_jml.satuan_id','left');
        $this->db->join('msatuan dok_satuan_vol','penerimaan_detail.datadokumen_satuan_volume = dok_satuan_vol.satuan_id','left');
        $this->db->join('msatuan dok_satuan_brt','penerimaan_detail.datadokumen_satuan_berat = dok_satuan_brt.satuan_id','left');
        $this->db->join('msatuan fis_satuan_jml','penerimaan_detail.datafisik_satuan_jumlah = fis_satuan_jml.satuan_id','left');
        $this->db->join('msatuan fis_satuan_vol','penerimaan_detail.datafisik_satuan_volume = fis_satuan_vol.satuan_id','left');
        $this->db->join('msatuan fis_satuan_brt','penerimaan_detail.datafisik_satuan_berat = fis_satuan_brt.satuan_id','left');
        $this->db->where('idpenerimaan',$idpenerimaan);
        $this->db->order_by('urutan','ASC');
        $query = $this->db->get();
        return $query->result();
    }
	
}


