<?php
class Penjualan_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
	
	function count($client_id = '',$qsearch = '')
	{
		$this->db->from('penjualan_head');
        $this->db->join('mbuyer','mbuyer.idbuyer = penjualan_head.idbuyer');
        $this->db->where('penjualan_head.client_id',$client_id);
        $this->db->where('penjualan_head.deleted',0);
        if($qsearch != ''){ 
            $this->db->where("(invoice LIKE '%$qsearch%' OR mbuyer.buyer LIKE '%$qsearch%')");
        }
        
        if($client_id != ''){ 
           $this->db->where('penjualan_head.client_id',$client_id);
        }
        
		$query = $this->db->count_all_results();
        return $query;
	}
    function list_index($limit,$offset,$client_id = '',$qsearch = '')
    {
        $this->db->select('penjualan_head.*,mbuyer.buyer,SUM(jumlah) jumlah_produk,SUM(volume) volume_produk,SUM(berat) berat_produk,
                           update_user.nama update_user_name,finalize_user.nama finalize_user_name',false);
        $this->db->from('penjualan_head');
        $this->db->join('mbuyer','mbuyer.idbuyer = penjualan_head.idbuyer');
        $this->db->join('penjualan_detail','penjualan_detail.idjual = penjualan_head.idjual');
        $this->db->join('user update_user','update_user.id = penjualan_head.updated_by','left');
        $this->db->join('user finalize_user','finalize_user.id = penjualan_head.finalize_by','left');
        $this->db->where('penjualan_head.deleted',0);
  
        if($qsearch != ''){ 
            $this->db->where("(invoice LIKE '%$qsearch%' OR mbuyer.buyer LIKE '%$qsearch%')");
        }
        
        if($client_id != ''){ 
           $this->db->where('penjualan_head.client_id',$client_id);
        }
        
        $this->db->group_by('penjualan_head.idjual');
        $this->db->order_by('penjualan_head.idjual');
        
        ($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
        $query = $this->db->get();
        return $query->result();
    }
    
    
	function cek_idbuyer($idbuyer){
		$this->db->where('idbuyer',$idbuyer);
		$this->db->from('mbuyer');
		$row=$this->db->count_all_results();
		//print_r($row);exit();
		return $row;
	}
	
    function save($client_id)
	{
		$this->client_id        = $client_id;
        $this->idbuyer          = $_POST['idbuyer'];
        $this->alamat           = $_POST['alamat'];
        $this->periode_awal     = to_mysql_date($_POST['periode_awal']);
        $this->tglterbit        = to_mysql_date($_POST['tglterbit']);
        $this->idprovinsi       = $_POST['idprovinsi'];
        $this->idkota           = $_POST['idkota'];
        $this->nilai            = to_mysql_number($_POST['nilai']);
        $this->invoice          = $_POST['invoice'];
        $this->nonota           = $_POST['nonota'];
        $this->no_po            = $_POST['no_po'];
        $this->tgl_po           = to_mysql_date($_POST['tgl_po']);
        $this->created_at		=	now_mysql();
		$this->created_by	    =	$this->session->userdata('userid');

		$this->db->insert('penjualan_head',$this);
        $idjual = $this->db->insert_id();
        $detail = json_decode($_POST['detail_value']);
		foreach($detail as $row){
			$data = array();
			$data['idjual'] 	            = $idjual;
			$data['idproduk']               = $row[1];
			$data['hscode']                 = $row[3];
            $data['volume']                 = $row[4];
            $data['satuan_volume']          = $row[6];
			$data['berat']                  = $row[7];
            $data['satuan_berat']           = $row[8];
            $data['jumlah']                 = $row[10];
			$data['satuan_jumlah']          = $row[12];
			$data['idkayu']                 = $row[13];
			$data['idkayu2']                = $row[15];
            $data['idkayu3']                = $row[17];
			$this->db->insert('penjualan_detail',$data);
		}
		
        
		return true;
	}
    
    function save_attachment($client_id)
    {
        $data = array('idekspor'            => $_POST['idekspor'],
                      'attach_doc'          => $_POST['attach_doc'],
                      
                      'attach_keterangan'   => $_POST['attach_keterangan'],
                      'attach_bln'          => $_POST['attach_bln'],
                      'attach_thn'          => $_POST['attach_thn'],
                      'attach_invoice'      => $_POST['attach_invoice'],
                      'attach_status'       => $_POST['attach_status'],
                      'attach_date'         => date('Y-m-d H:i:s'),
                      'no_doc'              => $_POST['no_doc'],
                      'client_id'           => $client_id,
                     );
                     
        if($data['attach_file'] = $this->upload($client_id)){
            $this->db->insert('attachment',$data);
            return true;
        }else{
            return false;
        }
        
    }
    
    function update_attachment($client_id)
    {
        $data = array('attach_doc'          => $_POST['attach_doc'],
                      'attach_keterangan'   => $_POST['attach_keterangan'],
                      'attach_bln'          => $_POST['attach_bln'],
                      'attach_thn'          => $_POST['attach_thn'],
                      'attach_status'       => $_POST['attach_status'],
                      'attach_date'         => date('Y-m-d H:i:s'),
                      'no_doc'              => $_POST['no_doc'],
                     );
                     
        if($data['attach_file'] = $this->upload($client_id,$_POST['attach_file'])){
            $this->db->where('attach_id',$_POST['attach_id']);
            $this->db->update('attachment',$data);
            return true;
        }else{
            return false;
        }
        
    }
    
    function upload($client_id,$oldfile='')
    {
        if(isset($_FILES['doc_file'])){
			if($_FILES['doc_file']['name']	!=	''){
                $lampiran_dir = "./assets/lampiran/$client_id/";
                if(!file_exists($lampiran_dir)) mkdir($lampiran_dir, 0775);
				 
                $config['upload_path']	    =	$lampiran_dir;
				$config['allowed_types']	=	'pdf|doc|docx|xls|xlsx|txt';
				$config['encrypt_name']	    =	TRUE;

				$this->load->library('upload',	$config);
				
				if	($this->upload->do_upload('doc_file'))
				{
					$file_upload	=	$this->upload->data();
                    if($oldfile != ''){
                        if(file_exists($lampiran_dir.$oldfile))	unlink($lampiran_dir.$oldfile);
                    }
					return $file_upload['file_name'];
				}else{
                    $this->error_message = $this->upload->display_errors();
                    return false;
                }
			}
            $this->error_message = "File Attachment Belum dipilih!";
            return false;
		}
    }
    
    function get_detail_attachment($attach_id)
    {
        $this->db->where('attach_id',$attach_id);
        $query = $this->db->get('attachment');
        return $query->row_array();
    }
    
    function delete_attachment($attach_id)
    {
        
        $data =  $this->get_detail_attachment($attach_id);
        if(count($data) > 0){
            $lampiran_dir = "./assets/lampiran/".$data['client_id']."/";
            if(file_exists($lampiran_dir.$data['attach_file']))	unlink($lampiran_dir.$data['attach_file']);
            $this->db->where('attach_id',$attach_id);
            $this->db->delete('attachment');
            return $data['idekspor'];
        }
    }
    
    
       
    

    function update()
    {
        $idjual                 = $_POST['idjual'];
        $this->idbuyer          = $_POST['idbuyer'];
        $this->alamat           = $_POST['alamat'];
        $this->periode_awal     = to_mysql_date($_POST['periode_awal']);
        $this->tglterbit        = to_mysql_date($_POST['tglterbit']);
        $this->idprovinsi       = $_POST['idprovinsi'];
        $this->idkota           = $_POST['idkota'];
        $this->nilai            = to_mysql_number($_POST['nilai']);;
        $this->invoice          = $_POST['invoice'];
        $this->nonota           = $_POST['nonota'];
        $this->no_po            = $_POST['no_po'];
        $this->tgl_po           = to_mysql_date($_POST['tgl_po']);
		$this->updated_by	    = $this->session->userdata('userid');

		
		if($this->db->update('penjualan_head', $this, array('idjual' => $idjual))){
            $this->db->where('idjual',$idjual);
            $this->db->delete('penjualan_detail');
            $detail = json_decode($_POST['detail_value']);
            foreach($detail as $row){
                $data = array();
                $data['idjual'] 	            = $idjual;
                $data['idproduk']               = $row[1];
                $data['hscode']                 = $row[3];
                $data['volume']                 = $row[4];
                $data['satuan_volume']          = $row[6];
                $data['berat']                  = $row[7];
                $data['satuan_berat']           = $row[8];
                $data['jumlah']                 = $row[10];
                $data['satuan_jumlah']          = $row[12];
                $data['idkayu']                 = $row[13];
                $data['idkayu2']                = $row[15];
                $data['idkayu3']                = $row[17];
                $this->db->insert('penjualan_detail',$data);
            }
            
            return true;
		}else{
			$this->error_message = "Penyimpanan Gagal";
			return false;
		}
    }
    
    function getNoUrut($client_id){
		
		$this->db->select("no_urut");
		$this->db->from('ekspor');
		$this->db->where("client_id",$client_id); 
		$this->db->order_by("no_urut","DESC");
		$this->db->limit(1);
		$query = $this->db->get();
		$array = $query->row_array();
		$query->free_result();
		unset($query);
        return $array["no_urut"] + 1;
	}
	
	function delete($idjual,$client_id = '')
    {
		$data = array('deleted' => 1);

		$this->db->where('idjual', $idjual);
		if($client_id != ''){
            $this->db->where('client_id',$client_id);
        }
		$this->db->update('penjualan_head', $data); 
    }
    
    function finalize($idjual,$client_id = '')
    {
		$data = array('final' => 1, 'finalize_date' => date('Y-m-d H:i:s'), 'finalize_by' => $this->session->userdata('userid'));

		$this->db->where('idjual', $idjual);
		if($client_id != ''){
            $this->db->where('client_id',$client_id);
        }
		$this->db->update('penjualan_head', $data); 
    }
	
    function detail($idjual)
    {
        $this->db->select('penjualan_head.*,mbuyer.buyer namabuyer,provinsi.nama_provinsi,kota.nama_kota');
        $this->db->from('penjualan_head');
        $this->db->join('provinsi','provinsi.id = penjualan_head.idprovinsi','left');
        $this->db->join('kota','kota.id = penjualan_head.idkota','left');
        $this->db->join('mbuyer','mbuyer.idbuyer = penjualan_head.idbuyer');
        $this->db->where('penjualan_head.idjual',$idjual);
        $query = $this->db->get();
        return $query->row_array();
    }
    
    function detail_list($idjual)
    {
        $this->db->select('penjualan_detail.*,mproduk.produk as namaproduk,kayu1.nama namakayu1,kayu2.nama namakayu2,
                           kayu3.nama namakayu3');
        $this->db->from('penjualan_detail');
        $this->db->join('mproduk','mproduk.idproduk = penjualan_detail.idproduk');
        $this->db->join('mjeniskayu kayu1','kayu1.idkayu = penjualan_detail.idkayu','left');
        $this->db->join('mjeniskayu kayu2','kayu2.idkayu = penjualan_detail.idkayu2','left');
        $this->db->join('mjeniskayu kayu3','kayu3.idkayu = penjualan_detail.idkayu3','left');
        $this->db->where('idjual',$idjual);
        $query = $this->db->get();
        return $query->result();
    }
    
    
    function review($idekspor)
    {
        $this->db->select('ekspor.*,ekspor.valuta nama_valuta,mbuyer.buyer namabuyer,mbuyer.alamat alamat_buyer,
                           mpelabuhan.uraian loading_name,mpelabuhan.kode loading_code,mdischarge.kode discharge_code,mdischarge.uraian discharge_name,mnegara.negara country_name,
                           clients.nama nama_client,clients.alamat alamat_client,provinsi.nama_provinsi,provinsi.kode kode_provinsi,kota.nama_kota,kota.kode kode_kota');
        $this->db->from('ekspor');
        $this->db->join('clients','ekspor.client_id = clients.id');
        $this->db->join('mpelabuhan','mpelabuhan.idloading = ekspor.loading');
        $this->db->join('mdischarge','mdischarge.iddischarge = ekspor.discharge');
        $this->db->join('mbuyer','mbuyer.idbuyer = ekspor.idbuyer');
        $this->db->join('mnegara','ekspor.idnegara = mnegara.idnegara','left');
        $this->db->join('provinsi','clients.provinsi_id = provinsi.id','left');
        $this->db->join('kota','clients.kota_id = kota.id','left');
        
        $this->db->where('ekspor.idekspor',$idekspor);
        $query = $this->db->get();
        return $query->row_array();
    }
    
    function updateNoUrut($idekspor,$no_urut)
	{
		$data = array(
			'no_urut'	=> $no_urut
		);
        $this->db->update('ekspor', $data, array('idekspor' => $idekspor));
	}
    
    function add_vlegal($data)
    {
        $this->db->insert('vlegal_status',$data);
        return $this->db->insert_id();
    }
    
    function getSendVlegalByIdEkspor($idekspor){
		$this->db->from("vlegal_status");
		$this->db->where("idekspor",$idekspor); 	
		$this->db->where("status",'send');
		$this->db->order_by("vid","DESC");
		$query = $this->db->get();
		$array = $query->row_array();
		$query->free_result();
		unset($query);
        return $array;
	}
    
    function getSendVlegalById($vid){
		$this->db->from("vlegal_status");
		$this->db->where("vid",$vid); 	
		$this->db->where("status",'send');
		$query = $this->db->get();
		$array = $query->row_array();
		$query->free_result();
		unset($query);
        return $array;
	}
    
    function getVlegalById($vid){
		$this->db->from("vlegal_status");
		$this->db->where("vid",$vid); 	
		$query = $this->db->get();
		$array = $query->row_array();
		$query->free_result();
		unset($query);
        return $array;
	}
    
    function updateStatusVLegal($vid,$status)
    {
        $this->db->where('vid',$vid);
        $this->db->update('vlegal_status',array('status' => $status));
    }
    
    function duplicate_check($id,$lvlk)
    {
        $itemDataitemData 	= $this->detail($id);
		
        if($itemData['status_liu'] != 'replace'){
            if($itemData["sertifikat"]==0){
                $nodocument = date('y') .'.'. str_pad($itemData['no_urut'], 5, "0", STR_PAD_LEFT) .'-00000.' . $lvlk . '-ID-' . $itemData['iso'];
            }else{
                $nodocument = date('y') .'.'. str_pad($itemData['no_urut'], 5, "0", STR_PAD_LEFT) .'-'. str_pad(substr(trim($itemData["sertifikat"]),-5), 5, "0", STR_PAD_LEFT) .'.' . $lvlk . '-ID-' . $itemData['iso'];
            }
            
            $this->db->where('no_dokumen',$nodocument);
            $this->db->where('status','send');
            $hasil = $this->db->count_all_results('vlegal_status');
            
            if($hasil > 0){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
	
    
    function get_no_invoice($idekspor)
    {
        $this->db->select('invoice');
        $this->db->where('idekspor',$idekspor);
        $query = $this->db->get('ekspor');
        return $query->row('invoice');
    }
    
    function updateStatusDokumen($idekspor,$status)
    {
        $this->db->where('idekspor',$idekspor);
        $this->db->update('ekspor',array('status_dokumen' => $status));
        return true;
    }
    
    function updateStatusLIU($idekspor,$status)
    {
        $this->db->where('idekspor',$idekspor);
        $this->db->update('ekspor',array('status_liu' => $status));
        return true;
    }
	
}

?>