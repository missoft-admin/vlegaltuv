<?php
class Ekspor_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
	
	function count($client_id = '',$status_liu='',$status_dokumen='',$qsearch = '')
	{
		$this->db->from('ekspor');
        if($status_liu == 'send'){
            $this->db->join("vlegal_status","vlegal_status.idekspor = ekspor.idekspor AND vlegal_status.status IN('send','send')",'left');
        }elseif($status_liu == 'reject'){
            $this->db->join("vlegal_status","vlegal_status.idekspor = ekspor.idekspor AND vlegal_status.status = 'rejected' ",'left');
        }
        $this->db->join('mbuyer','mbuyer.idbuyer = ekspor.idbuyer');
        $this->db->where('ekspor.client_id',$client_id);
        $this->db->where('ekspor.deleted',0);
        
        if($status_liu != ''){ 
            if($status_liu == 'send'){
                $this->db->where_in('status_liu',array('send','cancel'));
            }else{
                $this->db->where('status_liu',$status_liu);
            }
        }else{
            if($status_dokumen == 'Final'){
                $this->db->where('status_liu != ','reject');
                $this->db->where('status_liu != ','send');
            }
        }
        
        if($status_dokumen != '' && $status_liu != 'send'){ 
            $this->db->where('status_dokumen',$status_dokumen);
        }
        
        
        if($qsearch != ''){ 
            $this->db->where("(ekspor.invoice LIKE '%$qsearch%' OR mbuyer.buyer LIKE '%$qsearch%')",null,false);
        }
		$query = $this->db->count_all_results();
        return $query;
	}
    
    
    function count_pengajuan($qsearch = '')
	{
		$this->db->from('ekspor');
        $this->db->join('clients','clients.id = ekspor.client_id');
        
        $this->db->where('status_liu !=','send');
        $this->db->where('status_liu !=','cancel');
        $this->db->where('status_dokumen','Final');
        
         if($qsearch != ''){ 
            $this->db->where("(ekspor.invoice LIKE '%$qsearch%'  OR clients.nama LIKE '%$qsearch%')");
        }
        
		$query = $this->db->count_all_results();
        return $query;
	}
    
    function count_processed($status,$qsearch = '')
	{
		$this->db->from('vlegal_status');
		$this->db->join('ekspor','vlegal_status.idekspor = ekspor.idekspor');
        $this->db->join('clients','clients.id = ekspor.client_id');
        
        $this->db->where('vlegal_status.status',$status);
        
        if($qsearch != ''){ 
            $this->db->where("(vlegal_status.invoice LIKE '%$qsearch%'  OR clients.nama LIKE '%$qsearch%')");
        }
        
		$query = $this->db->count_all_results();
        
        return $query;
	}
    
     function count_all_attachment()
    {
        $query = $this->db->count_all_results('attachment');
        return $query;
    }
        
    function list_index($limit,$offset,$client_id = '',$status_liu='',$status_dokumen='',$qsearch = '')
    {
        if($status_liu == 'send'){
            $this->db->select('ekspor.*,vlegal_status.vid,vlegal_status.status status_vlegal,mbuyer.buyer,mpelabuhan.uraian loading_name,mdischarge.uraian discharge_name');
            $this->db->join("vlegal_status","vlegal_status.idekspor = ekspor.idekspor AND vlegal_status.status IN('send','cancel')",'left');
        }elseif($status_liu == 'reject'){
            $this->db->select('ekspor.*,vlegal_status.vid,vlegal_status.status status_vlegal,vlegal_status.keterangan keterangan_vlegal,mbuyer.buyer,mpelabuhan.uraian loading_name,mdischarge.uraian discharge_name');
            $this->db->join("vlegal_status","vlegal_status.idekspor = ekspor.idekspor AND vlegal_status.status = 'rejected' ",'left');
        }else{
            $this->db->select('ekspor.*,mbuyer.buyer,mpelabuhan.uraian loading_name,mdischarge.uraian discharge_name');
        }
        $this->db->from('ekspor');
        $this->db->join('mbuyer','mbuyer.idbuyer = ekspor.idbuyer');
        $this->db->join('mpelabuhan','mpelabuhan.idloading = ekspor.loading');
        $this->db->join('mdischarge','mdischarge.iddischarge = ekspor.discharge');
        $this->db->where('ekspor.client_id',$client_id);
        if($qsearch != ''){ 
            $this->db->where("(ekspor.invoice LIKE '%$qsearch%' OR mbuyer.buyer LIKE '%$qsearch%')",null,false);
        }
        
        if($status_liu != ''){ 
            if($status_liu == 'send'){
                $this->db->where_in('status_liu',array('send','cancel'));
            }else{
                $this->db->where('status_liu',$status_liu);
            }
        }else{
            if($status_dokumen == 'Final'){
                $this->db->where('status_liu != ','reject');
                $this->db->where('status_liu != ','send');
            }
        }
        
        if($status_dokumen != '' && $status_liu != 'send'){ 
            $this->db->where('status_dokumen',$status_dokumen);
        }
        $this->db->where('ekspor.deleted',0);
        $this->db->order_by('idekspor','DESC');
        ($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
        $query = $this->db->get();
        // print_r($this->db->last_query());exit();
        return $query->result();
    }
    
    function list_index_back($limit,$offset,$client_id = '',$status_liu='',$status_dokumen='',$qsearch = '')
    {
        $this->db->select('ekspor.*,mbuyer.buyer,mpelabuhan.uraian loading_name,mdischarge.uraian discharge_name');
        $this->db->from('ekspor');
        $this->db->join('mbuyer','mbuyer.idbuyer = ekspor.idbuyer');
        $this->db->join('mpelabuhan','mpelabuhan.idloading = ekspor.loading');
        $this->db->join('mdischarge','mdischarge.iddischarge = ekspor.discharge');
        $this->db->where('ekspor.client_id',$client_id);
        if($qsearch != ''){ 
            $this->db->like('invoice',$qsearch);
            $this->db->or_like('mbuyer.buyer',$qsearch);
        }
        
        if($status_liu != ''){ 
            $this->db->where('status_liu',$status_liu);
        }else{
            if($status_dokumen == 'Final'){
                $this->db->where('status_liu != ','reject');
                $this->db->where('status_liu != ','send');
            }
        }
        
        if($status_dokumen != ''){ 
            $this->db->where('status_dokumen',$status_dokumen);
        }
        ($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
        $query = $this->db->get();
        return $query->result();
    }
    
    function list_index_pengajuan($limit,$offset,$qsearch = '')
    {
        $this->db->select('ekspor.*,mbuyer.buyer,mpelabuhan.uraian loading_name,mdischarge.uraian discharge_name,clients.nama as nama_client');
        $this->db->from('ekspor');
        $this->db->join('clients','clients.id = ekspor.client_id');
        $this->db->join('mbuyer','mbuyer.idbuyer = ekspor.idbuyer');
        $this->db->join('mpelabuhan','mpelabuhan.idloading = ekspor.loading');
        $this->db->join('mdischarge','mdischarge.iddischarge = ekspor.discharge');
        $this->db->where('status_liu !=','send');
        $this->db->where('status_liu !=','cancel');
        $this->db->where('status_dokumen','Final');
        
        
        if($qsearch != ''){ 
            $this->db->where("(ekspor.invoice LIKE '%$qsearch%'  OR clients.nama LIKE '%$qsearch%')");
        }
        
        ($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
        $query = $this->db->get();
        return $query->result();
    }
    
    
    function list_index_processed($limit,$offset,$status,$qsearch = '')
    {
        $this->db->select('ekspor.*,vlegal_status.*,mbuyer.buyer,mpelabuhan.uraian loading_name,mdischarge.uraian discharge_name,clients.nama as nama_client,user.nama reviewer');
        $this->db->from('vlegal_status');
        $this->db->join('ekspor','vlegal_status.idekspor = ekspor.idekspor');
        $this->db->join('clients','clients.id = ekspor.client_id','left');
        $this->db->join('mbuyer','mbuyer.idbuyer = ekspor.idbuyer','left');
        $this->db->join('mpelabuhan','mpelabuhan.idloading = ekspor.loading','left');
        $this->db->join('mdischarge','mdischarge.iddischarge = ekspor.discharge','left');
        $this->db->join('user','vlegal_status.reviewerid = user.id','left','left');
        $this->db->where('vlegal_status.status',$status);
        
        
        if($qsearch != ''){ 
            $this->db->where("(vlegal_status.invoice LIKE '%$qsearch%'  OR clients.nama LIKE '%$qsearch%')");
        }
        
        $this->db->order_by('vid','DESC');
        ($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
        $query = $this->db->get();
        return $query->result();
    }
    
    function list_attachment($idekspor,$client_id = '')
    {
        $this->db->where('idekspor',$idekspor);
        if($client_id != ''){
            $this->db->where('client_id',$client_id);
        }
        $query = $this->db->get('attachment');
        return $query->result();
    }
    
    function list_all_attachment($limit,$offset)
    {
        ($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
        $query = $this->db->get('attachment');
        return $query->result();
    }
    
	function cek_idbuyer($idbuyer){
		$this->db->where('idbuyer',$idbuyer);
		$this->db->from('mbuyer');
		$row=$this->db->count_all_results();
		//print_r($row);exit();
		return $row;
	}
	
    function save($client_id)
	{
		$this->client_id    = $client_id;
        $this->bulan        = date('m');
        $this->tahun        = date('Y');
        $this->idbuyer      = $this->input->post('idbuyer');
        $this->alamat       = $this->input->post('alamat');
        $this->idnegara     = $this->input->post('idnegara');
        $this->iso          = $this->input->post('iso');
        $this->loading      = $this->input->post('loading');
        $this->discharge    = $this->input->post('discharge');
        $this->invoice      = $this->input->post('invoice');
        $this->tglinvoice   = to_mysql_date($this->input->post('tglinvoice'));
        $this->tglship      = to_mysql_date($this->input->post('tglship'));
        $this->vessel       = $this->input->post('vessel');
        $this->etpik        = $this->input->post('etpik');
        $this->sertifikat   = $this->input->post('sertifikat');
        $this->npwp         = $this->input->post('npwp');
        $this->keterangan   = $this->input->post('keterangan');
        $this->valuta       = $this->input->post('valuta');
        $this->no_urut      = $this->getNoUrut($client_id);
        $this->status       = $this->input->post('status');
        $this->stuffing     = $this->input->post('stuffing');
        $this->no_po        = $this->input->post('no_po');
        $this->paper_number = $this->input->post('paper_number');
        $this->tgl_po       = to_mysql_date($this->input->post('tgl_po'));
        
        $this->submitdate	= date("Y-m-d h:i:s");
        
								
		$this->db->insert('ekspor',$this);
        $id_ekspor = $this->db->insert_id();
        $detail = json_decode($_POST['detail_value']);
		foreach($detail as $row){
			$data = array();
			$data['idekspor'] 	            = $id_ekspor;
			$data['hscode']                 = $row[3];
			$data['idproduk']               = $row[1];
            $data['jumlah']                 = $row[11];
            $data['satuan_jumlah']          = $row[12];
			$data['volume']                 = $row[4];
			$data['satuan_volume']          = $row[6];
			$data['berat']                  = $row[7];
            $data['satuan_berat']           = $row[9];
            $data['jenis_kayu']             = $row[13];
			$data['negara_asal']            = $row[15];
            $data['nilai']                  = $row[18];
			$data['nilai_cnf']              = $row[19];
			$data['nilai_cif']              = $row[20];
			$this->db->insert('ekspor_detail',$data);
            $data_kayu = array();
            
		}
		return true;
	}
    
    function update()
    {
        $this->bulan        = date('m');
        $this->tahun        = date('Y');
        $this->idbuyer      = $this->input->post('idbuyer');
        $this->alamat       = $this->input->post('alamat');
        $this->idnegara     = $this->input->post('idnegara');
        $this->iso          = $this->input->post('iso');
        $this->loading      = $this->input->post('loading');
        $this->discharge    = $this->input->post('discharge');
        $this->invoice      = $this->input->post('invoice');
        $this->tglinvoice   = to_mysql_date($this->input->post('tglinvoice'));
        $this->tglship      = to_mysql_date($this->input->post('tglship'));
        $this->vessel       = $this->input->post('vessel');
        $this->etpik        = $this->input->post('etpik');
        $this->sertifikat   = $this->input->post('sertifikat');
        $this->npwp         = $this->input->post('npwp');
        $this->keterangan   = $this->input->post('keterangan');
        $this->valuta       = $this->input->post('valuta');
        $this->status       = $this->input->post('status');
        $this->stuffing     = $this->input->post('stuffing');
        $this->no_po        = $this->input->post('no_po');
        $this->tgl_po       = to_mysql_date($this->input->post('tgl_po'));
        $this->paper_number = $this->input->post('paper_number');
        $this->submitdate	= date("Y-m-d h:i:s");
        
        $statusEdit		    = $this->input->post('status_edit');
        switch($statusEdit){
			case "replace"	:
				$this->status_liu		= "replace";
				$this->status_dokumen	= "Editing";
				break;
			case "extend"	:
				$this->status_liu		= "extend";
				$this->status_dokumen	= "Editing";
				break;
			case "cancel"	:
				$this->status_liu		= "cancel";
				$this->status_dokumen	= "Editing";
				break;
		}
		if($this->db->update('ekspor', $this, array('idekspor' => $this->input->post('idekspor')))){
            $this->db->where('idekspor',$this->input->post('idekspor'));
            $this->db->delete('ekspor_detail');
			$detail = json_decode($this->input->post('detail_value'));
            foreach($detail as $row){
                $data = array();
                $data['idekspor'] 	            = $this->input->post('idekspor');
                $data['hscode']                 = $row[3];
                $data['idproduk']               = $row[1];
                $data['jumlah']                 = $row[11];
                $data['satuan_jumlah']          = $row[12];
                $data['volume']                 = $row[4];
                $data['satuan_volume']          = $row[6];
                $data['berat']                  = $row[7];
                $data['satuan_berat']           = $row[9];
                $data['jenis_kayu']             = $row[13];
                $data['negara_asal']            = $row[15];
                $data['nilai']                  = $row[18];
                $data['nilai_cnf']              = $row[19];
                $data['nilai_cif']              = $row[20];
                $this->db->insert('ekspor_detail',$data);
            }
            
            return true;
		}else{
			$this->error_message = "Penyimpanan Gagal";
			return false;
		}
    }
    
    function save_attachment($client_id)
    {
        $data = array('idekspor'            => $_POST['idekspor'],
                      'attach_doc'          => $_POST['attach_doc'],
                      
                      'attach_keterangan'   => $_POST['attach_keterangan'],
                      'attach_bln'          => $_POST['attach_bln'],
                      'attach_thn'          => $_POST['attach_thn'],
                      'attach_invoice'      => $_POST['attach_invoice'],
                      'attach_status'       => $_POST['attach_status'],
                      'attach_date'         => date('Y-m-d H:i:s'),
                      'no_doc'              => $_POST['no_doc'],
                      'client_id'           => $client_id,
                     );
                     
        if($data['attach_file'] = $this->upload($client_id)){
            $this->db->insert('attachment',$data);
            return true;
        }else{
            return false;
        }
        
    }
    
    function update_attachment($client_id)
    {
        $data = array('attach_doc'          => $_POST['attach_doc'],
                      'attach_keterangan'   => $_POST['attach_keterangan'],
                      'attach_bln'          => $_POST['attach_bln'],
                      'attach_thn'          => $_POST['attach_thn'],
                      'attach_status'       => $_POST['attach_status'],
                      'attach_date'         => date('Y-m-d H:i:s'),
                      'no_doc'              => $_POST['no_doc'],
                     );
                     
        if($data['attach_file'] = $this->upload($client_id,$_POST['attach_file'])){
            $this->db->where('attach_id',$_POST['attach_id']);
            $this->db->update('attachment',$data);
            return true;
        }else{
            return false;
        }
        
    }
    
    function upload($client_id,$oldfile='')
    {
        if(isset($_FILES['doc_file'])){
			if($_FILES['doc_file']['name']	!=	''){
                $lampiran_dir = "./assets/lampiran/$client_id/";
                if(!file_exists($lampiran_dir)) mkdir($lampiran_dir, 0775);
				 
                $config['upload_path']	    =	$lampiran_dir;
				$config['allowed_types']	=	'pdf|doc|docx|xls|xlsx|txt';
				$config['encrypt_name']	    =	TRUE;

				$this->load->library('upload',	$config);
				
				if	($this->upload->do_upload('doc_file'))
				{
					$file_upload	=	$this->upload->data();
                    if($oldfile != ''){
                        if(file_exists($lampiran_dir.$oldfile))	unlink($lampiran_dir.$oldfile);
                    }
					return $file_upload['file_name'];
				}else{
                    $this->error_message = $this->upload->display_errors();
                    return false;
                }
			}
            $this->error_message = "File Attachment Belum dipilih!";
            return false;
		}
    }
    
    function get_detail_attachment($attach_id)
    {
        $this->db->where('attach_id',$attach_id);
        $query = $this->db->get('attachment');
        return $query->row_array();
    }
    
    function delete_attachment($attach_id)
    {
        
        $data =  $this->get_detail_attachment($attach_id);
        if(count($data) > 0){
            $lampiran_dir = "./assets/lampiran/".$data['client_id']."/";
            if(file_exists($lampiran_dir.$data['attach_file']))	unlink($lampiran_dir.$data['attach_file']);
            $this->db->where('attach_id',$attach_id);
            $this->db->delete('attachment');
            return $data['idekspor'];
        }
    }   
       
    function detail_ekspor($idekspor)
    {
        $this->db->select('ekspor_detail.*,mproduk.produk as namaproduk');
        $this->db->from('ekspor_detail');
        $this->db->join('mproduk','mproduk.idproduk = ekspor_detail.idproduk');
        $this->db->where('idekspor',$idekspor);
        $query = $this->db->get();
        return $query->result();
    }
    
    function getNoUrut($client_id){
		
		$this->db->select("no_urut");
		$this->db->from('ekspor');
		$this->db->where("client_id",$client_id); 
		$this->db->order_by("no_urut","DESC");
		$this->db->limit(1);
		$query = $this->db->get();
		$array = $query->row_array();
		$query->free_result();
		unset($query);
        return $array["no_urut"] + 1;
	}
	
	function delete($id)
    {
		$data = array('deleted' => 1);
		$this->db->where('idekspor', $id);
		$this->db->update('ekspor', $data); 
    }
    
    function cancel($id,$vid)
    {
		$data = array();
        $data['status_liu']		= "cancel";
        $data['status_dokumen']	= "Editing";
		$this->db->where('idekspor', $id);
		$this->db->update('ekspor', $data); 
        
        $data_vlegal = array('status' => 'cancel');
		$this->db->where('vid', $vid);
		$this->db->update('vlegal_status', $data_vlegal); 
    }
	
	
    
    function detail($idekspor)
    {
        $this->db->select('ekspor.*,ekspor.valuta nama_valuta,mbuyer.buyer namabuyer,mbuyer.alamat,mpelabuhan.uraian loading_name,mdischarge.uraian discharge_name,mnegara.negara country_name');
        $this->db->from('ekspor');
        $this->db->join('mpelabuhan','mpelabuhan.idloading = ekspor.loading');
        $this->db->join('mdischarge','mdischarge.iddischarge = ekspor.discharge');
        $this->db->join('mbuyer','mbuyer.idbuyer = ekspor.idbuyer');
        $this->db->join('mnegara','mbuyer.idnegara = mnegara.idnegara','left');
        $this->db->where('ekspor.idekspor',$idekspor);
        $query = $this->db->get();
        return $query->row_array();
    }
    
    function review($idekspor)
    {
        $this->db->select('ekspor.*,ekspor.valuta nama_valuta,mbuyer.buyer namabuyer,mbuyer.alamat alamat_buyer,
                           mpelabuhan.uraian loading_name,mpelabuhan.kode loading_code,mdischarge.kode discharge_code,mdischarge.uraian discharge_name,mnegara.negara country_name,
                           clients.nama nama_client,clients.alamat alamat_client,provinsi.nama_provinsi,provinsi.kode kode_provinsi,kota.nama_kota,kota.kode kode_kota');
        $this->db->from('ekspor');
        $this->db->join('clients','ekspor.client_id = clients.id');
        $this->db->join('mpelabuhan','mpelabuhan.idloading = ekspor.loading');
        $this->db->join('mdischarge','mdischarge.iddischarge = ekspor.discharge');
        $this->db->join('mbuyer','mbuyer.idbuyer = ekspor.idbuyer');
        $this->db->join('mnegara','ekspor.idnegara = mnegara.idnegara','left');
        $this->db->join('provinsi','clients.provinsi_id = provinsi.id','left');
        $this->db->join('kota','clients.kota_id = kota.id','left');
        
        $this->db->where('ekspor.idekspor',$idekspor);
        $query = $this->db->get();
        return $query->row_array();
    }
    
    function updateNoUrut($idekspor,$no_urut)
	{
		$data = array(
			'no_urut'	=> $no_urut
		);
        $this->db->update('ekspor', $data, array('idekspor' => $idekspor));
	}
    
    function add_vlegal($data)
    {
        $this->db->insert('vlegal_status',$data);
        return $this->db->insert_id();
    }
    
    function getSendVlegalByIdEkspor($idekspor){
		$this->db->from("vlegal_status");
		$this->db->where("idekspor",$idekspor); 	
		$this->db->where("status",'send');
		$this->db->order_by("vid","DESC");
		$query = $this->db->get();
		$array = $query->row_array();
		$query->free_result();
		unset($query);
        return $array;
	}
    
    function getSendVlegalById($vid){
		$this->db->from("vlegal_status");
		$this->db->where("vid",$vid); 	
		$this->db->where("status",'send');
		$query = $this->db->get();
		$array = $query->row_array();
		$query->free_result();
		unset($query);
        return $array;
	}
    
    function getVlegalById($vid){
		$this->db->from("vlegal_status");
		$this->db->where("vid",$vid); 	
		$query = $this->db->get();
		$array = $query->row_array();
		$query->free_result();
		unset($query);
        return $array;
	}
    
    function updateStatusVLegal($vid,$status)
    {
        $this->db->where('vid',$vid);
        $this->db->update('vlegal_status',array('status' => $status));
    }
    
    function duplicate_check($id,$lvlk)
    {
        $itemDataitemData 	= $this->detail($id);
		
        if($itemData['status_liu'] != 'replace'){
            if($itemData["sertifikat"]==0){
                $nodocument = date('y') .'.'. str_pad($itemData['no_urut'], 5, "0", STR_PAD_LEFT) .'-00000.' . $lvlk . '-ID-' . $itemData['iso'];
            }else{
                $nodocument = date('y') .'.'. str_pad($itemData['no_urut'], 5, "0", STR_PAD_LEFT) .'-'. str_pad(substr(trim($itemData["sertifikat"]),-5), 5, "0", STR_PAD_LEFT) .'.' . $lvlk . '-ID-' . $itemData['iso'];
            }
            
            $this->db->where('no_dokumen',$nodocument);
            $this->db->where('status','send');
            $hasil = $this->db->count_all_results('vlegal_status');
            
            if($hasil > 0){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
	
    
    function get_no_invoice($idekspor)
    {
        $this->db->select('invoice');
        $this->db->where('idekspor',$idekspor);
        $query = $this->db->get('ekspor');
        return $query->row('invoice');
    }
    
    function updateStatusDokumen($idekspor,$status)
    {
        $this->db->where('idekspor',$idekspor);
        $this->db->update('ekspor',array('status_dokumen' => $status));
        return true;
    }
    
    function updateStatusLIU($idekspor,$status)
    {
        $this->db->where('idekspor',$idekspor);
        $this->db->update('ekspor',array('status_liu' => $status));
        return true;
    }
	
}

?>