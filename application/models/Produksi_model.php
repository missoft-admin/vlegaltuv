<?php
class Produksi_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
	
	function count($searchText = '',$type = '',$client_id = '')
	{
		$this->db->from('view_produksi');
        if($client_id != ''){
            $this->db->where('client_id',$client_id);
        }
        if($searchText != ''){
            $this->db->where("(nama_supplier LIKE '%$searchText%' OR nama_kayu LIKE '%$searchText%' OR nama_sortimen LIKE '%$searchText%')",null,false);
        }
        $this->db->where('deleted',0);
		$query = $this->db->count_all_results();
        return $query;
	}
        
    function list_index($limit,$offset,$searchText = '',$client_id = '')
    {
        if($client_id != ''){
            $this->db->where('client_id',$client_id);
        }
        if($searchText != ''){
            $this->db->where("(nama_supplier LIKE '%$searchText%' OR nama_kayu LIKE '%$searchText%' OR nama_sortimen LIKE '%$searchText%')",null,false);
        }
        $this->db->where('deleted',0);
        ($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
        $this->db->order_by('production_date','DESC');
        $query = $this->db->get('view_produksi');
        return $query->result();
    }
    
    
	function cek_idbuyer($idbuyer){
		$this->db->where('idbuyer',$idbuyer);
		$this->db->from('mbuyer');
		$row=$this->db->count_all_results();
		return $row;
	}
	
    function save()
	{
        $this->production_date		=	to_mysql_date($_POST['production_date']);
		$this->client_id			=	$this->session->userdata('client_id');
		$this->idproduk			    =	$_POST['idproduk'];
		$this->jumlah_produksi		=	to_mysql_number($_POST['jumlah_produksi']);
		$this->volume_produksi		=	to_mysql_number($_POST['volume_produksi']);
		$this->berat_produksi		=	to_mysql_number($_POST['berat_produksi']);
		$this->rendemen_volume		=	to_mysql_number($_POST['rendemen_volume']);
		$this->rendemen_berat		=	to_mysql_number($_POST['rendemen_berat']);
		$this->created_at		    =	now_mysql();
		$this->created_by	        =	$this->session->userdata('userid');
        
								
		$this->db->insert('produksi_head',$this);
		$id = $this->db->insert_id();
        
        $detail_supplier = $_POST['idsupplier'];
        foreach($detail_supplier as $row){
            $data = array();
			$data['idproduksi']     = $id;
			$data['idsupplier']     = $row;
            
			$this->db->insert('produksi_asal_kayu',$data);
        }
        
		$detail = json_decode($_POST['detail_value']);
		foreach($detail as $row){
			$data = array();
			$data['idproduksi']     = $id;
			$data['idsortimen']     = $row[1];
			$data['idkayu']         = $row[3];
			$data['jumlah'] 		= $row[5];
			$data['volume'] 	    = $row[7];
			$data['berat'] 	        = $row[9];
			$data['jumlah_wip'] 	= $row[11];
			$data['volume_wip']     = $row[13];
			$data['berat_wip']      = $row[15];
			$this->db->insert('produksi_detail',$data);
		}
		
		return $id;
	}

    function update()
    {
        $id                         =   $_POST['idproduksi'];
        $this->production_date		=	to_mysql_date($_POST['production_date']);
		$this->idproduk			    =	$_POST['idproduk'];
		$this->jumlah_produksi		=	to_mysql_number($_POST['jumlah_produksi']);
		$this->volume_produksi		=	to_mysql_number($_POST['volume_produksi']);
		$this->berat_produksi		=	to_mysql_number($_POST['berat_produksi']);
        $this->rendemen_volume		=	to_mysql_number($_POST['rendemen_volume']);
		$this->rendemen_berat		=	to_mysql_number($_POST['rendemen_berat']);
		$this->last_update_by	    =	$this->session->userdata('userid');
        
		
        $this->db->where('idproduksi',$id);
        $this->db->update('produksi_head',$this);


        $this->db->where('idproduksi',$id);
        $this->db->delete('produksi_asal_kayu');
		
		$detail_supplier = $_POST['idsupplier'];
        foreach($detail_supplier as $row){
            $data = array();
			$data['idproduksi']     = $id;
			$data['idsupplier']     = $row;
            
			$this->db->insert('produksi_asal_kayu',$data);
        }
        
        $this->db->where('idproduksi',$id);
        $this->db->delete('produksi_detail');
        
		$detail = json_decode($_POST['detail_value']);
		foreach($detail as $row){
			$data = array();
			$data['idproduksi']     = $id;
			$data['idsortimen']     = $row[1];
			$data['idkayu']         = $row[3];
            $data['jumlah'] 		= $row[5];
			$data['volume'] 	    = $row[7];
			$data['berat'] 	        = $row[9];
			$data['jumlah_wip'] 	= $row[11];
			$data['volume_wip']     = $row[13];
			$data['berat_wip']      = $row[15];
			$this->db->insert('produksi_detail',$data);
		}
		
		return $id;
    }
    
	
	function delete($idproduksi,$client_id = '')
    {
		$data = array('deleted' => 1);

		$this->db->where('idproduksi', $idproduksi);
		if($client_id != ''){
            $this->db->where('client_id',$client_id);
        }
		$this->db->update('produksi_head', $data); 
    }
    
    function finalize($idproduksi,$client_id = '')
    {
		$data = array('final' => 1, 'finalize_date' => date('Y-m-d H:i:s'), 'finalize_by' => $this->session->userdata('userid'));

		$this->db->where('idproduksi', $idproduksi);
		if($client_id != ''){
            $this->db->where('client_id',$client_id);
        }
		$this->db->update('produksi_head', $data); 
    }
	
	function detail($idproduksi,$client_id = '')
    {
        if($client_id != ''){
            $this->db->where('client_id',$client_id);
        }
        
        $this->db->where('idproduksi',$idproduksi);
        $query = $this->db->get('view_produksi');
        return $query->row_array();
    }
    
    function detail_list($idproduksi)
    {
        $this->db->select('produksi_detail.*,msortimen.sortimen,mjeniskayu.nama as nama_kayu,
                           stok_bahan_baku.jumlah stok_jumlah, stok_bahan_baku.volume stok_volume, stok_bahan_baku.berat stok_berat');
        $this->db->from('produksi_detail');
        $this->db->join('msortimen','msortimen.idsortimen = produksi_detail.idsortimen');
        $this->db->join('mjeniskayu','mjeniskayu.idkayu = produksi_detail.idkayu','left');
        $this->db->join('stok_bahan_baku','produksi_detail.idsortimen = stok_bahan_baku.idsortimen AND produksi_detail.idkayu = stok_bahan_baku.idkayu');
        $this->db->where('idproduksi',$idproduksi);
        //$this->db->order_by('urutan','ASC');
        $query = $this->db->get();
        return $query->result();
    }
    
    function detail_list_supplier($idproduksi)
    {
        $this->db->select('produksi_asal_kayu.*,msupplier.supplier');
        $this->db->from('produksi_asal_kayu');
        $this->db->join('msupplier','produksi_asal_kayu.idsupplier = msupplier.idsupplier');
        $this->db->where('idproduksi',$idproduksi);
        //$this->db->order_by('urutan','ASC');
        $query = $this->db->get();
        return $query->result();
    }
	
}


