<?php
class Bahan_baku_model extends CI_Model {


    function __construct()
    {
        parent::__construct();
    }
	function count($client_id = '')
	{
		$this->db->select('stok_bahan_baku.*,msortimen.sortimen,mjeniskayu.nama nama_jenis_kayu');
		$this->db->from('stok_bahan_baku');
		$this->db->join('msortimen','stok_bahan_baku.idsortimen = msortimen.idsortimen');
		$this->db->join('mjeniskayu','mjeniskayu.idkayu = stok_bahan_baku.idkayu','left');
        if($client_id != ''){
            $this->db->where('stok_bahan_baku.client_id',$client_id);
        }
		$query = $this->db->count_all_results();
        return $query;
	}
    function count_search($textsearch,$client_id = '')
	{
		$this->db->select('stok_bahan_baku.*,msortimen.sortimen,mjeniskayu.nama nama_jenis_kayu');
		$this->db->from('stok_bahan_baku');
		$this->db->join('msortimen','stok_bahan_baku.idsortimen = msortimen.idsortimen');
		$this->db->join('mjeniskayu','mjeniskayu.idkayu = stok_bahan_baku.idkayu','left');
        if($client_id != ''){
            $this->db->where('stok_bahan_baku.client_id',$client_id);
        }
        $this->db->where("(msortimen.sortimen LIKE '%$textsearch%' OR mjeniskayu.nama LIKE '%$textsearch%')");
		$query = $this->db->count_all_results();
        return $query;
	}
	function cek_idsortimen($idsortimen){
		$this->db->where('idsortimen',$idsortimen);
		$this->db->from('msortimen');
		$row=$this->db->count_all_results();
		return $row;
	}
	function insert($client_id = '')
    {	
        $this->idsortimen   = $_POST['idsortimen']; 
		$this->client_id    = ($client_id != '')? $client_id :  $_POST['idproduk']; 
		$this->sortimen     = $_POST['sortimen']; 
		$this->status   = (isset($_POST['status']))? 1 : 0 ;
		
		if($this->db->insert('msortimen', $this)){
			return true;
		}else{	
			$this->error_message = "Penyimpanan Gagal";
			return false;
		}
    }
    function update()
    {
		$this->idsortimen   = $_POST['idsortimen']; 
		$this->sortimen     = $_POST['sortimen']; 
        $this->status       = (isset($_POST['status']))? 1 : 0 ;
		
		if($this->db->update('msortimen', $this, array('idsortimen' => $_POST['idsortimen']))){
			return true;
		}else{
			$this->error_message = "Penyimpanan Gagal";
			return false;
		}
    }
	
	function delete($id,$client_id = '')
    {
		$data = array('deleted' => 1);
		$this->db->where('idsortimen', $id);
		if($client_id != ''){
            $this->db->where('client_id',$client_id);
        }
		$this->db->update('msortimen', $data); 
    }
	
	function bahan_baku_list($limit,$offset,$client_id = '')
    {
		$this->db->select('stok_bahan_baku.*,msortimen.sortimen,mjeniskayu.nama nama_jenis_kayu');
		$this->db->from('stok_bahan_baku');
		$this->db->join('msortimen','stok_bahan_baku.idsortimen = msortimen.idsortimen');
		$this->db->join('mjeniskayu','mjeniskayu.idkayu = stok_bahan_baku.idkayu','left');
        if($client_id != ''){
            $this->db->where('stok_bahan_baku.client_id',$client_id);
        }
		$this->db->order_by('msortimen.sortimen','ASC');
		($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
		$query = $this->db->get(); 	
		return $query->result();
    }
    
    
    function bahan_baku_search($textsearch,$limit,$offset,$client_id = '')
    {
		$this->db->select('stok_bahan_baku.*,msortimen.sortimen,mjeniskayu.nama nama_jenis_kayu');
		$this->db->from('stok_bahan_baku');
		$this->db->join('msortimen','stok_bahan_baku.idsortimen = msortimen.idsortimen');
		$this->db->join('mjeniskayu','mjeniskayu.idkayu = stok_bahan_baku.idkayu','left');
        if($client_id != ''){
            $this->db->where('stok_bahan_baku.client_id',$client_id);
        }
        $this->db->where("(msortimen.sortimen LIKE '%$textsearch%' OR mjeniskayu.nama LIKE '%$textsearch%')");
		$this->db->order_by('msortimen.sortimen','ASC');
		($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
		$query = $this->db->get(); 	
		return $query->result();
    }
	
	function detail($id,$client_id = '')
    {
		$this->db->select('msortimen.*');    
		$this->db->from('msortimen');
		$this->db->where('idsortimen',$id);
         if($client_id != ''){
            $this->db->where('client_id',$client_id);
        }
		$query = $this->db->get();
        return $query->row();
    }
	
	
}

?>