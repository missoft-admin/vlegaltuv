<?php
class Buyer_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
	
	function count($client_id='')
	{
		$this->db->from('mbuyer');
		if ($client_id != ''){
			$this->db->where('mbuyer.client_id',$client_id);
		}
		$this->db->where('deleted','0');
		$query = $this->db->count_all_results();
        return $query;
	}
    
    function count_search($textsearch,$client_id)
	{
		$this->db->from('mbuyer');
		if ($client_id != ''){
			$this->db->where('mbuyer.client_id',$client_id);
		}
        $this->db->where("(mbuyer.alamat LIKE '%$textsearch%' OR mbuyer.buyer LIKE '%$textsearch%' OR mbuyer.idnegara LIKE '%$textsearch%')",null,false);
        $this->db->where('deleted','0');
		$query = $this->db->count_all_results();
        return $query;
	}
    
	function cek_idbuyer($idbuyer){
		$this->db->where('idbuyer',$idbuyer);
		$this->db->from('mbuyer');
		$row=$this->db->count_all_results();
		return $row;
	}
    
	function insert()
    {	
		$this->client_id    = $_POST['client_id']; 
		$this->idnegara     = $_POST['idnegara']; 
		$this->buyer        = $_POST['buyer']; 
		$this->ket          = $_POST['ket']; 
		$this->alamat       = $_POST['alamat']; 
		$this->status       = (isset($_POST['status']))?  1 : 0 ;
		
        if($this->db->insert('mbuyer', $this)){
			return true;
		}else{	
			$this->error_message = "Penyimpanan Gagal";
			return false;
		}
    }

    function update()
    {
		$this->client_id    = $_POST['client_id']; 
		$this->idnegara     = $_POST['idnegara'];
		$this->buyer        = $_POST['buyer']; 
		$this->alamat       = $_POST['alamat']; 
		$this->ket          = $_POST['ket']; 
		$this->status       = (isset($_POST['status']))?  1 : 0 ;
		
		if($this->db->update('mbuyer', $this, array('idbuyer' => $_POST['idbuyer']))){
			return true;
		}else{
			$this->error_message = "Penyimpanan Gagal";
			return false;
		}
    }
	
	function delete($id)
    {
		$data = array('deleted' => 1);
		$this->db->where('idbuyer', $id);
		$this->db->update('mbuyer', $data); 
    }
	
	function buyer_list($limit,$offset,$client_id='')
    {
		if ($client_id != ''){
			$this->db->where('mbuyer.client_id',$client_id);
		}
		$this->db->select('mbuyer.*,clients.nama');
		$this->db->from('mbuyer');
		$this->db->join('clients','clients.id = mbuyer.client_id','LEFT');
		$this->db->where('mbuyer.deleted','0');
		$this->db->order_by('idnegara','ASC');
		($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
		$query = $this->db->get(); 	
		//print_r($this->db->last_query());exit();
		return $query->result();
    }
    function negara_list()
    {
		$this->db->select('idnegara,negara');
		$this->db->from('mnegara');
		$this->db->order_by('negara','ASC');		
		$query = $this->db->get(); 	
		return $query->result();
    }
    
    function buyer_search($textsearch,$limit,$offset,$client_id='')
    {
		$this->db->select('mbuyer.*,clients.nama');
		$this->db->from('mbuyer');
		if ($client_id != ''){
			$this->db->where('mbuyer.client_id',$client_id);
		}
		$this->db->join('clients','clients.id = mbuyer.client_id','LEFT');
        $this->db->where("(mbuyer.alamat LIKE '%$textsearch%' OR mbuyer.buyer LIKE '%$textsearch%' OR mbuyer.idnegara LIKE '%$textsearch%')",null,false);
		$this->db->where('mbuyer.deleted','0');
        $this->db->order_by('mbuyer.idnegara','ASC');
		($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
		$query = $this->db->get(); 	
		return $query->result();
    }
	
	function detail($id,$client_id='')
    {
		$this->db->select('mbuyer.*,mnegara.negara,clients.nama as client_nama');    
		$this->db->from('mbuyer');
		$this->db->join('mnegara','mnegara.idnegara=mbuyer.idnegara');
		$this->db->join('clients','clients.id = mbuyer.client_id','LEFT');
		$this->db->where('idbuyer',$id);
        if ($client_id != ''){
			$this->db->where('mbuyer.client_id',$client_id);
            $this->db->where('deleted','0');
		}
		$query = $this->db->get();
        return $query->row();
    }
	
	
	function neg_findajax($qsearch)
    {
		$this->db->like('negara',$qsearch);
        $this->db->or_like('idnegara',$qsearch);
		$this->db->order_by('idnegara','DESC');
		$query = $this->db->get('mnegara');	
		return $query->result_array();
		
    }
	
}

?>