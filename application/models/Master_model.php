<?php
class Master_model extends CI_Model {
    function __construct()
    {
        parent::__construct();
    }
	
	function get_list_supplier($qsearch='',$client_id = '',$arr = false)
	{
        if($client_id != ''){
            $this->db->where('client_id',$client_id);
        }
        
        if($qsearch != ''){
            $this->db->like('supplier',$qsearch);
        }
        
        $this->db->where('deleted',0);
        
        $this->db->order_by('supplier');
		$query = $this->db->get('msupplier');
        
        if($arr){
            return $query->result_array();
        }else{
            return $query->result();
        }
	}
    function get_list_produk($qsearch='',$client_id = '',$arr = false)
	{
        if($client_id != ''){
            $this->db->where('client_id',$client_id);
        }
        
        if($qsearch != ''){
            $this->db->like('produk',$qsearch);
        }
        
        $this->db->where('deleted',0);
        $this->db->limit(20);
        $this->db->order_by('produk');
		$query = $this->db->get('mproduk');
        
        if($arr){
            return $query->result_array();
        }else{
            return $query->result();
        }
	}
    function get_produk_hscode($idproduk)
	{
        $this->db->select('mproduk.kodehs,ikp.id kapasitas_id,ikp.berlaku_dari,ikp.berlaku_sampai,ikp.kapasitas kapasitas_produksi, saldo saldo_kapasitas');
        $this->db->from('mproduk');
        $this->db->join('izin_kapasitas_produksi ikp','ikp.kodehs = mproduk.kodehs','left');
        $this->db->where('idproduk',$idproduk);
        $this->db->where('deleted',0);
		$query = $this->db->get();
        return $query->row();
	}
    function get_list_client($qsearch='',$arr = false)
	{        
		if($qsearch != ''){
            $this->db->like('nama',$qsearch);
        }
        
        $this->db->order_by('nama');
		$query = $this->db->get('clients');
        
        if($arr){
            return $query->result_array();
        }else{
            return $query->result();
        }
	}
    
    function get_list_hutan($qsearch='',$arr = false)
	{
        if($qsearch != ''){
            $this->db->like('hutan',$qsearch);
        }
        
        $this->db->order_by('hutan');
		$query = $this->db->get('mhutan');
        
        if($arr){
            return $query->result_array();
        }else{
            return $query->result();
        }
	}
    
    function get_list_sortimen($qsearch='',$client_id = '',$arr = false)
	{
        if($client_id != ''){
            $this->db->where('client_id',$client_id);
        }
        
        if($qsearch != ''){
            $this->db->like('sortimen',$qsearch);
        }
        $this->db->where('deleted',0);
        
        $this->db->order_by('sortimen');
		$query = $this->db->get('msortimen');
        
        if($arr){
            return $query->result_array();
        }else{
            return $query->result();
        }
	}
    
    function get_list_jenis_dokumen($qsearch='',$arr = false)
	{
        if($qsearch != ''){
            $this->db->like('dok',$qsearch);
        }
        $this->db->where('status','1');
        $this->db->order_by('dok');
		$query = $this->db->get('mjenisdokumen');
        
        if($arr){
            return $query->result_array();
        }else{
            return $query->result();
        }
	}
    
    function get_list_sertifikasi($qsearch='',$arr = false)
	{
        if($qsearch != ''){
            $this->db->like('deskripsi',$qsearch);
        }
        $this->db->where('st_aktif','1');
        $this->db->where('jenis','5');
        $this->db->order_by('id');
		$query = $this->db->get('simplefield');
        
        if($arr){
            return $query->result_array();
        }else{
            return $query->result();
        }
	}
    
    function get_list_provinsi($qsearch='',$arr = false)
	{
        if($qsearch != ''){
            $this->db->like('nama_provinsi',$qsearch);
        }
        
        $this->db->order_by('nama_provinsi');
		$query = $this->db->get('provinsi');
        
        if($arr){
            return $query->result_array();
        }else{
            return $query->result();
        }
	}
    
    function get_kode_provinsi($provinsi_id)
    {
        $this->db->select('LEFT(kode,2) kode',false);
        $this->db->where('id',$provinsi_id);
        $query = $this->db->get('provinsi');
        return $query->row('kode');
    }

    function get_list_kota($qsearch='',$provinsi_id='',$arr = false)
	{
        $kode_prov = $this->get_kode_provinsi($provinsi_id);
        if($qsearch != ''){
            $this->db->like('nama_kota',$qsearch);
        }
        
        if($provinsi_id != ''){
            $this->db->like('kode',$kode_prov,'after');
        }
        
        $this->db->order_by('nama_kota');
		$query = $this->db->get('kota');
        
        if($arr){
            return $query->result_array();
        }else{
            return $query->result();
        }
	}
    
    function get_list_negara($qsearch='',$arr = false)
	{
        if($qsearch != ''){
            $this->db->like('negara',$qsearch);
            $this->db->or_like('idnegara',$qsearch);
        }
        
        $this->db->order_by('negara');
		$query = $this->db->get('mnegara');
        
        if($arr){
            return $query->result_array();
        }else{
            return $query->result();
        }
	}
    
    function get_list_negara_token($qsearch='',$arr = false)
	{
        $this->db->select('idnegara id,negara name');
        if($qsearch != ''){
            $this->db->like('negara',$qsearch);
            $this->db->or_like('idnegara',$qsearch);
        }
        
        $this->db->order_by('negara');
		$query = $this->db->get('mnegara');
        
        if($arr){
            return $query->result_array();
        }else{
            return $query->result();
        }
	}
    
    function get_list_jenis_kayu($qsearch='',$arr = false)
	{
        if($qsearch != ''){
            $this->db->like('nama',$qsearch);
        }
        
        $this->db->order_by('nama');
		$query = $this->db->get('mjeniskayu');
        
        if($arr){
            return $query->result_array();
        }else{
            return $query->result();
        }
	}
    
    function get_list_jenis_kayu_token($qsearch='',$arr = false)
	{
        $this->db->select('idkayu id,nama name');
        if($qsearch != ''){
            $this->db->like('nama',$qsearch);
        }
        
        $this->db->order_by('nama');
		$query = $this->db->get('mjeniskayu');
        
        if($arr){
            return $query->result_array();
        }else{
            return $query->result();
        }
	}
    
    function get_list_paper($client_id,$qsearch='',$arr = false)
	{
        $this->db->select('id,paper_number name');
        $this->db->where('client_id',$client_id);
        $this->db->where('st_used',0);
        if($qsearch != ''){
            $this->db->like('paper_number',$qsearch,'after');
        }
        
        $this->db->order_by('paper_number');
		$query = $this->db->get('vlegal_paper');
        
        if($arr){
            return $query->result_array();
        }else{
            return $query->result();
        }
	}
    
    function get_list_jenis_kayu_bahan_baku($idsortimen,$qsearch='',$arr = false)
	{
        $this->db->select('mjeniskayu.*');
        $this->db->from('stok_bahan_baku');
        $this->db->join('mjeniskayu','stok_bahan_baku.idkayu = mjeniskayu.idkayu');
        $this->db->where('stok_bahan_baku.idsortimen',$idsortimen);
        
        if($qsearch != ''){
            $this->db->like('mjeniskayu.nama',$qsearch);
        }
        
        $this->db->order_by('mjeniskayu.nama');
		$query = $this->db->get();
        
        if($arr){
            return $query->result_array();
        }else{
            return $query->result();
        }
	}
    
    function get_stok_bahan_baku($idsortimen,$idkayu,$arr = false)
	{
        $this->db->select('stok_bahan_baku.*');
        $this->db->from('stok_bahan_baku');
        $this->db->where('stok_bahan_baku.idsortimen',$idsortimen);
        
        if($idkayu != '' &&  !is_null($idkayu)){
            $this->db->where('stok_bahan_baku.idkayu',$idkayu);
        }
        
		$query = $this->db->get();
        
        if($arr){
            return $query->row_array();
        }else{
            return $query->row();
        }
	}
    
    function get_list_satuan($jenis_satuan = '',$arr = false)
    {
        if($jenis_satuan != ''){
            $this->db->where('satuan_jenis',$jenis_satuan);
        }
        $this->db->where('satuan_status',1);
        $this->db->order_by('satuan_id');
		$query = $this->db->get('msatuan');
        
        if($arr){
            return $query->result_array();
        }else{
            return $query->result();
        }
    }
    
    function get_list_pengeluaran($qsearch='',$arr = false)
	{
        if($qsearch != ''){
            $this->db->like('deskripsi',$qsearch);
        }
        $this->db->where('jenis','6');
        $this->db->where('st_aktif','1');
        $this->db->order_by('id');
		$query = $this->db->get('simplefield');
        
        if($arr){
            return $query->result_array();
        }else{
            return $query->result();
        }
	}
    
    function get_nama_client($id)
    {
		//print_r($id);exit();
		$this->db->select('clients.nama');    
		$this->db->from('clients');
		
		$query = $this->db->get();
		//print_r($query->row());exit();
        return $query->row('nama');
    }
    
    function get_list_buyer($qsearch='',$client_id = '',$arr = false)
	{
        if($qsearch != ''){
            $this->db->like('buyer',$qsearch);
        }
        
        if($client_id != ''){
            $this->db->where('client_id',$client_id);
        }
        $this->db->where('status','1');
        $this->db->where('deleted',0);
        $this->db->order_by('buyer');
        $this->db->limit(20,0);
		$query = $this->db->get('mbuyer');
        
        if($arr){
            return $query->result_array();
        }else{
            return $query->result();
        }
	}
    
    function get_buyer_detail($idbuyer,$arr = false)
    {
		$this->db->select('mbuyer.*,mnegara.negara country_name');    
		$this->db->from('mbuyer');
		$this->db->join('mnegara','mbuyer.idnegara = mnegara.idnegara','left');
		$this->db->where('idbuyer',$idbuyer);
		$query = $this->db->get();
		 if($arr){
            return $query->row_array();
        }else{
            return $query->row();
        }
    }
    
    
    function get_list_valuta($qsearch='',$arr = false)
	{
        if($qsearch != ''){
            $this->db->like('kode',$qsearch);
            $this->db->or_like('nama',$qsearch);
        }
        $this->db->order_by('kode');
        $this->db->limit(20,0);
		$query = $this->db->get('mmatauang');
        
        if($arr){
            return $query->result_array();
        }else{
            return $query->result();
        }
	}
    
    function get_list_loading($qsearch='',$arr = false)
	{
        if($qsearch != ''){
            $this->db->like('kode',$qsearch);
            $this->db->or_like('uraian',$qsearch);
        }
        $this->db->order_by('uraian');
		$query = $this->db->get('mpelabuhan');
        
        if($arr){
            return $query->result_array();
        }else{
            return $query->result();
        }
	}
    
    function get_list_discharge($qsearch='',$idnegara,$arr = false)
	{
        if($qsearch != ''){
           
            $this->db->like('uraian',$qsearch);
        }
         $this->db->like('kode',$idnegara,'after');
        $this->db->order_by('uraian');
        $this->db->limit(20,0);
		$query = $this->db->get('mdischarge');
        
        if($arr){
            return $query->result_array();
        }else{
            return $query->result();
        }
	}
    
    function get_client_vlegal($client_id,$arr = false)
    {
        $this->db->select('etpik,npwp,client_sertifikat,client_stuffing.stuffing_data');
        $this->db->from('clients');
        $this->db->join('(SELECT * FROM client_stuffing ORDER BY client_id,index_stuffing) client_stuffing','client_stuffing.client_id = clients.id');
        $this->db->where('clients.id',$client_id);
        $query = $this->db->get();
        
        if($arr){
            return $query->row_array();
        }else{
            return $query->row();
        }
    }
    
    function get_list_product($qsearch='',$client_id='',$arr = false)
	{
        if($qsearch != ''){
            $this->db->like('produk',$qsearch,'after');
            //$this->db->or_like('kodehs',$qsearch);
        }
        
        if($client_id != ''){
            $this->db->where('client_id',$client_id);
        }
        //$this->db->from('mproduk');
        $this->db->order_by('produk');
        $this->db->limit(5,0);
        //print_r($this->db->get_compiled_select());exit();
		$query = $this->db->get('mproduk');
        
        if($arr){
            return $query->result_array();
        }else{
            return $query->result();
        }
	}
    
    function get_deklarasi_import($qsearch='',$client_id='',$arr = false)
	{
        $this->db->select('deklarasi_import.id,no_deklarasi_import');
        if($qsearch != ''){
            $this->db->like('no_deklarasi_import',$qsearch,'after');
        }
        
        if($client_id != ''){
            $this->db->where('clients.id',$client_id);
        }
        $this->db->from('deklarasi_import');
        $this->db->join('clients','deklarasi_import.idgrup = clients.idgrup');
        $this->db->order_by('no_deklarasi_import');
		$query = $this->db->get();
        
        if($arr){
            return $query->result_array();
        }else{
            return $query->result();
        }
	}
    
    function get_deklarasi_import_detail($id,$arr = false)
	{
        $this->db->select("no_deklarasi_import,DATE_FORMAT(berlaku_dari,'%d-%m-%Y') berlaku_dari,DATE_FORMAT(berlaku_sampai,'%d-%m-%Y') berlaku_sampai,kuota");
        $this->db->where('id',$id);
		$query = $this->db->get('deklarasi_import');
        
         if($arr){
            return $query->row_array();
        }else{
            return $query->row();
        }
	}
    
    function get_hscode($idproduk)
	{
        $this->db->where('idproduk',$idproduk);
        $this->db->select('kodehs');
		$query = $this->db->get('mproduk');
        
        if($query->num_rows() > 0){
            return $query->row('kodehs');
        }else{
            return '';
        }
	}
    
    function get_list_pejabat_ttd()
    {
        $this->db->where('xstatus','1');
        $query = $this->db->get('vlegal_pejabatttd');
        return $query->result();
    }
    
    function get_pejabat_ttd($kode_pejabat)
    {
        $this->db->where('kode_pejabat',$kode_pejabat);
        $query = $this->db->get('vlegal_pejabatttd');
        return $query->row();
    }
    
    function get_email_content($key)
    {
        $this->db->where('key',$key);
        $query = $this->db->get('email_contents');
        return $query->row('content');
    }
    

    
}

?>