<?php
class Matauang_model extends CI_Model {

    var $nama	= '';    

    function __construct()
    {
        parent::__construct();
    }
	
	function count()
	{
		$this->db->from('mmatauang');
		$query = $this->db->count_all_results();
        return $query;
	}
    
    function count_search($textsearch)
	{
		$this->db->from('mmatauang');
        $this->db->like('kode',$textsearch);
        $this->db->or_like('nama',$textsearch);
        $this->db->or_like('iduang',$textsearch);
		$query = $this->db->count_all_results();
        return $query;
	}
	function cek_iduang($iduang){
		$this->db->where('iduang',$iduang);
		$this->db->from('mmatauang');
		$row=$this->db->count_all_results();
		//print_r($row);exit();
		return $row;
	}
	function insert()
    {	
		$this->iduang = $_POST['iduang']; 
		$this->nama = $_POST['nama']; 
		$this->kode = $_POST['kode']; 
		
		//$this->xstatus = $_POST['xstatus']; 
		//print_r($this);exit();
		if($this->db->insert('mmatauang', $this)){
			return true;
		}else{	
			$this->error_message = "Penyimpanan Gagal";
			return false;
		}
    }

    function update()
    {
		$this->iduang = $_POST['iduang']; 
		$this->nama = $_POST['nama']; 
		$this->kode = $_POST['kode']; 
		
		$this->iduang = $_POST['iduang']; 
		
		if($this->db->update('mmatauang', $this, array('iduang' => $_POST['iduang']))){
			return true;
		}else{
			$this->error_message = "Penyimpanan Gagal";
			return false;
		}
    }
	
	function delete($id)
    {		
		$this->db->where('iduang', $id);
		$this->db->delete('mmatauang'); 
		return true;
    }
	
	function matauang_list($limit,$offset)
    {
		$this->db->select('mmatauang.*');
		$this->db->from('mmatauang');
		$this->db->order_by('iduang','ASC');
		($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
		$query = $this->db->get(); 	
		//print_r($query->result());exit();
		return $query->result();
    }
    
    
    function matauang_search($textsearch,$limit,$offset)
    {
		$this->db->select('mmatauang.*');
		$this->db->from('mmatauang');
        $this->db->like('kode',$textsearch);
        $this->db->or_like('nama',$textsearch);
        $this->db->or_like('iduang',$textsearch);
		$this->db->order_by('iduang','ASC');
		($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
		$query = $this->db->get(); 	
		//print_r($this->db->last_query());exit();
		return $query->result();
    }
	
	function detail($id)
    {
		//print_r($id);exit();
		$this->db->select('mmatauang.*');    
		$this->db->from('mmatauang');
		$this->db->where('iduang',$id);
		$query = $this->db->get();
		//print_r($query->row());exit();
        return $query->row();
    }
	
	function neg_findajax($qsearch)
    {
		$this->db->like('negara',$qsearch);
        $this->db->or_like('iduang',$qsearch);
		$this->db->order_by('iduang','DESC');
		$query = $this->db->get('mnegara');	
		return $query->result_array();
		
    }
	
}

?>