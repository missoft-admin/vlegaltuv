<?php
class Peb_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
	
	function count($client_id='')
	{
		$this->db->from('ekspor_peb');
		if($client_id != ''){
            $this->db->where('client_id',$client_id);
        }
		$query = $this->db->count_all_results();
        return $query;
	}
    
    function count_approval($status)
	{
		$this->db->from('ekspor_peb');
		$this->db->join("clients","ekspor_peb.client_id = clients.id");
		$this->db->join("ekspor_detail","ekspor_peb.idekspor = ekspor_detail.idekspor");
		$this->db->where('ekspor_peb.status',$status);
        $this->db->group_by("ekspor_peb.id");
		$query = $this->db->count_all_results();
        return $query;
	}
    
    function count_search($textsearch,$client_id='')
	{
		$this->db->from('ekspor_peb');
        if($client_id != ''){
            $this->db->where('client_id',$client_id);
        }
        $this->db->like('no_vlegal',$textsearch);
		$query = $this->db->count_all_results();
        return $query;
	}
     function count_approval_search($status,$textsearch)
	{
		$this->db->from('ekspor_peb');
		$this->db->join("clients","ekspor_peb.client_id = clients.id");
		$this->db->join("ekspor_detail","ekspor_peb.idekspor = ekspor_detail.idekspor");
		$this->db->where('ekspor_peb.status',$status);
        $this->db->like('no_vlegal',$textsearch);
        $this->db->group_by("ekspor_peb.id");
		$query = $this->db->count_all_results();
        return $query;
	}
	function cek_idnegara($idnegara){
		$this->db->where('idnegara',$idnegara);
		$this->db->from('mnegara');
		$row=$this->db->count_all_results();
		//print_r($row);exit();
		return $row;
	}
	function insert($client_id)
    {	
		$this->vid          = $this->input->post('vid'); 
		$this->no_vlegal    = $this->input->post('no_vlegal'); 
		$this->tgl_vlegal   = to_mysql_date($this->input->post('tgl_vlegal')); 
		$this->client_id    = $client_id; 
		$this->nomor_peb    = $this->input->post('nomor_peb'); 
		$this->tgl_peb      = to_mysql_date($this->input->post('tgl_peb')); 
		$this->stuffing     = $this->input->post('stuffing'); 
		$this->volume       = to_mysql_number($this->input->post('volume')); 
		$this->netto        = to_mysql_number($this->input->post('netto')); 
		$this->jumlah       = to_mysql_number($this->input->post('jumlah')); 
		$this->nilai        = to_mysql_number($this->input->post('nilai')); 
		$this->nilai_usd    = to_mysql_number($this->input->post('nilai_usd')); 
		$this->created_at   = now_mysql();
		$this->created_by   = $this->session->userdata('client_userid');
        
		       		
		if($this->db->insert('ekspor_peb', $this)){
			return true;
		}else{	
			$this->error_message = "Penyimpanan Gagal";
			return false;
		}
    }

    function update()
    {
		
        $this->vid          = $this->input->post('vid'); 
		$this->no_vlegal    = $this->input->post('no_vlegal'); 
		$this->tgl_vlegal   = to_mysql_date($this->input->post('tgl_vlegal')); 
		$this->nomor_peb    = $this->input->post('nomor_peb'); 
		$this->tgl_peb      = to_mysql_date($this->input->post('tgl_peb')); 
		$this->stuffing     = $this->input->post('stuffing'); 
		$this->volume       = to_mysql_number($this->input->post('volume')); 
		$this->netto        = to_mysql_number($this->input->post('netto')); 
		$this->jumlah       = to_mysql_number($this->input->post('jumlah')); 
		$this->nilai        = to_mysql_number($this->input->post('nilai')); 
		$this->nilai_usd    = to_mysql_number($this->input->post('nilai_usd')); 
		$this->modified_at   = now_mysql();
		$this->modified_by   = $this->session->userdata('client_userid');
		
		if($this->db->update('ekspor_peb', $this, array('id' =>$this->input->post('id')))){
			return true;
		}else{
			$this->error_message = "Penyimpanan Gagal";
			return false;
		}
    }
	
	function delete($id)
    {
		$this->db->where('id', $id);
		$this->db->delete('ekspor_peb'); 
    }
    
    function approve($id)
    {
		$this->db->where('id', $id);
		$this->db->update('ekspor_peb',array('status' => 1)); 
    }
    
    function cancel($id)
    {
		$this->db->where('id', $id);
		$this->db->update('ekspor_peb',array('status' => 0)); 
    }
	
	function peb_list($limit,$offset,$client_id='')
    {
		$this->db->select('ekspor_peb.*');
		$this->db->from('ekspor_peb');
        if($client_id != ''){
            $this->db->where('client_id',$client_id);
        }
		$this->db->order_by('id','DESC');
		($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
		$query = $this->db->get(); 	
		return $query->result();
    }
    
    function peb_search($textsearch,$limit,$offset,$client_id = '')
    {
		$this->db->select('ekspor_peb.*');
		$this->db->from('ekspor_peb');
        if($client_id != ''){
            $this->db->where('client_id',$client_id);
        }
        $this->db->like('no_vlegal',$textsearch);
		$this->db->order_by('id','DESC');
		($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
		$query = $this->db->get(); 	
		return $query->result();
    }
    
    function peb_approval_list($status,$limit,$offset)
    {
		$this->db->select('ekspor_peb.*,clients.nama,clients.alamat,SUM(ekspor_detail.volume) v_legal_volume,SUM(ekspor_detail.berat) v_legal_netto,SUM(ekspor_detail.jumlah) v_legal_jumlah,SUM(ekspor_detail.nilai) v_legal_nilai');
		$this->db->from('ekspor_peb');
		$this->db->join("clients","ekspor_peb.client_id = clients.id");
		$this->db->join("ekspor_detail","ekspor_peb.idekspor = ekspor_detail.idekspor");
		$this->db->where('ekspor_peb.status',$status);
        $this->db->group_by("ekspor_peb.id");
		$this->db->order_by('id','ASC');
		($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
		$query = $this->db->get(); 	
		return $query->result();
    }
    
    function peb_approval_search($status,$textsearch,$limit,$offset)
    {
		$this->db->select('ekspor_peb.*,clients.nama,clients.alamat,SUM(ekspor_detail.volume) v_legal_volume,SUM(ekspor_detail.berat) v_legal_netto,SUM(ekspor_detail.jumlah) v_legal_jumlah,SUM(ekspor_detail.nilai) v_legal_nilai');
		$this->db->from('ekspor_peb');
		$this->db->join("clients","ekspor_peb.client_id = clients.id");
		$this->db->join("ekspor_detail","ekspor_peb.idekspor = ekspor_detail.idekspor");
		$this->db->where('ekspor_peb.status',$status);
        $this->db->like('no_vlegal',$textsearch);
        $this->db->group_by("ekspor_peb.id");
		$this->db->order_by('id','ASC');
		($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
		$query = $this->db->get(); 	
		return $query->result();
    }
	
	function detail($id)
    {
		$this->db->select('ekspor_peb.*');    
		$this->db->from('ekspor_peb');
		$this->db->where('id',$id);
		$query = $this->db->get();
        return $query->row();
    }
	
	function prov_findajax($qsearch)
    {
		$this->db->where('nama_provinsi LIKE',$qsearch.'%');
		$this->db->order_by('id','DESC');
		$query = $this->db->get('provinsi');	
		return $query->result_array();
		
    }
    
    function get_list_vlegal($qsearch,$client_id = '')
    {
		$this->db->select('vid,no_dokumen');
        if($client_id != ''){
            $this->db->where('clientid',$client_id);
        }
        $this->db->like('no_dokumen',$qsearch,'after');
        $this->db->where('status','send');
        $this->db->where('peb_status',0);
        $this->db->limit(10);
		$this->db->order_by('no_dokumen','ASC');
		$query = $this->db->get('vlegal_status');	
		return $query->result_array();
    }
    
    
    function get_vlegal_detail($vid)
    {
		$this->db->select('postdate');
        $this->db->where('vid',$vid);
		$query = $this->db->get('vlegal_status');	
		return human_date_short($query->row('postdate'));
    }
    
    function get_list_stuffing($qsearch,$client_id = '')
    {
		$this->db->select('stuffing');
        if($client_id != ''){
            $this->db->where('client_id',$client_id);
        }
        $this->db->like('stuffing',$qsearch);
        $this->db->where('status_liu','send');
		$this->db->order_by('stuffing','ASC');
		$this->db->group_by('stuffing');
		$query = $this->db->get('ekspor');	
		return $query->result_array();
    }
    
	
}

?>