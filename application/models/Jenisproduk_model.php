<?php
class Jenisproduk_model extends CI_Model {

    var $jenis_produk	= '';    

    function __construct()
    {
        parent::__construct();
    }
	
	function count()
	{
		$this->db->from('mjenisproduk');
		$this->db->where('xstatus','1');
		$query = $this->db->count_all_results();
        return $query;
	}
    
    function count_search($textsearch)
	{
		$this->db->from('mjenisproduk');
        $this->db->or_like('jenis_produk',$textsearch);
		$query = $this->db->count_all_results();
        return $query;
	}
	function cek_idjenisproduk($idjenisproduk){
		$this->db->where('idjenisproduk',$idjenisproduk);
		$this->db->from('mjenisproduk');
		$row=$this->db->count_all_results();
		//print_r($row);exit();
		return $row;
	}
	function insert()
    {	
		$this->jenis_produk = $_POST['jenis_produk']; 
		$this->satuan = $_POST['satuan']; 
		if (isset($_POST['xstatus'])) {
		  $this->xstatus = 1;
		} else {
		  $this->xstatus = 0;
		}
		//$this->xstatus = $_POST['xstatus']; 
		//print_r($this);exit();
		if($this->db->insert('mjenisproduk', $this)){
			return true;
		}else{	
			$this->error_message = "Penyimpanan Gagal";
			return false;
		}
    }

    function update()
    {
		$this->jenis_produk = $_POST['jenis_produk']; 
		$this->satuan = $_POST['satuan']; 
		if (isset($_POST['xstatus'])) {
		  $this->xstatus = 1;
		} else {
		  $this->xstatus = 0;
		}
		$this->idjenisproduk = $_POST['idjenisproduk']; 
		
		if($this->db->update('mjenisproduk', $this, array('idjenisproduk' => $_POST['idjenisproduk']))){
			return true;
		}else{
			$this->error_message = "Penyimpanan Gagal";
			return false;
		}
    }
	
	function delete($id)
    {
		$data = array(
               'xstatus' =>0
            );

		$this->db->where('idjenisproduk', $id);
		$this->db->update('mjenisproduk', $data); 
		/* $this->xstatus = '0'; 
		//$this->kayu = $_POST['jenis_produk']; 
		//print_r($id);exit();
		$this->db->update('mjenisproduk', $this, array('idjenisproduk' => $id)); */
    }
	
	function jenisproduk_list($limit,$offset)
    {
		$this->db->select('mjenisproduk.*,msatuan.satuan_kode');
		$this->db->from('mjenisproduk');
		$this->db->join('msatuan','msatuan.satuan_id=mjenisproduk.satuan');
		$this->db->where('xstatus','1');
		$this->db->order_by('jenis_produk','ASC');
		($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
		$query = $this->db->get(); 	
		//print_r($query->result());exit();
		return $query->result();
    }
    function satuan_list()
    {
		$this->db->select('msatuan.*');
		$this->db->from('msatuan');
		$this->db->where('satuan_status','1');
		$this->db->order_by('satuan_kode','ASC');
		$query = $this->db->get(); 	
		//print_r($query->result());exit();
		return $query->result();
    }
    
    function jenisproduk_search($textsearch,$limit,$offset)
    {
		$this->db->select('mjenisproduk.*,msatuan.satuan_kode');
		$this->db->from('mjenisproduk');
		$this->db->join('msatuan','msatuan.satuan_id=mjenisproduk.satuan');
        $this->db->or_like('jenis_produk',$textsearch);
		$this->db->order_by('jenis_produk','ASC');
		($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
		$query = $this->db->get(); 	
		return $query->result();
    }
	
	function detail($id)
    {
		//print_r($id);exit();
		$this->db->select('mjenisproduk.*');    
		$this->db->from('mjenisproduk');
		$this->db->where('idjenisproduk',$id);
		$query = $this->db->get();
		//print_r($query->row());exit();
        return $query->row();
    }
	
		
}

?>