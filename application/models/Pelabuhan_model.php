<?php
class Pelabuhan_model extends CI_Model {

    var $uraian	= '';    

    function __construct()
    {
        parent::__construct();
    }
	
	function count()
	{
		$this->db->from('mpelabuhan');
		$this->db->where('status','1');
		$query = $this->db->count_all_results();
        return $query;
	}
    
    function count_search($textsearch)
	{
		$this->db->from('mpelabuhan');
        $this->db->like('kode',$textsearch);
        $this->db->or_like('uraian',$textsearch);
        $this->db->or_like('idloading',$textsearch);
		$query = $this->db->count_all_results();
        return $query;
	}
	function cek_idloading($idloading){
		$this->db->where('idloading',$idloading);
		$this->db->from('mpelabuhan');
		$row=$this->db->count_all_results();
		//print_r($row);exit();
		return $row;
	}
	function insert()
    {	
		$this->idloading = $_POST['idloading']; 
		$this->uraian = $_POST['uraian']; 
		$this->kode = $_POST['kode']; 
		if (isset($_POST['status'])) {
		  $this->status = 1;
		} else {
		  $this->status = 0;
		}
		//$this->status = $_POST['status']; 
		//print_r($this);exit();
		if($this->db->insert('mpelabuhan', $this)){
			return true;
		}else{	
			$this->error_message = "Penyimpanan Gagal";
			return false;
		}
    }

    function update()
    {
		$this->idloading = $_POST['idloading']; 
		$this->uraian = $_POST['uraian']; 
		$this->kode = $_POST['kode']; 
		if (isset($_POST['status'])) {
		  $this->status = 1;
		} else {
		  $this->status = 0;
		}
		$this->idloading = $_POST['idloading']; 
		
		if($this->db->update('mpelabuhan', $this, array('idloading' => $_POST['idloading']))){
			return true;
		}else{
			$this->error_message = "Penyimpanan Gagal";
			return false;
		}
    }
	
	function delete($id)
    {
		$data = array(
               'status' =>0
            );

		$this->db->where('idloading', $id);
		$this->db->update('mpelabuhan', $data); 
		/* $this->status = '0'; 
		//$this->pelabuhan = $_POST['uraian']; 
		//print_r($id);exit();
		$this->db->update('mpelabuhan', $this, array('idloading' => $id)); */
    }
	
	function pelabuhan_list($limit,$offset)
    {
		$this->db->select('mpelabuhan.*');
		$this->db->from('mpelabuhan');
		$this->db->where('status','1');
		$this->db->order_by('idloading','ASC');
		($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
		$query = $this->db->get(); 	
		//print_r($query->result());exit();
		return $query->result();
    }
    
    
    function pelabuhan_search($textsearch,$limit,$offset)
    {
		$this->db->select('mpelabuhan.*');
		$this->db->from('mpelabuhan');
        $this->db->like('kode',$textsearch);
        $this->db->or_like('uraian',$textsearch);
        $this->db->or_like('idloading',$textsearch);
		$this->db->order_by('idloading','ASC');
		($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
		$query = $this->db->get(); 	
		return $query->result();
    }
	
	function detail($id)
    {
		//print_r($id);exit();
		$this->db->select('mpelabuhan.*');    
		$this->db->from('mpelabuhan');
		$this->db->where('idloading',$id);
		$query = $this->db->get();
		//print_r($query->row());exit();
        return $query->row();
    }
	
	function neg_findajax($qsearch)
    {
		$this->db->like('negara',$qsearch);
        $this->db->or_like('idloading',$qsearch);
		$this->db->order_by('idloading','DESC');
		$query = $this->db->get('mnegara');	
		return $query->result_array();
		
    }
	
}

?>